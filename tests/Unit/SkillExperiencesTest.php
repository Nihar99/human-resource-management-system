<?php

namespace Tests\Unit;

use App\SkillsAndExperience;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SkillExperiencesTest extends TestCase
{
    public function testStoreAndRetrieval()
    {
        DB::beginTransaction();
        $Id = (new SkillsAndExperience())->store(1,'abc xyz',1,null);
        $getSkill = (new SkillsAndExperience())->fetchById($Id);
        $this->assertEquals(1,$getSkill->employee_id);
        $this->assertEquals('abc xyz' , $getSkill->details);
        DB::rollBack();
    }
    public function testRetrievalFailure()
    {
        $getScholarship = (new SkillsAndExperience())->fetchById(4);
        $this->assertFalse($getScholarship);
    }
    public function testUpdateById()
    {
        DB::beginTransaction();
        $Id = (new SkillsAndExperience())->updateById(1,1,'abc xyzs',1,null);
        $getSkill = (new SkillsAndExperience())->fetchById($Id);
        $this->assertEquals('abc xyzs',$getSkill->details);
        DB::rollBack();
    }
    public function testDeleteById() {
        DB::beginTransaction();
        $skill = new SkillsAndExperience();
        $skill_delete = $skill->deleteById(1);
        $this->assertEquals(1,$skill_delete);
        $this->assertSoftDeleted('skills_and_experiences', ['id' => 1]);
        DB::rollBack();
    }
    public function testDeleteByIdFail()
    {
        DB::beginTransaction();
        $skill = new SkillsAndExperience();
        $skill_delete = $skill->deleteById(4);
        $this->assertFalse($skill_delete);
        DB::rollBack();
    }
}
