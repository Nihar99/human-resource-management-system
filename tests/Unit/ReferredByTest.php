<?php

namespace Tests\Unit;

use App\ReferredBy;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReferredByTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testStore()
    {
        DB::beginTransaction();
        $referredById=(new ReferredBy())->store(1,'Tejal','Developer','Palghar','7777777',1,1);
        $getDetails = (new ReferredBy())->fetchById($referredById);
        $this->assertEquals('Tejal', $getDetails->name);
        DB::rollBack();
    }

    public function testUpdate()
    {
        DB::beginTransaction();
        $referredBy=ReferredBy::find(1);
        $referredById=$referredBy->store(1,'Tejal','Developer','Palghar','88888888',1,1);
        $getDetails = (new ReferredBy())->fetchById($referredById);
        $this->assertEquals('88888888', $getDetails->tel_no);
        DB::rollBack();
    }
}
