<?php

namespace Tests\Unit;

use App\GeneralHoliday;
use App\Http\Controllers\SessionController;
use App\Rules\NoDuplicateHoliday;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NoDuplicateHolidayTest extends TestCase
{
    public function testDuplicate() {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $holiday            = new GeneralHoliday();
        $holiday->name      = 'Diwalisss';
        $holiday->holiday_date = '2019-01-01';
        $holiday->client_id = SessionController::getClientIdFromSession();
        $holiday->save();
        $duplicateHoliday = new NoDuplicateHoliday();
        $this->assertTrue($duplicateHoliday->passes('holiday', 'Holisss'));
        $this->assertFalse($duplicateHoliday->passes('holiday', 'Diwalisss'));
        DB::rollBack();
    }
}
