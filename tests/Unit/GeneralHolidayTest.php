<?php

namespace Tests\Unit;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;
use App\GeneralHoliday;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GeneralHolidayTest extends TestCase
{
    public function test()
    {
        $this->assertEquals(1,1);
    }
    public function testStoreAndRetrieval() {
        DB::beginTransaction();
        $holidayId  = (new GeneralHoliday())->store('Diwali', '2009-12-12', 1, 1,null);
        $getHoliday = (new GeneralHoliday())->fetchById($holidayId);
        $this->assertEquals('Diwali', $getHoliday->name);
        $this->assertEquals('2009-12-12',$getHoliday->holiday_date);
        $this->assertEquals(1, $getHoliday->client_id);
        DB::rollBack();
    }
    public function testRetrievalFailure() {
        $getHoliday = (new GeneralHoliday())->fetchById(4);
        $this->assertFalse($getHoliday);
    }
    public function testDeleteById() {
        DB::beginTransaction();
        $department = new GeneralHoliday();
        $holiday_delete = $department->deleteById(1);
        $this->assertEquals(1,$holiday_delete);
        $this->assertSoftDeleted('general_holidays', ['id' => 1]);
        DB::rollBack();
    }
    public function testDeleteByIdFail()
    {
        DB::beginTransaction();
        $holiday = new GeneralHoliday();
        $holiday_delete = $holiday->deleteById(4);
        $this->assertFalse($holiday_delete);
    }
    public function testUpdateById()
    {
        DB::beginTransaction();
        $holidayId = (new GeneralHoliday())->updateById(1,'Diwalis','2019-01-01',1,1,null);
        $getHoliday = (new GeneralHoliday())->fetchById($holidayId);
        $this->assertEquals('Diwalis',$getHoliday->name);
        $this->assertEquals('2019-01-01',$getHoliday->holiday_date);
        DB::rollBack();

    }
    public function testFetchNameById()
    {
        $holiday = new GeneralHoliday();
        DB::beginTransaction();
        $holidayId = (new GeneralHoliday())->store('Diwalis','2019-01-01',1,1,null);
        $name = $holiday->fetchNameById($holidayId);
        $this->assertEquals('Diwalis',$name);
        DB::rollBack();
    }
}
