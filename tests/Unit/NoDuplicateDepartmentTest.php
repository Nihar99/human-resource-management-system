<?php

namespace Tests\Unit;

use App\Department;
use App\Http\Controllers\SessionController;
use App\Rules\NoDuplicateDepartment;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;

class NoDuplicateDepartmentTest extends TestCase {
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testDuplicate() {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $department            = new Department();
        $department->name      = 'HR';
        $department->client_id = SessionController::getClientIdFromSession();
        $department->save();
        $duplicateDepartment = new NoDuplicateDepartment;
        $this->assertTrue($duplicateDepartment->passes('department', 'IT'));
        $this->assertFalse($duplicateDepartment->passes('department', 'HR'));
        DB::rollBack();
    }
}
