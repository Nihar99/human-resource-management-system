<?php

namespace Tests\Unit;

use App\AdminAuth;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class AdminAuthTest extends TestCase {
    public function testRetrievePasswordById() {
        $adminAuth = (new AdminAuth)->fetchById(1);
        $this->assertTrue(Hash::check('12345678', $adminAuth->password_hash));
        $this->assertFalse(Hash::check('12sdsa678', $adminAuth->password_hash));
    }

    public function testRetrievePasswordByIdFail() {
        $adminAuth = (new AdminAuth)->fetchById(2);
        $this->assertFalse($adminAuth);
    }
}
