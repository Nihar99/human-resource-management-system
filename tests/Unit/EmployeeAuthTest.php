<?php

namespace Tests\Unit;

use App\EmployeeAuth;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;

class EmployeeAuthTest extends TestCase {
    public function testGetEmployeeAuth() {
        $emp = new EmployeeAuth();
        $this->assertEquals(1, $emp->getEmployeeAuth(1)->id);
        $this->assertFalse($emp->getEmployeeAuth(2));
    }
    public function testStore()
    {
        DB::beginTransaction();
        $emp = new EmployeeAuth();
        $emp->store(1,Hash::make(123456));
        $this->assertEquals(1,$emp->employee_id);
        $this->assertTrue(Hash::check(123456,$emp->password_hash));
        DB::rollBack();

    }
}
