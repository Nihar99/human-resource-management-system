<?php

namespace Tests\Unit;

use App\Client;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class ClientTest extends TestCase {
    public function testInsertAndRetrieve() {
        DB::beginTransaction();
        $client   = new Client();
        $clientId = $client->insertOne('3iology');

        $fetchedClient = (new Client())->fetchById($clientId);
        $this->assertEquals('3iology', $fetchedClient->name);
        DB::rollBack();
    }

    public function testFetchAllClient() {
        $client    = new Client();
        $allClient = $client->fetchAll()->toJson();
        $this->assertContains('3iology', $allClient);
        $this->assertContains('Varadendra', $allClient);
    }
}
