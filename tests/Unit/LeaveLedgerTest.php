<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\LeaveLedger;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\ApplyLeave;

class LeaveLedgerTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testLeaveLedgerStore()
    {
        DB::beginTransaction();
        $firstDay = new Carbon('first day of January ' . date('Y'));
        $firstDay = $firstDay->format('Y-m-d');
        
        $lastDay = new Carbon('last day of December ' . date('Y'));
        $lastDay = $lastDay->format('Y-m-d');

        $applyLeaveId = (new ApplyLeave)->store(1, 1, '2019-01-01', '2019-01-02', "reason", 0,null, 1);
        $leaveLedger = new LeaveLedger();
        $ledger_id = $leaveLedger->store(1,1,0,4,
        3,$firstDay,$lastDay,null,$applyLeaveId);
        $ledger = $leaveLedger->getLedgerById($ledger_id);
        $this->assertDatabaseHas('leave_ledgers',['employee_id'=>1,'leave_master_id'=>1,'added_removed'=>0, 'no_of_leaves'=>4, 'inserted_from'=>3, 'policy_from' => $firstDay,'policy_to'=> $lastDay]);
        DB::rollback();

    }
}
