<?php

namespace Tests\Unit;

use App\Department;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class DepartmentTest extends TestCase {
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testStoreAndRetrieval() {
        DB::beginTransaction();
        $departmentId  = (new Department())->store('HR', 1, null, 1);
        $getDepartment = (new Department())->fetchById($departmentId);
        $this->assertEquals('HR', $getDepartment->name);
        $this->assertEquals(1, $getDepartment->client_id);
        DB::rollBack();
    }

    public function testRetrievalFailure() {
        $getDepartment = (new Department())->fetchById(4);
        $this->assertFalse($getDepartment);
    }

    public function testDeleteById() {
        DB::beginTransaction();
        $department = new Department();
        $dept_delete = $department->deleteById(1);
        $this->assertEquals(1,$dept_delete);
        $this->assertSoftDeleted('departments', ['id' => 1]);
        DB::rollBack();
    }
    public function testDeleteByIdFail()
    {
        DB::beginTransaction();
        $department = new Department();
        $dept_delete = $department->deleteById(4);
        $this->assertFalse($dept_delete);
    }
    public function testUpdate()
    {
        DB::beginTransaction();
        $departmentId = (new Department())->updateById(1,'HRS',1,1,null);
        $getDepartment = (new Department())->fetchById($departmentId);
        $this->assertEquals('HRS',$getDepartment->name);
        DB::rollBack();

    }

}
