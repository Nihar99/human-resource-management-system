<?php

namespace Tests\Unit;

use App\LanguagesKnown;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LanguagesKnownTest extends TestCase
{
    public function testStoreAndRetrieval() {
        DB::beginTransaction();
        $Id  = (new LanguagesKnown())->store(1,'English',1,1, 1,1, 1, null);
        $getLanguage = (new LanguagesKnown())->fetchById($Id);
        $this->assertEquals('English', $getLanguage->name);
        $this->assertEquals(1 , $getLanguage->read);
        $this->assertEquals(1 , $getLanguage->write);
        $this->assertEquals(1 , $getLanguage->speak);
        $this->assertEquals(1, $getLanguage->understand);
        DB::rollBack();
    }

    public function testRetrievalFailure() {
        $getlanguage = (new LanguagesKnown())->fetchById(4);
        $this->assertFalse($getlanguage);
    }

    public function testDeleteById() {
        DB::beginTransaction();
        $language = new LanguagesKnown();
        $language_delete = $language->deleteById(1);
        $this->assertEquals(1,$language_delete);
        $this->assertSoftDeleted('languages_knowns', ['id' => 1]);
        DB::rollBack();
    }
    public function testDeleteByIdFail()
    {
        DB::beginTransaction();
        $language = new LanguagesKnown();
        $language_delete = $language->deleteById(4);
        $this->assertFalse($language_delete);
    }
    public function testUpdateById()
    {
        DB::beginTransaction();
        $Id = (new LanguagesKnown())->updateById(1,1,'Englishs',1,1,1,1,1,null);
        $getLanguage = (new LanguagesKnown())->fetchById($Id);
        $this->assertEquals('Englishs',$getLanguage->name);
        $this->assertEquals(1,$getLanguage->read);
        $this->assertEquals(1,$getLanguage->write);
        $this->assertEquals(1,$getLanguage->speak);
        $this->assertEquals(1,$getLanguage->understand);
        DB::rollBack();

    }
}
