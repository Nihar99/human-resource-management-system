<?php

namespace Tests\Unit;

use App\ApplyLeave;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class ApplyLeaveTest extends TestCase {
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testStore() {
        DB::beginTransaction();
        $applyLeaveId = (new ApplyLeave)->store(1, 1, '2019-01-01', '2019-01-02',"reason", 0,null, 1);
        $this->assertEquals(1, (new ApplyLeave)->getAppliedLeaveById($applyLeaveId)->employee_id);
        $this->assertEquals(1, (new ApplyLeave)->getAppliedLeaveById($applyLeaveId)->leave_master_id);
        $this->assertEquals('2019-01-01', (new ApplyLeave)->getAppliedLeaveById($applyLeaveId)->from_date);
        $this->assertEquals('2019-01-02', (new ApplyLeave)->getAppliedLeaveById($applyLeaveId)->to_date);
        $this->assertEquals(null, (new ApplyLeave)->getAppliedLeaveById($applyLeaveId)->added_by_admin);
        $this->assertEquals(1, (new ApplyLeave)->getAppliedLeaveById($applyLeaveId)->added_by_employee);

        $applyLeaveId = (new ApplyLeave)->store(1, 1, '2019-01-01', '2019-01-02', "reason", 0,1, null);
        $this->assertEquals(1, (new ApplyLeave)->getAppliedLeaveById($applyLeaveId)->employee_id);
        $this->assertEquals(1, (new ApplyLeave)->getAppliedLeaveById($applyLeaveId)->leave_master_id);
        $this->assertEquals('2019-01-01', (new ApplyLeave)->getAppliedLeaveById($applyLeaveId)->from_date);
        $this->assertEquals('2019-01-02', (new ApplyLeave)->getAppliedLeaveById($applyLeaveId)->to_date);
        $this->assertEquals(1, (new ApplyLeave)->getAppliedLeaveById($applyLeaveId)->added_by_admin);
        $this->assertEquals(null, (new ApplyLeave)->getAppliedLeaveById($applyLeaveId)->added_by_employee);
        $this->assertEquals(2, (new ApplyLeave)->getAppliedLeaveById($applyLeaveId)->approved); // approved = 2 is pending for approval
        DB::rollBack();
    }

    public function testLeaveAccepted() {
        DB::beginTransaction();
        $applyLeaveId = (new ApplyLeave)->store(1, 1, '2019-01-01', '2019-01-02', "reason",0, null, 1);
        $appliedLeave = (new ApplyLeave)->getAppliedLeaveById($applyLeaveId);
        (new ApplyLeave)->updateById($applyLeaveId,1, 
        '2019-01-01', '2019-01-02',1);
        $appliedLeave = (new ApplyLeave)->getAppliedLeaveById($applyLeaveId);
        $this->assertEquals(1, (new ApplyLeave)->getAppliedLeaveById($applyLeaveId)->approved);
        DB::rollBack();
    }

    public function testLeaveRejected() {
        DB::beginTransaction();
        $applyLeaveId = (new ApplyLeave)->store(1, 1, '2019-01-01', '2019-01-02', "reason",0, null, 1);
        $appliedLeave = (new ApplyLeave)->getAppliedLeaveById($applyLeaveId);
        (new ApplyLeave)->updateById($applyLeaveId,1, 
        '2019-01-01', '2019-01-02',0);
        $appliedLeave = (new ApplyLeave)->getAppliedLeaveById($applyLeaveId);
        $this->assertEquals(0, (new ApplyLeave)->getAppliedLeaveById($applyLeaveId)->approved);
        DB::rollBack();
    }
}
