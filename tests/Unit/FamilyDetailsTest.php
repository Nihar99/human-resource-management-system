<?php

namespace Tests\Unit;

use App\FamilyDetails;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

    class FamilyDetailsTest extends TestCase
    {
        public function testStoreAndRetreival()
        {
            DB::beginTransaction();
            $familyId = (new FamilyDetails())->store(1, 'xyz', 40, 'uvw','mmm',1,null,1,1);
            $getDetails = (new FamilyDetails())->fetchById($familyId);
            $this->assertEquals('xyz', $getDetails->full_name);
            $this->assertEquals(40, $getDetails->age);
            DB::rollBack();
        }
        public function testRetrievalFailure() {
            $getDetail = (new FamilyDetails())->fetchById(4);
            $this->assertFalse($getDetail);
        }
        public function testDeleteById() {
            DB::beginTransaction();
            $detail = new FamilyDetails();
            $detail_delete = $detail->deleteById(1);
            $this->assertEquals(1,$detail_delete);
            $this->assertSoftDeleted('family_details', ['id' => 1]);
            DB::rollBack();
        }
        public function testDeleteByIdFail()
        {
            DB::beginTransaction();
            $detail = new FamilyDetails();
            $detail_delete = $detail->deleteById(4);
            $this->assertFalse($detail_delete);
        }
        public function testUpdate()
        {
            DB::beginTransaction();
            $Id = (new FamilyDetails())->updateById(1,1, 'xyz', 40, 'uvw','mmm',1,null,1,1);
            $getDetail = (new FamilyDetails())->fetchById($Id);
            $this->assertEquals('xyz',$getDetail->full_name);
            $this->assertEquals(40,$getDetail->age);
            $this->assertEquals('uvw',$getDetail->occupation);
            $this->assertEquals('mmm',$getDetail->relationship);
            DB::rollBack();

        }
    }
