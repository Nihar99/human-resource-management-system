<?php

namespace Tests\Unit;

use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use App\PersonalDetails;
use App\Employee;

class PersonalDetailsTest extends TestCase
{
    public function testCreateAndFetch()
    {
        DB::beginTransaction();
        $employeeId = (new Employee())->create(1, 1, NULL, 'EC0002', 1, 1);
        $per_detailsId = (new PersonalDetails())->create($employeeId, 1, 'Nihar', 'Nitesh', 'Nayak',
            1, '1999-01-15', 100, 'Mumbai,Maharashtra', 'Hindu', 1,
            'Konkani', 'nih@gmail.com', 'abc xyz w');
        $getPersonalDetails = (new PersonalDetails())->fetchById($per_detailsId);
        $this->assertEquals($employeeId, $getPersonalDetails->employee_id);
        $this->assertEquals(1, $getPersonalDetails->photo);
        $this->assertEquals('Nihar', $getPersonalDetails->first_name);
        $this->assertEquals('Nitesh', $getPersonalDetails->middle_name);
        $this->assertEquals('Nayak', $getPersonalDetails->last_name);
        $this->assertEquals(1, $getPersonalDetails->gender);
        $this->assertEquals('1999-01-15', $getPersonalDetails->date_of_birth);
        $this->assertEquals(100, $getPersonalDetails->nationality);
        $this->assertEquals('Mumbai,Maharashtra', $getPersonalDetails->place_state_of_birth);
        $this->assertEquals('Hindu', $getPersonalDetails->religion);
        $this->assertEquals(1, $getPersonalDetails->marital_status);
        $this->assertEquals('Konkani', $getPersonalDetails->mother_tongue);
        $this->assertEquals('nih@gmail.com', $getPersonalDetails->email);
        $this->assertEquals('abc xyz w', $getPersonalDetails->hobbies_and_interests);
        DB::rollBack();

    }

    public function testFetchFail()
    {
        $getPersonalDetails = (new PersonalDetails())->fetchById(2);
        $this->assertFalse($getPersonalDetails);
    }

    // public function testPersonalDetails()
    // {
    //     DB::beginTransaction();
    //     $personal = PersonalDetails::with('personalDetails')->get();
    //     $this->assertStringContainsString("first_name", $personal.'personal'[0]);
    //     $this->assertStringContainsString("Veeraj" , $personal.'personal'[0]);
    //     $this->assertStringContainsString('Shenoy',$personal.'personal'[0]);
    //     $this->assertStringNotContainsString('Nihar',$personal.'personal'[0]);
    //     DB::rollBack();
    // }
}

