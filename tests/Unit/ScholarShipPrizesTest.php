<?php

namespace Tests\Unit;

use App\ScholarShipPrizes;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ScholarShipPrizesTest extends TestCase
{
    public function testStoreAndRetrieval()
    {
        DB::beginTransaction();
        $Id = (new ScholarShipPrizes())->store(1,'abc xyz',1,null);
        $getScholarship = (new ScholarShipPrizes())->fetchById($Id);
        $this->assertEquals(1,$getScholarship->employee_id);
        $this->assertEquals('abc xyz' , $getScholarship->details);
        DB::rollBack();
    }
    public function testRetrievalFailure()
    {
        $getScholarship = (new ScholarShipPrizes())->fetchById(4);
        $this->assertFalse($getScholarship);
    }
    public function testUpdateById()
    {
        DB::beginTransaction();
        $Id = (new ScholarShipPrizes())->updateById(1,1,'abc xyzs',1,null);
        $getPrize = (new ScholarShipPrizes())->fetchById($Id);
        $this->assertEquals('abc xyzs',$getPrize->details);
        DB::rollBack();
    }
    public function testDeleteById() {
        DB::beginTransaction();
        $prize = new ScholarShipPrizes();
        $prize_delete = $prize->deleteById(1);
        $this->assertEquals(1,$prize_delete);
        $this->assertSoftDeleted('scholar_ship_prizes', ['id' => 1]);
        DB::rollBack();
    }
    public function testDeleteByIdFail()
    {
        DB::beginTransaction();
        $prize = new ScholarShipPrizes();
        $prize_delete = $prize->deleteById(4);
        $this->assertFalse($prize_delete);
        DB::rollBack();
    }
}
