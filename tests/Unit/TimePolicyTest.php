<?php

namespace Tests\Unit;

use App\TimePolicy;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TimePolicyTest extends TestCase
{
   public function testStoreAndFetch()
   {
       DB::beginTransaction();
       $time_policiesId  = (new TimePolicy())->store(1, 1, null, 1,
           '09:00:00','17:30:00','13:00:00','13:30:00','09:10:00',
           '17:20:00','10:00:00','16:00:00',
           false);
       $getPolicies = (new TimePolicy())->fetchById($time_policiesId);
       $this->assertEquals(1, $getPolicies->client_id);
       $this->assertEquals(1, $getPolicies->added_by_admin);
       $this->assertEquals(null, $getPolicies->added_by_employee);
       $this->assertEquals(1, $getPolicies->day);
       $this->assertEquals('09:00:00', $getPolicies->time_in);
       $this->assertEquals('17:30:00', $getPolicies->time_out);
       $this->assertEquals('13:00:00', $getPolicies->break_in);
       $this->assertEquals('13:30:00', $getPolicies->break_out);
       $this->assertEquals('09:10:00', $getPolicies->late_entry_after);
       $this->assertEquals('17:20:00', $getPolicies->early_exit_before);
       $this->assertEquals('10:00:00', $getPolicies->halfday_employee_comes_after);
       $this->assertEquals('16:00:00', $getPolicies->halfday_employee_leaves_before);
       $this->assertEquals(false, $getPolicies->weekoff);
       DB::rollBack();
   }
    public function testFetchFail()
    {
        $getTimePolicyDetails = (new TimePolicy())->fetchById(100);
        $this->assertFalse($getTimePolicyDetails);
    }
    public function testDeleteById()
    {
        DB::beginTransaction();
        $employee = new TimePolicy();
        $employee_delete = $employee->deleteById(1);
        $this->assertSoftDeleted('time_policies', ['id' => 1]);
        $this->assertEquals(1,$employee_delete);
        DB::rollBack();
    }
    public function testDeleteByIdFail()
    {
        DB::beginTransaction();
        $employee = new TimePolicy();
        $employee_delete = $employee->deleteById(10);
        $this->assertFalse($employee_delete);
    }
    public function testUpdateById()
    {
        DB::beginTransaction();
        $timePolicyId = (new TimePolicy())->updateById(1,1,1,null,7,'','','','','','','','',1);
        $getTimePolicy = (new TimePolicy())->fetchById($timePolicyId);
        $this->assertEquals(7,$getTimePolicy->day);
        $this->assertEquals(1,$getTimePolicy->weekoff);
        DB::rollBack();

    }
}
