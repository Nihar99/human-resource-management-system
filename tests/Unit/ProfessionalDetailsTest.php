<?php

namespace Tests\Unit;

use App\Employee;
use App\ProfessionalDetails;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProfessionalDetailsTest extends TestCase
{

    public function testCreateAndFetch() {
        DB::beginTransaction();
        $employeeId = (new Employee())->create(1, 1,NULL,'EC0002',1,1);
        $pro_detailsId = (new ProfessionalDetails())->create($employeeId,'abc@gmail.com',1,1,'2019-01-01','2019-01-03' );
        //var_dump($employeeId);
        $getProfessionalDetails = (new ProfessionalDetails())->fetchById($pro_detailsId);
        $this->assertEquals($employeeId,$getProfessionalDetails->employee_id);
        $this->assertEquals('abc@gmail.com',$getProfessionalDetails->email);
        $this->assertEquals(1,$getProfessionalDetails->department_id);
        $this->assertEquals(1,$getProfessionalDetails->designation_id);
        $this->assertEquals('2019-01-01',$getProfessionalDetails->probation_from);
        $this->assertEquals('2019-01-03',$getProfessionalDetails->confirmation_date);

        DB::rollBack();
    }
    public function testFetchFail()
    {
        $getProfessionalDetails = (new ProfessionalDetails())->fetchById(2);
        $this->assertFalse($getProfessionalDetails);
    }
//     public function testProfessionalDetails()
//     {
//         DB::beginTransaction();
//         $professional = ProfessionalDetails::with('professionalDetails')->get();
//         $this->assertStringContainsString("department_id", $professional.'professional'[0]);
//         $this->assertStringContainsString(1,$professional.'professional'[0]);
//         $this->assertStringContainsString("probation_from",$professional.'professional'[0]);
//         $this->assertStringContainsString("2011-01-01" , $professional.'professional'[0]);
//         $this->assertStringContainsString("confirmation_date",$professional.'professional'[0]);
//         $this->assertStringContainsString("2011-04-01" , $professional.'professional'[0]);
//         $this->assertStringNotContainsString("1970-01-01",$professional.'professional'[0]);
//         DB::rollBack();
//     }

 }
