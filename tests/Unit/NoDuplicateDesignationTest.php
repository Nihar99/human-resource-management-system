<?php

namespace Tests\Unit;

use App\Designation;
use App\Http\Controllers\SessionController;
use App\Rules\NoDuplicateDesignation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NoDuplicateDesignationTest extends TestCase
{
    public function testDuplicate() {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $designation            = new Designation();
        $designation->name      = 'Manager';
        $designation->client_id = SessionController::getClientIdFromSession();
        $designation->save();
        $duplicateDesignation = new NoDuplicateDesignation;
        $this->assertTrue($duplicateDesignation->passes('designation', 'Finance'));
        $this->assertFalse($duplicateDesignation->passes('designation', 'Manager'));
        DB::rollBack();
    }
}
