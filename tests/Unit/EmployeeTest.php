<?php

namespace Tests\Unit;

use App\Employee;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class EmployeeTest extends TestCase
{
    public function testCreateAndFetch()
    {
        DB::beginTransaction();
        $employeeId = (new Employee())->create(1, 1,NULL,'EC0002',1,1);
        $newEmployee = (new Employee())->fetchById($employeeId);
        $this->assertEquals(1,$newEmployee->client_id);
        $this->assertEquals(1,$newEmployee->added_by_admin);
        $this->assertEquals(NULL,$newEmployee->added_by_employee);
        $this->assertEquals('EC0002',$newEmployee->ecode);
        $this->assertEquals(1,$newEmployee->status);
        $this->assertEquals(1,$newEmployee->ess);
        DB::rollBack();
    }
    public function testFetchFail()
    {
        $getEmployeeDetails = (new Employee())->fetchById(2);
        $this->assertFalse($getEmployeeDetails);
    }
    public function testEmpCodeFail()
    {
        DB::beginTransaction();
        $employeeId1 = (new Employee())->create(1, 1,NULL,'EC0002',1,1);
        $this->assertFalse((new Employee())->EmpCodeExistsInClient('1','EC0003'));
    }
    public function testDeleteById()
    {
        DB::beginTransaction();
        $employee = new Employee();
        $employee_delete = $employee->deleteById(1);
        $this->assertSoftDeleted('employees', ['id' => 1]);
        $this->assertEquals(1,$employee_delete);
        DB::rollBack();
    }

    public function testFetchActiveUserList(){
        DB::beginTransaction();
        $employeeId = (new Employee())->create(1, 1,NULL,'EC0002',2,1);
        $employee = new Employee();
        $employee_list = $employee->getActiveEmployees();
        $employee_list->each(function ($employee) {
            $this->assertEquals(1,$employee->status);
            $this->assertEquals(null,$employee->deletedAt);
        });
        DB::rollBack();
    }

    public function testDeleteByIdFail()
    {
        DB::beginTransaction();
        $employee = new Employee();
        $employee_delete = $employee->deleteById(4);
        $this->assertFalse($employee_delete);
    }
    public function testFetchEcodeById()
    {
        DB::beginTransaction();
        $id = 1;
        $ecode = (new Employee())->fetchEcodeById($id);
        $this->assertDatabaseHas('employees',['ecode'=>$ecode]);
        DB::rollBack();
    }
}
