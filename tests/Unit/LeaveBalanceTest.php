<?php

namespace Tests\Unit;

use App\LeaveBalance;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;

class LeaveBalanceTest extends TestCase {
    public function testGetAvailableLeaves() {
        $leaveBalance = new LeaveBalance();
        $this->assertEquals(7, $leaveBalance->getAvailableLeaves(1, 2));
        $this->assertEquals(10.5, $leaveBalance->getAvailableLeaves(1, 1));
    }

    public function testInsertLeaveBalance(){
        DB::beginTransaction();
        $leaveBalance = new LeaveBalance();
        $leaveBalance->insertLeaveBalance(1,1,10);
        $this->assertDatabaseHas('leave_balances',['employee_id'=>1,'leave_master_id'=>1,'available'=>10]);
        DB::rollback();

    }

    public function testUpdateLeaveBalance(){
        DB::beginTransaction();
        $leaveBalance = new LeaveBalance();
        $balance_id = $leaveBalance->insertLeaveBalance(1,3,10);
        $leaveBalance->updateById($balance_id,1,3,20);
        $balance = $leaveBalance->getAvailableLeaves(1,3);
        $this->assertEquals('20',$balance);
        DB::rollback();

    }
    public function testLeaveBalance()
    {
        $employee_id = 1;
        $leave_master_id = 1;
        $availableLeaveBalance = (new LeaveBalance())->getLeaveBalance($employee_id , $leave_master_id);
        $this->assertEquals('10.5' , $availableLeaveBalance);
    }
}
