<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\WorkExperience;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class WorkExperienceTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testInsertionAndFetch()
    {
        DB::beginTransaction();
        $workExperience = new WorkExperience();
        $work_experience_id = $workExperience->store(1,'org test', 'org address test',$this->DateFormat('29/04/2019'),$this->DateFormat('29/04/2019'),"","","","","");
        $getExperience = (new WorkExperience())->fetchById($work_experience_id);
        $this->assertEquals('org test', $getExperience->organisation_name);
        DB::rollBack();

    }

    public function testUpdateAndFetch(){
        DB::beginTransaction();
        $workExperience = new WorkExperience();
        $work_experience_id = $workExperience->store(1,'org test check', 'org address test',$this->DateFormat('29/04/2019'),$this->DateFormat('29/04/2019'),"","","","","");

        $workExperience = new WorkExperience();
        $work_experience_update_id = $workExperience->updateById($work_experience_id,'org test', 'org address test',$this->DateFormat('29/04/2019'),$this->DateFormat('29/04/2019'),"","","","","");
        $getExperience = (new WorkExperience())->fetchById($work_experience_id);
        $this->assertEquals('org test', $getExperience->organisation_name);
        DB::rollBack();
    }

    public function testDeleteById(){
        DB::beginTransaction();
        $workExperience = new WorkExperience();
        $work_experience_id = $workExperience->store(1,'org test check', 'org address test',$this->DateFormat('29/04/2019'),$this->DateFormat('29/04/2019'),"","","","","");
        
        $workExperience = new WorkExperience();
        $experience_delete = $workExperience->deleteById($work_experience_id);
        $this->assertSoftDeleted('work_experiences', ['id' => $work_experience_id]);
        $this->assertEquals($work_experience_id,$experience_delete);
        DB::rollBack();
    }

    public function DateFormat($getDate)
    {
        if($getDate != null) {
            $dateString = date('Y-m-d', strtotime($getDate));
            return $dateString;
        }
        else
        {
            return null;
        }
    }
}
