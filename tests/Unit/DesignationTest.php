<?php

namespace Tests\Unit;

use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use App\Designation;


class DesignationTest extends TestCase
{
    public function testStoreAndRetrieval() {
        DB::beginTransaction();
        $designationId  = (new Designation())->store('Manager', 1,null,1);
        $getDesignation = (new Designation())->fetchById($designationId);
        $this->assertEquals('Manager', $getDesignation->name);
        $this->assertEquals(1, $getDesignation->client_id);
        DB::rollBack();
    }
    public function testRetrievalFailure() {
        $getDesignation = (new Designation())->fetchById(4);
        $this->assertFalse($getDesignation);
    }
    public function testDeleteById()
    {
        DB::beginTransaction();
        $designation = new Designation();
        $designation_delete = $designation->deleteById(1);
        $this->assertEquals(1,$designation_delete);
        $this->assertSoftDeleted('designations', ['id' => 1]);
        DB::rollBack();
    }
    public function testDeleteByIdFail()
    {
        DB::beginTransaction();
        $designation = new Designation();
        $designation_delete = $designation->deleteById(4);
        $this->assertFalse($designation_delete);
    }
    public function testUpdateById()
    {
        DB::beginTransaction();
        $designationId = (new Designation())->updateById(1,'Managerss',1,1,null);
        $getDesignation = (new Designation())->fetchById($designationId);
        $this->assertEquals('Managerss',$getDesignation->name);
        DB::rollBack();

    }

}
