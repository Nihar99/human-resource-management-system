<?php

namespace Tests\Unit;

use App\EducationalProfile;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use App\EmergencyContact;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EducationalProfileTest extends TestCase
{
    public function testStoreAndRetrieval()
    {
        DB::beginTransaction();
        $educationalProfileId = (new EducationalProfile())->store(1,'Bsc','March 2012','Mumbai','Science','70%',1,null);
        $getProfile = (new EducationalProfile())->fetchById($educationalProfileId);
        $this->assertEquals(1,$getProfile->employee_id);
        $this->assertEquals('Bsc' , $getProfile->board_degree);
        $this->assertEquals('March 2012' , $getProfile->year_of_passing);
        $this->assertEquals('Mumbai' , $getProfile->institution_name);
        $this->assertEquals('Science' , $getProfile->principle_subjects);
        $this->assertEquals('70%', $getProfile->grade_percentage);
        DB::rollBack();

    }
    public function testRetreivalFailure()
    {
        $getProfile = (new EducationalProfile())->fetchById(4);
        $this->assertFalse($getProfile);
    }
    public function testUpdateById()
    {
        DB::beginTransaction();
        $Id = (new EducationalProfile())->updateById(1,1,'Btechs','March 2012','Mumbai','Science','70%',1,null);
        $getProfile = (new EducationalProfile())->fetchById($Id);
        $this->assertEquals('Btechs',$getProfile->board_degree);
        $this->assertEquals('March 2012',$getProfile->year_of_passing);
        $this->assertEquals('Science',$getProfile->principle_subjects);
        $this->assertEquals('70%',$getProfile->grade_percentage);
        DB::rollBack();
    }
    public function testDeleteById() {
        DB::beginTransaction();
        $profile = new EducationalProfile();
        $profile_delete = $profile->deleteById(1);
        $this->assertEquals(1,$profile_delete);
        $this->assertSoftDeleted('educational_profiles', ['id' => 1]);
        DB::rollBack();
    }
    public function testDeleteByIdFail()
    {
        DB::beginTransaction();
        $profile = new EducationalProfile();
        $profile_delete = $profile->deleteById(4);
        $this->assertFalse($profile_delete);
    }
}
