<?php

namespace Tests\Unit;

use App\exposure_to_computer;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExposureComputerTest extends TestCase
{
    public function testStoreAndRetrieval() {
        DB::beginTransaction();
        $Id  = (new exposure_to_computer())->store(1,'Python','Parent','xyz uvw', 'My foot', 1, null);
        $getExposure = (new exposure_to_computer())->fetchById($Id);
        $this->assertEquals('Python', $getExposure->language);
        $this->assertEquals('Parent' , $getExposure->operating_system);
        $this->assertEquals('xyz uvw' , $getExposure->application);
        $this->assertEquals('My foot' , $getExposure->hardware);
        $this->assertEquals(1, $getExposure->employee_id);
        DB::rollBack();
    }

    public function testRetrievalFailure() {
        $getExposure = (new exposure_to_computer())->fetchById(4);
        $this->assertFalse($getExposure);
    }

    public function testDeleteById() {
        DB::beginTransaction();
        $exposure = new exposure_to_computer();
        $conta_delete = $exposure->deleteById(1);
        $this->assertEquals(1,$conta_delete);
        $this->assertSoftDeleted('exposure_to_computers', ['id' => 1]);
        DB::rollBack();
    }
    public function testDeleteByIdFail()
    {
        DB::beginTransaction();
        $exposure = new exposure_to_computer();
        $exposure_delete = $exposure->deleteById(4);
        $this->assertFalse($exposure_delete);
    }
    public function testUpdateById()
    {
        DB::beginTransaction();
        $Id = (new exposure_to_computer())->updateById(1,1,'Pythons','Parents','uvw xyz','MyFoots',1,null);
        $getExposure = (new exposure_to_computer())->fetchById($Id);
        $this->assertEquals('Pythons',$getExposure->language);
        $this->assertEquals('Parents',$getExposure->operating_system);
        $this->assertEquals('uvw xyz',$getExposure->application);
        $this->assertEquals('MyFoots',$getExposure->hardware);
        DB::rollBack();

    }
}
