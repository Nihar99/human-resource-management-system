<?php

namespace Tests\Unit;

use App\LeaveMaster;
use Tests\TestCase;

class LeaveMasterTest extends TestCase {
    public function testLeaveMasters() {
        $this->assertEquals('Casual', LeaveMaster::find(1)->type);
        $this->assertEquals('Privilage', LeaveMaster::find(2)->type);
        $this->assertEquals('Sick', LeaveMaster::find(3)->type);
        $this->assertEquals('Maternal', LeaveMaster::find(4)->type);
    }
}
