<?php

namespace Tests\Unit;

use App\Admin;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class AdminTest extends TestCase {
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testInsertAndRetrieval() {
        DB::beginTransaction();
        $admin        = new Admin();
        $adminId      = $admin->insert('Veeraj', 'veeraj@3iology.com', '8087198203', '1');
        $adminFetched = (new Admin())->fetchById($adminId);
        $this->assertEquals('Veeraj', $adminFetched->name);
        $this->assertEquals('veeraj@3iology.com', $adminFetched->email);
        $this->assertEquals('8087198203', $adminFetched->contact);
        DB::rollBack();
    }

    public function testFetchAdminByEmail() {
        $admin = (new Admin)->fetchAdminByEmail('sharad@3iology.com');
        $this->assertEquals(1, $admin->id);
    }

    public function testFetchAdminByEmailFail() {
        $admin = (new Admin)->fetchAdminByEmail('shara@3iology.com');
        $this->assertFalse($admin);
    }
}
