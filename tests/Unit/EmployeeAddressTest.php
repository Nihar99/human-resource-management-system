<?php

namespace Tests\Unit;

use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use App\Employee;
use App\EmployeeAddress;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeAddressTest extends TestCase
{
    public function testCreateAndFetch()
    {
        DB::beginTransaction();
        $employeeId = (new Employee())->create(1, 1,NULL,'EC0002',1,1);
        $employeeAddId = (new EmployeeAddress())->create($employeeId,'abc xyz',NULL,'ABC XYZ',NULL);
        $newEmployeeAddress = (new EmployeeAddress())->fetchById($employeeAddId);
        $this->assertEquals($employeeId,$newEmployeeAddress->employee_id);
        $this->assertEquals('abc xyz',$newEmployeeAddress->present_address);
        $this->assertEquals(NULL,$newEmployeeAddress->present_address_tel_no);
        $this->assertEquals('ABC XYZ',$newEmployeeAddress->permanent_address);
        $this->assertEquals(NULL,$newEmployeeAddress->permanent_address_tel_no);
        DB::rollBack();
    }
    public function testFetchFail()
    {
        $getEmployeeAddressDetails = (new EmployeeAddress())->fetchById(2);
        $this->assertFalse($getEmployeeAddressDetails);
    }
    // public function testEmployeeAddress()
    // {
    //     DB::beginTransaction();
    //     $address = EmployeeAddress::with('employeeAddress')->get();
    //     $this->assertStringContainsString("A-308, Shri Prastha Soc, Agashi Rd, Virar West", $address.'address'[0]);
    //     $this->assertStringContainsString("Same as above" , $address.'address'[0]);
    //     $this->assertStringNotContainsString('Siddharth Nagar Goregaon West',$address.'address'[0]);
    //     DB::rollBack();
    // }
}
