<?php

namespace Tests\Unit;

use App\ForgotPassword;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class ForgotPasswordTest extends TestCase {
    public function testInsert() {
        DB::beginTransaction();
        $forgotPassword      = new ForgotPassword();
        $lastInsertId        = $forgotPassword->insert('tokein', 1);
        $fetchForgotPassword = new ForgotPassword();
        $this->assertTrue($fetchForgotPassword->isTokenActive('tokein', 1));
        DB::rollBack();
    }
}
