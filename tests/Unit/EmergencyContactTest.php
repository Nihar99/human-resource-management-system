<?php

namespace Tests\Unit;

use App\EmergencyContact;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmergencyContactTest extends TestCase
{
    public function testStoreAndRetrieval() {
        DB::beginTransaction();
        $Id  = (new EmergencyContact())->store(1,'Nihar','Parent','xyz uvw', 9999999, 1, null);
        $getContact = (new EmergencyContact())->fetchById($Id);
        $this->assertEquals('Nihar', $getContact->name);
        $this->assertEquals('Parent' , $getContact->relationship);
        $this->assertEquals('xyz uvw' , $getContact->address);
        $this->assertEquals(9999999 , $getContact->phone);
        $this->assertEquals(1, $getContact->employee_id);
        DB::rollBack();
    }

    public function testRetrievalFailure() {
        $getContact = (new EmergencyContact())->fetchById(4);
        $this->assertFalse($getContact);
    }

    public function testDeleteById() {
        DB::beginTransaction();
        $contact = new EmergencyContact();
        $contact_delete = $contact->deleteById(1);
        $this->assertEquals(1,$contact_delete);
        $this->assertSoftDeleted('emergency_contacts', ['id' => 1]);
        DB::rollBack();
    }
    public function testDeleteByIdFail()
    {
        DB::beginTransaction();
        $contact = new EmergencyContact();
        $contact_delete = $contact->deleteById(4);
        $this->assertFalse($contact_delete);
    }
    public function testUpdate()
    {
        DB::beginTransaction();
        $Id = (new EmergencyContact())->updateById(1,1,'Nihars','Parents','uvw xyz',9999299,1,null);
        $getContact = (new EmergencyContact())->fetchById($Id);
        $this->assertEquals('Nihars',$getContact->name);
        $this->assertEquals('Parents',$getContact->relationship);
        $this->assertEquals('uvw xyz',$getContact->address);
        $this->assertEquals(9999299,$getContact->phone);
        DB::rollBack();

    }
}
