<?php

namespace Tests\Feature;

use App\EducationalProfile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EducationalProfileControllerTest extends TestCase
{
    public function testStore()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/educational-profiles', [
            'employeeId' => 1,
            'board_degree' => 'Bsc IT',
            'year_of_passing' => 'March 2012',
            'institution_name' =>'Mumbai',
            'principle_subjects' => 'Science',
            'grade_percentage' => '70%',
        ]);

        $educationalProfileId = $response->json()['educationalProfileId'];
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);

        $profile = (new EducationalProfile())->fetchById($educationalProfileId);
        $this->assertEquals('Bsc IT', $profile->board_degree);
        $this->assertEquals('March 2012' , $profile->year_of_passing);
        $this->assertEquals('Mumbai' , $profile->institution_name);
        $this->assertEquals('Science', $profile->principle_subjects);
        $this->assertEquals('70%', $profile->grade_percentage);
        $this->assertEquals(1, $profile->added_by_admin);
        $this->assertEquals(null, $profile->added_by_employee);
        Session::flush();

        Session::put('employee', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/educational-profiles', [
            'employeeId' => 1,
            'board_degree' => 'Bsc IT',
            'year_of_passing' => 'March 2013',
            'institution_name' =>'Mumbai',
            'principle_subjects' => 'Science',
            'grade_percentage' => '70%',
        ]);

        $educationalProfileId = $response->json()['educationalProfileId'];
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);

        $profile = (new EducationalProfile())->fetchById($educationalProfileId);
        $this->assertEquals('Bsc IT', $profile->board_degree);
        $this->assertEquals('March 2013' , $profile->year_of_passing);
        $this->assertEquals('Mumbai' , $profile->institution_name);
        $this->assertEquals('Science', $profile->principle_subjects);
        $this->assertEquals('70%', $profile->grade_percentage);
        $this->assertEquals(null, $profile->added_by_admin);
        $this->assertEquals(1, $profile->added_by_employee);

        DB::rollBack();
        Session::flush();
    }
    public function testStoreFail()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/educational-profiles', [
            'employeeId' => 1,
            'board_degree' => ' ',
            'year_of_passing' => ' ',
            'institution_name' =>'Mumbai',
            'principle_subjects' => 'Science',
            'grade_percentage' => '70%',
        ]);
        $response->assertStatus(400);
    }
    public function testUpdate()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->put('educational-profiles/' . 1,
            ['employeeId' => 1,'edit_board_degree' => 'Btech'  , 'edit_year_of_passing' => 'March 2012','edit_institution_name' =>'xyz', 'edit_principle_subjects' => 'Science','edit_grade_percentage'=> '70%' ]);
        $response->assertStatus(201);
        DB::rollBack();
        Session::flush();
    }
    public function testUpdateFail()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->put('educational-profiles/' . 1,
            ['employeeId' => 1,'edit_board_degree' => ' '  , 'edit_year_of_passing' => ' ','edit_institution_name' =>'xyz', 'edit_principle_subjects' => 'Science','edit_grade_percentage'=> '70%' ]);
        $response->assertStatus(400);
        DB::rollBack();
        Session::flush();
    }
    public function testDelete()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->call('DELETE','educational-profiles/' . 1);
        $response->assertStatus(201);
        DB::rollBack();
        Session::flush();
    }
}


