<?php

namespace Tests\Feature;

use App\Department;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;

class DepartmentControllerTest extends TestCase {
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testDepartmentIndex() {
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->get('departments');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'text/html; charset=UTF-8');
        $response->assertSee('Department');
        $response->assertViewIs('departments');
    }

    public function testStore() {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/departments', [
            'name' => 'WBC',
        ]);

        $departmentid = $response->json()['departmentid'];
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);

        $deparment = (new Department)->fetchById($departmentid);
        $this->assertEquals('WBC', $deparment->name);
        $this->assertEquals(1, $deparment->added_by_admin);
        $this->assertEquals(null, $deparment->added_by_employee);
        Session::flush();

        Session::put('employee', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/departments', [
            'name' => 'WBCC',
        ]);

        $departmentid = $response->json()['departmentid'];
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);

        $deparment = (new Department)->fetchById($departmentid);
        $this->assertEquals('WBCC', $deparment->name);
        $this->assertEquals(null, $deparment->added_by_admin);
        $this->assertEquals(1, $deparment->added_by_employee);

        DB::rollBack();
        Session::flush();
    }
    public function testDelete()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->call('DELETE','departments/' . 1);
        $response->assertStatus(201);
        DB::rollBack();
    }
    public function testUpdate()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->put('departments/' . 1,
            ['name' => 'HRS']);
        $response->assertStatus(201);
        DB::rollBack();
    }
    public function testUpdateFail()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->put('departments/' . 1,
            ['name' => 'Accounts']);
        $response->assertStatus(400);
        DB::rollBack();
    }


}
