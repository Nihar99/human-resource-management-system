<?php

namespace Tests\Feature;

use Tests\TestCase;

class UserAuthControllerTest extends TestCase {
    public function testView() {
        $response = $this->get('/login/user');
        $response->assertSee('Employee');
        $response->assertSee('Login');
        $response->assertSee('Enter Company Id');
        $response->assertSee('Enter Ecode');
        $response->assertSee('Enter Password');
        $response->assertSee('Sign In');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'text/html; charset=UTF-8');
    }
}
