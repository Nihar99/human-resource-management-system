<?php

namespace Tests\Feature;

use App\GeneralHoliday;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GeneralHolidaysControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGeneralHolidaysIndex()
    {
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->get('general-holidays');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'text/html; charset=UTF-8');
        $response->assertSee('General Holidays');
        $response->assertViewIs('general-holidays');
        Session::flush();
    }
    public function testStore() {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/general-holidays', [
            'name' => 'Christmas',
            'holiday_date' => '2019-12-12',
        ]);

        $holidayId = $response->json()['holidayId'];
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);

        $holiday = (new GeneralHoliday())->fetchById($holidayId);
        $this->assertEquals('Christmas', $holiday->name);
        $this->assertEquals('2019-12-12',$holiday->holiday_date);
        $this->assertEquals(1, $holiday->added_by_admin);
        $this->assertEquals(null, $holiday->added_by_employee);
        Session::flush();

        Session::put('employee', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/general-holidays', [
            'name' => 'XYZ',
            'holiday_date' => '2019-07-07'
        ]);

        $holidayId = $response->json()['holidayId'];
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);

        $holiday = (new GeneralHoliday())->fetchById($holidayId);
        $this->assertEquals('XYZ', $holiday->name);
        $this->assertEquals('2019-07-07',$holiday->holiday_date);
        $this->assertEquals(null, $holiday->added_by_admin);
        $this->assertEquals(1, $holiday->added_by_employee);

        DB::rollBack();
        Session::flush();
    }
    public function testDelete()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->call('DELETE','general-holidays/' . 1);
        $response->assertStatus(201);
        DB::rollBack();
        Session::flush();
    }
    public function testUpdate()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->put('general-holidays/' . 1,
            ['name' => 'Diwalis' , 'edit_holiday_date' => '2019-01-01']);
        $response->assertStatus(201);
        DB::rollBack();
    }
    public function testUpdateFail()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->put('general-holidays/' . 1,
            ['name' => 'Holi' , 'edit_holiday_date' => '2019-04-04']);
        $response->assertStatus(400);
        DB::rollBack();
    }
}
