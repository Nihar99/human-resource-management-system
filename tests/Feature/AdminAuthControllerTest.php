<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Session;
use Tests\TestCase;

class AdminAuthControllerTest extends TestCase {
    public function testView() {
        $response = $this->get('/login/admin');
        $response->assertSee('Admin');
        $response->assertSee('Login');
        $response->assertSee('Enter Email');
        $response->assertSee('Enter Password');
        $response->assertSee('Sign In');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'text/html; charset=UTF-8');
    }

    public function testStore() {
        $response = $this->post('/login/admin', [
            'email'    => 'sharad@3iology.com',
            'password' => '12345678',
        ]);
        $response->assertStatus(200);
        $response->assertExactJson(['success' => true]);
        Session::flush(); 
    }

    public function testStoreFailWithWrongEmail() {
        $response = $this->post('/login/admin', [
            'email'    => 'shara@3iology.com',
            'password' => '12345678',
        ]);
        $response->assertStatus(200);
        $response->assertJson(['success' => false]);
    }

    public function testStoreFailWithWrongPassword() {
        $response = $this->post('/login/admin', [
            'email'    => 'sharad@3iology.com',
            'password' => '1234578',
        ]);
        $response->assertStatus(200);
        $response->assertJson(['success' => false]);
    }
}
