<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class RequestControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAcceptLeaveRequest()
    {
        DB::beginTransaction();
        $fromDate = '06/18/2019';
        $toDate = '06/19/2019';
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/leave-management/apply', [
            'apply_leave_is_multiple' => 1,
            'apply_leave_management_from_date' => $fromDate,
            'apply_leave_management_to_date' => $toDate,
            'reason' => "reason",
            'apply_leave_management_leave_type' => 1,
            'apply_leave_management_select_emp' => 1
        ]);
    
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);
        $leave_id = $response->json()['leave_id'];

        $response = $this->post('/leave-management/accept-leave', [
            'leave_id' => $leave_id,
            'leave_staus' => 'accept',
            'apply_leave_management_leave_type' => 1,
            'apply_leave_is_multiple' => 1,
            'apply_leave_management_date' => 1,
            'apply_leave_management_from_date' => $fromDate,
            'apply_leave_management_to_date' => $toDate,
            'apply_leave_management_total_leaves' => 1
        ]);
   
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);
        Session::flush();
        DB::rollback();
    }
}
