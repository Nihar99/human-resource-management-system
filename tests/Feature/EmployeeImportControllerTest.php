<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\EmployeeExport;
use App\Department;
use App\Designation;
use App\Employee;
use App\Country;

class EmployeeImportControllerTest extends TestCase
{
    public function testIndex()
    {
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->get('employee/import');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'text/html; charset=UTF-8');
        $response->assertSee('Import Excel');
        $response->assertViewIs('employee.import');
    }
    // public function testDownloadSampleExcel()
    // {
    //     $data=(new EmployeeExport())->headings();

    //     $departments=(new Department())->getAllDepartment();
    //     $designations=(new Designation())->getAllDesignation();
    //     $countries=(new Country())->getAllCountries();

    //     $type='csv';
    //     Excel::create('Employee', function($excel) use ($data,$departments,$designations,$countries) {
    //         $excel->sheet('mySheet', function($sheet) use ($data,$departments,$designations,$countries)
    //         {
    //             $sheet->fromArray($data);

    //             $sheet->setCellValue('D4', 'Delete following before upload');
    //             $sheet->setCellValue('D5', 'Male');
    //             $sheet->setCellValue('D6', 'Female');

    //             $sheet->setCellValue('M4', 'Delete following before upload');
    //             $sheet->setCellValue('M5', 'Married');
    //             $sheet->setCellValue('M6', 'Unmarried');
    //             $sheet->setCellValue('M7', 'Divorse');

    //             $sheet->setCellValue('W4', 'Delete following before upload');
    //             $sheet->setCellValue('W5', 'Yes');
    //             $sheet->setCellValue('W6', 'No');

    //             $rowId=4;
    //             $sheet->setCellValue('F'.$rowId, 'Delete following before upload');
    //             foreach ($countries as $key => $county){
    //                 $rowId++;
    //                 $sheet->setCellValue('F'.$rowId,$county->name);
    //             }

    //             $rowId=4;
    //             $sheet->setCellValue('S'.$rowId, 'Delete following before upload');
    //             foreach ($departments as $key => $dept){
    //                 $rowId++;
    //                 $sheet->setCellValue('S'.$rowId,$dept->name);
    //             }

    //             $rowId=4;
    //             $sheet->setCellValue('T'.$rowId, 'Delete following before upload');
    //             foreach ($designations as $key => $desg){
    //                 $rowId++;
    //                 $sheet->setCellValue('T'.$rowId,$desg->name);
    //             }
    //         });
    //     })->download($type);

        // Excel::fake();

        // $this->actingAs($this->givenUser())
        //      ->get('/users/import/csv');

        // Excel::assertImported('filename.csv', 'diskName');

        // Excel::assertImported('filename.csv', 'diskName', function(UsersImport $import) {
        //     return true;
        // });

        // Excel::assertImported('filename.csv', function(UsersImport $import) {
        //     return true;
        // });
    }

