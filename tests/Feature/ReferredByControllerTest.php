<?php

namespace Tests\Feature;

use App\ReferredBy;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReferredByControllerTest extends TestCase
{
    public function testStoreByPost()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/referred-by', [
            'employeeId' => 1,
            'add_referred_by_name' => 'Tejal',
            'add_referred_by_position' => 'Developer',
            'add_referred_by_address' => 'Palghar',
            'add_referred_by_telephone' => '777777777',
        ]);
        $referredById = $response->json()['referredById'];
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);
        DB::rollBack();
        Session::flush();
    }

    public function testUpdateByPost()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);

        $response = $this->put('/referred-by/1', [
            'employeeId' => 1,
            'edit_referred_by_name' => 'Tejal',
            'edit_referred_by_position' => 'Developer',
            'edit_referred_by_address' => 'Palghar',
            'edit_referred_by_telephone' => '8888888',
        ]);

        $response->assertStatus(201);
        $response->assertJson(['success' => true]);

        DB::rollBack();
        Session::flush();
    }
}
