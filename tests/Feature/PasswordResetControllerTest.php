<?php

namespace Tests\Feature;

use Tests\TestCase;

class PasswordResetControllerTest extends TestCase
{
    public function testView()
    {
        $response = $this->get('/password/reset');
        $response->assertSee('Enter Email');
        $response->assertSee('Submit');
        $response->assertStatus(200);
    }
}
