<?php

namespace Tests\Feature;

use Tests\TestCase;

class OtherDetailsControllerTest extends TestCase {
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testFamilyDetailsIndex() {
        $response = $this->get('other-details/1');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'text/html; charset=UTF-8');
        $response->assertSee('Family Details');
        $response->assertViewIs('other-details');
    }

    public function testEmergencyContactIndex() {
        $response = $this->get('other-details/1');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'text/html; charset=UTF-8');
        $response->assertSee('Emergency Contact');
        $response->assertViewIs('other-details');
    }

    public function testLanguagesKnownIndex() {
        $response = $this->get('other-details/1');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'text/html; charset=UTF-8');
        $response->assertSee('Languages Known');
        $response->assertViewIs('other-details');
    }

    public function testEducationalProfileIndex() {
        $response = $this->get('other-details/1');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'text/html; charset=UTF-8');
        $response->assertSee('Educational Profile');
        $response->assertViewIs('other-details');
    }

    public function testScholarshipIndex() {
        $response = $this->get('other-details/1');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'text/html; charset=UTF-8');
        $response->assertSee('Scholarship / Prizes &amp; Awards');
        $response->assertViewIs('other-details');
    }

    public function testExposureComputerIndex() {
        $response = $this->get('other-details/1');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'text/html; charset=UTF-8');
        $response->assertSee('Exposure to Computer');
        $response->assertViewIs('other-details');
    }

    public function testSkillsExperienceIndex() {
        $response = $this->get('other-details/1');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'text/html; charset=UTF-8');
        $response->assertSee('Skills &amp; Experience');
        $response->assertViewIs('other-details');
    }

    public function testWorkExperienceIndex() {
        $response = $this->get('other-details/1');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'text/html; charset=UTF-8');
        $response->assertSee('Work Experience');
        $response->assertViewIs('other-details');
    }

    public function testReferredByIndex() {
        $response = $this->get('other-details/1');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'text/html; charset=UTF-8');
        $response->assertSee('Referred By');
        $response->assertViewIs('other-details');
    }
}
