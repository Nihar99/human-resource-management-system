<?php

namespace Tests\Feature;

use Tests\TestCase;

class LeaveManagementControllerTest extends TestCase {
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testApplyLeaveIndex() {
        $response = $this->get('leave-management/apply');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'text/html; charset=UTF-8');
        $response->assertSee('Apply Leave');
        $response->assertViewIs('leave-management.apply');
    }

    public function testRequestLeaveIndex() {
        $response = $this->get('leave-management/request');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'text/html; charset=UTF-8');
        $response->assertSee('Request Leave');
        $response->assertViewIs('leave-management.request');
    }

    public function testLeaveLedgerIndex() {
        $response = $this->get('leave-management/history');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'text/html; charset=UTF-8');
        $response->assertSee('Leave Ledger');
        $response->assertViewIs('leave-management.history');
    }

}
