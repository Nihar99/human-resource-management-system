<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\ApplyLeave;

class ApplyLeaveControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testApplyCasualLeave()
    {
        DB::beginTransaction();
        $employee_id = 1;
        $fromDate = '06/18/2019';
        $toDate = '06/19/2019';
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/leave-management/apply', [
            'apply_leave_is_multiple' => 1,
            'apply_leave_management_from_date' => $fromDate,
            'apply_leave_management_to_date' => $toDate,
            'reason' => "reason",
            'apply_leave_management_leave_type' => 1,
            'apply_leave_management_select_emp' => 1
        ]);
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);
        $leave_id = $response->json()['leave_id'];
        $leaveData = (new ApplyLeave())->fetchById($leave_id);
        $this->assertEquals('2019-06-18',$leaveData->from_date);
        $this->assertEquals('2019-06-19',$leaveData->to_date);
        $this->assertEquals(1,$leaveData->leave_master_id);
        $this->assertEquals(1,$leaveData->employee_id);
        Session::flush();
        DB::rollback();
    }

    public function testApplyPrivilegeLeave()
    {
        DB::beginTransaction();
        $employee_id = 1;
        $fromDate = '06/18/2019';
        $toDate = '06/19/2019';
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/leave-management/apply', [
            'apply_leave_is_multiple' => 1,
            'apply_leave_management_from_date' => $fromDate,
            'apply_leave_management_to_date' => $toDate,
            'reason' => "reason",
            'apply_leave_management_leave_type' => 3,
            'apply_leave_management_select_emp' => 1
        ]);
        $response->assertStatus(200);
        $response->assertJson(['failed' => true]);
        $this->assertEquals('Your leaves are exceeded',$response->json()['msg']);

        Session::flush();
        DB::rollback();
    }

    public function testApplySickLeave()
    {
        DB::beginTransaction();
        $employee_id = 1;
        $fromDate = '06/18/2019';
        $toDate = '06/19/2019';
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/leave-management/apply', [
            'apply_leave_is_multiple' => 1,
            'apply_leave_management_from_date' => $fromDate,
            'apply_leave_management_to_date' => $toDate,
            'reason' => "reason",
            'apply_leave_management_leave_type' => 3,
            'apply_leave_management_select_emp' => 1
        ]);
        $response->assertStatus(200);
        $response->assertJson(['failed' => true]);
        $this->assertEquals('Your leaves are exceeded',$response->json()['msg']);

        Session::flush();
        DB::rollback();
    }

    public function testApplySingleDayLeave()
    {
        DB::beginTransaction();
        $fromDate = '06/19/2019';
        Session::put('employee', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/leave-management/apply', [
            'apply_leave_is_multiple' => 0,
            'apply_leave_management_date' => $fromDate,
            'apply_leave_management_from_date' => '',
            'apply_leave_management_to_date' => '',
            'reason' => "reason",
            'apply_leave_management_leave_type' => 3
        ]);

        $response->assertStatus(200);
        $response->assertJson(['failed' => true]);
        $this->assertEquals('Your leaves are exceeded',$response->json()['msg']);

        Session::flush();
        DB::rollback();

    }

    public function testApplyLeaveExceeded()
    {
        DB::beginTransaction();
        $employee_id = 1;
        $fromDate = '06/01/2019';
        $toDate = '06/19/2019';
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/leave-management/apply', [
            'apply_leave_is_multiple' => 1,
            'apply_leave_management_from_date' => $fromDate,
            'apply_leave_management_to_date' => $toDate,
            'reason' => "reason",
            'apply_leave_management_leave_type' => 1,
            'apply_leave_management_select_emp' => 1
        ]);
        $response->assertStatus(200);
        $response->assertJson(['failed' => true]);
        $this->assertEquals('Your leaves are exceeded',$response->json()['msg']);

        Session::flush();
        DB::rollback();
    }
}
