<?php

namespace Tests\Feature;

use App\Designation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;

class DesignationControllerTest extends TestCase {
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testDesignationIndex() {
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->get('designations');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'text/html; charset=UTF-8');
        $response->assertSee('Designation');
        $response->assertViewIs('designations');
    }

    public function testStore() {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);

        $response = $this->post('/designations', [
            'name' => 'Country',
        ]);

        $designation_id = $response->json()['designation_id'];
        $designation = Designation::find($designation_id);
        $this->assertEquals('Country',$designation->name);
        $this->assertEquals(1,$designation->added_by_admin);
        $this->assertEquals(null,$designation->added_by_employee);
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);
        Session::flush();

        Session::put('employee', (object) ['id' => 1, 'client_id' => 1]);

        $response = $this->post('/designations', [
            'name' => 'Developer',
        ]);

        $designation_id = $response->json()['designation_id'];
        $designation = Designation::find($designation_id);
        $this->assertEquals('Developer',$designation->name);
        $this->assertEquals(null,$designation->added_by_admin);
        $this->assertEquals(1,$designation->added_by_employee);
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);


        DB::rollBack();
    }
    public function testDelete()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->call('DELETE','designations/' . 1);
        $response->assertStatus(201);
        DB::rollBack();
    }
    public function testUpdate()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->put('designations/' . 1,
            ['name' => 'Managersss']);
        $response->assertStatus(201);
        DB::rollBack();
    }
    public function testUpdateFail()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->put('designations/' . 1,
            ['name' => 'Developers']);
        $response->assertStatus(400);
        DB::rollBack();
    }
}
