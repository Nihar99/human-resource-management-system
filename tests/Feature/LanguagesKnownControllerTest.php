<?php

namespace Tests\Feature;

use App\LanguagesKnown;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LanguagesKnownControllerTest extends TestCase
{
    public function testStore() {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/languages-known', [
            'employeeId' => 1,
            'name' => 'Marathi',
            'read' => 'Yes',
            'write' =>'Yes',
            'speak' => 'Yes',
            'understand' => 'No'
        ]);

        $languagesKnownId = $response->json()['languagesKnownId'];
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);

        $language = (new LanguagesKnown())->fetchById($languagesKnownId);
        $this->assertEquals('Marathi', $language->name);
        $this->assertEquals(1 , $language->read);
        $this->assertEquals(1 , $language->write);
        $this->assertEquals(1, $language->speak);
        $this->assertEquals(1, $language->added_by_admin);
        $this->assertEquals(null, $language->added_by_employee);
        Session::flush();

        Session::put('employee', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/languages-known', [
            'employeeId' => 1,
            'name' => 'Marathis',
            'read' => 'Yes',
            'write' =>'Yes',
            'speak' => 'Yes',
            'understand' => 'No'
        ]);

        $languagesKnownId = $response->json()['languagesKnownId'];
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);

        $language = (new LanguagesKnown())->fetchById($languagesKnownId);
        $this->assertEquals('Marathis', $language->name);
        $this->assertEquals(1 , $language->read);
        $this->assertEquals(1 , $language->speak);
        $this->assertEquals(1, $language->write);
        $this->assertEquals(null, $language->added_by_admin);
        $this->assertEquals(1, $language->added_by_employee);

        DB::rollBack();
        Session::flush();
    }
    public function testStoreFail()
    {
        DB::beginTransaction();
        Session::put('admin', (object)['id' => 1, 'client_id' => 1]);
        $response = $this->post('/languages-known', [
            'employeeId' => 1,
            'name' => 'English',
            'read' => 'Yes',
            'write' => 'Yes',
            'speak' => 'Yes',
            'understand' => 'No'
        ]);

        $response->assertStatus(400);
    }
    public function testDelete()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->call('DELETE','languages-known/' . 1);
        $response->assertStatus(201);
        DB::rollBack();
        Session::flush();
    }
    public function testUpdate()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->put('languages-known/' . 1,
            ['employeeId' => 1,'edit_language' => 'Marathi'  , 'edit_read' => 'Yes','edit_write' =>'No', 'edit_speak' =>'Yes', 'edit_understand' => 'No',]);
        $response->assertStatus(201);
        DB::rollBack();
        Session::flush();
    }
    public function testUpdateFail()
    {

        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->put('languages-known/' . 1,
            ['employeeId' => 1,'edit_language' => ' '  , ]);
        $response->assertStatus(400);
        DB::rollBack();
        Session::flush();
    }
}
