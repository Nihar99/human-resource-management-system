<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TimePolicyControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testTimePolicyIndex()
    {
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->get('time-policy');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'text/html; charset=UTF-8');
        $response->assertSee('Time Policy');
        $response->assertViewIs('time-policy');
    }
    public function testStore()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('time-policy' , [
            'day' => 'AnyDay',

            'time_in' => '9:00pm',
            'time_out' => '9:00pm',
            'break_in' => '9:00pm',
            'break_out' => '9:00pm',
            'late_entry_after' => '9:00pm',
            'early_exit_before'=> '9:00pm',
            'halfday_employee_comes_after'=>'9:00pm',
            'halfday_employee_leaves_before' => '9:00pm',
            'weekoff' => 'Yes',


        ]);
        $response->assertStatus(400);
        DB::rollBack();
    }
    public function testStoreFail()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('time-policy' , [
            'day' => 'Monday',

            'time_in' => '9:00pm',
            'time_out' => '9:00pm',
            'break_in' => '9:00pm',
            'break_out' => '9:00pm',
            'late_entry_after' => '9:00pm',
            'early_exit_before'=> '9:00pm',
            'halfday_employee_comes_after'=>'9:00pm',
            'halfday_employee_leaves_before' => '9:00pm',
            'weekoff' => 'Yes',


        ]);
        $response->assertStatus(400);
        DB::rollBack();
    }
    public function testDelete()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->call('DELETE','time-policy/' . 1);
        $response->assertStatus(201);
        DB::rollBack();
    }
    public function testUpdate()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->put('time-policy/' . 1,
            ['edit_day' => 'Sunday' , 'edit_weekoff'=>'Yes']);
        $response->assertStatus(201);
        DB::rollBack();
    }
}
