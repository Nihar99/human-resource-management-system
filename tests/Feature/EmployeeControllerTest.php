<?php

namespace Tests\Feature;

use App\Employee;
use App\Http\Controllers\SessionController;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class EmployeeControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testEmployeeIndex()
    {
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->get('employee/create');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'text/html; charset=UTF-8');
        $response->assertSee('Create Employee');
        $response->assertViewIs('employee.create');
    }
    public function testCreate()
    {
        DB::beginTransaction();
        Storage::fake('public');
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
            $response = $this->post('employee/create' , [
             //PersonalDetails
                'ecode' => 'E0002',
            'first_name' => 'Nihar',
            'middle_name' => 'Nitesh',
                'employee_image' => $file = UploadedFile::fake()->image('random.jpg'),
            'last_name' => 'Nayak',
            'gender'=> 1,
            'date_of_birth' => '1999-01-01',
            'nationality'=> 100,
            'place_state_of_birth' => 'Mumbai,Maharashtra',
            'religion'=> 'Hindu',
            'marital_status' => 2,
            'mother_tongue'=> 'Konkani',
            'personal_email' => 'nih@gmail.com',
            'hobbies_and_interests' => 'abc ghi xyz',
                //ProfessionalDetails
                'probation_from' => '2019-01-01',
                'professional_email'=>'abc@gmail.com',
                'department_id' => 1,
                'designation_id' => 1,
                'enable_ess' => 'No',
                'confirmation_on' => '2019-01-03',
                //'status' => 1,
                'present_address' => 'abc xyz',
                'present_address_tel_no' => NULL,
                'permanent_address' => 'ABC XYZ',
                'permanent_address_tel_no' => NULL,

            ]);
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);
        DB::rollBack();
    }
    public function testCreateFail()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        Storage::fake('public');
        $response = $this->post('employee/create' , [
            //PersonalDetails

            'ecode' => 'E0001',
            'first_name' => 'Nihar',
            'middle_name' => 'Nitesh',
            'last_name' => 'Nayak',
            'employee_image' => $file = UploadedFile::fake()->image('random.jpg'),
            'gender'=> 1,
            'date_of_birth' => '1999-01-01',
            'nationality'=> 100,
            'place_state_of_birth' => 'Mumbai,Maharashtra',
            'religion'=> 'Hindu',
            'marital_status' => 2,
            'mother_tongue'=> 'Konkani',
            'personal_email' => 'nih@gmail.com',
            'hobbies_and_interests' => 'abc ghi xyz',
            //ProfessionalDetails
            'probation_from' => '2019-01-01',
            'professional_email'=>'abc@gmail.com',
            'department_id' => 1,
            'designation_id' => 1,
            'confirmation_on' => '2019-01-03',
            //'status' => 1,
            'enable_ess' => 1,
            'present_address' => 'abc xyz',
            'present_address_tel_no' => NULL,
            'permanent_address' => 'ABC XYZ',
            'permanent_address_tel_no' => NULL,

        ]);
        $response->assertStatus(400);
        DB::rollBack();
    }
    public function testDelete()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->call('DELETE','employee/create/' . 1);
        $response->assertStatus(201);
        DB::rollBack();
        Session::flush();
    }
    public function testS3bucket() {
        $result = Storage::disk('s3')->put('abc.txt', 'DIR' . "/EmployeeControllerTest.php");
        $this->assertTrue($result);
        $this->assertTrue(Storage::disk('s3')->exists('abc.txt'));
    }

}
