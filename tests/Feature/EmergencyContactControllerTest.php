<?php

namespace Tests\Feature;

use App\EmergencyContact;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmergencyContactControllerTest extends TestCase
{
    public function testStore() {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/emergency-contacts', [
            'employeeId' => 1,
            'name' => 'Nihar',
            'relationship' => 'Parent',
            'address' =>'xyz',
            'phone' => 99999,
        ]);

        $emergencyContactId = $response->json()['emergencyContactId'];
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);

        $contact = (new EmergencyContact())->fetchById($emergencyContactId);
        $this->assertEquals('Nihar', $contact->name);
        $this->assertEquals('Parent' , $contact->relationship);
        $this->assertEquals('xyz' , $contact->address);
        $this->assertEquals(99999, $contact->phone);
        $this->assertEquals(1, $contact->added_by_admin);
        $this->assertEquals(null, $contact->added_by_employee);
        Session::flush();

        Session::put('employee', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/emergency-contacts', [
            'employeeId' => 1,
            'name' => 'Nihars',
            'relationship' => 'Parent',
            'address' =>'xyz',
            'phone' => 99999,
        ]);

        $emergencyContactId = $response->json()['emergencyContactId'];
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);

        $contact = (new EmergencyContact())->fetchById($emergencyContactId);
        $this->assertEquals('Nihars', $contact->name);
        $this->assertEquals('Parent' , $contact->relationship);
        $this->assertEquals('xyz' , $contact->address);
        $this->assertEquals(99999, $contact->phone);
        $this->assertEquals(null, $contact->added_by_admin);
        $this->assertEquals(1, $contact->added_by_employee);

        DB::rollBack();
        Session::flush();
    }
    public function testStoreFail()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/emergency-contacts', [
            'employeeId' => 1,
            'name' => ' ',
            'relationship' => ' ',
            'address' =>'xyz',
            'phone' => 99999,
        ]);
        $response->assertStatus(400);

    }
    public function testDelete()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->call('DELETE','emergency-contacts/' . 1);
        $response->assertStatus(201);
        DB::rollBack();
        Session::flush();
    }
    public function testUpdate()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->put('emergency-contacts/' . 1,
            ['employeeId' => 1,'edit_name' => 'Nihar'  , 'edit_relationship' => 'Parent','edit_address' =>'xyz', 'edit_phone' => 99999, ]);
        $response->assertStatus(201);
        DB::rollBack();
        Session::flush();
    }
    public function testUpdateFail()
    {

        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->put('emergency-contacts/' . 1,
            ['employeeId' => 1,'edit_name' => ' '  , 'edit_relationship' => 'Parent','edit_address' =>'xyz', 'edit_phone' => 99999, ]);
        $response->assertStatus(400);
        DB::rollBack();
        Session::flush();
    }
}
