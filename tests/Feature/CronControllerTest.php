<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use App\LeaveBalance;
use App\LeaveLedger;

class CronControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function testIfCasualLeaveExist(){
        DB::beginTransaction();
        $response = $this->get('/cron');
        $leaveCount = (new LeaveBalance())->getLeaveCount(1,1);
        $this->assertEquals(1,$leaveCount);
        $checkBalance = (new LeaveBalance())->getAvailableLeaves(1,1);
        $this->assertEquals(5,$checkBalance);
        DB::rollback();
    }

    public function testIfSickLeaveExist(){
        DB::beginTransaction();
        $response = $this->get('/cron');
        $leaveCount = (new LeaveBalance())->getLeaveCount(1,3);
        $this->assertEquals(1,$leaveCount);
        $checkBalance = (new LeaveBalance())->getAvailableLeaves(1,3);
        $this->assertEquals(5,$checkBalance);

        DB::rollback();
    }

    public function testIfPrivilegeLeaveExist(){
        DB::beginTransaction();
        $leaveBalanceCL = (new LeaveBalance())->isLeaveExist(1,2);
        $response = $this->get('/cron');
        if($leaveBalanceCL){
            $checkBalance = (new LeaveBalance())->getAvailableLeaves(1,2);
            if($leaveBalanceCL->available > 6){
                $this->assertEquals(24,$checkBalance);
            }else{
                $this->assertEquals($leaveBalanceCL->available + 10,$checkBalance);
            }

        }
        $leaveCount = (new LeaveBalance())->getLeaveCount(1,1);
        $this->assertEquals(1,$leaveCount);

        DB::rollback();
    }
}
