<?php

namespace Tests\Feature;

use App\ScholarShipPrizes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ScholarshipPrizeControllerTest extends TestCase
{
    public function testStore()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/scholarships-prizes', [
            'employeeId' => 1,
            'details' => 'abc xyz',
        ]);

        $scholarshipPrizesId = $response->json()['scholarshipPrizesId'];
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);

        $prize = (new ScholarShipPrizes())->fetchById($scholarshipPrizesId);
        $this->assertEquals('abc xyz', $prize->details);
        $this->assertEquals(1, $prize->added_by_admin);
        $this->assertEquals(null, $prize->added_by_employee);
        Session::flush();

        Session::put('employee', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/scholarships-prizes', [
            'employeeId' => 1,
            'details' => 'abc xyz',
        ]);

        $scholarshipPrizesId = $response->json()['scholarshipPrizesId'];
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);

        $prize = (new ScholarShipPrizes())->fetchById($scholarshipPrizesId);
        $this->assertEquals('abc xyz', $prize->details);
        $this->assertEquals(null, $prize->added_by_admin);
        $this->assertEquals(1, $prize->added_by_employee);

        DB::rollBack();
        Session::flush();
    }
    public function testStoreFail()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/scholarships-prizes', [
            'details' => ' ',
            'employeeId' => 1,
        ]);
        $response->assertStatus(400);
    }
    public function testUpdate()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->put('scholarships-prizes/' . 1,
            ['edit_details' => 'abc xyz','employeeId' => 1,  ]);
        $response->assertStatus(201);
        DB::rollBack();
        Session::flush();
    }
    public function testUpdateFail()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->put('educational-profiles/' . 1,
            ['edit_details' => ' ','employeeId' => 1, ]);
        $response->assertStatus(400);
        DB::rollBack();
        Session::flush();
    }
   public function testDelete()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->call('DELETE','scholarships-prizes/' . 1);
        $response->assertStatus(201);
        DB::rollBack();
        Session::flush();
    }
}
