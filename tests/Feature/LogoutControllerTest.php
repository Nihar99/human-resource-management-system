<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Session;
use Tests\TestCase;

class LogoutControllerTest extends TestCase {
    public function testView() {
        Session::put('admin', ['client_id' => 1]);
        $this->assertTrue(Session::exists('admin'));
        $response = $this->get('/logout');
        $this->assertFalse(Session::exists('admin'));
        $response->assertStatus(302);
        $response->assertLocation('login/admin');

        Session::put('user', ['client_id' => 1]);
        $this->assertTrue(Session::exists('user'));
        $response = $this->get('/logout');
        $this->assertFalse(Session::exists('user'));
        $response->assertStatus(302);
        $response->assertLocation('login/user');
    }
}
