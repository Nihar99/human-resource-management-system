<?php

namespace Tests\Feature;

use App\SkillsAndExperience;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SkillExperienceControllerTest extends TestCase
{
    public function testStore()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/skills-experiences', [
            'employeeId' => 1,
            'details' => 'abc xyz',
        ]);

        $skillsExperienceId = $response->json()['skillsExperienceId'];
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);

        $skill = (new SkillsAndExperience())->fetchById($skillsExperienceId);
        $this->assertEquals('abc xyz', $skill->details);
        $this->assertEquals(1, $skill->added_by_admin);
        $this->assertEquals(null, $skill->added_by_employee);
        Session::flush();

        Session::put('employee', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/skills-experiences', [
            'details' => 'abc xyz',
            'employeeId' => 1,
        ]);

        $skillsExperienceId = $response->json()['skillsExperienceId'];
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);

        $skill = (new SkillsAndExperience())->fetchById($skillsExperienceId);
        $this->assertEquals('abc xyz', $skill->details);
        $this->assertEquals(null, $skill->added_by_admin);
        $this->assertEquals(1, $skill->added_by_employee);

        DB::rollBack();
        Session::flush();
    }
    public function testStoreFail()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/skills-experiences', [
            'details' => ' ',
            'employeeId' => 1,
        ]);
        $response->assertStatus(400);
    }
    public function testUpdate()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->put('skills-experiences/' . 1,
            ['edit_skills_details' => 'abc xyz','employeeId' => 1,  ]);
        $response->assertStatus(201);
        DB::rollBack();
        Session::flush();
    }
    public function testUpdateFail()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->put('skills-experiences/' . 1,
            ['edit_skills_details' => ' ','employeeId' => 1, ]);
        $response->assertStatus(400);
        DB::rollBack();
        Session::flush();
    }
    public function testDelete()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->call('DELETE','skills-experiences/' . 1);
        $response->assertStatus(201);
        DB::rollBack();
        Session::flush();
    }
}
