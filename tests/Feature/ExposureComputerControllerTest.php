<?php

namespace Tests\Feature;

use App\exposure_to_computer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExposureComputerControllerTest extends TestCase
{
    public function testStore() {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/exposure-computer', [
            'employeeId' => 1,
            'add_exposure_language' => 'oidweo',
            'add_exposure_operating_system' => 'dewfd',
            'add_exposure_application' => 'ewfefd',
            'add_exposure_hardware' => 'dwfwef',
        ]);

        $exposureId = $response->json()['exposureId'];
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);

        $exposure = (new exposure_to_computer())->fetchById($exposureId);
        $this->assertEquals('oidweo', $exposure->language);
        $this->assertEquals('dewfd' , $exposure->operating_system);
        $this->assertEquals('ewfefd' , $exposure->application);
        $this->assertEquals('dwfwef', $exposure->hardware);
        $this->assertEquals(1, $exposure->added_by_admin);
        $this->assertEquals(null, $exposure->added_by_employee);
        Session::flush();

        Session::put('employee', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/exposure-computer', [
            'employeeId' => 1,
            'add_exposure_language' => 'oidweo',
            'add_exposure_operating_system' => 'dewfd',
            'add_exposure_application' => 'ewfefd',
            'add_exposure_hardware' => 'dwfwef',
        ]);

        $exposureId = $response->json()['exposureId'];
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);

        $exposure = (new exposure_to_computer())->fetchById($exposureId);
        $this->assertEquals('oidweo', $exposure->language);
        $this->assertEquals('dewfd' , $exposure->operating_system);
        $this->assertEquals('ewfefd' , $exposure->application);
        $this->assertEquals('dwfwef', $exposure->hardware);
        $this->assertEquals(null, $exposure->added_by_admin);
        $this->assertEquals(1, $exposure->added_by_employee);

        DB::rollBack();
        Session::flush();
    }
    public function testStoreFail()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/exposure-computer', [
            'employeeId' => 1,
            'add_exposure_language' => ' ',
            'add_exposure_operating_system' => ' ',
            'add_exposure_application' => ' ',
            'add_exposure_hardware' => '',
        ]);
        $response->assertStatus(400);

    }
    public function testDelete()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->call('DELETE','exposure-computer/' . 1);
        $response->assertStatus(201);
        DB::rollBack();
        Session::flush();
    }
    public function testUpdate()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->put('exposure-computer/' . 1,
            [ 'employeeId' => 1,
                'edit_exposure_language' => 'oidweodwef',
                'edit_exposure_operating_system' => 'dewfdefwf',
                'edit_exposure_application' => 'ewfefewffd',
                'edit_exposure_hardware' => 'dwfwef', ]);
        $response->assertStatus(201);
        DB::rollBack();
        Session::flush();
    }
    public function testUpdateFail()
    {

        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->put('exposure-computer/' . 1,
            ['employeeId' => 1,'edit_exposure_language' => 'oidweodwef',
                'edit_exposure_operating_system' => ' ',
                'edit_exposure_application' => 'ewfefewffd',
                'edit_exposure_hardware' => 'dwfwef',]);
        $response->assertStatus(400);
        DB::rollBack();
        Session::flush();
    }
}
