<?php

namespace Tests\Feature;

use App\FamilyDetails;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FamilyDetailsControllerTest extends TestCase
{
    public function testStore()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/family-details', [
            'employeeId' => 1,
            'full_name' => 'XYZ',
            'age' => 3,
            'occupation' => 'UVW',
            'relationship' => 'xxx',
            'gender' => 'Male',
            'dependant' => 'Yes',
        ]);
        $familyDetailsId = $response->json()['familyDetailsId'];
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);

        $familyDetails = (new FamilyDetails())->fetchById($familyDetailsId);
        $this->assertEquals('XYZ', $familyDetails->full_name);
        $this->assertEquals(1,$familyDetails->gender);
        $this->assertEquals('xxx',$familyDetails->relationship);
        $this->assertEquals('UVW',$familyDetails->occupation);
        $this->assertEquals(3,$familyDetails->age);
        $this->assertEquals(1, $familyDetails->added_by_admin);
        $this->assertEquals(null, $familyDetails->added_by_employee);
        Session::flush();

        Session::put('employee', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->post('/family-details', [
            'employeeId' => 1,
            'full_name' => 'XYZW',
            'age' => 3,
            'occupation' => 'UVW',
            'relationship' => 'xxx',
            'gender' => 'Male',
            'dependant' => 'Yes',
        ]);

        $familyDetailsId = $response->json()['familyDetailsId'];
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);

        $detail = (new FamilyDetails())->fetchById($familyDetailsId);
        $this->assertEquals('XYZW', $detail->full_name);
        $this->assertEquals(1,$detail->gender);
        $this->assertEquals('xxx',$detail->relationship);
        $this->assertEquals('UVW',$detail->occupation);
        $this->assertEquals(3,$detail->age);
        $this->assertEquals(null, $detail->added_by_admin);
        $this->assertEquals(1, $detail->added_by_employee);
        DB::rollBack();
        Session::flush();
    }
    public function testDelete()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->call('DELETE','family-details/' . 1);
        $response->assertStatus(201);
        DB::rollBack();
        Session::flush();
    }
    public function testUpdate()
    {
        DB::beginTransaction();
        Session::put('admin', (object) ['id' => 1, 'client_id' => 1]);
        $response = $this->put('family-details/' . 1,
            ['employeeId' => 1,'edit_family_name' => 'Neha'  , 'edit_family_age' => 40,'edit_family_occupation' =>'xyz', 'edit_family_relationship' => 'www','edit_family_gender' => 'Male' , 'edit_family_dependant' => 'No', ]);
        $response->assertStatus(201);
        DB::rollBack();
        Session::flush();
    }
}
