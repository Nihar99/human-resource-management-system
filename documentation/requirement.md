# 3ioHRMS
## HR Management Software for companies to manage their Employees
## Entities & Features
1. Employee Management (ESS)
2. Attendance Management
3. Leave Management
4. Loan Approval System
5. HR Management
6. Payroll Management

Company registration as the super user of the system where he can see/access and use all the entities that it is subscribed to.  
## Dependancy of modules
ESS is dependant on HR  
Attendance management is dependant on ESS  
Leave management is dependant on Attendance  
Payroll is dependant on Leave Management

## Modules
### Attendance Status
1. Present - P
2. Absent - A
3. Outdoor duty - OD
4. WeekOff - WO
5. Half day - HD
6. Compensatory Off - CO
7. [Leave masters](#leave-masters-example)
### Leave masters example
1. Casual Leave - CL
2. Maternity Leave - ML
3. Sick Leave - SL
4. Privilege Leave - PL

### Name : Authentication
### Description : In this module employee/admin of the company will be able to login to the system
### Login Screen (For company admin)
1. Input
   1. Email Id | Type:Text | Compulsory
   2. Password | Type:Email | Compulsory
2. Validation
   1. Email id should be registered in the system
   2. The company under which it is registered should be active.
   3. Email id and password should be validated
3. Output
   1. Login successful
   2. Login failed, because of **_some validation_**
### Login Screen (For company employee)
1. Input
   1. Company Id | Type:int | Compulsory
   2. Ecode | Type:Text | Compulsory
2. Validation
   1. Company id should exists
   2. Company the emloyee belongs to should be active
   3. Ecode & Password should be validated
3. Ouput
   1. Login successful
   2. Login failed, because of **_some validation_**
### Forgot password screen (For company admin)
1. Input
   1. Email Id | Type:Text | Compulsory
2. Validation
   1. Email id should be registered in the system
   2. The company under which it is registered should be active.
3. Process
   1. If validation success, email should be sent to reset password to the email id.
4. Output
   1. Password reset mail, has been sent to your email id.
   2. Password reset failed, because of **_some validation_**

### Name : ESS (Employee Self Service) (Will not be visible to super admin, because he is not an employee)
### Description : In this module employee can check all his profile details, attendance and can raise exception and apply for leave.

### Employee details ([according to this file](employee-application-form.xls))
1. Personal
2. Professional
3. Salary
4. Education
5. Work history
6. Dependant 
### Attendance (If attendance management is enabled for the company)
1. Calendar view to check attendance & its status
2. Show shift details
3. Status involves [this](#attendance-status) entities
4. Raising Exception in calendar
   1. Inputs
      1. Time In | Type:Time | Compulsory
      2. Time Out | Type:Time | Compulsory
   2. Validation
      1. Timein should be less than timeout
   3. Ouput
      1. Exception raised successfully
### Leave (If leave management is enabled for the company)
1. Leave card showing leave available, leave taken, leave balance according to the [leave masters](#leave-masters-example)
2. Apply leave
    1. Leave calendar showing holidays and leaves taken, pending by employee
    2. Inputs
       1. Leave From | Type:Date |Compulsory
       2. Leave To | Type:Date | Compulsory
       3. Leave type | Type:Dropdown | Leave Masters | Compulsory
    3. Validation
       1. Validate according to leave policy
    4. Output
       1. Leave applied successfully
       2. Leave cannot a be applied (Show reason according to leave policy)

### Name : HR (Human Resource)
### Description : In this module HR can set department masters, designation masters, add new employees, set company-wise shift, leave masters, set leave policy

### Department masters
#### Create new department
1. Inputs
   1. Department Name | Type:Text | Compulsory | Unique:Companywise
2. Validation
   1. Non-deleted record of the department should not exists in the existing company
3. Output
   1. Department saved successfully
   2. Department already exists
#### View existing department
1. Server-side datatable showing view
#### Edit existing department
1. Inputs
   1. Department Name | Type:Text | Compulsory | Unique:Companywise
2. Validation
   1. Non-deleted record of the department should not exists in the existing company
   2. Previous name and new name should not be the same
3. Output
   1. Department saved successfully
   2. Department already exists
#### Delete existing department
1. Input
   1. Department id | Type:int | Nonviewable
   2. Are you sure you want to delete this _**Department**_?
   3. Yes | No
2. Validation
   1. Should not delete if it is assigned to an active employee
3. Output
   1. Department cannot be deleted
   2. Department deleted successfully

### Designation masters
#### Create new designation
1. Inputs
   1. Designation Name | Type:Text | Compulsory | Unique:Companywise
2. Validation
   1. Non-deleted record of the designation should not exists in the existing company
3. Output
   1. Designation saved successfully
   2. Designation already exists
#### View existing designation
1. Server-side datatable showing view
#### Edit existing designation
1. Inputs
   1. Designation Name | Type:Text | Compulsory | Unique:Companywise
2. Validation
   1. Non-deleted record of the designation should not exists in the existing company
   2. Previous name and new name should not be the same
3. Output
   1. Designation saved successfully
   2. Designation already exists
#### Delete existing designation
1. Input
   1. Designation id | Type:int | Nonviewable
   2. Are you sure you want to delete this _**Designation**_?
   3. Yes | No
2. Validation
   1. Should not delete if it is assigned to an active employee
3. Output
   1. Designation cannot be deleted
   2. Designation deleted successfully

### Employee Masters ([according to this file](employee-application-form.xls))
#### Add/Update new employee
**Personal Details**
1. Input
   1. Photo | Type:Image
   2. First Name | Type:Text | Compulsory
   3. Middle Name | Type:Text
   4. Last Name | Type:Text | Compulsory
   5. Gender | Type:Dropdown | Compulsory | 1-Male, 2-Female
   6. Date of Birth | Type:Date | Compulsory
   7. Nationality | Type:Text
   8. Present Address | Type:Text | Compulsory
   9. Tel No.(Of present address) | Type:Text
   10. Permanent Address | Type:Text | Compulsory
   11. Tel No. (of permanent address) | Type:Text
   12. Place & State of Birth | Type:Text | Compulsory
   13. Religion | Type:Text | Compulsory
   14. Marital Status | Type:Dropdown | Compulsory | 1-Married, 2-Unmarried, 3-Divorse
   15. Mother Tongue | Type:Text | Compulsory
   16. Email Id | Type:Email
   17. Hobbies & Interests | Type:Text
2.  Output
    1.  Unable to save because **_Validation reason_**
    2.  Employee details saved successfully
   
**Professional Details**
1. Input
   1. Employee Code | Type:Text | Compulsory | Unique:Companywise
   2. Email Id | Type:Email
   3. Department | Type:Dropdown | Department Masters
   4. Designation | Type:Dropdown | Designation Masters
   5. Date of joining on probation | Type:Date | Compulsory
   6. Date of confirmation | Type:Date
2. Validation
   1. Ecode should not be same even for employee that has left
3. Process
   1. An email should be sent to the email with the user id (Ecode) and automatically generated password of the system.
4. Output
   1. Employee details saved successfully
   
**Family details** (Multiple entries)
1. Input | Compulsory
   1. Name | Type:Text
   2. Age | Type:int
   3. Occupation | Type:Text
   4. Relationship | Type:Text
   5. Gender | Type:Dropdown | 1-Male, 2-Female
   6. Dependant | Type:Dropdown | 1-Dependant,2-Not Dependant
2. Output
   1. Unable to submit because _**Some validation**_
   2. Details saved successfully
   
**Emergency Contact** (Multiple entries)
1. Input | Compulsory
   1. Name | Type:Text
   2. Relationship | Type:Text
   3. Address | Type:Text
   4. Phone | Type:Text

**Languages known** (Multiple entries)
1. Input | Compulsory
   1. Language | Type:Text
   2. Read | Type:Dropdown | 1-Yes,2-No
   3. Write | Type:Dropdown | 1-Yes,2-No
   4. Speak | Type:Dropdown | 1-Yes,2-No
   5. Understand | Type:Dropdown | 1-Yes,2-No
2. Output
   1. Unable to submit because _**Some validation**_
   2. Details saved successfully
   
**Education profile** (Multiple entries)
1. Input | Compulsory
   1. Board/Degree | Type:Text
   2. Year of passing | Type:Dropdown | 4 digit int
   3. Name of institution | Type:Text
   4. Principle Subjects | Type:Text
   5. Grade/Percentage | Type:Text
2. Output
   1. Unable to submit because _**Some validation**_
   2. Details saved successfully
   
**Scholarship/Prizes and Awards** (Multiple entries)
1. Input | Compulsory
   1. Details | Type:Text
2. Output
   1. Unable to submit because _**Some validation**_
   2. Details saved successfully
   
**Exposure to computer** (Multiple entries)
1. Input|Compulsory
   1. Language | Type:Text
   2. Operating System | Type:Text
   3. Application | Type:Text
   4. Hardware | Type:Text
2. Output
   1. Unable to submit because _**Some validation**_
   2. Details saved successfully
   
**Any other skills and experience** (Multiple entries)
1. Input | Compulsory
   1. Details | Type:Text
2. Output
   1. Unable to submit because _**Some validation**_
   2. Details saved successfully

```
Commented out
c. Salary
Details
I. Salary components
II. Deduction components
IV. Bonus
```

**Work Experience** (Multiple entries)
1. Input | Compulsory
   1. Name of organisation | Type:Text
   2. Address of organisation | Type:Text
   3. From Date | Type:Date
   4. To Date | Type:Date
   5. Position Held | Type:Text
   6. Reporting to | Type:Text
   7. Nature of work | Type:Text
   8. Reason of leaving | Type:Text
   9. Gross Annual | Type:Text
2. Output
   1. Unable to submit because _**Some validation**_
   2. Details saved successfully
   
**Referred By** (Multiple entries)
1. Input | Compulsory
   1. Name | Type:Text
   2. Position | Type:Text
   3. Address | Type:Text
   4. Tel No. | Type:Text
2. Output
   1. Unable to submit because _**Some validation**_
   2. Details saved successfully

> All the data with multiple entries are subjected to deletion and can be deleted, but none of the non multiple entries in this module are deletable.  
> Every submodule should have a upload excel feature apart from Personal and Professional Details  
> Personal and Professional details should be captured in a single excel file

#### View Employee
1. Datatable to view employees list
2. When clicked on employee bootstrap wizard view, server-side for all the submodules as tags
3. Change password option in the datatable, to reset a particular users option

**Change Password**
1. Input
   1. New Password | Type:Password | Compulsory
   2. Retyp Password | Type:Password | Compulsory
2. Validation
   1. Both password should be same
3. Output
   1. Unable to update password because of _**some validation**_
   2. Password updated successfully

**Employee deletion**
1. Employee can be deleted because it was wrongly inserted (Permanent deletion)
   1. Input 
      1. id | Type:int | Nonviewable
      2. Are you sure you want to delete this employee, action cannot be reverted?
   2. Validation
      1. If attendance is not marked only than allow for deletion
   3. Ouput
      1. Cannot delete employee
      2. Employee deleted successfully
2. Employee Resigned
   1. Input
      1. id | Type:int | Nonviewable
      2. 2. Are you sure you want to do this?
   2. Ouput
      1. Employee marked as resigned
      2. Something went wrong
3. Employee Absconded
   1. Input
      1. id | Type:int | Nonviewable
      2. 2. Are you sure you want to do this?
   2. Ouput
      1. Employee marked as absconded
      2. Something went wrong

### Shift Management
#### Create shift
1. Input | Compulsory
   1. Time In | Type:Time
   2. Time Out | Type:Time
   3. Late Entry | Type:Time
   4. Early Exit | Type:Time
2. Validation
   1. Timein should be less than timeout
3. Output
   1. Shift created successfully
#### Update shift
1. Input | Compulsory
   1. Time In | Type:Time
   2. Time Out | Type:Time
   3. Late Entry | Type:Time
   4. Early Exit | Type:Time
2. Validation
   1. Timein should be less than timeout
   2. Time-in, Time-out should be different than previously saved
3. Output
   1. Shift updated successfully
   2. Time-in, Time-out should be different

#### Create Week Off (Multiple)
1. Input | Compulsory
   1. Select day | Type:Dropdown | Day, Int | Unique:Companywise
2. Validation
   1. Weekday should not repeat
3. Output
   1. Weekoff saved successfully
#### Delete Week Off
1. Input
   1. id | Day,Int | Unviewable
2. Output
   1. Weekoff deleted successfully

### Attendance Management
1. Dependance
   1. [Shift Management](#shift-management)
   2. [Weekoff Management](#create-week-off-multiple)
> Attendance status as per [this](#attendance-status)
> Status conditions

1. If employee comes and goes on time, mark as present
2. If employee goes early but for office duty its marked OD - Employee is marked as present
3. If employee is absent he is marked as Absent
4. If employee comes late for more than 3 days, each day after the 3rd day will be considered as Half day
5. If employee does shift from 9 to 1 - its a half day
6. If employee does shift from 1.30 to 5.30 - its a half day
7. Weekoff is mentioned as WO
8. If an employee works on WO he is entitled to take a day off in the upcoming days within 3 months, it will be considered as Comp Off
9. Leave as per the employee taking his leave and approved by his immediate senior
10. Employee can have Half working day and Half leave day instead of Half day

### Face training
#### Datatable of trained faces with employee details
#### Option to delete trained face
1. Input
   1. id | Day,Int | Unviewable
   2. Are you sure you want to delete trained face?
2. Output
   1. Trained face image deleted successfully
#### Only HR is allowed to train-retrain face from mobile app
#### Employees can mark self attendance from mobile, if their face is trained by HR
#### Import data sheet from Biometric device. Refer [this](april-2019-attendance.xls), sheet 2 - Basic Work Duration Report
#### Daily Report and Month to Date Report for Attendance
#### HR should be able to edit time in and time out of the employee in daily report
#### HR should be able to edit status in MTD report
#### When HR gets exception, open daily attendance report directly so that HR makes the respective edit