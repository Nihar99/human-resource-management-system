const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')

    .styles([
        'resources/theme/lib/select2/css/select2.min.css',
        'resources/theme/lib/timepicker/jquery.timepicker.css',
        'resources/theme/lib/datatables.net-dt/css/jquery.dataTables.min.css',
        'resources/theme/lib/datatables.net-dt/css/fixedColumns.dataTables.min.css',
        'resources/theme/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css',
        'resources/theme/css/bracket.css'
    ], 'public/css/theme.css')

    .scripts([
        'resources/theme/lib/jquery/jquery.min.js',
        'resources/theme/lib/jquery-ui/ui/widgets/datepicker.js',
        'resources/theme/lib/bootstrap/js/bootstrap.bundle.min.js',
        'resources/theme/lib/perfect-scrollbar/perfect-scrollbar.min.js',
        'resources/theme/lib/moment/min/moment.min.js',
        'resources/theme/lib/peity/jquery.peity.min.js',
        'resources/theme/lib/datatables.net/js/jquery.dataTables.min.js',
        'resources/theme/lib/datatables.net-dt/js/dataTables.dataTables.min.js',
        'resources/theme/lib/datatables.net-dt/js/dataTables.fixedColumns.min.js',
        'resources/theme/lib/datatables.net-responsive/js/dataTables.responsive.min.js',
        'resources/theme/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js',
        'resources/theme/lib/select2/js/select2.min.js',
        'resources/theme/lib/timepicker/jquery.timepicker.min.js',
        'resources/theme/lib/jquery-steps/build/jquery.steps.min.js',
        'resources/theme/lib/parsleyjs/parsley.min.js',
        'resources/theme/js/bracket.js',
        'resources/theme/js/tooltip-colored.js',
        'resources/theme/js/popover-colored.js',
    ], 'public/js/theme.js')

    .copyDirectory('resources/theme/img', 'public/img');
