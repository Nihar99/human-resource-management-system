@extends('layout.main')
@section('content')
    <div class="br-mainpanel">
        <div class="br-pagebody">
            <div class="br-pagetitle"></div>
            <div class="br-section-wrapper">
                <div class="col-12">
                    <div class="row">
                        <div class="col-6 p-0">
                            <h3 class="mb-4 text-dark">{{__('leaveManagement/applyLeave.headerTitle')}}</h3>
                        </div>
                        <div class="col-6 p-0 text-right">
                            <span class="text-danger font-weight-bold"> *</span>
                            Casual Leave : <span id="clLeave" class="text-danger font-weight-bold mr-2"> {{ $leaveBalanceCL }}</span>
                            Privilege Leave : <span id="plLeave" class="text-danger font-weight-bold mr-2"> {{ $leaveBalancePL }}</span>
                            Sick Leave : <span id="slLeave" class="text-danger font-weight-bold"> {{ $leaveBalanceSL }}</span>
                        </div>
                    </div>
                </div>

                <form id="applyLeaveForm" method="post">
                    <div class="col-12 p-0">
                        <div class="row">
                            <div class="col-4">
                                    <div class="form-group">
                                        <label for="apply_leave_management_leave_type"
                                                class="form-control-label">{{__('leaveManagement/applyLeave.applyLeaveLeaveType')}}
                                            <span class="tx-danger">*</span></label>
                                        <select class="form-control select2" name="apply_leave_management_leave_type"
                                                id="apply_leave_management_leave_type" required>
                                            <option selected disabled>Select Leave Type</option>
                                            @foreach ($activeLeaveType as $leave)
                                                <option value="{{ $leave->id }}" > {{ $leave->type }} </option>
                                            @endforeach
                                        </select>
                                    </div>
                            </div><!-- col-4 -->
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="apply_leave_management_day"
                                           class="form-control-label">{{__('leaveManagement/applyLeave.applyLeaveMultipleDay')}}
                                        <span class="tx-danger">*</span></label>
                                    <select class="form-control select2" name="apply_leave_is_multiple"
                                            id="apply_leave_is_multiple" data-placeholder="Select Is Multiple Days">
                                        <option value="-1" disabled>Is Multiple</option>
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    
                                    </select>
                                </div>
                            </div><!-- col-4 -->

                            <div class="col-4 singleDateBody">
                                <div class="form-group"><br><br>
                                    <label class="form-control-label">
                                        <input type="checkbox" name="apply_leave_is_half_day" id="apply_leave_is_half_day" value="1">
                                        {{__('leaveManagement/applyLeave.isHalfDay')}}
                                    </label>
                                </div><!-- form-group -->
                            </div><!-- col-4 -->
    
                            <div class="col-4 singleDateBody">
                                <div class="form-group">
                                    <label for="apply_leave_management_date"
                                           class="form-control-label">{{__('leaveManagement/applyLeave.applyLeaveDate')}}
                                        <span class="tx-danger">*</span></label>
                                    <input type="text" class="form-control fc-datepicker"
                                           name="apply_leave_management_date"
                                           id="apply_leave_management_date"
                                           placeholder="{{__('leaveManagement/applyLeave.selectApplyLeaveDate')}}">
                                </div><!-- form-group -->
                            </div><!-- col-4 -->

                        </div>
                    </div>
    
                   <div class="col-12 p-0">
                        <div class="row">
                            <div class="col-4 d-none fromDateBody">
                                <div class="form-group">
                                    <label for="apply_leave_management_from_date"
                                           class="form-control-label">{{__('leaveManagement/applyLeave.applyLeaveFromDate')}}
                                        <span class="tx-danger">*</span></label>
                                    <input type="text" class="form-control fc-datepicker"
                                           name="apply_leave_management_from_date"
                                           id="apply_leave_management_from_date"
                                           placeholder="{{__('leaveManagement/applyLeave.selectApplyLeaveDate')}}">
                                </div><!-- form-group -->
                            </div><!-- col-4 -->
    
                            <div class="col-4 d-none toDateBody">
                                <div class="form-group">
                                    <label for="apply_leave_management_to_date"
                                           class="form-control-label">{{__('leaveManagement/applyLeave.applyLeaveToDate')}}
                                        <span class="tx-danger">*</span></label>
                                    <input type="text" class="form-control fc-datepicker"
                                           name="apply_leave_management_to_date"
                                           id="apply_leave_management_to_date"
                                           placeholder="{{__('leaveManagement/applyLeave.selectApplyLeaveDate')}}" >
                                </div><!-- form-group -->
                            </div><!-- col-4 -->

                            <div class="col-4 d-none toTotalLeaves mt-3 align-items-center">
                                Total Leaves :  <span id="total_leaves" class="text-danger font-weight-bold mr-2 ml-2"> 0 </span>
                            </div><!-- col-4 -->
                        </div>
                    </div>

                    <div class="col-12 p-0">
                        <div class="row"> 
                            <input type="hidden" id="checkAdmin" value="{{ Session::has('admin') }}">
                            <input type="hidden" id="checkHR" value="{{ Session::get('hr') }}">
                        
                            @if(Session::get('admin') || Session::get('hr') == 1 )
                                <div class="col-4">
                                    <div class="form-group">
                                        <label for="apply_leave_management_select_emp"
                                            class="form-control-label">{{__('leaveManagement/applyLeave.applyLeaveSelectEmployee')}}
                                            <span class="tx-danger">*</span></label>
                                        <select class="form-control select2" name="apply_leave_management_select_emp"
                                                id="apply_leave_management_select_emp">
                                            <option selected disabled>Select Employee</option>
                                    
                                            @foreach ($activeEmployee as $employee)
                                            
                                                <option value="{{ $employee->id }}">{{ $employee->employeeDetails->first_name }} {{ $employee->employeeDetails->last_name }}</option>

                                            @endforeach   
    
                                        </select>
                                    </div>
                                </div><!-- col-4 -->
                            @endif
    
                        </div>
                    </div>
    
                    <div class="col-12 p-0">
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="apply_leave_management_select_emp"
                                        class="form-control-label">{{__('leaveManagement/applyLeave.reason')}}
                                        <span class="tx-danger">*</span></label>
                                        <textarea class="form-control" rows="5"
                                        id="reason"
                                        name="reason"
                                        placeholder="{{__('leaveManagement/applyLeave.reason')}}"
                                        required></textarea>
                                        
                                </div>
                            </div><!-- col-4 -->
                        </div>
                    </div>
                    <div class="col-12 p-0 text-right">
                        <button type="button" id="btnSubmitApplyLeave" class="btn btn-primary tx-uppercase">
                            {{__('leaveManagement/applyLeave.applyLeaveButtonSubmit')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
