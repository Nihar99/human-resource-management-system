@extends('layout.main')
@section('content')
    <div class="br-mainpanel">
        <div class="br-pagebody">
            <div class="br-pagetitle"></div>
            <div class="br-section-wrapper">
                <div class="col-12">
                    <div class="row">
                        <div class="col-6 p-0">
                            <h3 class="mb-4 text-dark">{{__('leaveManagement/requestLeave.headerTitle')}}</h3>
                        </div>
                    </div>
                </div>

                <div class="table-wrapper">
                    <table id="requestLeaveDataTable" class="table display nowrap">
                        <thead>
                        <tr>
                            <th>{{__('leaveManagement/requestLeave.requestLeaveEmpCode')}}</th>
                            <th>{{__('leaveManagement/requestLeave.requestLeaveEmpName')}}</th>
                            <th>{{__('leaveManagement/requestLeave.requestLeaveDepartment')}}</th>
                            <th>{{__('leaveManagement/requestLeave.requestLeaveDesignation')}}</th>
                            <th>{{__('leaveManagement/requestLeave.requestLeaveLeaveDate')}}</th>
                            <th>{{__('leaveManagement/requestLeave.requestLeaveNoDays')}}</th>
                            <th>{{__('leaveManagement/requestLeave.requestLeaveReason')}}</th>
                            <th>{{__('leaveManagement/requestLeave.requestLeaveStatus')}}</th>
                            <th class="text-right">{{__('leaveManagement/requestLeave.requestLeaveActions')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>0001</td>
                            <td>Test</td>
                            <td>IT</td>
                            <td>Developer</td>
                            <td>10 Jun 19 - 15 Jun 19</td>
                            <td>5</td>
                            <td>Reason for Leave</td>
                            <td>Reason for Leave</td>
                            <td class="text-right">
                                <div class="col-12 p-0">
                                    <i class="fa fa-check cursor-pointer mr-2 font-size-18 text-primary"
                                       id="acceptLeaveRequest"
                                       title="{{__('leaveManagement/requestLeave.acceptLeaveRequest')}}"></i>
                                    <i class="fa fa-times cursor-pointer font-size-18 text-danger"
                                       id="rejectLeaveRequest"
                                       title="{{__('leaveManagement/requestLeave.rejectLeaveRequest')}}"></i>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div><!-- table-wrapper -->

                <!-- BASIC MODAL -->
                <div id="acceptLeaveRequestModal" class="modal fade">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content bd-0 tx-14">
                            <div class="modal-header pd-y-20 pd-x-25">
                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('leaveManagement/requestLeave.acceptLeaveRequest')}}</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body pd-25">
                                <form id="leaveStatusForm">
                                    <div class="col-12 p-0">
                                        <div class="row">
                                            <div class="col-4">
                                                <input type="hidden" name="leave_id" id="leave_id">

                                                <input type="hidden" name="leave_staus" id="leave_status">

                                                <div class="form-group">
                                                        <label for="apply_leave_management_leave_type"
                                                                class="form-control-label">{{__('leaveManagement/applyLeave.applyLeaveLeaveType')}}
                                                            <span class="tx-danger">*</span></label>
                                                        <select class="form-control select2" name="apply_leave_management_leave_type"
                                                                id="apply_leave_management_leave_type" required>
                                                            <option selected disabled>Select Leave Type</option>
                                                            @foreach ($activeLeaveType as $leave)
                                                                <option value="{{ $leave->id }}" > {{ $leave->type }} </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                            </div><!-- col-4 -->
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="apply_leave_management_day"
                                                        class="form-control-label">{{__('leaveManagement/applyLeave.applyLeaveMultipleDay')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <select class="form-control select2" name="apply_leave_is_multiple"
                                                            id="apply_leave_is_multiple" data-placeholder="Select Is Multiple Days">
                                                        <option value="-1" disabled>Is Multiple</option>
                                                        <option value="0">No</option>
                                                        <option value="1">Yes</option>

                                                    </select>
                                                </div>
                                            </div><!-- col-4 -->

                                            <div class="col-4 singleDateBody">
                                                <div class="form-group">
                                                    <label for="apply_leave_management_date"
                                                        class="form-control-label">{{__('leaveManagement/applyLeave.applyLeaveDate')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <input type="text" class="form-control fc-datepicker"
                                                        name="apply_leave_management_date"
                                                        id="apply_leave_management_date"
                                                        placeholder="{{__('leaveManagement/applyLeave.selectApplyLeaveDate')}}">
                                                </div><!-- form-group -->
                                            </div><!-- col-4 -->

                                        </div>
                                    </div>
                                    <div class="col-12 p-0">
                                        <div class="row">
                                            <div class="col-4 d-none fromDateBody">
                                                <div class="form-group">
                                                    <label for="apply_leave_management_from_date"
                                                        class="form-control-label">{{__('leaveManagement/applyLeave.applyLeaveFromDate')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <input type="text" class="form-control fc-datepicker"
                                                        name="apply_leave_management_from_date"
                                                        id="apply_leave_management_from_date"
                                                        placeholder="{{__('leaveManagement/applyLeave.selectApplyLeaveDate')}}">
                                                </div><!-- form-group -->
                                            </div><!-- col-4 -->

                                            <div class="col-4 d-none toDateBody">
                                                <div class="form-group">
                                                    <label for="apply_leave_management_to_date"
                                                        class="form-control-label">{{__('leaveManagement/applyLeave.applyLeaveToDate')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <input type="text" class="form-control fc-datepicker"
                                                        name="apply_leave_management_to_date"
                                                        id="apply_leave_management_to_date"
                                                        placeholder="{{__('leaveManagement/applyLeave.selectApplyLeaveDate')}}" >
                                                </div><!-- form-group -->
                                            </div><!-- col-4 -->

                                            {{-- <div class="col-4 d-none toTotalLeaves mt-3 align-items-center">
                                                Total Leaves :  <span id="total_leaves" class="text-danger font-weight-bold mr-2 ml-2"> 0 </span>
                                            </div><!-- col-4 --> --}}

                                            <div class="col-4 d-none toTotalLeaves">
                                                <div class="form-group">
                                                    <label for="apply_leave_management_to_date"
                                                        class="form-control-label">{{__('leaveManagement/applyLeave.total_leaves')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <input type="text" class="form-control"
                                                        name="apply_leave_management_total_leaves"
                                                        id="total_leaves"
                                                        placeholder="{{__('leaveManagement/applyLeave.total_leaves')}}" >
                                                </div><!-- form-group -->
                                            </div><!-- col-4 -->

                                        </div>
                                    </div>
                                    <div class="col-12 p-0">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label for="apply_leave_management_select_emp"
                                                        class="form-control-label">{{__('leaveManagement/applyLeave.reason')}}
                                                        <span class="tx-danger">*</span></label>
                                                        <textarea class="form-control"  disabled rows="5"
                                                        id="reason"
                                                        name="reason"
                                                        placeholder="{{__('leaveManagement/applyLeave.reason')}}"
                                                        required></textarea>

                                                </div>
                                            </div><!-- col-4 -->
                                        </div>
                                    </div>
                                    <div class="col-12 p-0 leaveRequestButtons">

                                        <button type="submit"
                                                class="btn btn-primary mr-2 tx-uppercase btnLeaveStatus" status="accept">
                                            {{__('leaveManagement/requestLeave.acceptLeaveRequest')}}
                                        </button>

                                        <button type="submit"
                                                class="btn btn-danger tx-uppercase btnLeaveStatus" status="reject">
                                            {{__('leaveManagement/requestLeave.rejectLeaveRequest')}}
                                        </button>

                                    </div>
                                </form>
                                <hr>
                                <div class="col-12 p-0 mt-3">
                                    <div class="table-wrapper">
                                        <table id="requestUserLeaveDataTable" class="table display nowrap">
                                            <thead>
                                            <tr>
                                                <th>{{__('leaveManagement/requestLeave.requestLeaveLeaveDate')}}</th>
                                                <th>{{__('leaveManagement/requestLeave.requestLeaveNoDays')}}</th>
                                                <th>{{__('leaveManagement/requestLeave.requestLeaveReason')}}</th>
                                                <th>{{__('leaveManagement/requestLeave.requestLeaveStatus')}}</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>10 Jun 19 - 15 Jun 19</td>
                                                <td>5</td>
                                                <td>Reason for Leave</td>
                                                <td>Approved</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div><!-- table-wrapper -->
                                </div>
                            </div>
                        </div>
                    </div><!-- modal-dialog -->
                </div><!-- modal -->
            </div>
        </div>
    </div>
@endsection
