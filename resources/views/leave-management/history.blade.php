@extends('layout.main')
@section('content')
    <div class="br-mainpanel">
        <div class="br-pagebody">
            <div class="br-pagetitle"></div>
            <div class="br-section-wrapper">
                <div class="col-12">
                    <div class="row">
                        <div class="col-6 p-0">
                            <h3 class="mb-4 text-dark">{{__('leaveManagement/leaveLedger.headerTitle')}}</h3>
                        </div>
                    </div>
                </div>

                <div class="table-wrapper">
                    <table id="leaveLedgerDataTable" class="table display nowrap">
                        <thead>
                        <tr>
                            <th>{{__('leaveManagement/leaveLedger.leaveLedgerEmpCode')}}</th>
                            <th>{{__('leaveManagement/leaveLedger.leaveLedgerEmpName')}}</th>
                            <th>{{__('leaveManagement/leaveLedger.leaveLedgerDepartment')}}</th>
                            <th>{{__('leaveManagement/leaveLedger.leaveLedgerDesignation')}}</th>
                            <th>{{__('leaveManagement/leaveLedger.leaveLedgerCasualLeave')}}</th>
                            <th>{{__('leaveManagement/leaveLedger.leaveLedgerPrivilegeLeave')}}</th>
                            <th>{{__('leaveManagement/leaveLedger.leaveLedgerSickLeave')}}</th>
                            <th>{{__('leaveManagement/leaveLedger.leaveLedgerMaternityLeave')}}</th>
                            <th class="text-right">{{__('leaveManagement/leaveLedger.leaveLedgerActions')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {{-- <tr>
                            <td>0001</td>
                            <td>Test</td>
                            <td>IT</td>
                            <td>Developer</td>
                            <td>5</td>
                            <td>5</td>
                            <td>5</td>
                            <td>5</td>
                            <td class="text-right">
                                <div class="col-12 p-0">
                                    <button class="btn btn-primary"
                                            id="leaveLedgerDetails">{{__('leaveManagement/leaveLedger.leaveLedgerDetails')}}</button>
                                </div>
                            </td>
                        </tr> --}}
                        </tbody>
                    </table>
                </div><!-- table-wrapper -->

                <!-- BASIC MODAL -->
                <div id="leaveLedgerDetailsModal" class="modal fade">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content bd-0 tx-14">
                            <div class="modal-header pd-y-20 pd-x-25">
                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('leaveManagement/leaveLedger.leaveLedgerDetailsHeader')}}</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body pd-25">
                                <div class="table-wrapper">
                                    <table id="leaveLedgerDetailsDataTable" class="table display nowrap">
                                        <thead>
                                        <tr>
                                            <th>{{__('leaveManagement/leaveLedger.leaveLedgerLeaveType')}}</th>
                                            <th>{{__('leaveManagement/leaveLedger.leaveLedgerFromDate')}}</th>
                                            <th>{{__('leaveManagement/leaveLedger.leaveLedgerToDate')}}</th>
                                            <th>{{__('leaveManagement/leaveLedger.leaveLedgerNoLeaves')}}</th>
                                            <th>{{__('leaveManagement/leaveLedger.Added/Removed')}}</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        {{-- <tr>
                                            <td>0001</td>
                                            <td>Test</td>
                                            <td>IT</td>
                                            <td>Developer</td>
                                            <td>Added</td>

                                        </tr> --}}
                                        </tbody>
                                    </table>
                                </div><!-- table-wrapper -->

                            </div>
                        </div>
                    </div><!-- modal-dialog -->
                </div><!-- modal -->
            </div>
        </div>
    </div>
@endsection
