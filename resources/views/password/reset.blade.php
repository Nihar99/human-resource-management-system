@include('layout.header')
<div class="d-flex align-items-center justify-content-center bg-br-primary ht-100v">

    <div class="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-40 bg-white rounded shadow-base">
        <div class="signin-logo tx-center tx-28 tx-bold tx-inverse">
            <span class="tx-normal">
                {{__('login/reset.passwordReset')}}
            </span>
        </div>
        <br>
        <div class="form-group">
            <input type="email" class="form-control" placeholder="{{__('login/reset.email')}}">
        </div><!-- form-group -->
        <div class="form-group">
            <input type="password" class="form-control" placeholder="{{__('login/reset.newPassword')}}">
        </div><!-- form-group -->
        <div class="form-group">
            <input type="password" class="form-control" placeholder="{{__('login/reset.retypePassword')}}">
        </div><!-- form-group -->
        <button type="submit" class="btn btn-info btn-block">{{__('login/reset.buttonSubmit')}}</button>
    </div><!-- login-wrapper -->
</div><!-- d-flex -->
@include('layout.footer')
