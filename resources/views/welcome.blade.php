@extends('layout.main')
@section('content')
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
        <div class="br-pagetitle ml-5">
            <i class="icon icon ion-ios-book-outline"></i>
            <div>
                <h4>Blank Page (Icon Menu)</h4>
                <p class="mg-b-0">Introducing Bracket Plus admin template, the most handsome admin template of all time.
                </p>
            </div>
        </div><!-- d-flex -->
        <div class="br-pagebody">
            <!-- start you own content here -->
        </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->
@endsection
