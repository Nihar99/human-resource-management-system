@extends('layout.main')
@section('content')
    <div class="br-mainpanel">
        <div class="br-pagebody">
            <div class="br-pagetitle"></div>
            <div class="br-section-wrapper">
                <div class="col-12">
                    <div class="row">
                        <div class="col-6 p-0">
                            <h3 class="m-0 text-dark">{{__('monthlyAttendance.headerTitle')}}</h3>
                        </div>

                        <div class="col-6 p-0 text-right">
                            <button class="btn btn-primary mr-3" id="addMonthlyAttendanceDetails">
                                <i class="fa fa-plus mr-2"></i>{{__('monthlyAttendance.addMonthlyAttendanceDetails')}}
                            </button>

                            <button class="btn btn-primary" id="exportEmployee">
                                <i class="fa fa-upload mr-2"></i>{{__('monthlyAttendance.uploadMonthlyAttendanceExcel')}}
                            </button>
                        </div>
                    </div>
                </div>

                <div class="col-12 p-0 my-5">
                    <div class="row">
                        <div class="col">
                            <div class="bg-success p-2 rounded-5 text-white text-center cursor-pointer status">
                                <span>P : Present</span>
                            </div>
                            <div class="bg-danger p-2 rounded-5 text-white text-center cursor-pointer status">
                                <span>A : Absent</span>
                            </div>
                        </div>

                        <div class="col">
                            <div class="bg-primary p-2 rounded-5 text-white text-center cursor-pointer status">
                                <span>L : Leave</span>
                            </div>
                            <div class="bg-purple p-2 rounded-5 text-white text-center cursor-pointer status">
                                <span>LE : Late Entry</span>
                            </div>
                        </div>

                        <div class="col">
                            <div class="bg-purple p-2 rounded-5 text-white text-center cursor-pointer status">
                                <span>EE : Early Exit</span>
                            </div>
                            <div class="bg-dark p-2 rounded-5 text-white text-center cursor-pointer status">
                                <span>HD : Half Day</span>
                            </div>
                        </div>

                        <div class="col">
                            <div class="bg-success p-2 rounded-5 text-white text-center cursor-pointer status">
                                <span>OD : Outdoor Duty</span>
                            </div>
                            <div class="bg-success p-2 rounded-5 text-white text-center cursor-pointer status">
                                <span>PL : Paid Leave</span>
                            </div>
                        </div>

                        <div class="col">
                            <div class="bg-success p-2 rounded-5 text-white text-center cursor-pointer status">
                                <span>CO : Comp Off</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 p-0">
                    <table id="monthlyAttendanceTable" class="table display nowrap">
                        <thead>
                        <tr>
                            <th>Ecode</th>
                            <th>Employee <br> Name</th>
                            @for($i = 1; $i <=  date('t'); $i++)
                                <th class="text-center">{{date('D',strtotime(date('Y') . "-" . date('m') . "-" . str_pad($i, 2, '0', STR_PAD_LEFT))) }} <br> {{$i}}</th>
                            @endfor
                        </tr>
                        </thead>
                        <tbody>
                        {{-- <tr>
                            <td>E0001</td>
                            <td>Test</td>
                            @for($i=1; $i<32; $i++)
                                <td class="text-center">
                                    <button class="btn presentButtonColor py-1 px-2 rounded-5 cursor-pointer">P
                                    </button>
                                </td>
                            @endfor
                        </tr> --}}
                        </tbody>
                    </table>
                </div>

                <!-- BASIC MODAL -->
                <div id="addMonthlyAttendanceModal" class="modal fade">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content bd-0 tx-14">
                            <div class="modal-header pd-y-20 pd-x-25">
                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">
                                    {{__('monthlyAttendance.addMonthlyAttendanceDetails')}}</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form role="form" id="addTimePolicyForm">
                                <div class="modal-body pd-25">
                                    <div class="col-12 p-0">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="form-group mb-4">
                                                    <label for="add_monthly_attendance_photo"
                                                           class="form-control-label">{{__('monthlyAttendance.monthlyAttendancePhoto')}}
                                                        <span
                                                            class="tx-danger">*</span></label>
                                                    <div class="container cursor-pointer">
                                                        <img src="{{url('img/img1.jpg')}}" alt=""
                                                             class="img-fluid rounded-circle ht-150 chooseImage">
                                                        <div class="middleText text-center position-absolute op-0">
                                                            <div
                                                                class="tx-white tx-bold font-size-18">{{__('monthlyAttendance.monthlyAttendancePhotoTitle')}}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="add_monthly_attendance_ecode"
                                                           class="form-control-label">{{__('monthlyAttendance.monthlyAttendanceEcode')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <input type="text" class="form-control"
                                                           name="add_monthly_attendance_ecode"
                                                           id="add_monthly_attendance_ecode" readonly
                                                           placeholder="{{__('monthlyAttendance.enterMonthlyAttendanceEcode')}}"
                                                           required>
                                                </div><!-- form-group -->
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="add_monthly_attendance_emp_name"
                                                           class="form-control-label">{{__('monthlyAttendance.monthlyAttendanceEmpName')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <input type="text" class="form-control"
                                                           name="add_monthly_attendance_emp_name"
                                                           id="add_monthly_attendance_emp_name" readonly
                                                           placeholder="{{__('monthlyAttendance.enterMonthlyAttendanceEmpName')}}"
                                                           required>
                                                </div><!-- form-group -->
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="add_monthly_attendance_location"
                                                           class="form-control-label">{{__('monthlyAttendance.monthlyAttendanceLocation')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <input type="text" class="form-control"
                                                           name="add_monthly_attendance_location"
                                                           id="add_monthly_attendance_location"
                                                           placeholder="{{__('monthlyAttendance.enterMonthlyAttendanceLocation')}}"
                                                           required>
                                                </div><!-- form-group -->
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="add_monthly_attendance_status"
                                                           class="form-control-label">{{__('monthlyAttendance.monthlyAttendanceStatus')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <select class="form-control select2"
                                                            name="add_monthly_attendance_status"
                                                            id="add_monthly_attendance_status" required>
                                                        <option selected disabled>
                                                            {{__('monthlyAttendance.selectMonthlyAttendanceStatus')}}
                                                        </option>
                                                        <option value="Present">Present</option>
                                                        <option value="Absent">Absent</option>
                                                        <option value="Leave">Leave</option>
                                                        <option value="Late Entry">Late Entry</option>
                                                        <option value="Early Exit">Early Exit</option>
                                                        <option value="Half Day">Half Day</option>
                                                        <option value="Outdoor Duty">Outdoor Duty</option>
                                                        <option value="Paid Holiday">Paid Holiday</option>
                                                        <option value="Comp Off">Comp Off</option>
                                                    </select>
                                                </div><!-- form-group -->
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label
                                                        for="add_monthly_attendance_timein">{{__('monthlyAttendance.monthlyAttendanceTimeIn')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>
                                                        </div>
                                                        <input type="text" class="form-control tpBasic"
                                                               id="add_monthly_attendance_timein"
                                                               name="add_monthly_attendance_timein"
                                                               placeholder="{{__('monthlyAttendance.setMonthlyAttendanceTimeIn')}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label
                                                        for="add_monthly_attendance_timeout">{{__('monthlyAttendance.monthlyAttendanceTimeOut')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>
                                                        </div>
                                                        <input type="text" class="form-control tpBasic"
                                                               id="add_monthly_attendance_timeout"
                                                               name="add_monthly_attendance_timeout"
                                                               placeholder="{{__('monthlyAttendance.setMonthlyAttendanceTimeOut')}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label
                                                        for="add_monthly_attendance_breakin">{{__('monthlyAttendance.monthlyAttendanceBreakIn')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>
                                                        </div>
                                                        <input type="text" class="form-control tpBasic"
                                                               id="add_monthly_attendance_breakin"
                                                               name="add_monthly_attendance_breakin"
                                                               placeholder="{{__('monthlyAttendance.setMonthlyAttendanceBreakIn')}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label
                                                        for="add_monthly_attendance_breakout">{{__('monthlyAttendance.monthlyAttendanceBreakOut')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>
                                                        </div>
                                                        <input type="text" class="form-control tpBasic"
                                                               id="add_monthly_attendance_breakout"
                                                               name="add_monthly_attendance_breakout"
                                                               placeholder="{{__('monthlyAttendance.setMonthlyAttendanceBreakOut')}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 p-0 text-right">
                                        <button type="submit" class="btn btn-primary tx-uppercase">
                                            {{__('monthlyAttendance.monthlyAttendanceButtonSubmit')}}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- modal-dialog -->
                </div><!-- modal -->
            </div>
        </div>
    </div>
@endsection
