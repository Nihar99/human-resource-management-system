@extends('layout.main')
@section('content')
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
        <div class="br-pagebody">
            <div class="br-pagetitle"></div>
            <div class="br-section-wrapper">
                <div class="col-12 p-0">
                    <h3 class="mb-4 text-dark">{{__('employee/details.headerTitle')}}</h3>
                </div>
                <form role="form" id="addEmployeeForm" enctype="multipart/form-data">
                    <div id="wizard2">
                        <h3 class="d-none">{{__('employee/details.personalDetails')}}</h3>
                        <section>
                            <div class="col-12 p-0">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="first_name"
                                                   class="form-control-label">{{__('employee/details.fname')}}<span
                                                    class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" id="first_name" name="first_name"
                                                   placeholder="{{__('employee/details.enterFirstName')}}"
                                                   required>
                                        </div><!-- form-group -->
                                    </div>

                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="middle_name"
                                                   class="form-control-label">{{__('employee/details.mname')}}</label>
                                            <input type="text" class="form-control" id="middle_name" name="middle_name"
                                                   placeholder="{{__('employee/details.enterMiddleName')}}" required>
                                        </div><!-- form-group -->
                                    </div>

                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="last_name"
                                                   class="form-control-label">{{__('employee/details.lname')}}<span
                                                    class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" id="last_name" name="last_name"
                                                   placeholder="{{__('employee/details.enterLastName')}}" required>
                                        </div><!-- form-group -->
                                    </div>

                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="gender"
                                                   class="form-control-label">{{__('employee/details.gender')}}
                                                <span class="tx-danger">*</span></label>
                                            <select class="form-control select2" name="gender" id="gender" required>
                                                <option selected
                                                        disabled>{{__('employee/details.selectGender')}}</option>
                                                <option
                                                    value="{{__('employee/details.male')}}">{{__('employee/details.male')}}</option>
                                                <option
                                                    value="{{__('employee/details.female')}}">{{__('employee/details.female')}}</option>
                                            </select>
                                        </div><!-- form-group -->
                                    </div>

                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="date_of_birth"
                                                   class="form-control-label">{{__('employee/details.dob')}}
                                                <span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control fc-datepicker" name="date_of_birth"
                                                   id="date_of_birth"
                                                   placeholder="{{__('employee/details.selectDob')}}" required>
                                        </div><!-- form-group -->
                                    </div>

                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="nationality"
                                                   class="form-control-label">{{__('employee/details.nationality')}}
                                                <span class="tx-danger">*</span></label>
                                            <select class="form-control select2-show-search" name="nationality"
                                                    id="nationality"
                                                    required>
                                                <option selected
                                                        disabled>{{__('employee/details.selectNationality')}}</option>
                                                @foreach($countries as $country)
                                                    <option value="{{$country->id}}">{{$country->name}}</option>
                                                @endforeach
                                            </select>
                                        </div><!-- form-group -->
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="present_address"
                                                   class="form-control-label">{{__('employee/details.presentAddress')}}
                                                <span
                                                    class="tx-danger">*</span></label>
                                            <textarea class="form-control" rows="5" name="present_address"
                                                      id="present_address"
                                                      placeholder="{{__('employee/details.enterPresentAddress')}}"
                                                      required></textarea>
                                        </div><!-- form-group -->
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="permanent_address"
                                                   class="form-control-label">{{__('employee/details.permanentAddress')}}
                                                <span class="tx-danger">*</span></label>
                                            <textarea class="form-control" rows="5" name="permanent_address"
                                                      id="permanent_address"
                                                      placeholder="{{__('employee/details.enterPermanentAddress')}}"
                                                      required></textarea>
                                        </div><!-- form-group -->
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="present_address_tel_no"
                                                   class="form-control-label">{{__('employee/details.telephonePresent')}}
                                               </label>
                                            <input type="number" class="form-control" id="present_address_tel_no"
                                                   name="present_address_tel_no"
                                                   placeholder="{{__('employee/details.enterTelephonePresent')}}"
                                                   required>
                                        </div><!-- form-group -->
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="permanent_address_tel_no"
                                                   class="form-control-label">{{__('employee/details.telephonePermanent')}}
                                               </label>
                                            <input type="number" class="form-control" id="permanent_address_tel_no"
                                                   name="permanent_address_tel_no"
                                                   placeholder="{{__('employee/details.enterTelephonePermanent')}}"
                                                   required>
                                        </div><!-- form-group -->
                                    </div>

                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="place_state_of_birth"
                                                   class="form-control-label">{{__('employee/details.psob')}}
                                                <span
                                                    class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" id="place_state_of_birth"
                                                   name="place_state_of_birth"
                                                   placeholder="{{__('employee/details.enterPsob')}}"
                                                   required>
                                        </div><!-- form-group -->
                                    </div>

                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="religion"
                                                   class="form-control-label">{{__('employee/details.religion')}}<span
                                                    class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" id="religion" name="religion"
                                                   placeholder="{{__('employee/details.enterReligion')}}" required>
                                        </div><!-- form-group -->
                                    </div>

                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="marital_status"
                                                   class="form-control-label">{{__('employee/details.maritalStatus')}}
                                                <span
                                                    class="tx-danger">*</span></label>
                                            <select class="form-control select2" name="marital_status"
                                                    id="marital_status"
                                                    required>
                                                <option selected
                                                        disabled>{{__('employee/details.selectMaritalStatus')}}</option>
                                                <option
                                                    value="{{__('employee/details.married')}}">{{__('employee/details.married')}}</option>
                                                <option
                                                    value="{{__('employee/details.unmarried')}}">{{__('employee/details.unmarried')}}</option>
                                                <option
                                                    value="{{__('employee/details.divorse')}}">{{__('employee/details.divorse')}}</option>
                                            </select>
                                        </div><!-- form-group -->
                                    </div>

                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="mother_tongue"
                                                   class="form-control-label">{{__('employee/details.motherTongue')}}
                                                <span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" id="mother_tongue"
                                                   name="mother_tongue"
                                                   placeholder="{{__('employee/details.enterMotherToungue')}}" required>
                                        </div><!-- form-group -->
                                    </div>

                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="personal_email"
                                                   class="form-control-label">{{__('employee/details.personalEmail')}}
                                               </label>
                                            <input type="email" class="form-control" id="personal_email"
                                                   name="personal_email"
                                                   placeholder="{{__('employee/details.enterPersonalEmail')}}" required>
                                        </div><!-- form-group -->
                                    </div>

                                    <div class="col-10">
                                        <div class="form-group">
                                            <label for="hobbies_and_interests"
                                                   class="form-control-label">{{__('employee/details.hobbiesInterest')}}
                                               </label>
                                            <textarea class="form-control" rows="5" name="hobbies_and_interests"
                                                      id="hobbies_and_interests"
                                                      placeholder="{{__('employee/details.enterHobbiesInterest')}}"
                                                      required></textarea>
                                        </div><!-- form-group -->
                                    </div>

                                    <div class="col-2">
                                        <div class="form-group">
                                            <label for="employee_image"
                                                   class="form-control-label">{{__('employee/details.employeeImage')}}
                                                <span
                                                    class="tx-danger">*</span></label>
                                            <div class="employee_image container cursor-pointer">

                                                <img src="{{url('img/img1.jpg')}}" alt="" id="employee_image_preview"
                                                     class="img-fluid rounded-circle ht-150 w-100 chooseImage">
                                                <div class=" middle text-center position-absolute op-0">
                                                    <div
                                                        class=" tx-white tx-bold font-size-18">{{__('employee/details.uploadEmployeeImage')}}</div>
                                                </div>
                                            </div>
                                            <input class="employee_image_select d-none" type="file" name="employee_image" id="employee_image">
                                            <div class="form-group">
                                                <p class="red error"></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <h3 class="d-none">{{__('employee/details.professionalDetails')}}</h3>

                        <section>
                            <div class="col-12 p-0">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="ecode"
                                                   class="form-control-label">{{__('employee/details.empCode')}}<span
                                                    class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" id="ecode" name="ecode"
                                                   placeholder="{{__('employee/details.enterEmpCode')}}" required>
                                            <div class="form-group">
                                                <p class="red error"></p>
                                            </div>
                                        </div><!-- form-group -->
                                    </div>

                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="professional_email"
                                                   class="form-control-label">{{__('employee/details.companyEmail')}}
                                                </label>
                                            <input type="email" class="form-control" id="professional_email"
                                                   name="professional_email"
                                                   placeholder="{{__('employee/details.enterCompanyEmail')}}" required>
                                        </div><!-- form-group -->
                                    </div>

                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="department_id"
                                                   class="form-control-label">{{__('employee/details.department')}}
                                                <span class="tx-danger">*</span></label>
                                            <select class="form-control select2-show-search" name="department_id"
                                                    id="department_id" required>
                                                <option selected
                                                        disabled>{{__('employee/details.selectDepartment')}}</option>
                                                @foreach($departments as $department)
                                                    <option value="{{$department->id}}">{{$department->name}}</option>
                                                @endforeach
                                            </select>
                                        </div><!-- form-group -->
                                    </div>

                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="designation_id"
                                                   class="form-control-label">{{__('employee/details.designation')}}
                                                <span class="tx-danger">*</span></label>
                                            <select class="form-control select2-show-search" name="designation_id"
                                                    id="designation_id" required>
                                                <option selected
                                                        disabled>{{__('employee/details.selectDesignation')}}</option>
                                                @foreach($designations as $designation)
                                                    <option value="{{$designation->id}}">{{$designation->name}}</option>
                                                @endforeach
                                            </select>
                                        </div><!-- form-group -->
                                    </div>

                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="probation_from"
                                                   class="form-control-label">{{__('employee/details.probationDate')}}
                                                <span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control fc-datepicker" name="probation_from"
                                                   id="probation_from"
                                                   placeholder="{{__('employee/details.selectProbationDate')}}"
                                                   required>
                                        </div><!-- form-group -->
                                    </div>

                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="confirmation_on"
                                                   class="form-control-label">{{__('employee/details.doc')}}</label>
                                            <input type="text" class="form-control fc-datepicker" name="confirmation_on"
                                                   id="confirmation_on"
                                                   placeholder="{{__('employee/details.selectDoc')}}" required>
                                        </div><!-- form-group -->
                                    </div>

                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="enable_ess"
                                                   class="form-control-label">{{__('employee/details.enableESS')}}<span
                                                    class="tx-danger">*</span></label>
                                            <select class="form-control select2 add_ess" name="enable_ess" id="enable_ess"
                                                    required>
                                                <option selected
                                                        disabled>{{__('employee/details.enableESS')}}</option>
                                                <option
                                                    value="{{__('employee/details.yes')}}">{{__('employee/details.yes')}}</option>
                                                <option
                                                    value="{{__('employee/details.no')}}">{{__('employee/details.no')}}</option>
                                            </select>
                                        </div><!-- form-group -->
                                    </div>

                                    <div class="col-4" id="enter_password">
                                        <div class="form-group">
                                            <label for="ess_password"
                                                   class="form-control-label">{{__('employee/details.enterPassword')}}
                                                <span
                                                    class="tx-danger">*</span></label>
                                            <input type="password" class="form-control" name="ess_password"
                                                   id="ess_password"
                                                   placeholder="{{__('employee/details.enterPassword')}}" required>
                                        </div><!-- form-group -->
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </form>
            </div>
        </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->
@endsection
