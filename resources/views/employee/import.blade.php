@extends('layout.main')
@section('content')
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
        <div class="br-pagebody">
            <div class="br-pagetitle"></div>
            <div class="br-section-wrapper">
                <div class="col-12 p-0">
                    <h3 class="mb-4 text-dark">{{__('employee/import.headerTitle')}}</h3>
                </div>
                <a href="{{ url('downloadSampleExcel') }}"><button class="btn btn-primary"><i class="fa fa-download"></i> Download Sample Excel</button></a>
                <form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;" action="{{ url('importEmployeeExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                    @csrf
                    @if (\Session::has('errors'))
                        <div class="alert alert-danger">
                            <p>{!! \Session::get('errors') !!}</p>
                        </div>
                    @endif

                    @if (Session::has('success'))
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <p>{{ Session::get('success') }}</p>
                        </div>
                    @endif

                    <input type="file" name="import_file" />
                    <button class="btn btn-primary">Import File</button>
                </form>
            </div>
        </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->
@endsection
