@extends('layout.main')
@section('content')
    <div class="br-mainpanel">
        <div class="br-pagebody">
            <div class="br-pagetitle"></div>
            <div class="br-section-wrapper">
                <div class="col-12 p-0">
                    <h3 class="mb-4 text-dark">{{__('faceTrain.headerTitle')}}</h3>
                </div>

                <div class="table-wrapper">
                    <table id="faceTrainTable" class="table display nowrap">
                        <thead>
                        <tr>
                            <th>{{__('faceTrain.faceTrainEcode')}}</th>
                            <th>{{__('faceTrain.faceTrainEmployeeName')}}</th>
                            <th>{{__('faceTrain.faceTrainTrainedFace')}}</th>
                            <th class="text-right">{{__('faceTrain.faceTrainActions')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>0001</td>
                            <td>Test Tester Testing</td>
                            <td><img src="{{url('img/img1.jpg')}}"
                                     class="img-fluid rounded-circle ht-10p cursor-pointer"
                                     id="viewTrainedFaceImage" alt=""></td>
                            <td class="text-right">
                                <div class="col-12 p-0">
                                    <i class="fa fa-undo cursor-pointer mr-2 font-size-18 text-primary"
                                       id="reTrainFace"
                                       title="{{__('faceTrain.reTrainFace')}}"></i>
                                    <i class="fa fa-chain-broken cursor-pointer font-size-18 text-danger"
                                       id="unTrainFace"
                                       title="{{__('faceTrain.unTrainFace')}}"></i>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div><!-- table-wrapper -->

                <!-- BASIC MODAL -->
                <div id="viewTrainedFaceImageModal" class="modal fade">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content bd-0 tx-14">
                            <div class="modal-header pd-y-20 pd-x-25">
                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('faceTrain.faceTrainTrainedFace')}}</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body pd-25">
                                <img src="{{url('img/img1.jpg')}}" class="img-fluid h-2" alt="">
                            </div>
                        </div>
                    </div><!-- modal-dialog -->
                </div><!-- modal -->

                <!-- BASIC MODAL -->
                <div id="reTrainFaceModal" class="modal fade">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content bd-0 tx-14">
                            <div class="modal-header pd-y-20 pd-x-25">
                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('faceTrain.reTrainFace')}}</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body pd-25">
                                <div class="col-12 p-0">
                                    <div class="row">
                                        <div class="col-4">
                                            <div class="form-group mb-4">
                                                <label for="add_monthly_attendance_photo"
                                                       class="form-control-label">{{__('faceTrain.faceTrainEmployeePhoto')}}
                                                    <span
                                                        class="tx-danger">*</span></label>
                                                <div class="container cursor-pointer">
                                                    <img src="{{url('img/img1.jpg')}}" alt=""
                                                         class="img-fluid rounded-circle ht-150 chooseImage">
                                                    <div class="middleText text-center position-absolute op-0">
                                                        <div
                                                            class="tx-white tx-bold font-size-18">{{__('faceTrain.faceTrainEmployeePhotoTitle')}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="ess_ecode"
                                                       class="form-control-label">{{__('faceTrain.faceTrainEcode')}}
                                                    <span class="tx-danger">*</span></label>
                                                <input type="text" class="form-control" name="ess_ecode"
                                                       id="ess_ecode" readonly
                                                       placeholder="{{__('faceTrain.enterFaceTrainEcode')}}"
                                                       required>
                                            </div><!-- form-group -->
                                        </div>

                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="ess_emp_name"
                                                       class="form-control-label">{{__('faceTrain.faceTrainEmployeeName')}}
                                                    <span class="tx-danger">*</span></label>
                                                <input type="text" class="form-control" name="ess_emp_name"
                                                       id="ess_emp_name" readonly
                                                       placeholder="{{__('faceTrain.enterFaceTrainEmployeeName')}}"
                                                       required>
                                            </div><!-- form-group -->
                                        </div>

                                        <div class="col-12 text-right">
                                            <button type="submit"
                                                    class="btn btn-primary tx-uppercase">
                                                {{__('faceTrain.faceTrainButtonRetrain')}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- modal-dialog -->
                </div><!-- modal -->
            </div>
        </div>
    </div>
@endsection
