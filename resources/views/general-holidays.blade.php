@extends('layout.main')
@section('content')
    <div class="br-mainpanel">
        <div class="br-pagebody">
            <div class="br-pagetitle"></div>
            <div class="br-section-wrapper">
                <div class="col-12">
                    <div class="row">
                        <div class="col-6 p-0">
                            <h3 class="m-0 text-dark">{{__('generalHolidays.headerTitle')}}</h3>
                        </div>

                        <div class="col-6 p-0 text-right">
                            <button class="btn btn-primary mr-3 mb-4"
                                    id="addGeneralHolidays">
                                <i class="fa fa-plus mr-2"></i>{{__('generalHolidays.addGeneralHolidays')}}
                            </button>

                            <button class="btn btn-primary mb-4" id="importEmployee">
                                <i class="fa fa-download mr-2"></i>{{__('employees.empImportExcel')}}
                            </button>
                        </div>
                    </div>
                </div>

                <div class="table-wrapper">
                    <table id="generalHolidaysTable" class="table display nowrap">
                        <thead>
                        <tr>
                            <th>{{__('generalHolidays.generalHolidaysName')}}</th>
                            <th>{{__('generalHolidays.generalHolidaysDate')}}</th>
                            <th class="text-right">{{__('generalHolidays.generalHolidaysActions')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Test</td>
                            <td>20-05-2019</td>
                            <td class="text-right">
                                <div class="col-12 p-0">
                                    <i class="fa fa-edit cursor-pointer mr-2 font-size-18 text-primary"
                                       id="editGeneralHolidays"
                                       title="{{__('generalHolidays.editGeneralHolidays')}}"></i>
                                    <i class="fa fa-trash cursor-pointer font-size-18 text-danger"
                                       id="deleteGeneralHolidays"
                                       title="{{__('generalHolidays.deleteGeneralHolidays')}}"></i>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div><!-- table-wrapper -->

                <!-- BASIC MODAL -->
                <div id="addGeneralHolidaysModal" class="modal fade">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content bd-0 tx-14">
                            <div class="modal-header pd-y-20 pd-x-25">
                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('generalHolidays.addGeneralHolidays')}}</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form role="form" id="addGeneralHolidaysForm">
                                <div class="modal-body pd-25">
                                    <div class="col-12 p-0">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="name"
                                                           class="form-control-label">{{__('generalHolidays.generalHolidaysName')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <input type="text" class="form-control"
                                                           id="name"
                                                           name="name"
                                                           placeholder="{{__('generalHolidays.enterGeneralHolidaysName')}}"
                                                           required>
                                                    <div class="form-group">
                                                        <p class="red error"></p>
                                                    </div>
                                                </div><!-- form-group -->
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="holiday_date"
                                                           class="form-control-label">{{__('generalHolidays.generalHolidaysDate')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <input type="text" class="form-control fc-datepicker"
                                                           name="holiday_date"
                                                           id="holiday_date"
                                                           placeholder="{{__('generalHolidays.selectGeneralHolidaysDate')}}"
                                                           required>
                                                </div><!-- form-group -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 p-0 text-right">
                                        <button type="submit"
                                                class="btn btn-primary tx-uppercase">
                                            {{__('generalHolidays.generalHolidaysButtonSubmit')}}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- modal-dialog -->
                </div><!-- modal -->

                <!-- BASIC MODAL -->
                <div id="editGeneralHolidaysModal" class="modal fade">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content bd-0 tx-14">
                            <div class="modal-header pd-y-20 pd-x-25">
                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('generalHolidays.editGeneralHolidaysHeader')}}</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form role="form" id="editGeneralHolidaysForm">
                                <div class="modal-body pd-25">
                                    <div class="col-12 p-0">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="name"
                                                           class="form-control-label">{{__('generalHolidays.generalHolidaysName')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <input type="text" class="form-control"
                                                           id="edit_name"
                                                           name="name"
                                                           placeholder="{{__('generalHolidays.enterGeneralHolidaysName')}}"
                                                           required>
                                                    <div class="form-group">
                                                        <p class="red error"></p>
                                                    </div>
                                                </div><!-- form-group -->
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="edit_holiday_date"
                                                           class="form-control-label">{{__('generalHolidays.generalHolidaysDate')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <input type="text" class="form-control fc-datepicker"
                                                           name="edit_holiday_date"
                                                           id="edit_holiday_date"
                                                           placeholder="{{__('generalHolidays.selectGeneralHolidaysDate')}}"
                                                           required>
                                                </div><!-- form-group -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 p-0 text-right">
                                        <button type="submit"
                                                class="btn btn-primary tx-uppercase">
                                            {{__('generalHolidays.generalHolidaysButtonUpdate')}}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- modal-dialog -->
                </div><!-- modal -->
            </div>
        </div>
    </div>
@endsection
