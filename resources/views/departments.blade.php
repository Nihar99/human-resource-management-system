@extends('layout.main')
@section('content')
<!-- ########## START: MAIN PANEL ########## -->
<div class="br-mainpanel">
    <div class="br-pagebody">
        <div class="br-pagetitle"></div>
        <div class="br-section-wrapper">
            <div class="col-12">
                <div class="row">
                    <div class="col-6 p-0">
                        <h3 class="m-0 text-dark">{{__('departments.headerTitle')}}</h3>
                    </div>

                    <div class="col-6 p-0 text-right">
                        <button class="btn btn-primary mb-4" id="addDepartment">
                            <i class="fa fa-plus mr-2"></i>{{__('departments.deptAdd')}}
                        </button>
                    </div>
                </div>
            </div>

            <div class="table-wrapper">
                <table id="departmentDataTable" class="table display nowrap">
                    <thead>
                        <tr>
                            <th>{{__('departments.deptName')}}</th>
                            <th class="text-right">{{__('departments.deptActions')}}</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div><!-- table-wrapper -->
        </div>
    </div><!-- br-pagebody -->
</div><!-- br-mainpanel -->

<!-- BASIC MODAL -->
<div id="addDepartmentModal" class="modal fade">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content bd-0 tx-14">
            <div class="modal-header pd-y-20 pd-x-25">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('departments.deptAdd')}}</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form role="form" id="addDepartmentForm">
                <div class="modal-body pd-25">
                    <div class="form-group">
                        <label for="name">{{__('departments.deptName')}} <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control" id="newDepartmentName" name="name"
                            placeholder="{{__('departments.deptPlaceholderName')}}" required>
                    </div>
                    <div class="form-group">
                        <p class="red error"></p>
                    </div>
                    <div class="col-12 p-0 text-right">
                        <button type="submit" class="btn btn-primary tx-uppercase">
                            {{__('departments.deptButtonSubmit')}}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div><!-- modal-dialog -->
</div><!-- modal -->

<div id="editDepartmentModal" class="modal fade">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content bd-0 tx-14">
            <div class="modal-header pd-y-20 pd-x-25">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('departments.deptEditDepartment')}}</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form role="form" id="editDepartmentForm" >
                <div class="modal-body pd-25">
                    <div class="form-group">
                        <label for="name">{{__('departments.deptName')}} <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control" id="editDepartmentName" name="name"
                            placeholder="{{__('departments.deptPlaceholderName')}}" required>
                    </div>
                    <div class="form-group">
                        <p class="red error"></p>
                    </div>
                    <div class="col-12 p-0 text-right">
                        <button class="btn btn-primary tx-uppercase">
                            {{__('departments.deptButtonSubmit')}}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div><!-- modal-dialog -->
</div><!-- modal -->
@endsection
