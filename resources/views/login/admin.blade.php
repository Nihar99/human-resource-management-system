@include('layout.header')
<form class="d-flex align-items-center justify-content-center bg-br-primary ht-100v" id="LoginAdminForm">

    <div class="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-40 bg-white rounded shadow-base">
        <div class="signin-logo tx-center tx-28 tx-bold tx-inverse">
            <span class="tx-normal">
                {{__('login/auth.admin')}} <span class="tx-info">{{__('login/auth.login')}}</span>
            </span>
        </div>
        <br>
        <div class="form-group">
            <input type="email" class="form-control" placeholder="{{__('login/auth.email')}}" required id="emailId">
        </div><!-- form-group -->
        <div class="form-group">
            <input type="password" class="form-control" placeholder="{{__('login/auth.password')}}" required
                   id="password">
            <a href="{{url('password/forgot')}}" class="tx-info tx-12 d-block mg-t-10">{{__('login/auth.forgotPassword')}}</a>
        </div><!-- form-group -->
        <button type="submit" class="btn btn-info btn-block">{{__('login/auth.signin')}}</button>
    </div><!-- login-wrapper -->
</form><!-- d-flex -->
@include('layout.footer')
