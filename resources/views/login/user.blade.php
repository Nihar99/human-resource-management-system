@include('layout.header')
<div class="d-flex align-items-center justify-content-center bg-br-primary ht-100v">

    <form class="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-40 bg-white rounded shadow-base" id="loginForm">
        <div class="signin-logo tx-center tx-28 tx-bold tx-inverse">
            <span class="tx-normal">
                {{__('login/auth.employee')}} <span class="tx-info">{{__('login/auth.login')}}</span>
            </span>
        </div>
        <br>
        <div class="form-group">
            <input type="text" class="form-control" placeholder="{{__('login/auth.company')}}" name="companyid"
                required>
        </div><!-- form-group -->
        <div class="form-group">
            <input type="text" class="form-control" placeholder="{{__('login/auth.ecode')}}" name="ecode" required>
        </div><!-- form-group -->
        <p class="error red"></p>
        <div class="form-group">
            <input type="password" class="form-control" placeholder="{{__('login/auth.password')}}" name="password"
                required>
            <a href="javascript:void(0);" id="forgotPassword"
                class="tx-info tx-12 d-block mg-t-10">{{__('login/auth.forgotPassword')}}</a>
        </div><!-- form-group -->
        <button type="submit" class="btn btn-info btn-block">{{__('login/auth.signin')}}</button>
    </form><!-- login-wrapper -->
</div><!-- d-flex -->
@include('layout.footer')