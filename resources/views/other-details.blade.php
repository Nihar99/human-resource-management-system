@extends('layout.main')
@section('content')
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
        <div class="br-pagebody">
            <div class="br-pagetitle"></div>
            <div class="br-section-wrapper">
                <div class="col-12 p-0">
                    <h3 class="mb-4 text-dark">{{__('otherDetails.headerTitle')}}</h3>
                </div>

                <div id="wizard2">
                    @include('other-details.family-details')

                    @include('other-details.emergency-contact')

                    @include('other-details.languages-known')

                    @include('other-details.educational-profile')

                    @include('other-details.scholarship')

                    @include('other-details.exposure-computer')

                    @include('other-details.skills-experience')

                    @include('other-details.work-experience')

                    @include('other-details.referred-by')
                </div>
            </div>
        </div>
    </div>
@endsection
