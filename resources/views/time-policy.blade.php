@extends('layout.main')
@section('content')
    <div class="br-mainpanel">
        <div class="br-pagebody">
            <div class="br-pagetitle"></div>
            <div class="br-section-wrapper">
                <div class="col-12">
                    <div class="row">
                        <div class="col-6 p-0">
                            <h3 class="m-0 text-dark">{{__('timePolicy.headerTitle')}}</h3>
                        </div>

                        <div class="col-6 p-0 text-right">
                            <button class="btn btn-primary mb-4"
                                    id="addTimePolicy">
                                <i class="fa fa-plus mr-2"></i>{{__('timePolicy.addTimePolicy')}}
                            </button>
                        </div>
                    </div>
                </div>

                <div class="table-wrapper">
                    <table id="timePolicyTable" class="table display nowrap">
                        <thead>
                        <tr>
                            <th>{{__('timePolicy.timePolicyDay')}}</th>
                            <th>{{__('timePolicy.timePolicyWeekOff')}}</th>
                            <th>{{__('timePolicy.timePolicyTimeIn')}}</th>
                            <th>{{__('timePolicy.timePolicyTimOut')}}</th>
                            <th>{{__('timePolicy.timePolicyBreakIn')}}</th>
                            <th>{{__('timePolicy.timePolicyBreakOut')}}</th>
                            <th class="text-right">{{__('timePolicy.timePolicyActions')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {{--<tr>
                            <td>Monday</td>
                            <td>Yes</td>
                            <td>9.00 AM</td>
                            <td>6.00 PM</td>
                            <td>12.30 PM</td>
                            <td>1.15 PM</td>
                            <td class="text-right">
                                <div class="col-12 p-0">
                                    <i class="fa fa-eye cursor-pointer mr-2 font-size-18 text-info"
                                       id="viewTimePolicy"
                                       title="{{__('timePolicy.viewTimePolicy')}}"></i>
                                    <i class="fa fa-edit cursor-pointer mr-2 font-size-18 text-primary"
                                       id="editTimePolicy"
                                       title="{{__('timePolicy.editTimePolicy')}}"></i>
                                    <i class="fa fa-trash cursor-pointer font-size-18 text-danger"
                                       id="deleteTimePolicy"
                                       title="{{__('timePolicy.deleteTimePolicy')}}"></i>
                                </div>
                            </td>
                        </tr>--}}
                        </tbody>
                    </table>
                </div><!-- table-wrapper -->

                <!-- BASIC MODAL -->
                <div id="viewTimePolicyModal" class="modal fade">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content bd-0 tx-14">
                            <div class="modal-header pd-y-20 pd-x-25">
                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('timePolicy.viewTimePolicy')}}</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body pd-25">
                                <div class="col-12 p-0">
                                    <div class="row mb-3">
                                        <div class="col-5">
                                            <span class="font-weight-bold">
                                                {{__('timePolicy.timePolicyLateEntryAfter')}}
                                            </span>
                                        </div>

                                        <div class="col-1">
                                            <span>
                                                :
                                            </span>
                                        </div>

                                        <div class="col-6">
                                            <span>
                                                {{__('timePolicy.timePolicyLateEntryAfter')}}
                                            </span>
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <div class="col-5">
                                            <span class="font-weight-bold">
                                                {{__('timePolicy.timePolicyEarlyExitBefore')}}
                                            </span>
                                        </div>

                                        <div class="col-1">
                                            <span>
                                                :
                                            </span>
                                        </div>

                                        <div class="col-6">
                                            <span>
                                                {{__('timePolicy.timePolicyEarlyExitBefore')}}
                                            </span>
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <div class="col-5">
                                            <span class="font-weight-bold">
                                                {{__('timePolicy.timePolicyComesAfterHalfDay')}}
                                            </span>
                                        </div>

                                        <div class="col-1">
                                            <span>
                                                :
                                            </span>
                                        </div>

                                        <div class="col-6">
                                            <span>
                                                {{__('timePolicy.timePolicyComesAfterHalfDay')}}
                                            </span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-5">
                                            <span class="font-weight-bold">
                                                {{__('timePolicy.timePolicyLeavesBeforeHalfDay')}}
                                            </span>
                                        </div>

                                        <div class="col-1">
                                            <span>
                                                :
                                            </span>
                                        </div>

                                        <div class="col-6">
                                            <span>
                                                {{__('timePolicy.timePolicyLeavesBeforeHalfDay')}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- modal-dialog -->
                </div><!-- modal -->

                <!-- BASIC MODAL -->
                <div id="addTimePolicyModal" class="modal fade">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content bd-0 tx-14">
                            <div class="modal-header pd-y-20 pd-x-25">
                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('timePolicy.addTimePolicy')}}</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form role="form" id="addTimePolicyForm">
                                <div class="modal-body pd-25">
                                    <div class="col-12 p-0">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label
                                                        for="day">{{__('timePolicy.timePolicyDay')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <select class="form-control select2" name="day"
                                                            id="day" required aria-required="true">

                                                        <option selected
                                                                disabled>{{__('timePolicy.selectTimePolicyDay')}}</option>
                                                        <option value="Monday">Monday</option>
                                                        <option value="Tuesday">Tuesday</option>
                                                        <option value="Wednesday">Wednesday</option>
                                                        <option value="Thursday">Thursday</option>
                                                        <option value="Friday">Friday</option>
                                                        <option value="Saturday">Saturday</option>
                                                        <option value="Sunday">Sunday</option>
                                                    </select>
                                                    <div class="form-group">
                                                        <p class="red error"></p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label
                                                        for="weekoff">{{__('timePolicy.timePolicyWeekOff')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <select class="form-control select2 add_weekoff" name="weekoff"
                                                            id="weekoff" required>
                                                        <option selected
                                                                disabled>{{__('timePolicy.selectTimePolicyWeekOff')}}</option>
                                                        <option value="Yes">Yes</option>
                                                        <option value="No">No</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-6" id="time_in_div">
                                                <div class="form-group">
                                                    <label
                                                        for="time_in">{{__('timePolicy.timePolicyTimeIn')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>
                                                        </div>
                                                        <input type="text" class="form-control tpBasic"
                                                               id="time_in"
                                                               name="time_in"
                                                               placeholder="{{__('timePolicy.setTimePolicyTimeIn')}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-6" id="time_out_div">
                                                <div class="form-group">
                                                    <label
                                                        for="time_out">{{__('timePolicy.timePolicyTimOut')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>
                                                        </div>
                                                        <input type="text" class="form-control tpBasic"
                                                               id="time_out"
                                                               name="time_out"
                                                               placeholder="{{__('timePolicy.setTimePolicyTimeOut')}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-6" id="break_in_div">
                                                <div class="form-group">
                                                    <label
                                                        for="break_in">{{__('timePolicy.timePolicyBreakIn')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>
                                                        </div>
                                                        <input type="text" class="form-control tpBasic"
                                                               id="break_in"
                                                               name="break_in"
                                                               placeholder="{{__('timePolicy.setTimePolicyBreakIn')}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-6" id="break_out_div">
                                                <div class="form-group">
                                                    <label
                                                        for="break_out">{{__('timePolicy.timePolicyBreakOut')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>
                                                        </div>
                                                        <input type="text" class="form-control tpBasic"
                                                               id="break_out"
                                                               name="break_out"
                                                               placeholder="{{__('timePolicy.setTimePolicyBreakOut')}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-6" id="late_entry_after_div">
                                                <div class="form-group">
                                                    <label
                                                        for="late_entry_after">{{__('timePolicy.timePolicyLateEntryAfter')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>
                                                        </div>
                                                        <input type="text" class="form-control tpBasic"
                                                               id="late_entry_after"
                                                               name="late_entry_after"
                                                               placeholder="{{__('timePolicy.setTimePolicyLateEntryAfter')}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-6" id="early_exit_before_div">
                                                <div class="form-group">
                                                    <label
                                                        for="early_exit_before">{{__('timePolicy.timePolicyEarlyExitBefore')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>
                                                        </div>
                                                        <input type="text" class="form-control tpBasic"
                                                               id="early_exit_before"
                                                               name="early_exit_before"
                                                               placeholder="{{__('timePolicy.setTimePolicyEarlyExitBefore')}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-6" id="halfday_employee_comes_after_div">
                                                <div class="form-group">
                                                    <label
                                                        for="halfday_employee_comes_after">{{__('timePolicy.timePolicyComesAfterHalfDay')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>
                                                        </div>
                                                        <input type="text" class="form-control tpBasic"
                                                               id="halfday_employee_comes_after"
                                                               name="halfday_employee_comes_after"
                                                               placeholder="{{__('timePolicy.setTimePolicyComesAfterHalfDay')}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-6" id="halfday_employee_leaves_before_div">
                                                <div class="form-group">
                                                    <label
                                                        for="halfday_employee_leaves_before">{{__('timePolicy.timePolicyLeavesBeforeHalfDay')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>
                                                        </div>
                                                        <input type="text" class="form-control tpBasic"
                                                               id="halfday_employee_leaves_before"
                                                               name="halfday_employee_leaves_before"
                                                               placeholder="{{__('timePolicy.setTimePolicyLeavesBeforeHalfDay')}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 p-0 text-right">
                                        <button type="submit"
                                                class="btn btn-primary tx-uppercase">
                                            {{__('timePolicy.timePolicyButtonSubmit')}}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- modal-dialog -->
                </div><!-- modal -->

                <!-- BASIC MODAL -->
                <div id="editTimePolicyModal" class="modal fade">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content bd-0 tx-14">
                            <div class="modal-header pd-y-20 pd-x-25">
                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('timePolicy.editTimePolicyHeader')}}</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form role="form" id="editTimePolicyForm">
                                <div class="modal-body pd-25">
                                    <div class="col-12 p-0">
                                        <div class="row">
                                            <div class="col-6" id="edit_day_div">
                                                <div class="form-group">
                                                    <label
                                                        for="edit_day">{{__('timePolicy.timePolicyDay')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <select  class="form-control select2" name="edit_day"
                                                            id="edit_day" required>
                                                        <option selected
                                                                disabled>{{__('timePolicy.selectTimePolicyDay')}}</option>
                                                        <option value="Monday">Monday</option>
                                                        <option value="Tuesday">Tuesday</option>
                                                        <option value="Wednesday">Wednesday</option>
                                                        <option value="Thursday">Thursday</option>
                                                        <option value="Friday">Friday</option>
                                                        <option value="Saturday">Saturday</option>
                                                        <option value="Sunday">Sunday</option>
                                                    </select>
                                                    <div class="form-group">
                                                        <p class="red error"></p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-6" id="edit_weekoff_div">
                                                <div class="form-group">
                                                    <label
                                                        for="edit_weekoff">{{__('timePolicy.timePolicyWeekOff')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <select class="form-control select2 edit_weekoff" name="edit_weekoff"
                                                            id="edit_weekoff" required>
                                                        <option selected
                                                                disabled>{{__('timePolicy.selectTimePolicyWeekOff')}}</option>
                                                        <option value="Yes">Yes</option>
                                                        <option value="No">No</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-6" id="edit_time_in_div">
                                                <div class="form-group">
                                                    <label
                                                        for="edit_time_in">{{__('timePolicy.timePolicyTimeIn')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>
                                                        </div>
                                                        <input type="text" class="form-control tpBasic"
                                                               id="edit_time_in"
                                                               name="edit_time_in"
                                                               placeholder="{{__('timePolicy.setTimePolicyTimeIn')}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-6" id="edit_time_out_div">
                                                <div class="form-group">
                                                    <label
                                                        for="edit_time_out">{{__('timePolicy.timePolicyTimOut')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text ">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>
                                                        </div>
                                                        <input type="text" class="form-control tpBasic"
                                                               id="edit_time_out"
                                                               name="edit_time_out"
                                                               placeholder="{{__('timePolicy.setTimePolicyTimeOut')}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-6" id="edit_break_in_div">
                                                <div class="form-group">
                                                    <label
                                                        for="edit_breakin">{{__('timePolicy.timePolicyBreakIn')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>
                                                        </div>
                                                        <input type="text" class="form-control tpBasic"
                                                               id="edit_break_in"
                                                               name="edit_break_in"
                                                               placeholder="{{__('timePolicy.setTimePolicyBreakIn')}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-6" id="edit_break_out_div">
                                                <div class="form-group">
                                                    <label
                                                        for="edit_breakout">{{__('timePolicy.timePolicyBreakOut')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>
                                                        </div>
                                                        <input type="text" class="form-control tpBasic"
                                                               id="edit_break_out"
                                                               name="edit_break_out"
                                                               placeholder="{{__('timePolicy.setTimePolicyBreakOut')}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-6" id="edit_late_entry_after_div">
                                                <div class="form-group">
                                                    <label
                                                        for="edit_late_entry_after">{{__('timePolicy.timePolicyLateEntryAfter')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>
                                                        </div>
                                                        <input type="text" class="form-control tpBasic"
                                                               id="edit_late_entry_after"
                                                               name="edit_late_entry_after"
                                                               placeholder="{{__('timePolicy.setTimePolicyLateEntryAfter')}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-6" id="edit_early_exit_before_div">
                                                <div class="form-group">
                                                    <label
                                                        for="edit_early_exit_before">{{__('timePolicy.timePolicyEarlyExitBefore')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>
                                                        </div>
                                                        <input type="text" class="form-control tpBasic"
                                                               id="edit_early_exit_before"
                                                               name="edit_early_exit_before"
                                                               placeholder="{{__('timePolicy.setTimePolicyEarlyExitBefore')}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-6" id="edit_halfday_employee_comes_after_div">
                                                <div class="form-group">
                                                    <label
                                                        for="edit_halfday_employee_comes_after">{{__('timePolicy.timePolicyComesAfterHalfDay')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>
                                                        </div>
                                                        <input type="text" class="form-control tpBasic"
                                                               id="edit_halfday_employee_comes_after"
                                                               name="edit_halfday_employee_comes_after"
                                                               placeholder="{{__('timePolicy.setTimePolicyComesAfterHalfDay')}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-6" id="edit_halfday_employee_leaves_before_div">
                                                <div class="form-group">
                                                    <label
                                                        for="edit_halfday_employee_leaves_before">{{__('timePolicy.timePolicyLeavesBeforeHalfDay')}}
                                                        <span class="tx-danger">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>
                                                        </div>
                                                        <input type="text" class="form-control tpBasic"
                                                               id="edit_halfday_employee_leaves_before"
                                                               name="edit_halfday_employee_leaves_before"
                                                               placeholder="{{__('timePolicy.setTimePolicyLeavesBeforeHalfDay')}}"
                                                               required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 p-0 text-right">
                                        <button type="submit"
                                                class="btn btn-primary tx-uppercase">
                                            {{__('timePolicy.timePolicyButtonUpdate')}}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- modal-dialog -->
                </div><!-- modal -->
            </div>
        </div>
    </div>
@endsection
