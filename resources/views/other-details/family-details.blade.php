{{--Family Details--}}
<h3 class="d-none">{{__('otherDetails.familyDetails')}}</h3>

<section>
    <div class="col-12 text-right p-0">
        <button class="btn btn-primary mb-4"
                id="addFamilyDetails">
            <i class="fa fa-plus mr-2"></i>{{__('otherDetails.addFamilyDetails')}}
        </button>
    </div>

    <div class="table-wrapper">
        <table id="familyDetailsTable" class="table display nowrap">
            <thead>
            <tr>
                <th>{{__('otherDetails.familyDetailsFullName')}}</th>
                <th>{{__('otherDetails.familyDetailsAge')}}</th>
                <th>{{__('otherDetails.familyDetailsOccupation')}}</th>
                <th>{{__('otherDetails.familyDetailsRelationship')}}</th>
                <th>{{__('otherDetails.familyDetailsGender')}}</th>
                <th>{{__('otherDetails.familyDetailsDependant')}}</th>
                <th class="text-right">{{__('otherDetails.familyDetailsActions')}}</th>
            </tr>
            </thead>
            <tbody>
           {{-- <tr>
                <td>Test Tester Testing</td>
                <td>21</td>
                <td>Tester</td>
                <td>Brother</td>
                <td>Male</td>
                <td>Yes</td>
                <td class="text-right">
                    <div class="col-12 p-0">
                        <i class="fa fa-edit cursor-pointer mr-2 font-size-18 text-primary"
                           id="editFamilyDetails"
                           title="{{__('otherDetails.editFamilyDetails')}}"></i>
                        <i class="fa fa-trash cursor-pointer font-size-18 text-danger"
                           id="deleteFamilyDetails"
                           title="{{__('otherDetails.deleteFamilyDetails')}}"></i>
                    </div>
                </td>
            </tr>--}}
            </tbody>
        </table>
    </div><!-- table-wrapper -->

    <!-- BASIC MODAL -->
    <div id="addFamilyDetailsModal" class="modal fade">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('otherDetails.addFamilyDetails')}}</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="addFamilyDetailsForm">
                    <input id="employeeId" value="{{$employee->id}}" name="employeeId" type="hidden">
                    <div class="modal-body pd-25">
                        <div class="col-12 p-0">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="full_name">{{__('otherDetails.familyDetailsFullName')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="full_name" name="full_name"
                                               placeholder="{{__('otherDetails.enterFamilyDetailsFullName')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="age">{{__('otherDetails.familyDetailsAge')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="number" class="form-control"
                                               id="age" name="age"
                                               placeholder="{{__('otherDetails.enterFamilyDetailsAge')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="occupation">{{__('otherDetails.familyDetailsOccupation')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="occupation"
                                               name="occupation"
                                               placeholder="{{__('otherDetails.enterFamilyDetailsOccupation')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="relationship">{{__('otherDetails.familyDetailsRelationship')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="relationship"
                                               name="relationship"
                                               placeholder="{{__('otherDetails.enterFamilyDetailsRelationship')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="gender"
                                               class="form-control-label">{{__('otherDetails.familyDetailsGender')}}
                                            <span class="tx-danger">*</span></label>
                                        <select class="form-control select2"
                                                name="gender"
                                                id="gender" required>
                                            <option selected
                                                    disabled>{{__('otherDetails.selectFamilyDetailsGender')}}</option>
                                            <option
                                                value="{{__('otherDetails.familyDetailsMale')}}">{{__('otherDetails.familyDetailsMale')}}</option>
                                            <option
                                                value="{{__('otherDetails.familyDetailsFemale')}}">{{__('otherDetails.familyDetailsFemale')}}</option>
                                        </select>
                                    </div><!-- form-group -->
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="dependant">{{__('otherDetails.familyDetailsDependant')}}
                                            <span class="tx-danger">*</span></label>
                                        <select class="form-control select2"
                                                name="dependant"
                                                id="dependant" required>
                                            <option selected
                                                    disabled>{{__('otherDetails.selectFamilyDetailsDependant')}}</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <p class="red error"></p>
                        </div>
                        <div class="col-12 p-0 text-right">
                            <button type="submit"
                                    class="btn btn-primary tx-uppercase">
                                {{__('otherDetails.familyDetailsButtonSubmit')}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->

    <!-- BASIC MODAL -->
    <div id="editFamilyDetailsModal" class="modal fade">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('otherDetails.editFamilyDetailsHeader')}}</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="editFamilyDetailsForm">
                        <input id="employeeId" value="{{$employee->id}}" name="employeeId" type="hidden">
                    <div class="modal-body pd-25">
                        <div class="col-12 p-0">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_family_name">{{__('otherDetails.familyDetailsFullName')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="edit_family_name" name="edit_family_name"
                                               placeholder="{{__('otherDetails.enterFamilyDetailsFullName')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_family_age">{{__('otherDetails.familyDetailsAge')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="number" class="form-control"
                                               id="edit_family_age" name="edit_family_age"
                                               placeholder="{{__('otherDetails.enterFamilyDetailsAge')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_family_occupation">{{__('otherDetails.familyDetailsOccupation')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="edit_family_occupation"
                                               name="edit_family_occupation"
                                               placeholder="{{__('otherDetails.enterFamilyDetailsOccupation')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_family_relationship">{{__('otherDetails.familyDetailsRelationship')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="edit_family_relationship"
                                               name="edit_family_relationship"
                                               placeholder="{{__('otherDetails.enterFamilyDetailsRelationship')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="edit_family_gender"
                                               class="form-control-label">{{__('otherDetails.familyDetailsGender')}}
                                            <span class="tx-danger">*</span></label>
                                        <select class="form-control select2"
                                                name="edit_family_gender"
                                                id="edit_family_gender" required>
                                            <option selected
                                                    disabled>{{__('otherDetails.selectFamilyDetailsGender')}}</option>
                                            <option
                                                value="{{__('otherDetails.familyDetailsMale')}}">{{__('otherDetails.familyDetailsMale')}}</option>
                                            <option
                                                value="{{__('otherDetails.familyDetailsFemale')}}">{{__('otherDetails.familyDetailsFemale')}}</option>
                                        </select>
                                    </div><!-- form-group -->
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_family_dependant">{{__('otherDetails.familyDetailsDependant')}}
                                            <span
                                                class="tx-danger">*</span></label>
                                        <select class="form-control select2"
                                                name="edit_family_dependant"
                                                id="edit_family_dependant" required>
                                            <option selected
                                                    disabled>{{__('otherDetails.selectFamilyDetailsDependant')}}</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 p-0 text-right">
                            <button type="submit"
                                    class="btn btn-primary tx-uppercase">
                                {{__('otherDetails.familyDetailsButtonUpdate')}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
</section>
