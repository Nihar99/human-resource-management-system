{{--Exposure to Computer--}}
<h3 class="d-none">{{__('otherDetails.exposureComputer')}}</h3>

<section>
    <div class="col-12 text-right p-0">
        <button class="btn btn-primary mb-4"
                id="addExposureComputer">
            <i class="fa fa-plus mr-2"></i>{{__('otherDetails.addExposureComputer')}}
        </button>
    </div>

    <div class="table-wrapper">
        <table id="exposureComputerTable" class="table display nowrap">
            <thead>
            <tr>
                <th>{{__('otherDetails.exposureComputerLanguage')}}</th>
                <th>{{__('otherDetails.exposureComputerOperatingSystem')}}</th>
                <th>{{__('otherDetails.exposureComputerApplication')}}</th>
                <th>{{__('otherDetails.exposureComputerHardware')}}</th>
                <th class="text-right">{{__('otherDetails.exposureComputerActions')}}</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Laravel</td>
                <td>Windows</td>
                <td>Android</td>
                <td>High Configuration</td>
                <td class="text-right">
                    <div class="col-12 p-0">
                        <i class="fa fa-edit cursor-pointer mr-2 font-size-18 text-primary"
                           id="editExposureComputer"
                           title="{{__('otherDetails.editExposureComputer')}}"></i>
                        <i class="fa fa-trash cursor-pointer font-size-18 text-danger"
                           id="deleteExposureComputer"
                           title="{{__('otherDetails.deleteExposureComputer')}}"></i>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div><!-- table-wrapper -->

    <!-- BASIC MODAL -->
    <div id="addExposureComputerModal" class="modal fade">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('otherDetails.addExposureComputer')}}</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="addExposureComputerForm">
                    <input id="employeeId" value="{{$employee->id}}" name="employeeId" type="hidden">
                    <div class="modal-body pd-25">
                        <div class="col-12 p-0">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="add_exposure_language">{{__('otherDetails.exposureComputerLanguage')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="add_exposure_language"
                                               name="add_exposure_language"
                                               placeholder="{{__('otherDetails.enterExposureComputerLanguage')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="add_exposure_operating_system">{{__('otherDetails.exposureComputerOperatingSystem')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="add_exposure_operating_system"
                                               name="add_exposure_operating_system"
                                               placeholder="{{__('otherDetails.enterExposureComputerOperatingSystem')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="add_exposure_application">{{__('otherDetails.exposureComputerApplication')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="add_exposure_application"
                                               name="add_exposure_application"
                                               placeholder="{{__('otherDetails.enterExposureComputerApplication')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="add_exposure_hardware">{{__('otherDetails.exposureComputerHardware')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="add_exposure_hardware"
                                               name="add_exposure_hardware"
                                               placeholder="{{__('otherDetails.enterExposureComputerHardware')}}"
                                               required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 p-0 text-right">
                            <button type="submit"
                                    class="btn btn-primary tx-uppercase">
                                {{__('otherDetails.exposureComputerButtonSubmit')}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->

    <!-- BASIC MODAL -->
    <div id="editExposureComputerModal" class="modal fade">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('otherDetails.editExposureComputerHeader')}}</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="editExposureComputerForm">
                    <input id="employeeId" value="{{$employee->id}}" name="employeeId" type="hidden">
                    <div class="modal-body pd-25">
                        <div class="col-12 p-0">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_exposure_language">{{__('otherDetails.exposureComputerLanguage')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="edit_exposure_language"
                                               name="edit_exposure_language"
                                               placeholder="{{__('otherDetails.enterExposureComputerLanguage')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_exposure_operating_system">{{__('otherDetails.exposureComputerOperatingSystem')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="edit_exposure_operating_system"
                                               name="edit_exposure_operating_system"
                                               placeholder="{{__('otherDetails.enterExposureComputerOperatingSystem')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_exposure_application">{{__('otherDetails.exposureComputerApplication')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="edit_exposure_application"
                                               name="edit_exposure_application"
                                               placeholder="{{__('otherDetails.enterExposureComputerApplication')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_exposure_hardware">{{__('otherDetails.exposureComputerHardware')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="edit_exposure_hardware"
                                               name="edit_exposure_hardware"
                                               placeholder="{{__('otherDetails.enterExposureComputerHardware')}}"
                                               required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 p-0 text-right">
                            <button type="submit"
                                    class="btn btn-primary tx-uppercase">
                                {{__('otherDetails.exposureComputerButtonUpdate')}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
</section>
