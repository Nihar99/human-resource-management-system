{{--Work Experience--}}
<h3 class="d-none">{{__('otherDetails.workExperience')}}</h3>

<section>
    <div class="col-12 text-right p-0">
        <button class="btn btn-primary mb-4"
                id="addWorkExperience">
            <i class="fa fa-plus mr-2"></i>{{__('otherDetails.addWorkExperience')}}
        </button>
    </div>

    <div class="table-wrapper">
        <table id="workExperienceTable" class="table display nowrap">
            <thead>
            <tr>
                <th>{{__('otherDetails.workExperienceOrganisationName')}}</th>
                <th>{{__('otherDetails.workExperienceOrganisationAddress')}}</th>
                <th>{{__('otherDetails.workExperienceFromDate')}}</th>
                <th>{{__('otherDetails.workExperienceToDate')}}</th>
                <th>{{__('otherDetails.workExperiencePositionHeld')}}</th>
                <th class="text-right">{{__('otherDetails.workExperienceActions')}}</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>3iology</td>
                <td>Virar</td>
                <td>10th April 2015</td>
                <td>10th April 2018</td>
                <td>Manager</td>
                <td class="text-right">
                    <div class="col-12 p-0">
                        <i class="fa fa-edit cursor-pointer mr-2 font-size-18 text-primary"
                           id="editWorkExperience"
                           title="{{__('otherDetails.editWorkExperience')}}"></i>
                        <i class="fa fa-trash cursor-pointer font-size-18 text-danger"
                           id="deleteWorkExperience"
                           title="{{__('otherDetails.deleteWorkExperience')}}"></i>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div><!-- table-wrapper -->

    <!-- BASIC MODAL -->
    <div id="addWorkExperienceModal" class="modal fade">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('otherDetails.addWorkExperience')}}</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            
                <form role="form" id="addWorkExperienceForm">
                    <div class="modal-body pd-25">
                        <div class="col-12 p-0">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label
                                            for="add_work_experience_organisation_name">{{__('otherDetails.workExperienceOrganisationName')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="add_work_experience_organisation_name"
                                               name="organisation_name"
                                               placeholder="{{__('otherDetails.enterWorkExperienceOrganisationName')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group">
                                        <label
                                            for="add_work_experience_organisation_address">{{__('otherDetails.workExperienceOrganisationAddress')}}
                                            <span class="tx-danger">*</span></label>
                                        <textarea class="form-control" rows="5"
                                                  id="add_work_experience_organisation_address"
                                                  name="organisation_address"
                                                  placeholder="{{__('otherDetails.enterWorkExperienceOrganisationAddress')}}"
                                                  required></textarea>
                                    </div>
                                </div>
                                
                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="add_work_experience_from_date">{{__('otherDetails.workExperienceFromDate')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control fc-datepicker"
                                               name="from_date"
                                               id="add_work_experience_from_date"
                                               placeholder="{{__('otherDetails.selectWorkExperienceFromDate')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="add_work_experience_to_date">{{__('otherDetails.workExperienceToDate')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control fc-datepicker"
                                               name="to_date"
                                               id="add_work_experience_to_date"
                                               placeholder="{{__('otherDetails.selectWorkExperienceToDate')}}"
                                               required>
                                    </div>
                                </div>
                                
                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="add_work_experience_position_held">{{__('otherDetails.workExperiencePositionHeld')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="add_work_experience_position_held"
                                               name="position_held"
                                               placeholder="{{__('otherDetails.enterWorkExperiencePositionHeld')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="add_work_experience_reporting_to">{{__('otherDetails.workExperienceReportingTo')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="add_work_experience_reporting_to"
                                               name="reporting_to"
                                               placeholder="{{__('otherDetails.enterWorkExperienceReportingTo')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="add_work_experience_nature_work">{{__('otherDetails.workExperienceWorkNature')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="add_work_experience_nature_work"
                                               name="nature_of_work"
                                               placeholder="{{__('otherDetails.enterWorkExperienceWorkNature')}}"
                                               required>
                                    </div>
                                </div>
                            

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="add_work_experience_leaving_reason">{{__('otherDetails.workExperienceLeavingReason')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="add_work_experience_leaving_reason"
                                               name="reason_of_leaving"
                                               placeholder="{{__('otherDetails.enterWorkExperienceLeavingReason')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="add_work_experience_annual_gross">{{__('otherDetails.workExperienceAnnualGross')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="add_work_experience_annual_gross"
                                               name="gross_annual"
                                               placeholder="{{__('otherDetails.enterWorkExperienceAnnualGross')}}"
                                               required>
                                            
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 p-0 text-right">
                            <button type="submit"
                                    class="btn btn-primary tx-uppercase"
                                    id="btnSubmitWorkExperience">
                                {{__('otherDetails.workExperienceButtonSubmit')}}
                            </button>
                        </div>
                    <input type="hidden" id="employee_id" name="employee_id" value="{{ $employee->id }}">
                    </div>
                </form>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->

    <!-- BASIC MODAL -->
    <div id="editWorkExperienceModal" class="modal fade">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('otherDetails.editWorkExperienceHeader')}}</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="editWorkExperienceForm">
                    <div class="modal-body pd-25">
                        <div class="col-12 p-0">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label
                                            for="edit_work_experience_organisation_name">{{__('otherDetails.workExperienceOrganisationName')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="edit_work_experience_organisation_name"
                                               name="edit_work_experience_organisation_name"
                                               placeholder="{{__('otherDetails.enterWorkExperienceOrganisationName')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group">
                                        <label
                                            for="edit_work_experience_organisation_address">{{__('otherDetails.workExperienceOrganisationAddress')}}
                                            <span class="tx-danger">*</span></label>
                                        <textarea class="form-control" rows="5"
                                                  id="edit_work_experience_organisation_address"
                                                  name="edit_work_experience_organisation_address"
                                                  placeholder="{{__('otherDetails.enterWorkExperienceOrganisationAddress')}}"
                                                  required></textarea>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_work_experience_from_date">{{__('otherDetails.workExperienceFromDate')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control fc-datepicker"
                                               name="edit_work_experience_from_date"
                                               id="edit_work_experience_from_date"
                                               placeholder="{{__('otherDetails.selectWorkExperienceFromDate')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_work_experience_to_date">{{__('otherDetails.workExperienceToDate')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control fc-datepicker"
                                               name="edit_work_experience_to_date"
                                               id="edit_work_experience_to_date"
                                               placeholder="{{__('otherDetails.selectWorkExperienceToDate')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_work_experience_position_held">{{__('otherDetails.workExperiencePositionHeld')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="edit_work_experience_position_held"
                                               name="edit_work_experience_position_held"
                                               placeholder="{{__('otherDetails.enterWorkExperiencePositionHeld')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_work_experience_reporting_to">{{__('otherDetails.workExperienceReportingTo')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="edit_work_experience_reporting_to"
                                               name="edit_work_experience_reporting_to"
                                               placeholder="{{__('otherDetails.enterWorkExperienceReportingTo')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_work_experience_nature_work">{{__('otherDetails.workExperienceWorkNature')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="edit_work_experience_nature_work"
                                               name="edit_work_experience_nature_work"
                                               placeholder="{{__('otherDetails.enterWorkExperienceWorkNature')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_work_experience_leaving_reason">{{__('otherDetails.workExperienceLeavingReason')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="edit_work_experience_leaving_reason"
                                               name="edit_work_experience_leaving_reason"
                                               placeholder="{{__('otherDetails.enterWorkExperienceLeavingReason')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_work_experience_annual_gross">{{__('otherDetails.workExperienceAnnualGross')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="edit_work_experience_annual_gross"
                                               name="edit_work_experience_annual_gross"
                                               placeholder="{{__('otherDetails.enterWorkExperienceAnnualGross')}}"
                                               required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 p-0 text-right">
                            <button type="submit"
                                    class="btn btn-primary tx-uppercase">
                                {{__('otherDetails.workExperienceButtonSubmit')}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
</section>
