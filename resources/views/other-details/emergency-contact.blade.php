{{--Emergency Contact--}}
<h3 class="d-none">{{__('otherDetails.emergencyContact')}}</h3>

<section>
    <div class="col-12 text-right p-0">
        <button class="btn btn-primary mb-4"
                id="addEmergencyContact">
            <i class="fa fa-plus mr-2"></i>{{__('otherDetails.addEmergencyContact')}}
        </button>
    </div>

    <div class="table-wrapper">
        <table id="emergencyContactTable" class="table display nowrap">
            <thead>
            <tr>
                <th>{{__('otherDetails.emergencyContactName')}}</th>
                <th>{{__('otherDetails.emergencyContactRelationship')}}</th>
                <th>{{__('otherDetails.emergencyContactAddress')}}</th>
                <th>{{__('otherDetails.emergencyContactPhone')}}</th>
                <th class="text-right">{{__('otherDetails.emergencyContactActions')}}</th>
            </tr>
            </thead>
            <tbody>
           {{-- <tr>
                <td>Test Tester Testing</td>
                <td>Brother</td>
                <td>Thane</td>
                <td>9876543210</td>
                <td class="text-right">
                    <div class="col-12 p-0">
                        <i class="fa fa-edit cursor-pointer mr-2 font-size-18 text-primary"
                           id="editEmergencyContact"
                           title="{{__('otherDetails.editEmergencyContact')}}"></i>
                        <i class="fa fa-trash cursor-pointer font-size-18 text-danger"
                           id="deleteEmergencyContact"
                           title="{{__('otherDetails.deleteEmergencyContact')}}"></i>
                    </div>
                </td>
            </tr>--}}
            </tbody>
        </table>
    </div><!-- table-wrapper -->

    <!-- BASIC MODAL -->
    <div id="addEmergencyContactModal" class="modal fade">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('otherDetails.addEmergencyContact')}}</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="addEmergencyContactForm">
                    <input id="employeeId" value="{{$employee->id}}" name="employeeId" type="hidden">
                    <div class="modal-body pd-25">
                        <div class="col-12 p-0">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">

                                        <label
                                            for="name">{{__('otherDetails.emergencyContactName')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="name" name="name"
                                               placeholder="{{__('otherDetails.enterEmergencyContactName')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="relationship">{{__('otherDetails.emergencyContactRelationship')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="relationship"
                                               name="relationship"
                                               placeholder="{{__('otherDetails.enterEmergencyContactRelationship')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group">
                                        <label
                                            for="address">{{__('otherDetails.emergencyContactAddress')}}
                                            <span class="tx-danger">*</span></label>
                                        <textarea class="form-control" rows="5"
                                                  id="address"
                                                  name="address"
                                                  placeholder="{{__('otherDetails.enterEmergencyContactAddress')}}"
                                                  required></textarea>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="phone">{{__('otherDetails.emergencyContactPhone')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="number" class="form-control"
                                               id="phone" name="phone"
                                               placeholder="{{__('otherDetails.enterEmergencyContactPhone')}}"
                                               required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 p-0 text-right">
                            <button type="submit"
                                    class="btn btn-primary tx-uppercase">
                                {{__('otherDetails.emergencyContactButtonSubmit')}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->

    <!-- BASIC MODAL -->
    <div id="editEmergencyContactModal" class="modal fade">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('otherDetails.editEmergencyContactHeader')}}</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="editEmergencyContactForm">
                    <input id="employeeId" value="{{$employee->id}}" name="employeeId" type="hidden">
                    <div class="modal-body pd-25">
                        <div class="col-12 p-0">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_name">{{__('otherDetails.emergencyContactName')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="edit_name" name="edit_name"
                                               placeholder="{{__('otherDetails.enterEmergencyContactName')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_relationship">{{__('otherDetails.emergencyContactRelationship')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="edit_relationship"
                                               name="edit_relationship"
                                               placeholder="{{__('otherDetails.enterEmergencyContactRelationship')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group">
                                        <label
                                            for="edit_address">{{__('otherDetails.emergencyContactAddress')}}
                                            <span class="tx-danger">*</span></label>
                                        <textarea class="form-control" rows="5"
                                                  id="edit_address"
                                                  name="edit_address"
                                                  placeholder="{{__('otherDetails.enterEmergencyContactAddress')}}"
                                                  required></textarea>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_phone">{{__('otherDetails.emergencyContactPhone')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="number" class="form-control"
                                               id="edit_phone" name="edit_phone"
                                               placeholder="{{__('otherDetails.enterEmergencyContactPhone')}}"
                                               required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 p-0 text-right">
                            <button type="submit"
                                    class="btn btn-primary tx-uppercase">
                                {{__('otherDetails.emergencyContactButtonUpdate')}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
</section>
