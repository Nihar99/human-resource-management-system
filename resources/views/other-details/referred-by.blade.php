{{--Work Experience--}}
<h3 class="d-none">{{__('otherDetails.referredBy')}}</h3>

<section>
    <div class="col-12 text-right p-0">
        <button class="btn btn-primary mb-4"
                id="addReferredBy">
            <i class="fa fa-plus mr-2"></i>{{__('otherDetails.addReferredBy')}}
        </button>
    </div>

    <div class="table-wrapper">
        <table id="referredByTable" class="table display nowrap">
            <thead>
            <tr>
                <th>{{__('otherDetails.referredByName')}}</th>
                <th>{{__('otherDetails.referredByPosition')}}</th>
                <th>{{__('otherDetails.referredByAddress')}}</th>
                <th>{{__('otherDetails.referredByTelephone')}}</th>
                <th class="text-right">{{__('otherDetails.referredByActions')}}</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Test</td>
                <td>Manager</td>
                <td>Virar</td>
                <td>9876543210</td>
                <td class="text-right">
                    <div class="col-12 p-0">
                        <i class="fa fa-edit cursor-pointer mr-2 font-size-18 text-primary"
                           id="editReferredBy"
                           title="{{__('otherDetails.editReferredBy')}}"></i>
                        <i class="fa fa-trash cursor-pointer font-size-18 text-danger"
                           id="deleteReferredBy"
                           title="{{__('otherDetails.deleteReferredBy')}}"></i>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div><!-- table-wrapper -->

    <!-- BASIC MODAL -->
    <div id="addReferredByModal" class="modal fade">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('otherDetails.addReferredBy')}}</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="addReferredByForm">
                    <input id="employeeId" value="{{$employee->id}}" name="employeeId" type="hidden">
                    <div class="modal-body pd-25">
                        <div class="col-12 p-0">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="add_referred_by_name">{{__('otherDetails.referredByName')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="add_referred_by_name"
                                               name="add_referred_by_name"
                                               placeholder="{{__('otherDetails.enterReferredByName')}}"
                                               required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p class="red error"></p>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="add_referred_by_phone">{{__('otherDetails.referredByPosition')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="add_referred_by_position"
                                               name="add_referred_by_position"
                                               placeholder="{{__('otherDetails.enterReferredByPosition')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group">
                                        <label
                                            for="add_referred_by_address">{{__('otherDetails.referredByAddress')}}
                                            <span class="tx-danger">*</span></label>
                                        <textarea class="form-control" rows="5"
                                                  id="add_referred_by_address"
                                                  name="add_referred_by_address"
                                                  placeholder="{{__('otherDetails.enterReferredByAddress')}}"
                                                  required></textarea>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="add_work_experience_from_telephone">{{__('otherDetails.referredByTelephone')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="add_referred_by_telephone"
                                               name="add_referred_by_telephone"
                                               placeholder="{{__('otherDetails.enterReferredByTelephone')}}"
                                               required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 p-0 text-right">
                            <button type="submit"
                                    class="btn btn-primary tx-uppercase">
                                {{__('otherDetails.workExperienceButtonSubmit')}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->

    <!-- BASIC MODAL -->
    <div id="editReferredByModal" class="modal fade">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('otherDetails.editReferredByHeader')}}</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="editReferredByForm">
                    <input id="employeeId" value="{{$employee->id}}" name="employeeId" type="hidden">
                    <div class="modal-body pd-25">
                        <div class="col-12 p-0">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_referred_by_name">{{__('otherDetails.referredByName')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="edit_referred_by_name"
                                               name="edit_referred_by_name"
                                               placeholder="{{__('otherDetails.enterReferredByName')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_referred_by_phone">{{__('otherDetails.referredByPosition')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="edit_referred_by_position"
                                               name="edit_referred_by_position"
                                               placeholder="{{__('otherDetails.enterReferredByPosition')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group">
                                        <label
                                            for="edit_referred_by_address">{{__('otherDetails.referredByAddress')}}
                                            <span class="tx-danger">*</span></label>
                                        <textarea class="form-control" rows="5"
                                                  id="edit_referred_by_address"
                                                  name="edit_referred_by_address"
                                                  placeholder="{{__('otherDetails.enterReferredByAddress')}}"
                                                  required></textarea>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_work_experience_from_telephone">{{__('otherDetails.referredByTelephone')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="edit_referred_by_telephone"
                                               name="edit_referred_by_telephone"
                                               placeholder="{{__('otherDetails.enterReferredByTelephone')}}"
                                               required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 p-0 text-right">
                            <button type="submit"
                                    class="btn btn-primary tx-uppercase">
                                {{__('otherDetails.referredByButtonUpdate')}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
</section>
