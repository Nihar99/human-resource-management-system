{{--Educational Profile--}}
<h3 class="d-none">{{__('otherDetails.educationalProfile')}}</h3>

<section>
    <div class="col-12 text-right p-0">
        <button class="btn btn-primary mb-4"
                id="addEducationalProfile">
            <i class="fa fa-plus mr-2"></i>{{__('otherDetails.addEducationalProfile')}}
        </button>
    </div>

    <div class="table-wrapper">
        <table id="educationalProfileTable" class="table display nowrap">
            <thead>
            <tr>
                <th>{{__('otherDetails.educationalProfileQualification')}}</th>
                <th>{{__('otherDetails.educationalProfilePassingYear')}}</th>
                <th>{{__('otherDetails.educationalProfileInstitution')}}</th>
                <th>{{__('otherDetails.educationalProfileSubjects')}}</th>
                <th>{{__('otherDetails.educationalProfileScore')}}</th>
                <th class="text-right">{{__('otherDetails.educationalProfileActions')}}</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>SSC Board</td>
                <td>2013</td>
                <td>Lawrence</td>
                <td>Science</td>
                <td>100</td>
                <td class="text-right">
                    <div class="col-12 p-0">
                        <i class="fa fa-edit cursor-pointer mr-2 font-size-18 text-primary"
                           id="editEducationalProfile"
                           title="{{__('otherDetails.editEducationalProfile')}}"></i>
                        <i class="fa fa-trash cursor-pointer font-size-18 text-danger"
                           id="deleteEducationalProfile"
                           title="{{__('otherDetails.deleteEducationalProfile')}}"></i>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div><!-- table-wrapper -->

    <!-- BASIC MODAL -->
    <div id="addEducationalProfileModal" class="modal fade">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('otherDetails.addEducationalProfile')}}</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="addEducationalProfileForm">
                    <input id="employeeId" value="{{$employee->id}}" name="employeeId" type="hidden">
                    <div class="modal-body pd-25">
                        <div class="col-12 p-0">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="board_degree">{{__('otherDetails.educationalProfileQualification')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="board_degree"
                                               name="board_degree"
                                               placeholder="{{__('otherDetails.enterEducationalProfileQualification')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="year_of_passing">{{__('otherDetails.educationalProfilePassingYear')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="year_of_passing"
                                               name="year_of_passing"
                                               placeholder="{{__('otherDetails.enterEducationalProfilePassingYear')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="institution_name">{{__('otherDetails.educationalProfileInstitution')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="institution_name"
                                               name="institution_name"
                                               placeholder="{{__('otherDetails.enterEducationalProfileInstitution')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="principle_subjects">{{__('otherDetails.educationalProfileSubjects')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="principle_subjects"
                                               name="principle_subjects"
                                               placeholder="{{__('otherDetails.enterEducationalProfileSubjects')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="grade_percentage">{{__('otherDetails.educationalProfileScore')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="grade_percentage"
                                               name="grade_percentage"
                                               placeholder="{{__('otherDetails.enterEducationalProfileScore')}}"
                                               required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 p-0 text-right">
                            <button type="submit"
                                    class="btn btn-primary tx-uppercase">
                                {{__('otherDetails.educationalProfileButtonSubmit')}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->

    <!-- BASIC MODAL -->
    <div id="editEducationalProfileModal" class="modal fade">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('otherDetails.editEducationalProfileHeader')}}</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="editEducationalProfileForm">
                    <input id="employeeId" value="{{$employee->id}}" name="employeeId" type="hidden">
                    <div class="modal-body pd-25">
                        <div class="col-12 p-0">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_board_degree">{{__('otherDetails.educationalProfileQualification')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="edit_board_degree"
                                               name="edit_board_degree"
                                               placeholder="{{__('otherDetails.enterEducationalProfileQualification')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_year_of_passing">{{__('otherDetails.educationalProfilePassingYear')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="edit_year_of_passing"
                                               name="edit_year_of_passing"
                                               placeholder="{{__('otherDetails.enterEducationalProfilePassingYear')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_institution_name">{{__('otherDetails.educationalProfileInstitution')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="edit_institution_name"
                                               name="edit_institution_name"
                                               placeholder="{{__('otherDetails.enterEducationalProfileInstitution')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_principle_subjects">{{__('otherDetails.educationalProfileSubjects')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="edit_principle_subjects"
                                               name="edit_principle_subjects"
                                               placeholder="{{__('otherDetails.enterEducationalProfileSubjects')}}"
                                               required>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_grade_percentage">{{__('otherDetails.educationalProfileScore')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="edit_grade_percentage"
                                               name="edit_grade_percentage"
                                               placeholder="{{__('otherDetails.enterEducationalProfileScore')}}"
                                               required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 p-0 text-right">
                            <button type="submit"
                                    class="btn btn-primary tx-uppercase">
                                {{__('otherDetails.educationalProfileButtonUpdate')}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
</section>
