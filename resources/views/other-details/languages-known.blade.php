{{--Languages Known--}}
<h3 class="d-none">{{__('otherDetails.languagesKnown')}}</h3>

<section>
    <div class="col-12 text-right p-0">
        <button class="btn btn-primary mb-4"
                id="addLanguagesKnown">
            <i class="fa fa-plus mr-2"></i>{{__('otherDetails.addLanguagesKnown')}}
        </button>
    </div>

    <div class="table-wrapper">
        <table id="languagesKnownTable" class="table display nowrap">
            <thead>
            <tr>
                <th>{{__('otherDetails.languagesKnownLanguage')}}</th>
                <th>{{__('otherDetails.languagesKnownRead')}}</th>
                <th>{{__('otherDetails.languagesKnownWrite')}}</th>
                <th>{{__('otherDetails.languagesKnownSpeak')}}</th>
                <th>{{__('otherDetails.languagesKnownUnderstand')}}</th>
                <th class="text-right">{{__('otherDetails.languagesKnownActions')}}</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>English</td>
                <td>Yes</td>
                <td>Yes</td>
                <td>Yes</td>
                <td>Yes</td>
                <td class="text-right">
                    <div class="col-12 p-0">
                        <i class="fa fa-edit cursor-pointer mr-2 font-size-18 text-primary"
                           id="editLanguagesKnown"
                           title="{{__('otherDetails.editLanguagesKnown')}}"></i>
                        <i class="fa fa-trash cursor-pointer font-size-18 text-danger"
                           id="deleteLanguagesKnown"
                           title="{{__('otherDetails.deleteLanguagesKnown')}}"></i>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div><!-- table-wrapper -->

    <!-- BASIC MODAL -->
    <div id="addLanguagesKnownModal" class="modal fade">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('otherDetails.addLanguagesKnown')}}</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="addLanguagesKnownForm">
                    <input id="employeeId" value="{{$employee->id}}" name="employeeId" type="hidden">
                    <div class="modal-body pd-25">
                        <div class="col-12 p-0">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="name">{{__('otherDetails.languagesKnownLanguage')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="name"
                                               name="name"
                                               placeholder="{{__('otherDetails.enterLanguagesKnownLanguage')}}"
                                               required>
                                        <div class="form-group">
                                            <p class="red error"></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="read">{{__('otherDetails.languagesKnownRead')}}
                                            <span class="tx-danger">*</span></label>
                                        <select class="form-control select2"
                                                name="read" id="read"
                                                required>
                                            <option selected
                                                    disabled>{{__('otherDetails.selectLanguagesKnownRead')}}</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="write">{{__('otherDetails.languagesKnownWrite')}}
                                            <span class="tx-danger">*</span></label>
                                        <select class="form-control select2"
                                                name="write" id="write"
                                                required>
                                            <option selected
                                                    disabled>{{__('otherDetails.selectLanguagesKnownWrite')}}</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="speak">{{__('otherDetails.languagesKnownSpeak')}}
                                            <span class="tx-danger">*</span></label>
                                        <select class="form-control select2"
                                                name="speak" id="speak"
                                                required>
                                            <option selected
                                                    disabled>{{__('otherDetails.selectLanguagesKnownSpeak')}}</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="understand">{{__('otherDetails.languagesKnownUnderstand')}}
                                            <span class="tx-danger">*</span></label>
                                        <select class="form-control select2"
                                                name="understand"
                                                id="understand" required>
                                            <option selected
                                                    disabled>{{__('otherDetails.selectLanguagesKnownUnderstand')}}</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 p-0 text-right">
                            <button type="submit"
                                    class="btn btn-primary tx-uppercase">
                                {{__('otherDetails.languagesKnownButtonSubmit')}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->

    <!-- BASIC MODAL -->
    <div id="editLanguagesKnownModal" class="modal fade">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('otherDetails.editLanguagesKnownHeader')}}</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="editLanguagesKnownForm">
                    <input id="employeeId" value="{{$employee->id}}" name="employeeId" type="hidden">
                    <div class="modal-body pd-25">
                        <div class="col-12 p-0">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_language">{{__('otherDetails.languagesKnownLanguage')}}
                                            <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control"
                                               id="edit_language"
                                               name="edit_language"
                                               placeholder="{{__('otherDetails.enterLanguagesKnownLanguage')}}"
                                               required>
                                        <div class="form-group">
                                            <p class="red error"></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_read">{{__('otherDetails.languagesKnownRead')}}
                                            <span class="tx-danger">*</span></label>
                                        <select class="form-control select2"
                                                name="edit_read" id="edit_read"
                                                required>
                                            <option selected
                                                    disabled>{{__('otherDetails.selectLanguagesKnownRead')}}</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_write">{{__('otherDetails.languagesKnownWrite')}}
                                            <span class="tx-danger">*</span></label>
                                        <select class="form-control select2"
                                                name="edit_write"
                                                id="edit_write"
                                                required>
                                            <option selected
                                                    disabled>{{__('otherDetails.selectLanguagesKnownWrite')}}</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_speak">{{__('otherDetails.languagesKnownSpeak')}}
                                            <span class="tx-danger">*</span></label>
                                        <select class="form-control select2"
                                                name="edit_speak"
                                                id="edit_speak"
                                                required>
                                            <option selected
                                                    disabled>{{__('otherDetails.selectLanguagesKnownSpeak')}}</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="form-group">
                                        <label
                                            for="edit_understand">{{__('otherDetails.languagesKnownUnderstand')}}
                                            <span class="tx-danger">*</span></label>
                                        <select class="form-control select2"
                                                name="edit_understand"
                                                id="edit_understand" required>
                                            <option selected
                                                    disabled>{{__('otherDetails.selectLanguagesKnownUnderstand')}}</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 p-0 text-right">
                            <button type="submit"
                                    class="btn btn-primary tx-uppercase">
                                {{__('otherDetails.languagesKnownButtonUpdate')}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
</section>
