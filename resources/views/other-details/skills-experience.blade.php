{{--Skills & Experience--}}
<h3 class="d-none">{{__('otherDetails.skillsExperience')}}</h3>

<section>
    <div class="col-12 text-right p-0">
        <button class="btn btn-primary mb-4"
                id="addSkillsExperience">
            <i class="fa fa-plus mr-2"></i>{{__('otherDetails.addSkillsExperience')}}
        </button>
    </div>

    <div class="table-wrapper">
        <table id="skillsExperienceTable" class="table display nowrap">
            <thead>
            <tr>
                <th>{{__('otherDetails.skillsExperienceDetails')}}</th>
                <th class="text-right">{{__('otherDetails.skillsExperienceActions')}}</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>5 Years Experience</td>
                <td class="text-right">
                    <div class="col-12 p-0">
                        <i class="fa fa-edit cursor-pointer mr-2 font-size-18 text-primary"
                           id="editSkillsExperience"
                           title="{{__('otherDetails.editSkillsExperience')}}"></i>
                        <i class="fa fa-trash cursor-pointer font-size-18 text-danger"
                           id="deleteSkillsExperience"
                           title="{{__('otherDetails.deleteSkillsExperience')}}"></i>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div><!-- table-wrapper -->

    <!-- BASIC MODAL -->
    <div id="addSkillsExperienceModal" class="modal fade">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('otherDetails.addSkillsExperience')}}</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="addSkillsExperienceForm">
                    <input id="employeeId" value="{{$employee->id}}" name="employeeId" type="hidden">
                    <div class="modal-body pd-25">
                        <div class="col-12 p-0">
                            <div class="form-group">
                                <label
                                    for="details">{{__('otherDetails.skillsExperienceDetails')}}
                                    <span class="tx-danger">*</span></label>
                                <textarea class="form-control" rows="5"
                                          id="details"
                                          name="details"
                                          placeholder="{{__('otherDetails.enterSkillsExperienceDetails')}}"
                                          required></textarea>
                            </div>
                        </div>
                        <div class="col-12 p-0 text-right">
                            <button type="submit"
                                    class="btn btn-primary tx-uppercase">
                                {{__('otherDetails.skillsExperienceButtonSubmit')}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->

    <!-- BASIC MODAL -->
    <div id="editSkillsExperienceModal" class="modal fade">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('otherDetails.editSkillsExperienceHeader')}}</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="editSkillsExperienceForm">
                    <input id="employeeId" value="{{$employee->id}}" name="employeeId" type="hidden">
                    <div class="modal-body pd-25">
                        <div class="col-12 p-0">
                            <div class="form-group">
                                <label
                                    for="edit_details">{{__('otherDetails.skillsExperienceDetails')}}
                                    <span class="tx-danger">*</span></label>
                                <textarea class="form-control" rows="5"
                                          id="edit_skills_details"
                                          name="edit_skills_details"
                                          placeholder="{{__('otherDetails.enterSkillsExperienceDetails')}}"
                                          required></textarea>
                            </div>
                        </div>
                        <div class="col-12 p-0 text-right">
                            <button type="submit"
                                    class="btn btn-primary tx-uppercase">
                                {{__('otherDetails.skillsExperienceButtonUpdate')}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
</section>
