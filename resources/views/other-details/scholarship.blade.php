{{--Scholarship / Prizes & Awards--}}
<h3 class="d-none">{{__('otherDetails.scholarship')}}</h3>

<section>
    <div class="col-12 text-right p-0">
        <button class="btn btn-primary mb-4"
                id="addScholarship">
            <i class="fa fa-plus mr-2"></i>{{__('otherDetails.addScholarship')}}
        </button>
    </div>

    <div class="table-wrapper">
        <table id="scholarshipTable" class="table display nowrap">
            <thead>
            <tr>
                <th>{{__('otherDetails.scholarshipDetails')}}</th>
                <th class="text-right">{{__('otherDetails.scholarshipActions')}}</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Sports Competitions</td>
                <td class="text-right">
                    <div class="col-12 p-0">
                        <i class="fa fa-edit cursor-pointer mr-2 font-size-18 text-primary"
                           id="editScholarship"
                           title="{{__('otherDetails.editScholarship')}}"></i>
                        <i class="fa fa-trash cursor-pointer font-size-18 text-danger"
                           id="deleteScholarship"
                           title="{{__('otherDetails.deleteScholarship')}}"></i>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div><!-- table-wrapper -->

    <!-- BASIC MODAL -->
    <div id="addScholarshipModal" class="modal fade">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('otherDetails.addScholarship')}}</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="addScholarshipForm">
                    <input id="employeeId" value="{{$employee->id}}" name="employeeId" type="hidden">
                    <div class="modal-body pd-25">
                        <div class="col-12 p-0">
                            <div class="form-group">
                                <label
                                    for="details">{{__('otherDetails.scholarshipDetails')}}
                                    <span class="tx-danger">*</span></label>
                                <textarea class="form-control" rows="5"
                                          id="details"
                                          name="details"
                                          placeholder="{{__('otherDetails.enterScholarshipDetails')}}"
                                          required></textarea>
                            </div>
                        </div>
                        <div class="col-12 p-0 text-right">
                            <button type="submit"
                                    class="btn btn-primary tx-uppercase">
                                {{__('otherDetails.scholarshipButtonSubmit')}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->

    <!-- BASIC MODAL -->
    <div id="editScholarshipModal" class="modal fade">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">{{__('otherDetails.editScholarshipHeader')}}</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="editScholarshipForm">
                    <input id="employeeId" value="{{$employee->id}}" name="employeeId" type="hidden">
                    <div class="modal-body pd-25">
                        <div class="col-12 p-0">
                            <div class="form-group">
                                <label
                                    for="edit_details">{{__('otherDetails.scholarshipDetails')}}
                                    <span class="tx-danger">*</span></label>
                                <textarea class="form-control" rows="5"
                                          id="edit_details"
                                          name="edit_details"
                                          placeholder="{{__('otherDetails.enterScholarshipDetails')}}"
                                          required></textarea>
                            </div>
                        </div>
                        <div class="col-12 p-0 text-right">
                            <button type="submit"
                                    class="btn btn-primary tx-uppercase">
                                {{__('otherDetails.scholarshipButtonUpdate')}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
</section>
