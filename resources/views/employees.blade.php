@extends('layout.main')
@section('content')
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
        <div class="br-pagebody">
            <div class="br-pagetitle"></div>
            <div class="br-section-wrapper">
                <div class="col-12">
                    <div class="row">
                        <div class="col-6 p-0">
                            <h3 class="m-0 text-dark">{{__('employees.headerTitle')}}</h3>
                        </div>
                        <div class="col-6 p-0 text-right">
                            <a href="{{url('employee/create')}}">
                                <button class="btn btn-primary mb-4 mr-3" id="addEmployee">
                                    <i class="fa fa-plus mr-2"></i>{{__('employees.empAdd')}}
                                </button>
                            </a>

                            <a href="{{url('employee/import')}}">
                                <button class="btn btn-primary mb-4" id="importEmployee">
                                    <i class="fa fa-download mr-2"></i>{{__('employees.empImportExcel')}}
                                </button>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="table-wrapper">
                    <table id="employeeDataTable" class="table display nowrap">
                        <thead>
                        <tr>
                            <th>{{__('employees.empEcode')}}</th>
                            <th>{{__('employees.empFullName')}}</th>
                            <th>{{__('employees.empDepartment')}}</th>
                            <th>{{__('employees.empDesignation')}}</th>
                            <th class="text-right">{{__('employees.empActions')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                       {{-- <tr>
                            <td>E101</td>
                            <td>Test Tester Testing</td>
                            <td>Test Department</td>
                            <td>Writing Test Cases</td>
                            <td class="text-right">
                                <div class="col-12 p-0">
                                    <i class="fa fa-edit cursor-pointer mr-2 font-size-18 text-primary"
                                       id="editEmployee" title="{{__('employees.empEdit')}}"></i>
                                    <i class="fa fa-eye cursor-pointer mr-2 font-size-18 text-info"
                                       id="otherEmployeeDetails" title="{{__('employees.empOtherDetails')}}"
                                       onclick="window.location.href='{{url('other-details')}}'"></i>
                                    <i class="fa fa-trash cursor-pointer font-size-18 text-danger" id="deleteEmployee"
                                       title="{{__('employees.empDelete')}}"></i>
                                </div>
                            </td>
                        </tr>--}}
                        </tbody>
                    </table>
                </div><!-- table-wrapper -->
            </div>
        </div><!-- br-pagebody -->
    </div><!-- br-mainpanel -->
@endsection
