<!-- ########## START: MAIN PANEL ########## -->
<div class="br-mainpanel">
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="javascript:void(0);">Bracket</a>
            <span class="breadcrumb-item active">Blank Page</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="br-pagetitle">
        <i class="icon icon ion-ios-book-outline"></i>
        <div>
            <h4>Blank Page (Icon Menu)</h4>
            <p class="mg-b-0">Introducing Bracket Plus admin template, the most handsome admin template of all time.
            </p>
        </div>
    </div><!-- d-flex -->

    <div class="br-pagebody">

        <!-- start you own content here -->

    </div><!-- br-pagebody -->
</div><!-- br-mainpanel -->
