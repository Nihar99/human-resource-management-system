<!-- ########## START: LEFT PANEL ########## -->
<div class="br-logo"><a href="javascript:void(0);"><span>3io-HRMS</span></a></div>
<div class="br-sideleft sideleft-scrollbar pt-2">
    <ul class="br-sideleft-menu">
        <li class="br-menu-item">
            <a href="javascript:void(0);" class="br-menu-link">
                <i class="menu-item-icon icon ion-ios-home-outline tx-24"></i>
                <span class="menu-item-label op-lg-0-force">Dashboard</span>
            </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
            <a href="javascript:void(0);" class="br-menu-link with-sub">
                <i class="menu-item-icon icon ion-ios-people-outline tx-20"></i>
                <span class="menu-item-label op-lg-0-force">HR</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
                <li class="sub-item"><a href="{{'/departments'}}" class="sub-link">Department</a></li>
                <li class="sub-item"><a href="{{'/designations'}}" class="sub-link">Designation</a></li>
                <li class="sub-item"><a href="{{'/employees'}}" class="sub-link">Employee</a></li>
            </ul>
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
            <a href="{{url('/time-policy')}}" class="br-menu-link">
                <i class="menu-item-icon icon ion-ios-clock-outline tx-24"></i>
                <span class="menu-item-label op-lg-0-force">Time Policy</span>
            </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
            <a href="{{url('/face-train')}}" class="br-menu-link">
                <i class="menu-item-icon icon ion-ios-contact-outline tx-24"></i>
                <span class="menu-item-label op-lg-0-force">Face Train</span>
            </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
            <a href="{{url('/general-holidays')}}" class="br-menu-link">
                <i class="menu-item-icon icon ion-ios-gear-outline tx-24"></i>
                <span class="menu-item-label op-lg-0-force">General Holidays</span>
            </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
            <a href="{{url('/monthly-attendance')}}" class="br-menu-link">
                <i class="menu-item-icon icon ion-ios-time-outline tx-24"></i>
                <span class="menu-item-label op-lg-0-force">Monthly Attendance</span>
            </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
            <a href="javascript:void(0);" class="br-menu-link with-sub">
                <i class="menu-item-icon icon ion-ios-paper-outline tx-20"></i>
                <span class="menu-item-label op-lg-0-force">Leave Management</span>
            </a><!-- br-menu-link -->
            <ul class="br-menu-sub">
                <li class="sub-item"><a href="{{'/leave-management/apply'}}" class="sub-link">Apply Leave</a></li>
                <li class="sub-item"><a href="{{'/leave-management/request'}}" class="sub-link">Request Leave</a></li>
                <li class="sub-item"><a href="{{'/leave-management/history'}}" class="sub-link">Leave Ledger</a></li>
            </ul>
        </li><!-- br-menu-item -->
    </ul><!-- br-sideleft-menu -->
</div><!-- br-sideleft -->
<!-- ########## END: LEFT PANEL ########## -->
