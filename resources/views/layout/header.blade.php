<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Meta -->
    <meta name="description" content="3ioHRMS Admin Panel">
    <meta name="author" content="3iology Tech Solutions LLP">
    <title>{{__('app.name')}}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{URL::asset('css/theme.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/app.css')}}">
</head>

<body class="collapsed-menu">
