@include('layout.header')
@include('layout.leftpanel')
@include('layout.headpanel')
@include('layout.rightpanel')
@yield('content')
@include('layout.footer')