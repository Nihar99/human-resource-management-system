/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./modals');
require('./formssubmit');
switch (window.location.pathname) {
    case '/login/user':
        require('./employeelogin');
        require('./forgotpassword');
        break;
    case '/departments':
        require('./departments');
        break;
    case '/designations':
        require('./designations');
        break;
    case '/employees':
        require('./employees');
        break;
    case '/login/admin':
        require('./loginadmin');
        break;
    case '/employee/create':
        require('./employee/createEmployee');
        break;
    case '/employee/create/'+$('#employeeId').attr("value")+'/edit':
        require('./employee/editEmployee');
        break;
    case '/time-policy':
        require('./timePolicy');
        break;
    case '/face-train':
        require('./faceTrain');
        break;
    case '/general-holidays':
        require('./generalHolidays');
        break;
    case '/monthly-attendance':
        require('./monthlyAttendance');
        break;
    case '/leave-management/apply':
        require('./leaveManagement/applyLeave');
        break;
    case '/leave-management/request':
        require('./leaveManagement/requestLeave');
        break;
    case '/leave-management/history':
        require('./leaveManagement/leaveLedger');
        break;
}


if (window.location.pathname.includes("/other-details")) {
    require('./otherDetails');
}
