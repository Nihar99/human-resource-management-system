$(document).ready(function () {
    $('#wizard2').steps({
        headerTag: 'h3',
        bodyTag: 'section',
        enableAllSteps: true,
        autoFocus: true,
        titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
        stepsOrientation: 1,
        enablePagination: false,
    });
    const employeeId = $('#employeeId').val();
    /*Family Details*/
    const familyDetailsTable = $('#familyDetailsTable');
    const addFamilyDetails = $('#addFamilyDetails');
    const addFamilyDetailsModal = $('#addFamilyDetailsModal');
    const editFamilyDetails = $('#editFamilyDetails');
    const editFamilyDetailsModal = $('#editFamilyDetailsModal');
    const addFamilyDetailsForm = $('#addFamilyDetailsForm');
    const editFamilyDetailsForm  = $('#editFamilyDetailsForm');
    const deleteFamilyDetails = $('#deleteFamilyDetails');
    familyDetailsTable.DataTable({
        responsive: false,
        destroy: true,
        processing: true,
        //serverSide: true,
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page'
        },
        "order": [],
        ajax: '/data-list/family-details/'+employeeId,
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('DataTables', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('DataTables'));
        },
        columns: [
            {data: 'full_name'},
            {data: 'age'},
            {data: 'occupation'},
            {data: 'relationship'},
            {data: 'gender'},
            {data: 'dependant'},
            {data: 'actions'},
        ]
    });


    setTimeout(function () {
        $('.dataTables_length select').select2({minimumResultsForSearch: Infinity});
    }, 500);

    addFamilyDetails.click(function () {
        $.modal(addFamilyDetailsModal);

    });

    $.formsubmitpostbyId('addFamilyDetailsForm', '/family-details', function (response) {
        if (response['success'] !== undefined && response['success'] === true) {
            familyDetailsTable.DataTable().ajax.reload();
            addFamilyDetailsModal.modal('hide');
            $.alertmodal('Family Detail', 'Added Successfully');
            addFamilyDetailsForm.trigger("reset");
            $("#gender").val(null).trigger('change');
            $("#dependant").val(null).trigger('change');
        }
    });

    $(document).on("click", ".editFamilyDetails", function () {


        window.family_id = $(this).parent().attr('data_id');
        console.log(family_id);
        $('#edit_family_name').val($(this).parent().attr('data_name'));
        $('#edit_family_age').val($(this).parent().attr('data_age'));
        $('#edit_family_occupation').val($(this).parent().attr('data_occupation'));
        $('#edit_family_relationship').val($(this).parent().attr('data_relationship'));

        var gender = $(this).parent().attr('data_gender');
        if(gender == 1){ $('#edit_family_gender').val('Male').trigger("change");}
        else{  $('#edit_family_gender').val('Female').trigger("change");}

        var dependant = $(this).parent().attr('data_dependant');
        if(dependant == 1){ $('#edit_family_dependant').val('Yes').trigger("change");}
        else{  $('#edit_family_dependant').val('No').trigger("change");}

        $.modal(editFamilyDetailsModal);

     });
    $.formsubmitput(editFamilyDetailsForm,'/family-details', 'family_id', function (response) {
        if (response.success) {
            familyDetailsTable.DataTable().ajax.reload();
            editFamilyDetailsModal.modal('hide');
            $.alertmodal('Detail', 'Updated Successfully');

            $("#edit_family_gender").val(null).trigger('change');
            $("#edit_family_dependant").val(null).trigger('change');
        }
    });


    $(document).on("click", ".deleteFamilyDetails", function () {
        window.family_id = $(this).parent().attr('data_id');
        $.prompt('Delete Detail', 'Are you sure you want to delete?', true, function () {
            $.formsubmitdel('/family-details', 'family_id', function (response) {
                if (response['success'] !== undefined && response['success'] === true) {
                    $.alertmodal('Detail', 'Deleted Successfully');
                    familyDetailsTable.DataTable().ajax.reload();

                }
            })
        });
    });

    /*Emergency Contact*/
    const emergencyContactTable = $('#emergencyContactTable');
    const addEmergencyContact = $('#addEmergencyContact');
    const addEmergencyContactForm = $('#addEmergencyContactForm');
    const addEmergencyContactModal = $('#addEmergencyContactModal');
    const editEmergencyContactModal = $('#editEmergencyContactModal');
    const editEmergencyContactForm = $('#editEmergencyContactForm');
    emergencyContactTable.DataTable({
        responsive: false,
        destroy: true,
        processing: true,
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page'
        },
        "order": [],
        ajax: '/data-list/emergency-contacts/'+employeeId,
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('DataTables', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('DataTables'));
        },
        columns: [
            {data: 'name'},
            {data: 'relationship'},
            {data: 'address'},
            {data: 'phone'},
            {data: 'actions'},
        ]
    });

    setTimeout(function () {
        $('.dataTables_length select').select2({minimumResultsForSearch: Infinity});
    }, 500);


    addEmergencyContact.click(function () {
        $.modal(addEmergencyContactModal);
    });
    $.formsubmitpost(addEmergencyContactForm, '/emergency-contacts', function (response) {
        if (response['success'] !== undefined && response['success'] === true) {
            emergencyContactTable.DataTable().ajax.reload();
            addEmergencyContactModal.modal('hide');
            $.alertmodal('Emergency Contact', 'Added Successfully');
            addEmergencyContactForm.trigger("reset");
        }
    });

    $(document).on("click", ".deleteEmergencyContact", function () {
        window.contact_id = $(this).parent().attr('data_id');
        $.prompt('Delete Contact', 'Are you sure you want to delete this contact?', true, function () {
            $.formsubmitdel('/emergency-contacts', 'contact_id', function (response) {
                if (response['success'] !== undefined && response['success'] === true) {
                    $.alertmodal('Contact', 'Deleted Successfully');
                    emergencyContactTable.DataTable().ajax.reload();

                }
            })
        });
    });

    $(document).on("click", ".editEmergencyContact", function () {
        window.contact_id = $(this).parent().attr('data_id');
        $('#edit_name').val($(this).parent().attr('data_name'));
        $('#edit_relationship').val($(this).parent().attr('data_relationship'));
        $('#edit_address').val($(this).parent().attr('data_address'));
        $('#edit_phone').val($(this).parent().attr('data_phone'));
        $.modal(editEmergencyContactModal);
    });
    $.formsubmitput(editEmergencyContactForm, '/emergency-contacts', 'contact_id', function (response) {
        if (response.success) {
            emergencyContactTable.DataTable().ajax.reload();
            editEmergencyContactModal.modal('hide');
            $.alertmodal('Contact', 'Updated Successfully');
        }
    });

    /*LanguagesKnown*/
    const languagesKnownTable = $('#languagesKnownTable');
    const addLanguagesKnown = $('#addLanguagesKnown');
    const addLanguagesKnownModal = $('#addLanguagesKnownModal');
    const addLanguagesKnownForm = $('#addLanguagesKnownForm');
    const editLanguagesKnownModal = $('#editLanguagesKnownModal');
    const editLanguagesKnownForm = $('#editLanguagesKnownForm');
    languagesKnownTable.DataTable({
        responsive: false,
        destroy: true,
        processing: true,
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page'
        },
        "order": [],
        ajax: '/data-list/languages-known/'+employeeId,
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('DataTables', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('DataTables'));
        },
        columns: [
            {data: 'name'},
            {data: 'read'},
            {data: 'write'},
            {data: 'speak'},
            {data: 'understand'},
            {data: 'actions'},
        ]
    });

    setTimeout(function () {
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    }, 500);

    addLanguagesKnown.click(function () {
        $.modal(addLanguagesKnownModal);
    });
    $.formsubmitpost(addLanguagesKnownForm, '/languages-known', function (response) {
        if (response['success'] !== undefined && response['success'] === true) {
            languagesKnownTable.DataTable().ajax.reload();
            addLanguagesKnownModal.modal('hide');
            $.alertmodal('Language', 'Added Successfully');
            addLanguagesKnownForm.trigger("reset");

            $("#read").val(null).trigger('change');
            $("#write").val(null).trigger('change');
            $("#speak").val(null).trigger('change');
            $("#understand").val(null).trigger('change');

        }
    });
    $(document).on("click", ".deleteLanguageKnown", function () {
        window.language_id = $(this).parent().attr('data_id');
        $.prompt('Delete Language', 'Are you sure you want to delete this language?', true, function () {
            $.formsubmitdel('/languages-known', 'language_id', function (response) {
                if (response['success'] !== undefined && response['success'] === true) {
                    $.alertmodal('Language', 'Deleted Successfully');
                    languagesKnownTable.DataTable().ajax.reload();

                }
            })
        });
    });
    $(document).on("click", ".editLanguageKnown", function () {
        window.language_id = $(this).parent().attr('data_id');
        $('#edit_language').val($(this).parent().attr('data_name'));
        console.log($(this).parent().attr('data_understand'));
        var read ;($(this).parent().attr('data_read') == 1) ?  read = 'Yes' : read = 'No';
        $('#edit_read').val(read).trigger("change");
        var write = $(this).parent().attr('data_write');
        if(write == 1){ $('#edit_write').val('Yes').trigger("change");}
        else{  $('#edit_write').val('No').trigger("change");}
        var speak = $(this).parent().attr('data_speak');
        if(speak == 1){$('#edit_speak').val('Yes').trigger("change");}
        else{ $('#edit_speak').val('No').trigger("change");}
        var understand = $(this).parent().attr('data_understand');
        if(understand == 1){ $('#edit_understand').val('Yes').trigger("change");}
        else{ $('#edit_understand').val('No').trigger("change");}
        $.modal(editLanguagesKnownModal);
    });
    $.formsubmitput(editLanguagesKnownForm, '/languages-known', 'language_id', function (response) {
        if (response.success) {
            languagesKnownTable.DataTable().ajax.reload();
            editLanguagesKnownModal.modal('hide');
            $.alertmodal('Language', 'Updated Successfully');
        }
    });


    /*Educational Profile*/
    const educationalProfileTable = $('#educationalProfileTable');
    const addEducationalProfile = $('#addEducationalProfile');
    const addEducationalProfileModal = $('#addEducationalProfileModal');
    const editEducationalProfile = $('#editEducationalProfile');
    const editEducationalProfileModal = $('#editEducationalProfileModal');
    const deleteEducationalProfile = $('#deleteEducationalProfile');
    const addEducationalProfileForm = $('#addEducationalProfileForm');
    const editEducationalProfileForm = $('#editEducationalProfileForm');
    educationalProfileTable.DataTable({
        responsive: false,
        destroy: true,
        processing: true,
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page'
        },
        "order": [],
        ajax: '/data-list/educational-profiles/'+employeeId,
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('DataTables', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('DataTables'));
        },
        columns: [
            {data: 'board_degree'},
            {data: 'year_of_passing'},
            {data: 'institution_name'},
            {data: 'principle_subjects'},
            {data: 'grade_percentage'},
            {data: 'actions'},
        ]
    });

    setTimeout(function () {
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    }, 500);

    addEducationalProfile.click(function () {
        $.modal(addEducationalProfileModal);
    });
    $.formsubmitpost(addEducationalProfileForm, '/educational-profiles', function (response) {
        if (response['success'] !== undefined && response['success'] === true) {
            educationalProfileTable.DataTable().ajax.reload();
            addEducationalProfileModal.modal('hide');
            $.alertmodal('Educational Profile', 'Added Successfully');
            addEducationalProfileForm.trigger("reset");
        }
    });
    $(document).on("click", ".editEducationalProfile", function () {
        window.profileId = $(this).parent().attr('data_id');
        $('#edit_board_degree').val($(this).parent().attr('data_board_degree'));
        $('#edit_year_of_passing').val($(this).parent().attr('data_year_of_passing'));
        $('#edit_institution_name').val($(this).parent().attr('data_institution_name'));
        $('#edit_principle_subjects').val($(this).parent().attr('data_principle_subjects'));
        $('#edit_grade_percentage').val($(this).parent().attr('data_grade_percentage'));
        $.modal(editEducationalProfileModal);
    });
    $.formsubmitput(editEducationalProfileForm, '/educational-profiles', 'profileId', function (response) {
        if (response.success) {
            educationalProfileTable.DataTable().ajax.reload();
            editEducationalProfileModal.modal('hide');
            $.alertmodal('Educational Profile', 'Updated Successfully');
        }
    });
    $(document).on("click", ".deleteEducationalProfile", function () {
        window.profileId = $(this).parent().attr('data_id');
        $.prompt('Delete Educational Profile', 'Are you sure you want to delete this Profile?', true, function () {
            $.formsubmitdel('/educational-profiles', 'profileId', function (response) {
                if (response['success'] !== undefined && response['success'] === true) {
                    $.alertmodal('Profile', 'Deleted Successfully');
                    educationalProfileTable.DataTable().ajax.reload();

                }
            })
        });
    });

    /*Scholarship / Prizes & Awards*/
    const scholarshipTable = $('#scholarshipTable');
    const addScholarship = $('#addScholarship');
    const addScholarshipModal = $('#addScholarshipModal');
    const editScholarshipModal = $('#editScholarshipModal');
    const editScholarshipForm = $('#editScholarshipForm');
    const addScholarshipForm = $('#addScholarshipForm');

    scholarshipTable.DataTable({
        responsive: false,
        destroy: true,
        processing: true,
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page'
        },
        "order": [],
        ajax: '/data-list/scholarships-prizes/'+employeeId,
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('DataTables', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('DataTables'));
        },
        columns: [
            {data: 'details'},
            {data: 'actions'},
        ]
    });

    setTimeout(function () {
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    }, 500);

    addScholarship.click(function () {
        $.modal(addScholarshipModal);
    });
    $.formsubmitpost(addScholarshipForm, '/scholarships-prizes', function (response) {
        if (response['success'] !== undefined && response['success'] === true) {
            scholarshipTable.DataTable().ajax.reload();
            addScholarshipModal.modal('hide');
            $.alertmodal('Scholarship/Prize', 'Added Successfully');
            addScholarshipForm.trigger("reset");
        }
    });
    $(document).on("click", ".editScholarship", function () {
        window.scholarshipId = $(this).parent().attr('data_id');
        $('#edit_details').val($(this).parent().attr('data_details'));
        $.modal(editScholarshipModal);
    });
    $.formsubmitput(editScholarshipForm, '/scholarships-prizes', 'scholarshipId', function (response) {
        if (response.success) {
            scholarshipTable.DataTable().ajax.reload();
            editScholarshipModal.modal('hide');
            $.alertmodal('Scholarship/Prize', 'Updated Successfully');
        }
    });
    $(document).on("click", ".deleteScholarship", function () {
        window.scholarshipId = $(this).parent().attr('data_id');
        $.prompt('Delete Scholarship/Prize', 'Are you sure you want to delete?', true, function () {
            $.formsubmitdel('/scholarships-prizes', 'scholarshipId', function (response) {
                if (response['success'] !== undefined && response['success'] === true) {
                    $.alertmodal('Scholarship/Prize', 'Deleted Successfully');
                    scholarshipTable.DataTable().ajax.reload();

                }
            })
        });
    });

    /*Exposure to Computer*/
    const exposureComputerTable = $('#exposureComputerTable');
    const addExposureComputer = $('#addExposureComputer');
    const addExposureComputerModal = $('#addExposureComputerModal');
    const editExposureComputerModal = $('#editExposureComputerModal');
    const addExposureComputerForm = $('#addExposureComputerForm');
    const editExposureComputerForm = $('#editExposureComputerForm');
    exposureComputerTable.DataTable({
        responsive: false,
        destroy: true,
        processing: true,
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page'
        },
        "order": [],
        ajax: '/data-list/exposure-computer/'+employeeId,
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('DataTables', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('DataTables'));
        },

        columns: [
            {data: 'language'},
            {data: 'operating_system'},
            {data: 'application'},
            {data: 'hardware'},
            {
                data: null, render: function (data) {
                    return "<div class='text-right' data='"+ JSON.stringify(data) +"'>\n" +
                        '<a href="javascript:void(0);" class="editComputerExposure" title="Edit"><i class="fa fa-edit cursor-pointer mr-2 font-size-18 text-primary"></i></a>\n' +
                        '<a href="javascript:void(0);" class="deleteComputerExposure" title="Delete"><i class="fa fa-trash cursor-pointer mr-2 font-size-18 text-danger"></i></a>\n' +
                        '</div>';
                }, name: 'id',searchable: false
            }
        ]
    });

    setTimeout(function () {
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    }, 500);

    addExposureComputer.click(function () {
        $.modal(addExposureComputerModal);
    });
    $.formsubmitpost(addExposureComputerForm, '/exposure-computer', function (response) {
        if (response['success'] !== undefined && response['success'] === true) {
            exposureComputerTable.DataTable().ajax.reload();
            addExposureComputerModal.modal('hide');
            $.alertmodal('Computer Skill', 'Added Successfully');
            addExposureComputerForm.trigger("reset");
        }
    });
    $(document).on("click", ".editComputerExposure", function () {
        let data=JSON.parse($(this).parent().attr('data'));
        window.exposureId = data.id;
        $('#edit_exposure_language').val(data.language);
        $('#edit_exposure_operating_system').val(data.operating_system);
        $('#edit_exposure_application').val(data.application);
        $('#edit_exposure_hardware').val(data.hardware);
        $.modal(editExposureComputerModal);
    });
    $.formsubmitput(editExposureComputerForm, '/exposure-computer', 'exposureId', function (response) {
        if (response.success) {
            exposureComputerTable.DataTable().ajax.reload();
            editExposureComputerModal.modal('hide');
            $.alertmodal('Computer Exposure', 'Updated Successfully');
        }
    });
    $(document).on("click", ".deleteComputerExposure", function () {
        let data=JSON.parse($(this).parent().attr('data'));
        window.exposureId = data.id;
        $.prompt('Delete Exposure', 'Are you sure you want to delete?', true, function () {
            $.formsubmitdel('/exposure-computer', 'exposureId', function (response) {
                if (response['success'] !== undefined && response['success'] === true) {
                    $.alertmodal('Computer Exposure', 'Deleted Successfully');
                    exposureComputerTable.DataTable().ajax.reload();

                }
            })
        });
    });

    /*Skills & Experience*/
    const skillsExperienceTable = $('#skillsExperienceTable');
    const addSkillsExperience = $('#addSkillsExperience');
    const addSkillsExperienceModal = $('#addSkillsExperienceModal');
    const editSkillsExperienceModal = $('#editSkillsExperienceModal');
    const addSkillsExperienceForm = $('#addSkillsExperienceForm');
    const editSkillsExperienceForm = $('#editSkillsExperienceForm');
    skillsExperienceTable.DataTable({
        responsive: false,
        destroy: true,
        processing: true,
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page'
        },
        "order": [],
        ajax: '/data-list/skills-experiences/'+employeeId,
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('DataTables', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('DataTables'));
        },
        columns: [
            {data: 'details'},
            {data: 'actions'},
        ]
    });

    setTimeout(function () {
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    }, 500);

    addSkillsExperience.click(function () {
        $.modal(addSkillsExperienceModal);
    });
    $.formsubmitpost(addSkillsExperienceForm, '/skills-experiences', function (response) {
        if (response['success'] !== undefined && response['success'] === true) {
            skillsExperienceTable.DataTable().ajax.reload();
            addSkillsExperienceModal.modal('hide');
            $.alertmodal('Skill', 'Added Successfully');
            addSkillsExperienceForm.trigger("reset");
        }
    });
    $(document).on("click", ".editSkills", function () {
        window.skillsId = $(this).parent().attr('data_id');;
        $('#edit_skills_details').val($(this).parent().attr('data_details'));
        $.modal(editSkillsExperienceModal);
    });
    $.formsubmitput(editSkillsExperienceForm, '/skills-experiences', 'skillsId', function (response) {
        if (response.success) {
            skillsExperienceTable.DataTable().ajax.reload();
            editSkillsExperienceModal.modal('hide');
            $.alertmodal('Skill/Experiences', 'Updated Successfully');
        }
    });
    $(document).on("click", ".deleteSkills", function () {
        window.skillsId = $(this).parent().attr('data_id');
        $.prompt('Delete Skill/Experience', 'Are you sure you want to delete?', true, function () {
            $.formsubmitdel('/skills-experiences', 'skillsId', function (response) {
                if (response['success'] !== undefined && response['success'] === true) {
                    $.alertmodal('Skill/Experience', 'Deleted Successfully');
                    skillsExperienceTable.DataTable().ajax.reload();

                }
            })
        });
    });

    /*Work Experience*/
    const workExperienceTable = $('#workExperienceTable');
    const addWorkExperienceForm = $('#addWorkExperienceForm');
    const addWorkExperience = $('#addWorkExperience');
    const addWorkExperienceModal = $('#addWorkExperienceModal');
    const editWorkExperience = $('#editWorkExperienceDetails');
    const editWorkExperienceForm = $('#editWorkExperienceForm');
    const editWorkExperienceModal = $('#editWorkExperienceModal');
    const deleteWorkExperience = $('#deleteWorkExperience');
    const btnSubmitWorkExperience = $('#btnSubmitWorkExperience');
    const employee_id = $("#employee_id").val();
    workExperienceTable.DataTable({
        responsive: false,
        destroy: true,
        processing: true,
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page'
        },
        "order": [],

        ajax: '/data-list/work-experience/'+employee_id,
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('DataTables', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('DataTables'));
        },
        columns: [
            {data: 'organisation_name'},
            {data: 'organisation_address'},
            {data: 'from_date'},
            {data: 'to_date'},
            {data: 'position_held'},
            // {data: 'reporting_to'},
            // {data: 'nature_of_work'},
            // {data: 'reason_of_leaving'},
            // {data: 'gross_annual'},
            {data: 'actions'},
        ]
    });

    $.formsubmitpost(addWorkExperienceForm, '/work-experience', function (response) {
        if (response['success'] !== undefined && response['success'] === true) {
            workExperienceTable.DataTable().ajax.reload();
            addWorkExperienceModal.modal('hide');
            $.alertmodal('Work Experience', 'Added Successfully');
            addWorkExperienceForm.trigger("reset");
        }
    });

    $.formsubmitput(editWorkExperienceForm, '/work-experience', 'experienceId', function (response) {
        if (response.success) {
            workExperienceTable.DataTable().ajax.reload();
            editWorkExperienceModal.modal('hide');
            editWorkExperienceForm.trigger("reset");
            $.alertmodal('Educational Profile', 'Updated Successfully');
        }
    });

    setTimeout(function () {
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    }, 500);

    $('.fc-datepicker').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true
    });

    addWorkExperience.click(function () {
        $.modal(addWorkExperienceModal);
    });

    $(document).on('click','.editWorkExperienceDetails',function(){
        window.experienceId = $(this).parent().attr('data_id');
        $.formFetchData('/work-experience', 'experienceId', function (response) {
            console.log(response);
            if (response['success'] !== undefined && response['success'] === true) {
                var data = response['data']
                $("#edit_work_experience_organisation_name").val(data.organisation_name);
                $("#edit_work_experience_organisation_address").val(data.organisation_address);
                $("#edit_work_experience_from_date").val(data.from_date);
                $("#edit_work_experience_to_date").val(data.to_date);
                $("#edit_work_experience_position_held").val(data.position_held);
                $("#edit_work_experience_reporting_to").val(data.reporting_to);
                $("#edit_work_experience_nature_work").val(data.nature_of_work);
                $("#edit_work_experience_leaving_reason").val(data.reason_of_leaving);
                $("#edit_work_experience_annual_gross").val(data.gross_annual);
            }
        })
        $.modal(editWorkExperienceModal);
    })

    // deleteWorkExperience.click(function () {
    //     window.experienceId = $(this).parent().attr('data_id');
    //     $.prompt('Delete Work Experience', 'Are you sure you want to delete?', true, function () {
    //         $.formsubmitdel('/work-experience', 'experienceId', function (response) {
    //             if (response['success'] !== undefined && response['success'] === true) {
    //                 $.alertmodal('Work Experience', 'Deleted Successfully');
    //                 workExperienceTable.DataTable().ajax.reload();

    //             }
    //         })

    //     });
    // });

    $(document).on('click','.deletetWorkExperienceDetails',function(){
        window.experienceId = $(this).parent().attr('data_id');
        $.prompt('Delete Work Experience', 'Are you sure you want to delete?', true, function () {
            $.formsubmitdel('/work-experience', 'experienceId', function (response) {
                if (response['success'] !== undefined && response['success'] === true) {
                    $.alertmodal('Work Experience', 'Deleted Successfully');
                    workExperienceTable.DataTable().ajax.reload();

                }
            });
        });
    });

    /*Referred By*/
    const referredByTable = $('#referredByTable');
    const addReferredBy = $('#addReferredBy');
    const addReferredByModal = $('#addReferredByModal');
    const editReferredByModal = $('#editReferredByModal');
    const addReferredByForm = $('#addReferredByForm');
    const editReferredByForm = $('#editReferredByForm');
    /*    const referredByEmployeeId = $('#employeeId').val();*/
    referredByTable.DataTable({
        responsive: false,
        destroy: true,
        processing: true,
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page'
        },
        "order": [],
        ajax: '/data-list/referred-by/'+employeeId,
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('DataTables', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings,oData) {
            return JSON.parse(localStorage.getItem('DataTables'));
        },
        columns: [
            {data: 'name'},
            {data: 'position'},
            {data: 'address'},
            {data: 'tel_no'},
            {
                data: null, render: function (data) {
                    return "<div class='text-right' data='"+ JSON.stringify(data) +"'>\n" +
                        '<a href="javascript:void(0);" class="editReferredBy" title="Edit"><i class="fa fa-edit cursor-pointer mr-2 font-size-18 text-primary"></i></a>\n' +
                        '<a href="javascript:void(0);" class="deleteReferredBy" title="Delete"><i class="fa fa-trash cursor-pointer mr-2 font-size-18 text-danger"></i></a>\n' +
                        '</div>';
                }, name: 'id',searchable: false
            }
        ],
        "initComplete": function(settings, json){
            var info = this.api().page.info();
            if(info.recordsTotal > 0){
                $("#addReferredBy").addClass('d-none');
            }else{
                $("#addReferredBy").removeClass('d-none');
            }
        }
    });

    addReferredBy.click(function () {
        $.modal(addReferredByModal);
    });

    $.formsubmitpost(addReferredByForm, '/referred-by', function (response) {
        if (response['success'] !== undefined && response['success'] === true) {
            referredByTable.DataTable().ajax.reload();
            addReferredByModal.modal('hide');
            $.alertmodal('Referred By', 'Added Successfully');
            $("#addReferredBy").addClass('d-none');
            addReferredByForm.trigger("reset");
        }
    });

    $(document).on("click", ".editReferredBy", function () {
        let data=JSON.parse($(this).parent().attr('data'));
        window.referredById = data.id;
        $('#edit_referred_by_name').val(data.name);
        $('#edit_referred_by_position').val(data.position);
        $('#edit_referred_by_address').val(data.address);
        $('#edit_referred_by_telephone').val(data.tel_no);
        $.modal(editReferredByModal);
    });

    $.formsubmitput(editReferredByForm, '/referred-by', 'referredById', function (response) {
        if (response.success) {
            referredByTable.DataTable().ajax.reload();
            editReferredByModal.modal('hide');
            $.alertmodal('Referred By', 'Updated Successfully');
        }
    });

    $(document).on("click", ".deleteReferredBy", function () {
        let data=JSON.parse($(this).parent().attr('data'));
        window.referredById = data.id;
        $.prompt('Delete Referred By', 'Are you sure you want to delete?', true, function () {
            $.formsubmitdel('/referred-by', 'referredById', function (response) {
                if (response['success'] !== undefined && response['success'] === true) {
                    $.alertmodal('Referred By', 'Deleted Successfully');
                    $("#addReferredBy").removeClass('d-none');
                    referredByTable.DataTable().ajax.reload();
                }
            })
        });
    });
});
