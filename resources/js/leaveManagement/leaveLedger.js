$(document).ready(function () {
    const leaveLedgerDataTable = $('#leaveLedgerDataTable');
    const leaveLedgerDetails = $('#leaveLedgerDetails');
    const leaveLedgerDetailsModal = $('#leaveLedgerDetailsModal');
    const leaveLedgerDetailsDataTable = $('#leaveLedgerDetailsDataTable');


    leaveLedgerDataTable.DataTable({
        responsive: false,
        scrollX: true,
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page'
        },
        "order": [],
        ajax: '/data-list/leave-ledger',
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('DataTables', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('DataTables'));
        },
         columns: [
            {data : 'ecode'},
            {data: 'full_name'},
            {data : 'department'},
            {data: 'designation'},
            {data: 'casual_leave'},
            {data: 'privilege_leave'},
            {data: 'sick_leave'},
            {data: 'maternity_leave'},
            { data: 'actions'},
         ],
         "initComplete": function(settings, json) {
             console.log('hii');
            $('div.dataTables_scrollBody table thead .sorting').removeClass('sorting');
            $('div.dataTables_scrollBody table thead .sorting_asc').removeClass('sorting_asc');
            $('div.dataTables_scrollBody table thead .sorting_desc').removeClass('sorting_desc');
        },
        "drawCallback": function( settings ) {
            $('div.dataTables_scrollBody table thead .sorting').removeClass('sorting');
            $('div.dataTables_scrollBody table thead .sorting_asc').removeClass('sorting_asc');
            $('div.dataTables_scrollBody table thead .sorting_desc').removeClass('sorting_desc');
        }
        });

    $(document).on("click", ".leaveLedgerDetails", function (){

        const employeeId = $(this).parent().attr('data_id');
       leaveLedgerDetailsDataTable.DataTable({
        destroy: true,
        responsive: false,
        scrollX: true,
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page'
        },
        ajax : '/data-list/leaveLedgerDetails/'+employeeId,
        "order": [],
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('DataTables', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('DataTables'));
        },
        columns : [
            {data : 'leave_type'},
            {data : 'policy_from'},
            {data : 'policy_to'},
            {data : 'no_of_leaves'},
            {data : 'added_removed'},
        ],
        "initComplete": function(settings, json) {
           $('div.dataTables_scrollBody table thead .sorting').removeClass('sorting');
           $('div.dataTables_scrollBody table thead .sorting_asc').removeClass('sorting_asc');
           $('div.dataTables_scrollBody table thead .sorting_desc').removeClass('sorting_desc');
       },
       "drawCallback": function( settings ) {
           $('div.dataTables_scrollBody table thead .sorting').removeClass('sorting');
           $('div.dataTables_scrollBody table thead .sorting_asc').removeClass('sorting_asc');
           $('div.dataTables_scrollBody table thead .sorting_desc').removeClass('sorting_desc');
       }
        })
        $.modal(leaveLedgerDetailsModal);
    });


    setTimeout(function () {
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    }, 500);

    $('.fc-datepicker').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true
    });

});
