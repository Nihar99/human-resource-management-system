$(document).ready(function () {

    const applyLeaveForm = $('#applyLeaveForm');

    $('.fc-datepicker').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        minDate: new Date(),
        maxDate: '+1Y',
    });

    $(document).on('change','#apply_leave_is_multiple',function(){
        $(".singleDateBody").toggleClass('d-none');
        $(".fromDateBody").toggleClass('d-none');
        $(".toDateBody").toggleClass('d-none');
        $(".toTotalLeaves").toggleClass('d-none d-flex');
        if($(this).val()==1) {
            $("#apply_leave_is_half_day").prop('checked', false);
        }
    });

    $(document).on('change','#apply_leave_management_from_date',function(){
        $('#apply_leave_management_to_date').datepicker('destroy');
        const minDate = stringToDate($(this).val(),"MM/dd/yyyy",'/');
        $('#apply_leave_management_to_date').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            minDate: minDate,
            maxDate: '+1Y',
        });
    });

    // convert string date to date in js
    function stringToDate(_date,_format,_delimiter)
    {
        var formatLowerCase=_format.toLowerCase();
        var formatItems=formatLowerCase.split(_delimiter);
        var dateItems=_date.split(_delimiter);
        var monthIndex=formatItems.indexOf("mm");
        var dayIndex=formatItems.indexOf("dd");
        var yearIndex=formatItems.indexOf("yyyy");
        var month=parseInt(dateItems[monthIndex]);
        month-=1;
        var formatedDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
        return formatedDate;
    }

    function inputErrorCheck(element){

        if(element.val()=="" || element.val()==null){
            element.focus();
            return true;
        }else{
            return false;
        }
    }

    // $(document).on("focus", ".select2", function (e) {
    //     if (e.originalEvent) {
    //       var s2element = $(this).siblings("select:enabled");
    //       s2element.select2("open");
    //       // Set focus back to select2 element on closing.
    //       s2element.on("select2:closing", function () {
    //         if (s2element.val()) s2element.select2("focus");
    //       });
    //     }
    //   });


    $(document).on('click','#btnSubmitApplyLeave',function(){

        const apply_leave_is_multiple = $("#apply_leave_is_multiple").val();
        const apply_leave_management_date = $("#apply_leave_management_date");
        const apply_leave_management_from_date = $("#apply_leave_management_from_date");
        const apply_leave_management_to_date = $("#apply_leave_management_to_date");
        const apply_leave_management_select_emp = $("#apply_leave_management_select_emp");
        const apply_leave_management_leave_type = $("#apply_leave_management_leave_type");
        const admin = $("#checkAdmin").val();
        const hr = $("#checkHR").val();
        if(apply_leave_is_multiple==1){
            if(inputErrorCheck(apply_leave_management_from_date)){
                $("#apply_leave_management_from_date").focus();
                return false;
            }

            if(inputErrorCheck(apply_leave_management_to_date)){
                $("#apply_leave_management_to_date").focus();
                return false;
            }

        }else{
            //if(inputErrorCheck(apply_leave_management_date))
            if(apply_leave_management_date.val()==""){
                $("#apply_leave_management_date").focus();
                return false;
            }
        }

        if(inputErrorCheck(apply_leave_management_leave_type)){
            return false;
        }

        if(admin == 1 || hr==1){
            if(apply_leave_management_select_emp.val()==""){
                $("#apply_leave_management_select_emp").focus();
                return false;
            }
        }

        $.formsubmitpost(applyLeaveForm, '/leave-management/apply', function (response) {
            if (response['success'] !== undefined && response['success'] === true) {
                $.alertmodal('Leave', 'Applied Successfully');
                applyLeaveForm[0].reset();
                return false;
            }else if(response['failed'] !== undefined && response['failed'] === true){
                $.alertmodal('Error', response['msg']);
                return false;
            }
            return true;

        });
    });


    $(document).on('change','#apply_leave_management_to_date',function(){
        var leaveSelected = $("#apply_leave_management_leave_type").val();
        var toDate = $(this).val();
        var fromDate = $("#apply_leave_management_from_date").val();
        var from = moment(fromDate,'M/D/YYYY');
        var to = moment(toDate,'M/D/YYYY');
        var diffDays = to.diff(from, 'days');
        console.log(diffDays);
        $("#total_leaves").html(diffDays+1);

    })

});
