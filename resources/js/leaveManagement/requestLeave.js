$(document).ready(function () {

    const requestLeaveDataTable = $('#requestLeaveDataTable');
    const requestUserLeaveDataTable = $('#requestUserLeaveDataTable');
    const acceptLeaveRequest = $('#acceptLeaveRequest');
    const acceptLeaveRequestModal = $('#acceptLeaveRequestModal');
    const rejectLeaveRequest = $('#rejectLeaveRequest');
    const leaveStatusForm = $("#leaveStatusForm");
    requestLeaveDataTable.DataTable({
        responsive: false,
        destroy:false,
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page'
        },
        "order": [],
        ajax: '/data-list/applied-leaves/0',
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('DataTables', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('DataTables'));
        },
        columns:[
            {data: 'ecode'},
            {
                data: 'first_name',render:function(data,type,row){
                    return row.first_name + " " + row.last_name
                }
            },
            {data: 'deptName'},
            {data: 'designName'},
            {
                data: 'leave',render:function(data,type,row){
                    return moment(row.from_date,'YYYY/M/D').format('LL') + "  -  " + moment(row.to_date,'YYYY/M/D').format('LL')
                }
            },
            {
                data: 'days',render:function(data,type,row){
                    if(row.is_half_day==1){
                        return 0.5;
                    }else {
                        var from = moment(row.from_date, 'YYYY/M/D');
                        var to = moment(row.to_date, 'YYYY/M/D');
                        var diffDays = to.diff(from, 'days');
                        return (diffDays + 1);
                    }
                }
            },
            {
                data: 'reason',render:function(data,type,row){

                    return row.reason.slice(0, 50)
                }
            },
            {
                data: 'approved',render:function(data,type,row){

                    if(data==2){
                        return "Pending";
                    }else if(data==0){
                        return "Rejected";
                    }else if(data==1){
                        return "Approved";
                    }

                }
            },
            {data: 'actions'},
        ]
    });

    $('.fc-datepicker').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        minDate: new Date(),
        maxDate: '+1Y',
    });

    setTimeout(function () {
        $('.dataTables_length select').select2({minimumResultsForSearch: Infinity});
    }, 500);


    rejectLeaveRequest.click(function () {
        $.prompt('Reject Leave', 'Are you sure you want to reject leave application?', true, function () {
            $.alertmodal('Reject Leave', 'Leave Reject Successfully');
        });
    });

    $(document).on('click','#actionLeaveRequest',function(){
        let leave_id = $(this).parent().attr('data_id');
        formData = {};
        formData['leave_id'] = leave_id;
        $.formAjaxRequest(formData,'/leave-management/fetchLeave',(response)=>{
            $("#apply_leave_management_leave_type").val(response.data.leave_master_id).trigger('change');
            $("#apply_leave_management_from_date").val(response.data.from_date);
            $("#apply_leave_management_to_date").val(response.data.to_date);
            $("#leave_id").val(response.data.id);
            $("#reason").val(response.data.reason);

            if(response.data.is_half_day==1){
                $("#total_leaves").val(0.5);
            }else {
                var from = moment(response.data.from_date, 'YYYY/M/D');
                var to = moment(response.data.to_date, 'YYYY/M/D');
                var diffDays = to.diff(from, 'days');
                $("#total_leaves").val(diffDays+1);
            }

            if(response.data.from_date==response.data.to_date){
                $("#apply_leave_is_multiple").val(0).trigger('change');
                $("#apply_leave_management_date").val(response.data.from_date)
            }else{
                $("#apply_leave_is_multiple").val(1).trigger('change');

            }
            if(response.data.approved==1){
                $(".leaveRequestButtons").addClass('d-none');
            }else{
                $(".leaveRequestButtons").removeClass('d-none');
            }
            requestUserLeaveDataTable.DataTable({
                responsive: false,
                destroy:true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page'
                },
                "order": [],
                ajax: '/data-list/applied-leaves/'+response.data.employee_id,
                "bStateSave": true,
                "fnStateSave": function (oSettings, oData) {
                    localStorage.setItem('DataTables', JSON.stringify(oData));
                },
                "fnStateLoad": function (oSettings) {
                    return JSON.parse(localStorage.getItem('DataTables'));
                },
                columns:[
                    {
                        data: 'leave',render:function(data,type,row){
                            return moment(row.from_date,'YYYY/M/D').format('LL') + "  -  " + moment(row.to_date,'YYYY/M/D').format('LL')
                        }
                    },
                    {
                        data: 'days',render:function(data,type,row){
                            if(row.is_half_day==1){
                                return 0.5;
                            }else {
                                var from = moment(row.from_date, 'YYYY/M/D');
                                var to = moment(row.to_date, 'YYYY/M/D');
                                var diffDays = to.diff(from, 'days');
                                return (diffDays + 1);
                            }
                        }
                    },
                    {
                        data: 'reason',render:function(data,type,row){

                            return row.reason.slice(0, 50)
                        }
                    },
                    {
                        data: 'approved',render:function(data,type,row){

                            if(data==2){
                                return "Pending";
                            }else if(data==0){
                                return "Rejected";
                            }else if(data==1){
                                return "Approved";
                            }

                        }
                    }
                ]
            });

            $.modal(acceptLeaveRequestModal);

        })

    })

    $(document).on('change','#apply_leave_is_multiple',function(){
        if($(this).val()==1) {
            $(".singleDateBody").addClass('d-none');
            $(".fromDateBody").removeClass('d-none');
            $(".toDateBody").removeClass('d-none');
            $(".toTotalLeaves").removeClass('d-none d-flex');
        }else{
            $(".singleDateBody").removeClass('d-none');
            $(".fromDateBody").addClass('d-none');
            $(".toDateBody").addClass('d-none');
            $(".toTotalLeaves").addClass('d-none d-flex');
        }
    });

    $(document).on('change','#apply_leave_management_from_date',function(){
        $('#apply_leave_management_to_date').datepicker('destroy');
        const minDate = stringToDate($(this).val(),"MM/dd/yyyy",'/');
        $('#apply_leave_management_to_date').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            minDate: minDate,
            maxDate: '+1Y',
        });
    });

    // convert string date to date in js
    function stringToDate(_date,_format,_delimiter)
    {
        var formatLowerCase=_format.toLowerCase();
        var formatItems=formatLowerCase.split(_delimiter);
        var dateItems=_date.split(_delimiter);
        var monthIndex=formatItems.indexOf("mm");
        var dayIndex=formatItems.indexOf("dd");
        var yearIndex=formatItems.indexOf("yyyy");
        var month=parseInt(dateItems[monthIndex]);
        month-=1;
        var formatedDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
        return formatedDate;
    }

    $(document).on('change','#apply_leave_management_to_date',function(){
        var leaveSelected = $("#apply_leave_management_leave_type").val();
        var toDate = $(this).val();
        var fromDate = $("#apply_leave_management_from_date").val();
        var from = moment(fromDate,'M/D/YYYY');
        var to = moment(toDate,'M/D/YYYY');
        var diffDays = to.diff(from, 'days');
        console.log(diffDays);
        $("#total_leaves").val(diffDays+1);

    })

    $(document).on('click','.btnLeaveStatus',function(){
        let status = $(this).attr('status');
        $("#leave_status").val(status);
        let site_url = "";
        if(status=="accept"){
            site_url = "/leave-management/accept-leave"
        }else{
            site_url = "/leave-management/reject-leave"
        }
        $.formsubmitpost(leaveStatusForm, site_url, function (response) {
            if (response['success'] !== undefined && response['success'] === true) {
                if(status == "accept"){
                $.alertmodal('Leave', 'Accepted Successfully');
                leaveStatusForm[0].reset();
                setTimeout(function() {  window.location.reload(); }, 2000);
                    return false;
                }
                else
                {
                    $.alertmodal('Leave', 'Rejected Successfully');
                leaveStatusForm[0].reset();
                setTimeout(function() {  window.location.reload(); }, 2000);
                    return false;
                }
            }else if(response['failed'] !== undefined && response['failed'] === true){
                $.alertmodal('Error', response['msg']);
                return false;
            }
            return true;

        });
    })


});
