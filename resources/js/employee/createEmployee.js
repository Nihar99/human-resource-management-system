$(document).ready(function () {
    $(document).on('click','.employee_image', () => {
        $('.employee_image_select').trigger('click');
    });

    $(document).on("change", "#employee_image", function (evt) {
        var files = evt.target.files;
        var file = files[0];

        if (file) {
            var formData = new FormData();
            formData.append('file', file);
            var reader = new FileReader();
            reader.onload = function(e) {
                document.getElementById('employee_image_preview').src = e.target.result;
            };
            reader.readAsDataURL(file);
        }
    });


    const employeeDataTable = $('#employeeDataTable');
    $('#wizard2').steps({
        headerTag: 'h3',
        bodyTag: 'section',
        autoFocus: true,
        titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
        onStepChanging: function (event, currentIndex, newIndex) {
            if (currentIndex < newIndex) {
                // Step 1 form validation
                if (currentIndex === 0) {
                    var fname = $('#first_name').parsley();
                    if (!fname.isValid()) {
                        fname.validate();
                    }
                    var lname = $('#last_name').parsley();
                    if (!lname.isValid()) {
                        lname.validate();
                    }

                    var gender = $('#gender').parsley();
                    if (!gender.isValid()) {
                        gender.validate();
                    }else{
                        gender.destroy();
                    }

                    var dob = $('#date_of_birth').parsley();
                    if (!dob.isValid()) {
                        dob.validate();
                    }
                    else
                    {
                        dob.destroy();
                    }

                    var nationality = $('#nationality').parsley();
                    if (!nationality.isValid()) {
                        nationality.validate();
                    }else
                    {
                        nationality.destroy()
                    };
                    var present_address = $('#present_address').parsley();
                    if (!present_address.isValid()) {
                        present_address.validate();
                    }
                    var permanent_address = $('#permanent_address').parsley();
                    if (!permanent_address.isValid()) {
                        permanent_address.validate();
                    }
                    var psob = $('#place_state_of_birth').parsley();
                    if (!psob.isValid()) {
                        psob.validate();
                    }
                    var religion = $('#religion').parsley();
                    if (!religion.isValid()) {
                        religion.validate();
                    }
                    var image = $('#employee_image').parsley();
                    if(!image.isValid())
                    {image.validate();}

                    var marital_status = $('#marital_status').parsley();
                    if (!marital_status.isValid()) {
                        marital_status.validate();
                    }
                    else
                    {
                        marital_status.destroy();
                    }
                    var mother_tongue = $('#mother_tongue').parsley();
                    if (!mother_tongue.isValid()) {
                        mother_tongue.validate();
                    }
                    if(fname.isValid() && lname.isValid() && gender.isValid() &&dob.isValid()&&
                        nationality.isValid()&& permanent_address.isValid()&&present_address.isValid()&&
                    psob.isValid()&&religion.isValid()&&marital_status.isValid()&&mother_tongue.isValid())
                    {
                        return true;
                    }
                }

            }else {
                 return true;
                }
            },
        onStepChanged : function(event,currentIndex,priorIndex)
        {
            if(currentIndex === 1)
            {
                $("#enter_password").hide();
                $("select.add_ess").change(function() {
                    var ess = $(this).children("option:selected").val();
                    if(ess === 'Yes')
                    {
                        $("#enter_password").show();
                    }
                    else
                    {
                        $("#enter_password").hide();
                    }

                });
            }
            else {$("select.add_ess").val('No').select('No');
                }
            },

        onFinishing : function(event,currentIndex)
        {
                if (currentIndex === 1) {
                    var ecode = $('#ecode').parsley();
                    if (!ecode.isValid()) {
                        ecode.validate();
                    }
                    var department = $('#department_id').parsley();

                    if (!department.isValid()) {
                        department.validate();
                    }
                    else
                    {
                        department.destroy();
                    }
                    var designation = $('#designation_id').parsley();

                    if (!designation.isValid()) {
                        designation.validate();
                    }
                    else
                    {
                        designation.destroy();
                    }
                    var prob = $('#probation_from').parsley();

                    if (!prob.isValid()) {
                        prob.validate();
                    }
                    else
                    {
                        prob.destroy();
                    }
                    var ess = $('#enable_ess').parsley();
                    if(!ess.isValid())
                    {
                        ess.validate();
                    }
                    else
                    {
                        ess.destroy();
                    }

                    if(ecode.isValid()&&department.isValid()&&designation.isValid()&&prob.isValid()&&ess.isValid())
                    {
                        return  true;
                    }

                }
                // Always allow step back to the previous step even if the current step is not valid.
             else {

                return true;
            }
            },

        onFinished : function(event,currentIndex)
        {
            const employeeDataTable = $('#employeeDataTable');
            const addEmployeeForm = $('#addEmployeeForm');
            const formData = new FormData();
            let image=document.getElementById('employee_image').files;
            let employee_image=image[0];

            var unindexed_array = addEmployeeForm.serializeArray();
            $.map(unindexed_array, function (n, i) {
                formData.append(n['name'],n['value']);
            });
            const error = $('.error');
            addEmployeeForm.click(function () {
                error.html('');
            });
            addEmployeeForm.focusout(function () {
                error.html('');
            });
            formData.append('employee_image', employee_image);
            $.ajax({
                url: '/employee/create',
                type: 'POST',
                data: formData,
                success: function (data) {
                    if(data.success !== undefined && data.success === true ) {
                        employeeDataTable.DataTable().ajax.reload();
                        $.alertmodal('Employee' , 'Added Successfully');
                        setTimeout(function() {  window.location.href = '/employees'; }, 2000);

                    }
                },
                statusCode: {
                    400: function (response) {
                        error.html(response['responseJSON']['ecode'][0]);
                        if (onFailure !== undefined) {
                            onFailure(response);
                        }
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });
    $('#date_of_birth').datepicker({
        maxDate: new Date(),
    });
    $('.fc-datepicker').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'dd-mm-yy',

    });
});

