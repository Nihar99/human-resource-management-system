$(document).ready(function () {
    $(document).on('click','.employee_image', () => {
        $('.employee_image_select').trigger('click');
    });

    $(document).on("change", "#employee_image", function (evt) {
        var files = evt.target.files;
        var file = files[0];

        if (file) {
            var formData = new FormData();
            formData.append('file', file);
            var reader = new FileReader();
            reader.onload = function(e) {
                document.getElementById('employee_image_preview').src = e.target.result;
            };
            reader.readAsDataURL(file);
        }
    });
    const employee_id = $('#employeeId').attr("value");

    $(function () {
        $.ajax({
            url : '/employee/create/'+employee_id,
            type: 'GET',
            success: function(data){
                console.log(data);
                $('#edit_first_name').val(data.personal.first_name);
                $('#middle_name').val(data.personal.middle_name);
                $('#last_name').val(data.personal.last_name);
                $('#photo').val(data.personal.photo);
                if(data.personal.files!=null)
                $('#employee_image_preview').attr('src',data.personal.files.og_name);
                var gender; (data.personal.gender === 1) ?  gender = 'Male' : gender = 'Female';
                $('#gender').val(gender).trigger("change");
                $('#date_of_birth').val(data.personal.date_of_birth);
                $("#nationality").val(data.personal.nationality.toString()).trigger("change");
                $('#present_address').val(data.address.present_address);
                $('#permanent_address').val(data.address.permanent_address);
                $('#present_address_tel_no').val(data.address.present_address_tel_no);
                $('#permanent_address_tel_no').val(data.address.permanent_address_tel_no);
                $('#place_state_of_birth').val(data.personal.place_state_of_birth);
                $('#religion').val(data.personal.religion);
                var marital_status; if(data.personal.marital_status === 1){marital_status = 'Married'}
                else if(data.personal.marital_status === 2){marital_status = 'Unmarried'}
                else if(data.personal.marital_status === 3){marital_status = 'Divorse'}
                $('#marital_status').val(marital_status).trigger("change");
                $('#mother_tongue').val(data.personal.mother_tongue);
                $('#hobbies_and_interests').val(data.personal.hobbies_and_interests);
                $('#personal_email').val(data.personal.email);
                $('#ecode').val(data.employee.ecode);
                $('#professional_email').val(data.professional.email);
                $('#department_id').val(data.professional.department_id.toString()).trigger("change");
                $('#designation_id').val(data.professional.designation_id.toString()).trigger("change");
                var ess = (data.employee.ess === 1) ? 'Yes' : 'No';
                $('#enable_ess').val(ess);
                $('#confirmation_on').val(data.professional.confirmation_date);
                $('#probation_from').val(data.professional.probation_from);
                $('#ess_password').val('');


            },
        })
    });



    $('#wizard2').steps({
        headerTag: 'h3',
        bodyTag: 'section',
        autoFocus: true,
        titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
        onStepChanging: function (event, currentIndex, newIndex) {
            if (currentIndex < newIndex) {
                // Step 1 form validation
                if (currentIndex === 0) {
                    var edit_fname = $('#edit_first_name').parsley();
                    if (!edit_fname.isValid()) {
                        edit_fname.validate();
                    }
                    var lname = $('#last_name').parsley();
                    if (!lname.isValid()) {
                        lname.validate();
                    }
                    var gender = $('#gender').parsley();
                    if (!gender.isValid()) {
                        gender.validate();
                    }
                    else
                    {
                        gender.destroy();
                    }
                    var dob = $('#date_of_birth').parsley();
                    if (!dob.isValid()) {
                        dob.validate();
                    }
                    else{
                        dob.destroy();
                    }
                    var nationality = $('#nationality').parsley();
                    if (!nationality.isValid()) {
                        nationality.validate();
                    }
                    else{
                        nationality.destroy();
                    }
                    var present_address = $('#present_address').parsley();
                    if (!present_address.isValid()) {
                        present_address.validate();
                    }
                    var permanent_address = $('#permanent_address').parsley();
                    if (!permanent_address.isValid()) {
                        permanent_address.validate();
                    }
                    var psob = $('#place_state_of_birth').parsley();
                    if (!psob.isValid()) {
                        psob.validate();
                    }
                    var religion = $('#religion').parsley();
                    if (!religion.isValid()) {
                        religion.validate();
                    }
                    var image = $('#employee_image').parsley();
                    if(!image.isValid())
                    {image.validate();}

                    var marital_status = $('#marital_status').parsley();
                    if (!marital_status.isValid()) {
                        marital_status.validate();
                    }
                    else
                    {
                        marital_status.destroy();
                    }
                    var mother_tongue = $('#mother_tongue').parsley();
                    if (!mother_tongue.isValid()) {
                        mother_tongue.validate();
                    }
                    if(edit_fname.isValid() && lname.isValid() && gender.isValid() &&dob.isValid()&&
                        nationality.isValid()&& permanent_address.isValid()&&present_address.isValid()&&
                        psob.isValid()&&religion.isValid()&&marital_status.isValid()&&mother_tongue.isValid())
                    {
                        return true;
                    }
                }

            }else {
                return true;
            }
        },
        onStepChanged : function(event,currentIndex,priorIndex)
        {
            if(currentIndex === 1)
            {
                $("#enter_password").hide();
                $("select.add_ess").change(function() {
                    var ess = $(this).children("option:selected").val();
                    if(ess === 'Yes')
                    {
                        $("#enter_password").show();

                    }
                    else
                    {
                        $("#enter_password").hide();
                    }

                });
                $("select.add_ess").change();
            }
            else {$("select.add_ess").val('No').select('No');
            }
        },

        onFinishing : function(event,currentIndex)
        {
            if (currentIndex === 1) {
                var ecode = $('#ecode').parsley();
                if (!ecode.isValid()) {
                    ecode.validate();
                }
                var department = $('#department_id').parsley();

                if (!department.isValid()) {
                    department.validate();
                }
                else
                {
                    department.destroy();
                }
                var designation = $('#designation_id').parsley();

                if (!designation.isValid()) {
                    designation.validate();
                }
                else
                {
                    designation.destroy();
                }
                var prob = $('#probation_from').parsley();

                if (!prob.isValid()) {
                    prob.validate();
                }
                else
                {
                    prob.destroy();
                }
                var ess = $('#enable_ess').parsley();
                if(!ess.isValid())
                {
                    ess.validate();
                }
                else
                {
                    ess.destroy();
                }

                if(ecode.isValid()&&department.isValid()&&designation.isValid()&&prob.isValid()&&ess.isValid())
                {
                    return  true;
                }
            }
            // Always allow step back to the previous step even if the current step is not valid.
            else {

                return true;
            }
        },
        onFailure: function()
        {
            $.alertmodal('Alert' , 'Please see to it that all required fields are filled');

        },

        onFinished : function(event,currentIndex)
        {
            const employeeDataTable = $('#employeeDataTable');
            const employeeId = $('#employeeId').attr("value");
            const editEmployeeForm = $('#editEmployeeForm');
            const formData = new FormData();
            let image=document.getElementById('employee_image').files;
            let employee_image=image[0];

            var unindexed_array = editEmployeeForm.serializeArray();
            $.map(unindexed_array, function (n, i) {
                formData.append(n['name'],n['value']);
            });
            const error = $('.error');
            editEmployeeForm.click(function () {
                error.html('');
            });
            editEmployeeForm.focusout(function () {
                error.html('');
            });
            formData.append('employee_image', employee_image);
            formData.append('_method', 'PUT');
            $.ajax({
                url: '/employee/create/'+employeeId,
                type: 'POST',
                data: formData,
                success: function (data) {
                    console.log(data);
                    if(data.success !== undefined && data.success === true ) {
                        employeeDataTable.DataTable().ajax.reload();
                        $.alertmodal('Employee' , 'Edited Successfully');
                        setTimeout(function() {  window.location.href = '/employees'; }, 2000);
                    }
                },
                statusCode: {
                    400: function (response) {
                        error.html(response['responseJSON']['ecode']);
                        if (onFailure !== undefined) {
                            onFailure(response);
                        }
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });
    $('#date_of_birth').datepicker({
        maxDate: new Date(),
    });
    $('.fc-datepicker').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'dd-mm-yy',
    })
});
