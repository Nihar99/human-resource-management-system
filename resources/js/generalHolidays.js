$(document).ready(function () {
    const generalHolidaysTable = $('#generalHolidaysTable');
    const addGeneralHolidays = $('#addGeneralHolidays');
    const addGeneralHolidaysModal = $('#addGeneralHolidaysModal');
    const addGeneralHolidaysForm = $('#addGeneralHolidaysForm');
    const editGeneralHolidaysForm = $('#editGeneralHolidaysForm');
    const editGeneralHolidaysModal = $('#editGeneralHolidaysModal');
    const edit_holiday_date = $('#edit_holiday_date');
    const edit_name = $('#edit_name');
    generalHolidaysTable.DataTable({
        responsive: false,
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page'
        },
        "order": [],
        ajax: '/data-list/general-holidays',
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('DataTables', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('DataTables'));
        },
        columns: [
            { data: 'name' },
            {data: 'holiday_date'},
            { data: 'actions' },
        ]
    });

    setTimeout(function () {
        $('.dataTables_length select').select2({minimumResultsForSearch: Infinity});
    }, 500);

    $('.fc-datepicker').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'dd-mm-yy',
    });

    addGeneralHolidays.click(function () {
        $.modal(addGeneralHolidaysModal);
    });
    $.formsubmitpost(addGeneralHolidaysForm, '/general-holidays', function (response) {
        if (response['success'] !== undefined && response['success'] === true) {
            generalHolidaysTable.DataTable().ajax.reload();
            addGeneralHolidaysModal.modal('hide');
            $.alertmodal('Department', 'Added Successfully');
            addGeneralHolidaysForm.trigger("reset");
        }
    });
    $(document).on("click", ".deleteHoliday", function () {
        window.holiday_id = $(this).parent().attr('data_id');
        $.prompt('Delete Holiday', 'Are you sure you want to delete this holiday?', true, function () {
            $.formsubmitdel('/general-holidays', 'holiday_id', function (response) {
                if (response['success'] !== undefined && response['success'] === true) {
                    $.alertmodal('Holiday', 'Deleted Successfully');
                    $('#generalHolidaysTable').DataTable().ajax.reload();

                }
            })
        });
    });
    $(document).on("click", ".editHoliday", function () {
        window.holiday_id = $(this).parent().attr('data_id');
        edit_name.val($(this).parent().attr('data_name'));
        edit_holiday_date.val($(this).parent().attr('data_holiday_date'));
        $.modal(editGeneralHolidaysModal);
    });
    $.formsubmitput(editGeneralHolidaysForm, '/general-holidays', 'holiday_id', function (response) {
        if (response.success) {
            generalHolidaysTable.DataTable().ajax.reload();
            $('#editGeneralHolidaysModal').modal('hide');
            $.alertmodal('Holiday', 'Updated Successfully');
        }
    });

});
