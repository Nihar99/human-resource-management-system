$(document).ready(function () {
    const faceTrainTable = $('#faceTrainTable');
    const viewTrainedFaceImage = $('#viewTrainedFaceImage');
    const viewTrainedFaceImageModal = $('#viewTrainedFaceImageModal');
    const reTrainFace = $('#reTrainFace');
    const reTrainFaceModal = $('#reTrainFaceModal');
    const unTrainFace = $('#unTrainFace');
    faceTrainTable.DataTable({
        responsive: false,
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page'
        }
    });

    setTimeout(function () {
        $('.dataTables_length select').select2({minimumResultsForSearch: Infinity});
    }, 500);

    viewTrainedFaceImage.click(function () {
        $.modal(viewTrainedFaceImageModal);
    });

    reTrainFace.click(function () {
        $.modal(reTrainFaceModal);
    });

    unTrainFace.click(function () {
        $.prompt('Untrain Face', 'Are you sure you want to untrain face?', true, function () {
            $.alertmodal('Untrain Face', 'Face Untrained Successfully');
        });
    });
});
