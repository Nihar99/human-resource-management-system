$(document).ready(function () {
    const designationDataTable = $('#designationDataTable');
    const addDesignation = $('#addDesignation');
    const addDesignationModal = $('#addDesignationModal');
    const editDesignation = $('#editDesignation');
    const editDesignationModal = $('#editDesignationModal');
    const editDesignationForm = $('#editDesignationForm');
    const deleteDesignation = $('#deleteDesignation');
    const addDesignationForm = $('#addDesignationForm');
    const newDesignationName = $('#newDesignationName');

    designationDataTable.DataTable({
        responsive: false,
        destroy: true,
        processing: true,
        serverSide: true,
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page'
        },
        "order": [],
        ajax: '/data-list/designations',
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('DataTables', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('DataTables'));
        },
        columns: [
            { data: 'name' },
            { data: 'actions' },
        ]

    });
    setTimeout(function () {
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    }, 500);

    addDesignation.click(function () {
        $.modal(addDesignationModal);
    });
    $.formsubmitpost(addDesignationForm, '/designations', function (response) {
        if (response['success'] !== undefined && response['success'] === true) {
            designationDataTable.DataTable().ajax.reload();
            addDesignationModal.modal('hide');
            $.alertmodal('Designation', 'Added Successfully');
            addDesignationForm[0].reset();
        }
    });
    $(document).on("click", ".deleteDesignation", function () {
        window.designation_id = $(this).parent().attr('data_id');
        $.prompt('Delete Designation', 'Are you sure you want to delete?', true, function () {
            $.formsubmitdel('/designations', 'designation_id', function (response) {
                if (response['success'] !== undefined && response['success'] === true) {
                    $('#designationDataTable').DataTable().ajax.reload();
                    $.alertmodal('Designation', 'Deleted Successfully');
                }
            })
        });
    });

    $(document).on("click", ".editDesignation", function () {
        window.designation_id = $(this).parent().attr('data_id');
        $('#editDesignationName').val($(this).parent().attr('data_name'));
        $.modal(editDesignationModal);
    });

    $.formsubmitput(editDesignationForm, '/designations', 'designation_id', function (response) {
        if (response.success) {
            $('#designationDataTable').DataTable().ajax.reload();
            $('#editDesignationModal').modal('hide');
            $.alertmodal('Designation', 'Updated Successfully');
        }
    });
});
