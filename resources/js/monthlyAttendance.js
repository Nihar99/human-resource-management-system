$(document).ready(function () {
    const monthlyAttendanceTable = $('#monthlyAttendanceTable');
    const addMonthlyAttendanceDetails = $('#addMonthlyAttendanceDetails');
    const addMonthlyAttendanceModal = $('#addMonthlyAttendanceModal');
    const tpBasic = $('.tpBasic');

    var date = new Date();
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    var startDate = new Date(firstDay.getFullYear() + '-' + (firstDay.getMonth() + 1) + '-' + (firstDay.getDate()));
    var endDate = new Date(lastDay.getFullYear() + '-' + (lastDay.getMonth() + 1) + '-' + (lastDay.getDate()));

    var getDateArray = function(start, end) {
        var arr = new Array();
        var dt = new Date(start);
        while (dt <= end) {
            arr.push(new Date(dt));
            dt.setDate(dt.getDate() + 1);
        }
        return arr;
    }

    var dateArr = getDateArray(startDate, endDate);


    let columns=[{data: 'ecode'},{data: 'full_name'}];
    $.each(dateArr, function( index, value ) {
        columns.push({
            data:  null, render: function ( data) {
                let status=data.days[index];
                switch (status) {
                    case '1':
                        return '<button class="btn presentButtonColor py-1 px-2 rounded-5 cursor-pointer">P</button>';
                        break;
                    case '2':
                        return '<button class="btn absentButtonColor py-1 px-2 rounded-5 cursor-pointer">A</button>';
                        break;
                    case '3':
                        return '<button class="btn lateEntryButtonColor py-1 px-2 rounded-5 cursor-pointer">LE</button>';
                        break;
                    case '4':
                        return '<button class="btn earlyExitButtonColor py-1 px-2 rounded-5 cursor-pointer">EE</button>';
                        break;
                    case '5':
                        return '<button class="btn halfDayButtonColor py-1 px-2 rounded-5 cursor-pointer">HD</button>';
                        break;
                    case '6':
                        return '<button class="btn presentButtonColor py-1 px-2 rounded-5 cursor-pointer">OD</button>';
                        break;
                    case '7':
                        return '<button class="btn presentButtonColor py-1 px-2 rounded-5 cursor-pointer">PH</button>';
                        break;
                    case '8':
                        return '<button class="btn presentButtonColor py-1 px-2 rounded-5 cursor-pointer">CO</button>';
                        break;
                    case '9':
                        return '<button class="btn leaveButtonColor py-1 px-2 rounded-5 cursor-pointer">L</button>';
                        break;
                    case '0':
                        return '<button class="btn py-1 px-2 rounded-5 cursor-pointer">ND</button>';
                        break;
                }
            }
        });
    });

    monthlyAttendanceTable.DataTable({
        responsive : false,
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page'
        },
        "order": [],
        ajax: '/data-list/monthly-attendance',
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('DataTables', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('DataTables'));
        },
        columns: columns,
        scrollY: "300px",
        scrollX: true,
        fixedColumns: {
            leftColumns: 2
        },
         scrollCollapse: true,
         paging: true,
         ordering: false,
        });

    setTimeout(function () {
        $('.dataTables_length select').select2({minimumResultsForSearch: Infinity});
    }, 500);

    addMonthlyAttendanceDetails.click(function () {
        $.modal(addMonthlyAttendanceModal);
    });

    tpBasic.timepicker();
});
