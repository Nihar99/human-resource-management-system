$(document).ready(function () {
    const forgotPassword = $('#forgotPassword');
    forgotPassword.click(function () {
        $.alertmodal('Alert', `
        Please contact your admin to reset your password.
        `);
    });
});