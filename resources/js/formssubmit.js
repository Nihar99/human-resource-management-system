function getFormData($form) {
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function (n, i) {
        indexed_array[n['name']] = n['value'];
    });
    return indexed_array;
}

$.formsubmitpost = function (formModule, serverUrl, onSuccess, onFailure) {
    const error = $('.error');
    formModule.click(function () {
        error.html('');
    });
    formModule.focusout(function () {
        error.html('');
    });
    //formModule.submit(function () {
        const payload = getFormData(formModule);
        //if(onSuccess !== undefined || onFailure !== undefined){
        $.ajax(serverUrl, {
            type: "POST",
            data: payload,
            statusCode: {
                201: function (response) {
                    if (onSuccess !== undefined) {
                        onSuccess(response);
                        //onSuccess = undefined;
                        //onFailure = undefined;
                    }
                },
                400: function (response) {
                    error.html(response['responseJSON']['name'][0]);
                    if (onFailure !== undefined) {
                        onFailure(response);
                        onSuccess = undefined;
                        onFailure = undefined;
                    }
                },
                500: function (response) {
                    error.html(response['responseJSON']['name']);
                    if (onFailure !== undefined) {
                        onFailure(response);
                        onSuccess = undefined;
                        onFailure = undefined;
                    }

                    if (onSuccess !== undefined) {
                        onSuccess(response);
                        onSuccess = undefined;
                        onFailure = undefined;
                    }
                },
                200: function (response) {
                    if (onFailure !== undefined) {
                        onFailure(response);
                        onSuccess = undefined;
                        onFailure = undefined;
                    }

                    if (onSuccess !== undefined) {
                        onSuccess(response);
                        onSuccess = undefined;
                        onFailure = undefined;
                    }
                }
            }
        });
    //}
        return false;
    //})
}

$.formsubmitput = function (formModule, serverUrl, idField, onSuccess, onFailure) {
    const error = $('.error');
    formModule.click(function () {
        error.html('');
    });
    formModule.focusout(function () {
        error.html('');
    });
    formModule.submit(function () {
        const id = window[idField];
        const payload = getFormData(formModule);
        $.ajax(serverUrl + '/' + id, {
            type: "PUT",
            data: payload,
            statusCode: {
                201: function (response) {
                    if (onSuccess !== undefined) {
                        onSuccess(response);
                    }
                },
                400: function (response) {

                    error.html(response['responseJSON']['name']);
                    if (onFailure !== undefined) {
                        onFailure(response);
                    }
                }
            }
        });
        return false;
    })
}

$.formsubmitdel = function (serverUrl, idField, onSuccess, onFailure) {
    const id = window[idField];
    $.ajax(serverUrl + '/' + id, {
        type: 'POST',
        data: {
            '_method': 'DELETE'
        },
        statusCode: {
            201: function (response) {
                if (onSuccess !== undefined) {
                    onSuccess(response);
                }
            }
        }
    });
}

$.formFetchData = function (serverUrl, idField, onSuccess, onFailure) {
    const id = window[idField];
    $.ajax(serverUrl + '/' + id, {
        type: 'GET',
        statusCode: {
            201: function (response) {
                if (onSuccess !== undefined) {
                    onSuccess(response);
                }
            }
        }
    });
}


$.formsubmitpostbyId = function (formId, serverUrl, onSuccess, onFailure) {
    const formModule = $('#' + formId);
    const error = $('.error');
    formModule.click(function () {
        error.html('');
    });
    formModule.focusout(function () {
        error.html('');
    });
    $(document).on('submit', '#' + formId, function () {
        const payload = getFormData(formModule);
        $.ajax(serverUrl, {
            type: "POST",
            data: payload,
            statusCode: {
                201: function (response) {
                    if (onSuccess !== undefined) {
                        onSuccess(response);
                    }
                },
                400: function (response) {
                    error.html(response['responseJSON']['name'][0]);
                    if (onFailure !== undefined) {
                        onFailure(response);
                    }
                }
            }
        });
        return false;
    })
}

$.formAjaxRequest = function(formData,serverUrl,onSuccess,onFailure){
        $.ajax(serverUrl, {
            type: "POST",
            data: formData,
            statusCode: {
                200: function (response) {
                    if (onSuccess !== undefined) {
                        onSuccess(response);
                        onSuccess = undefined;
                        onFailure = undefined;
                    }
                },
                400: function (response) {
                    if (onFailure !== undefined) {
                        onFailure(response);
                        onSuccess = undefined;
                        onFailure = undefined;
                    }
                },
                500: function (response) {
                    if (onFailure !== undefined) {
                        onFailure(response);
                        onSuccess = undefined;
                        onFailure = undefined;
                    }
                }
            }
        });
        return false;
}
