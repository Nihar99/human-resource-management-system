$(document).ready(function () {
    const LoginAdminForm = $('#LoginAdminForm');
    const emailId = $('#emailId');
    const password = $('#password');
    LoginAdminForm.submit(function () {
        const request = {
            email: emailId.val(),
            password: password.val()
        };
        $.post('/login/admin', request, function (response) {
            if (response['success'] !== undefined && response['success']) {
                location.href = '/departments';
            } else {
                $.alertmodal('Alert', 'Login failed');
            }
        });

        return false;
    });


});
