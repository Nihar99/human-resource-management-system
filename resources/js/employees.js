$(document).ready(function () {
    const employeeDataTable = $('#employeeDataTable');
    const addEmployeePersonalForm = $('#addEmployeePersonalForm');
    employeeDataTable.DataTable({
        responsive: false,
        destroy: true,
        processing: true,
        serverSide: true,
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page'
        },
        "order": [],
        ajax: '/data-list/employees',
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('DataTables', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('DataTables'));
        },
        columns: [
            {data: 'ecode'},
            {data: 'full_name'},
            {data: 'department'},
            {data: 'designation'},
            {data: 'actions'},
        ]
    });

    setTimeout(function () {
        $('.dataTables_length select').select2({minimumResultsForSearch: Infinity});
    }, 500);

    //add
    $.formsubmitpost(addEmployeePersonalForm, '/employee/create', function (response) {
        if (response['success'] !== undefined && response['success'] === true) {
            employeeDataTable.DataTable().ajax.reload();
        }
    })
    /*$(document).()*/
    $(document).on("click", ".deleteEmployee", function () {
        window.employee_id = $(this).parent().attr('data_id');
        //console.log(window.employee_id);
        $.prompt('Delete Employee', 'Are you sure you want to delete permanently?', true, function () {
            $.formsubmitdel('/employee/create', 'employee_id', function (response) {
                if (response['success'] !== undefined && response['success'] === true) {
                    $.alertmodal('Employee', 'Deleted Successfully');
                    employeeDataTable.DataTable().ajax.reload();
                }
            })
        });
    });
    $(document).on("click", ".editEmployee", function () {
        window.employee_id = $(this).parent().attr('data_id');
        window.location.href = 'employee/create/' + employee_id + '/edit';
    });
});

