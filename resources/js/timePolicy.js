$(document).ready(function () {
    const timePolicyTable = $('#timePolicyTable');
    const viewTimePolicy = $('#viewTimePolicy');
    const viewTimePolicyModal = $('#viewTimePolicyModal');
    const addTimePolicy = $('#addTimePolicy');
    const addTimePolicyModal = $('#addTimePolicyModal');
    const editTimePolicyModal = $('#editTimePolicyModal');
    const editTimePolicyForm = $('#editTimePolicyForm');
    const addTimePolicyForm = $('#addTimePolicyForm');
    const day = $('#day');
    const tpBasic = $('.tpBasic');
    timePolicyTable.DataTable({
        responsive: false,
        destroy: true,
        //processing: true,
        //serverSide: true,
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page'
        },
        "order": [],
        ajax: '/data-list/time-policy',
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('DataTables', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('DataTables'));
        },
        columns: [
            { data: 'day',name:'day' },
            { data: 'weekoff' ,name:'weekoff'},
            {data: 'time_in',name:'time_in'},
            {data: 'time_out',name:'time_out'},
            {data: 'break_in',name:'break_in'},
            {data: 'break_out',name:'break_out'},
            {data: 'actions',name:'actions'},
        ]
    });

    setTimeout(function () {
        $('.dataTables_length select').select2({minimumResultsForSearch: Infinity});
    }, 500);

    viewTimePolicy.click(function () {
        $.modal(viewTimePolicyModal);
    });

    addTimePolicy.click(function () {
        $.modal(addTimePolicyModal);

        $("select.add_weekoff").change(function() {
            var selectedWeekOff = $(this).children("option:selected").val();
            if(selectedWeekOff === 'No')
            {
                $("#time_in_div").show();
                $("#time_out_div").show();
                $("#break_in_div").show();
                $("#break_out_div").show();
                $("#late_entry_after_div").show();
                $("#early_exit_before_div").show();
                $("#halfday_employee_comes_after_div").show();
                $("#halfday_employee_leaves_before_div").show();
            }
            else
            {
                $("#time_in_div").hide();
                $("#time_out_div").hide();
                $("#break_in_div").hide();
                $("#break_out_div").hide();
                $("#late_entry_after_div").hide();
                $("#early_exit_before_div").hide();
                $("#halfday_employee_comes_after_div").hide();
                $("#halfday_employee_leaves_before_div").hide();
                $("#addTimePolicyForm").attr('novalidate', 'novalidate');
            }

        });

    });

    $.formsubmitpost(addTimePolicyForm, '/time-policy', function (response) {
        if (response['success'] !== undefined && response['success'] === true) {

            addTimePolicyModal.modal('hide');
            timePolicyTable.DataTable().ajax.reload();
            $.alertmodal('Time Policy', 'Added Successfully');
            addTimePolicyForm.trigger("reset");
            $('#edit_time_in').select2("val" , "");

        }
    });
    $(document).on("click", ".deleteTimePolicy", function () {
        window.timePolicyId = $(this).parent().attr('data_id');
        $.prompt('Delete Time Policy', 'Are you sure you want to delete this day?', true, function () {
            $.formsubmitdel('/time-policy', 'timePolicyId', function (response) {
                if (response['success'] !== undefined && response['success'] === true) {
                    $.alertmodal('Time Policy', 'Deleted Successfully');
                    timePolicyTable.DataTable().ajax.reload();
                }
            })
        });
    });
    $(document).on("click", ".editTimePolicy", function () {
        window.timePolicyId = $(this).parent().attr('data_id');
        //console.log(timePolicyId);
         var day  = $(this).parent().attr('data_day');

         if(day == 1){
             $('#edit_day').val('Monday').trigger("change");
         }
        if(day ==2){$('#edit_day').val('Tuesday').trigger("change");}
        if(day ==3){$('#edit_day').val('Wednesday').trigger("change");}
        if(day ==4){$('#edit_day').val('Thursday').trigger("change");}
        if(day ==5){$('#edit_day').val('Friday').trigger("change");}
        if(day ==6){$('#edit_day').val('Saturday').trigger("change");}
        if(day ==7){$('#edit_day').val('Sunday').trigger("change");}

        var edit_weekoff = $(this).parent().attr('data_weekoff');
        console.log(edit_weekoff);
        if(edit_weekoff == 1){$('#edit_weekoff').val('Yes').trigger("change");}
        else{$('#edit_weekoff').val('No').trigger("change");}

        //$('#edit_day').val($(this).parent().attr('data_day'));
        $('#edit_time_in').val($(this).parent().attr('data_time_in'));
        $('#edit_time_out').val($(this).parent().attr('data_time_out'));
        $('#edit_break_in').val($(this).parent().attr('data_break_in'));
        $('#edit_break_out').val($(this).parent().attr('data_break_out'));
        $('#edit_late_entry_after').val($(this).parent().attr('data_late_entry_after'));
        $('#edit_early_exit_before').val($(this).parent().attr('data_early_exit_before'));
        $('#edit_halfday_employee_comes_after').val($(this).parent().attr('data_halfday_employee_comes_after'));
        $('#edit_halfday_employee_leaves_before').val($(this).parent().attr('data_halfday_employee_leaves_before'));
        $.modal(editTimePolicyModal);
        $("select.edit_weekoff").change(function () {
            var selectedWeekOff = $(this).children("option:selected").val();
            if (selectedWeekOff === 'No') {
                $("#edit_time_in_div").show();
                $("#edit_time_out_div").show();
                $("#edit_break_in_div").show();
                $("#edit_break_out_div").show();
                $("#edit_late_entry_after_div").show();
                $("#edit_early_exit_before_div").show();
                $("#edit_halfday_employee_comes_after_div").show();
                $("#edit_halfday_employee_leaves_before_div").show();
            }
            else {
                $("#edit_time_in_div").hide();
                $("#edit_time_out_div").hide();
                $("#edit_break_in_div").hide();
                $("#edit_break_out_div").hide();
                $("#edit_late_entry_after_div").hide();
                $("#edit_early_exit_before_div").hide();
                $("#edit_halfday_employee_comes_after_div").hide();
                $("#edit_halfday_employee_leaves_before_div").hide();
            }
        });
    });
    //console.log(timePolicyId);
    $.formsubmitput(editTimePolicyForm, '/time-policy', 'timePolicyId', function (response) {
        if (response.success) {
            $('#timePolicyTable').DataTable().ajax.reload();
            $('#editTimePolicyModal').modal('hide');
            $.alertmodal('TimePolicy', 'Updated Successfully');
            editTimePolicyForm.trigger("reset");
            $('#edit_day').val(0);
            $('#edit_weekoff').reset();
        }
    });
    tpBasic.timepicker();
    });




