$(document).ready(function () {
    const departmentDataTable = $('#departmentDataTable');
    const addDepartment = $('#addDepartment');
    const addDepartmentModal = $('#addDepartmentModal');
    const editDepartmentForm = $('#editDepartmentForm');
    const editDepartmentModal = $('#editDepartmentModal');
    const deleteDepartment = $('#deleteDepartment');
    const addDepartmentForm = $('#addDepartmentForm');
    const newDepartmentName = $('#newDepartmentName');
    const editDepartmentName = $('#editDepartmentName');

    departmentDataTable.DataTable({
        responsive: false,
        destroy: true,
        processing: true,
        serverSide: true,
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page'
        },
        "order": [],
        ajax: '/data-list/departments',
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('DataTables', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('DataTables'));
        },
        columns: [
            { data: 'name' },
            { data: 'actions' },
        ]
    });

    setTimeout(function () {
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    }, 500);

    addDepartment.click(function () {
        $.modal(addDepartmentModal);
    });

    $.formsubmitpost(addDepartmentForm, '/departments', function (response) {
        if (response['success'] !== undefined && response['success'] === true) {
            departmentDataTable.DataTable().ajax.reload();
            addDepartmentModal.modal('hide');
            $.alertmodal('Department', 'Added Successfully');
            addDepartmentForm[0].reset();
        }
    });
    $(document).on("click", ".deleteDepartment", function () {
        window.department_id = $(this).parent().attr('data_id');
        $.prompt('Delete Department', 'Are you sure you want to delete?', true, function () {
            $.formsubmitdel('/departments', 'department_id', function (response) {
                if (response['success'] !== undefined && response['success'] === true) {
                    $.alertmodal('Department', 'Deleted Successfully');
                    $('#departmentDataTable').DataTable().ajax.reload();

                }
            })
        });
    });
    $(document).on("click", ".editDepartment", function () {
        window.department_id = $(this).parent().attr('data_id');
        editDepartmentName.val($(this).parent().attr('data_name'));
        $.modal(editDepartmentModal);
    });

    $.formsubmitput(editDepartmentForm, '/departments', 'department_id', function (response) {
        if (response.success) {
            $('#departmentDataTable').DataTable().ajax.reload();
            $('#editDepartmentModal').modal('hide');
            $.alertmodal('Department', 'Updated Successfully');
        }
    });
});












