$.modal = function (element) {
    element.modal({backdrop: 'static', keyboard: false});
}
$.modalclose = function (element) {
    element.modal('hide');
}

$.alertmodal = function (title, msg, large = false) {
    let modalsm = !large && 'modal-sm';
    const randomString = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    const modal = `
    <!-- BASIC MODAL -->
          <div id="${randomString}" class="modal fade">
            <div class="modal-dialog ${modalsm} modal-dialog-centered" role="document">
              <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                  <h4 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">${title}</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body pd-25">
                  <p class="tx-14 mg-b-5">${msg}</p>
                  <div class="col-12 p-0 text-right mt-4">
                  <button type="button" class="btn btn-default tx-12 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div><!-- modal-dialog -->
          </div><!-- modal -->
    `;
    $('body').append(modal);
    $.modal($('#' + randomString));
}

$.prompt = function (title, msg, large = false, onSuccess, onFailure) {
    let modalsm = !large && 'modal-sm';
    const randomString = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    const onSuccessString = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    const onFailureString = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    const modal = `
        <div id="${randomString}" class="modal fade">
            <div class="modal-dialog ${modalsm} modal-dialog-centered" role="document">
              <div class="modal-content bd-0">
                <div class="modal-header pd-y-20 pd-x-25">
                  <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">${title}</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body pd-25">
                  <p class="mg-b-5">${msg}</p>
                  <div class="col-12 p-0 text-right mt-4">
                  <button type="button" class="btn btn-danger tx-mont tx-medium tx-12 tx-uppercase pd-y-12 pd-x-25 tx-spacing-1 mr-3" id="${onSuccessString}">Yes</button>
                  <button type="button" class="btn btn-default tx-mont tx-medium tx-12 tx-uppercase pd-y-12 pd-x-25 tx-spacing-1" id="${onFailureString}">No</button>
                  </div>
                </div>
              </div>
            </div><!-- modal-dialog -->
        </div><!-- modal -->
    `;
    $('body').append(modal);
    $.modal($('#' + randomString));

    $('#' + onSuccessString).click(function () {
        if (onSuccess !== undefined) {
            onSuccess();
        }
        $.modalclose($('#' + randomString));
    });
    $('#' + onFailureString).click(function () {
        if (onFailure !== undefined) {
            onFailure();
        }
        $.modalclose($('#' + randomString));
    });

}
