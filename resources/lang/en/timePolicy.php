<?php

return [
    'headerTitle' => 'Time Policy',
    'addTimePolicy' => 'Add Time Policy',
    'editTimePolicyHeader' => 'Edit Time Policy',
    'timePolicyDay' => 'Day',
    'timePolicyWeekOff' => 'Week Off',
    'timePolicyTimeIn' => 'Time In',
    'timePolicyTimOut' => 'Time Out',
    'timePolicyBreakIn' => 'Break In',
    'timePolicyBreakOut' => 'Break Out',
    'timePolicyLateEntryAfter' => 'Late Entry After',
    'timePolicyEarlyExitBefore' => 'Early Exit Before',
    'timePolicyComesAfterHalfDay' => 'Set Half Day, if Employee comes after',
    'timePolicyLeavesBeforeHalfDay' => 'Set Half Day, if Employee comes before',
    'timePolicyActions' => 'Actions',
    'viewTimePolicy' => 'View Details',
    'editTimePolicy' => 'Edit',
    'deleteTimePolicy' => 'Delete',
    'selectTimePolicyDay' => 'Select Day',
    'selectTimePolicyWeekOff' => 'Select Week Off',
    'setTimePolicyTimeIn' => 'Set Time In',
    'setTimePolicyTimeOut' => 'Set Time Out',
    'setTimePolicyBreakIn' => 'Set Break In',
    'setTimePolicyBreakOut' => 'Set Break Out',
    'setTimePolicyLateEntryAfter' => 'Set Late Entry After',
    'setTimePolicyEarlyExitBefore' => 'Set Early Exit Before',
    'setTimePolicyComesAfterHalfDay' => 'Set Half Day, if Employee comes after',
    'setTimePolicyLeavesBeforeHalfDay' => 'Set Half Day, if Employee comes before',
    'timePolicyButtonSubmit' => 'Submit',
    'timePolicyButtonUpdate' => 'Update',
];
