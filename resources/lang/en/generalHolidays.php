<?php

return[
    'headerTitle' => 'General Holidays',
    'addGeneralHolidays' => 'Add General Holidays',
    'editGeneralHolidaysHeader' => 'Edit General Holidays',
    'generalHolidaysName' => 'Name',
    'generalHolidaysDate' => 'Date',
    'generalHolidaysActions' => 'Actions',
    'editGeneralHolidays' => 'Edit',
    'deleteGeneralHolidays' => 'Delete',
    'enterGeneralHolidaysName' => 'Enter Name',
    'selectGeneralHolidaysDate' => 'Select Date',
    'generalHolidaysButtonSubmit' => 'Submit',
    'generalHolidaysButtonUpdate' => 'Update',
];
