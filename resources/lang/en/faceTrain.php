<?php

return [
    'headerTitle' => 'Face Train',
    'faceTrainEcode' => 'Ecode',
    'faceTrainEmployeeName' => 'Employee Name',
    'faceTrainTrainedFace' => 'Trained Face',
    'faceTrainEmployeePhoto' => 'Employee Photo',
    'faceTrainActions' => 'Actions',
    'reTrainFace' => 'Retrain Face',
    'unTrainFace' => 'Untrain Face',
    'faceTrainEmployeePhotoTitle' => 'Change Employee Profile',
    'enterFaceTrainEcode' => 'Enter Ecode',
    'enterFaceTrainEmployeeName' => 'Enter Employee Name',
    'faceTrainButtonRetrain' => 'Retrain Face',
];
