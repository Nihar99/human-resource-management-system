<?php

return [
    'headerTitle' => 'Designation',
    'designAdd' => 'Add Designation',
    'designName' => 'Name',
    'designActions' => 'Actions',
    'designEdit' => 'Edit',
    'designEditDesignation' => 'Edit Designation',
    'designDelete' => 'Delete',
    'designPlaceholderName' => 'Enter Name',
    'designButtonSubmit' => 'Submit',
];
