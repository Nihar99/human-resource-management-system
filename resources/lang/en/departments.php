<?php

return[
    'headerTitle' => 'Department',
    'deptAdd' => 'Add Department',
    'deptName' => 'Name',
    'deptActions' => 'Actions',
    'deptEdit' => 'Edit',
    'deptEditDepartment' => 'Edit Department',
    'deptDelete' => 'Delete',
    'deptPlaceholderName' => 'Enter Name',
    'deptButtonSubmit' => 'Submit',
];
