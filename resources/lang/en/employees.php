<?php

return [
    'headerTitle' => 'Employees',
    'empAdd' => 'Add Employee',
    'empImportExcel' => 'Import Excel',
    'empEcode' => 'Ecode',
    'empFullName' => 'Full Name',
    'empDepartment' => 'Department',
    'empDesignation' => 'Designation',
    'empActions' => 'Actions',
    'empEdit' => 'Edit',
    'empOtherDetails' => 'Other Details',
    'empDelete' => 'Delete'
];
