<?php

return [
    'headerTitle' => 'Request Leave',
    'requestLeaveEmpCode' => 'EmpCode',
    'requestLeaveEmpName' => 'Employee Name',
    'requestLeaveDepartment' => 'Department',
    'requestLeaveDesignation' => 'Designation',
    'requestLeaveLeaveDate' => 'Leave (From Date - To Date)',
    'requestLeaveNoDays' => 'No of Days',
    'requestLeaveReason' => 'Reason',
    'requestLeaveActions' => 'Actions',
    'acceptLeaveRequest' => 'Accept Leave Request',
    'rejectLeaveRequest' => 'Reject Leave Request',
    'requestLeaveDetails' => 'Leave Details',
    'enterRequestLeaveDetails' => 'Enter Leave Details',
    'requestLeaveButtonAccept' => 'Accept',
    'requestLeaveButtonCancel' => 'Cancel',
    'requestLeaveStatus' => 'Status',
];
