<?php

return [
    'headerTitle' => 'Apply Leave',
    'applyLeaveMultipleDay' => 'Are Multiple Days',
    'applyLeaveDate' => 'Date',
    'selectApplyLeaveDate' => 'Select Date',
    'applyLeaveFromDate' => 'From Date',
    'applyLeaveToDate' => 'To Date',
    'applyLeaveLeaveType' => 'Leave Type',
    'applyLeaveSelectEmployee' => 'Select Employee',
    'applyLeaveButtonSubmit' => 'Submit',
    'reason'=>'Reason',
    'total_leaves'=>'Total Leaves',
    'isHalfDay'=>'Is Half Day'
];
