<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */


Route::resources([
    'login/admin' => 'AdminAuthController',
    'login/user' => 'UserAuthController',
    'password/reset' => 'PasswordResetController',
    'password/forgot' => 'PasswordForgotController',
    'departments' => 'DepartmentController',
    'designations' => 'DesignationController',
    'emergency-contacts' => 'EmergencyContactController',
    'exposure-computer' => 'ExposureComputerController',
    'employees' => 'EmployeesController',
    'educational-profiles' => 'EducationalProfileController',
    'employee/create' => 'EmployeeController',
    'other-details' => 'OtherDetailsController',
    'logout' => 'LogoutController',
    'time-policy' => 'TimePolicyController',
    'face-train' => 'FaceTrainController',
    'family-details'  => 'FamilyDetailsController',
    'general-holidays'  => 'GeneralHolidaysController',
    'monthly-attendance'  => 'MonthlyAttendanceController',
    'languages-known' => 'LanguagesKnownController',
    'leave-management/apply'  => 'ApplyLeaveController',
    'leave-management/request'  => 'RequestLeaveController',
    'leave-management/history'  => 'LeaveLedgerController',
    'work-experience'  => 'WorkExperienceController',
    'referred-by'  => 'ReferredByController',
    'scholarships-prizes' => 'ScholarshipPrizeController',
    'skills-experiences' => 'SkillsExperienceController',
    'employee/import' => 'EmployeeImportController',
]);

Route::get('/', 'IndexController@index');
Route::get('/data-list/departments', 'DepartmentController@fetchUTList');
Route::get('/data-list/designations', 'DesignationController@fetchUTList');
Route::get('/data-list/employees', 'EmployeeController@fetchUTList');
Route::get('/data-list/time-policy', 'TimePolicyController@fetchUTList');
Route::get('/data-list/family-details/{id}', 'FamilyDetailsController@fetchUTList');
Route::get('/data-list/general-holidays', 'GeneralHolidaysController@fetchUTList');
Route::get('/data-list/emergency-contacts/{id}', 'EmergencyContactController@fetchUTList');
Route::get('/data-list/languages-known/{id}', 'LanguagesKnownController@fetchUTList');
Route::get('/data-list/educational-profiles/{id}', 'EducationalProfileController@fetchUTList');
Route::get('/data-list/work-experience/{id}', 'WorkExperienceController@fetchUTList');
Route::get('/data-list/referred-by/{id}', 'ReferredByController@fetchUTList');
Route::get('/data-list/exposure-computer/{id}', 'ExposureComputerController@fetchUTList');
Route::get('/data-list/scholarships-prizes/{id}', 'ScholarshipPrizeController@fetchUTList');
Route::get('/data-list/skills-experiences/{id}', 'SkillsExperienceController@fetchUTList');
Route::get('/data-list/monthly-attendance', 'MonthlyAttendanceController@fetchUTList');
Route::get('/data-list/leave-ledger', 'LeaveLedgerController@fetchUTList');
Route::get('/data-list/applied-leaves/{id}', 'RequestLeaveController@listLeaves');
Route::get('/data-list/leaveLedgerDetails/{id}', 'LeaveLedgerController@fetchDetailsList');
Route::post('/leave-management/fetchLeave', 'RequestLeaveController@fetchLeave');
Route::post('/leave-management/accept-leave', 'RequestLeaveController@acceptLeave');
Route::post('/leave-management/reject-leave', 'RequestLeaveController@rejectLeave');

Route::get('downloadSampleExcel', 'EmployeeImportController@downloadSampleExcel');
Route::post('importEmployeeExcel', 'EmployeeImportController@importExcel');

Route::get('cron','CronController@runCron');

