#!/bin/bash

# any future command that fails will exit the script
set -e

# Lets write the public key of our aws instance
eval $(ssh-agent -s)
echo "${!PASSWORD}" | tr -d '\r' | ssh-add - > /dev/null

# ** Alternative approach
# echo -e "$PRIVATE_KEY" > /root/.ssh/id_rsa
# chmod 600 /root/.ssh/id_rsa
# ** End of alternative approach

# disable the host key checking.
bash ./deploy/disableHostKeyChecking.sh

echo "deploying to ${!USER}@${!SERVER} in ${!DESTINATION_DIR}"
ssh ${!USER}@${!SERVER} "cd ${!DESTINATION_DIR}; if [ ! -d "vendor" ]; then doesnotexists=1; fi; if [ ! -f ".env" ]; then echo 'ENV does not exists'; exit; fi;"
rsync -auv -e ssh $(pwd)/${!SOURCE_DIR} ${!USER}@${!SERVER}:${!DESTINATION_DIR}
ssh ${!USER}@${!SERVER} "cd ${!DESTINATION_DIR}; if [ ! -z "$doesnotexists" ]; then composer install --prefer-dist --no-dev; php artisan key:generate; php artisan storage:link; php artisan passport:install; fi"
ssh ${!USER}@${!SERVER} "cd ${!DESTINATION_DIR}; composer update --prefer-dist --no-dev; php artisan migrate; php artisan cache:clear; php artisan view:clear; php artisan config:cache; php artisan route:cache;"


