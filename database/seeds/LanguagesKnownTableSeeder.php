<?php

use App\LanguagesKnown;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class LanguagesKnownTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (!App::environment('production')) {
            $langKnown              = new LanguagesKnown();
            $langKnown->employee_id = 1;
            $langKnown->name        = 'English';
            $langKnown->read        = true;
            $langKnown->write       = true;
            $langKnown->speak       = true;
            $langKnown->understand  = true;
            $langKnown->save();
        }
    }
}
