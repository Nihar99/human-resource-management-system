<?php

use App\EmployeeAddress;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class EmployeeAddressTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (!App::environment('production')) {
            $empAddress                    = new EmployeeAddress();
            $empAddress->present_address   = 'A-308, Shri Prastha Soc, Agashi Rd, Virar West';
            $empAddress->permanent_address = 'Same as above';
            $empAddress->employee_id       = 1;
            $empAddress->save();
        }
    }
}
