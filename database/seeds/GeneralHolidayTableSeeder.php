<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use App\GeneralHoliday;

class GeneralHolidayTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!App::environment('production')) {
            $this->insert('Diwali','2019-01-01',1,1,null);
            $this->insert('Holi','2019-02-02',1,1,null);
        }
    }
    public function insert($name,$date,$client_id,$admin_id,$employee_id){
        $holiday            = new GeneralHoliday();
        $holiday->name      = $name;
        $holiday->holiday_date = $date;
        $holiday->client_id = $client_id;
        $holiday->added_by_admin = $admin_id;
        $holiday->added_by_employee = $employee_id;
        $holiday->save();
    }
}
