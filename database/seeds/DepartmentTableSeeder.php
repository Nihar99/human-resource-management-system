<?php

use App\Department;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class DepartmentTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (!App::environment('production')) {
            $this->insert('HR',1,1,null);
            $this->insert('Accounts',1,1,null);
            $this->insert('Research & Development',1,null,1);
        }
    }

    public function insert($name,$client_id,$admin_id,$employee_id){
        $deparment            = new Department();
        $deparment->name      = $name;
        $deparment->client_id = $client_id;
        $deparment->added_by_admin = $admin_id;
        $deparment->added_by_employee = $employee_id;
        $deparment->save();
    }
}
