<?php

use App\FamilyDetails;
use Illuminate\Database\Seeder;

class FamilyDetailsTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (!App::environment('production')) {
            $familyDetails               = new FamilyDetails();
            $familyDetails->full_name    = 'Sneha Shenoy';
            $familyDetails->age          = 53;
            $familyDetails->occupation   = 'House Wife';
            $familyDetails->relationship = 'Mother';
            $familyDetails->gender       = false;
            $familyDetails->dependant    = true;
            $familyDetails->dependant_to = 1;
            $familyDetails->save();
        }
    }
}
