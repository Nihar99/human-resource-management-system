<?php

use App\EmergencyContact;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class EmergencyContactSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (!App::environment('production')) {
            $emergencyContact               = new EmergencyContact();
            $emergencyContact->employee_id  = 1;
            $emergencyContact->name         = "Ramanath Shenoy";
            $emergencyContact->relationship = "Father";
            $emergencyContact->address      = "Virar West";
            $emergencyContact->phone        = "9999999999";
            $emergencyContact->save();
        }
    }
}
