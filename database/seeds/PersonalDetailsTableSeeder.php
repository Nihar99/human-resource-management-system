<?php

use App\PersonalDetails;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class PersonalDetailsTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (!App::environment('production')) {
            $personalDetails                        = new PersonalDetails();
            $personalDetails->employee_id           = 1;
            $personalDetails->photo                 = 1;
            $personalDetails->first_name            = 'Veeraj';
            $personalDetails->middle_name           = 'Ramanath';
            $personalDetails->last_name             = 'Shenoy';
            $personalDetails->gender                = true;
            $personalDetails->date_of_birth         = '1991-08-06';
            $personalDetails->nationality           = 100;
            $personalDetails->place_state_of_birth  = 'Mumbai, Maharashtra';
            $personalDetails->religion              = 'Hindu';
            $personalDetails->marital_status        = 2;
            $personalDetails->mother_tongue         = 'Konkani';
            $personalDetails->email                 = 'veerajthegreat@gmail.com';
            $personalDetails->hobbies_and_interests = 'Cricket, Swimming';
            $personalDetails->save();
        }
    }
}
