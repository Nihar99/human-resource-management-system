<?php

use App\Files;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class FilesTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (!App::environment('production')) {
            $files                 = new Files();
            $files->ref_name       = "photo";
            $files->og_name        = "Photo";
            $files->size           = 1.5;
            $files->client_id      = 1;
            $files->added_by_admin = 1;
            $files->save();
        }
    }
}
