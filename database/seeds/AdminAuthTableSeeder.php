<?php

use App\AdminAuth;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;

class AdminAuthTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (!App::environment('production')) {
            $admin                = new AdminAuth();
            $admin->password_hash = Hash::make('12345678');
            $admin->admin_id      = 1;
            $admin->save();
        }
    }
}
