<?php

use App\ReferredBy;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class ReferredBySeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (!App::environment('production')) {
            $referredBy              = new ReferredBy();
            $referredBy->employee_id = 1;
            $referredBy->name        = "Balram Maurya";
            $referredBy->position    = "Sales Manager";
            $referredBy->address     = "Nalasopara";
            $referredBy->tel_no      = "999999999";
            $referredBy->save();
        }
    }
}
