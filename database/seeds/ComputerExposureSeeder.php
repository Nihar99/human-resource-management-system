<?php

use App\exposure_to_computer;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class ComputerExposureSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (!App::environment('production')) {
            $exposure                   = new exposure_to_computer();
            $exposure->employee_id      = 1;
            $exposure->language         = "PHP";
            $exposure->operating_system = 'Any';
            $exposure->application      = 'Web Apps';
            $exposure->hardware         = 'Server';
            $exposure->save();
        }
    }
}
