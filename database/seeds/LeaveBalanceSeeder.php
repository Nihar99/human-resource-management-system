<?php

use App\LeaveBalance;
use Illuminate\Database\Seeder;

class LeaveBalanceSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (!App::environment('production')) {
            $leaveBalance                  = new LeaveBalance;
            $leaveBalance->employee_id     = 1;
            $leaveBalance->leave_master_id = 1;
            $leaveBalance->available       = 10.5;
            $leaveBalance->save();
        }
        if (!App::environment('production')) {
            $leaveBalance                  = new LeaveBalance;
            $leaveBalance->employee_id     = 1;
            $leaveBalance->leave_master_id = 2;
            $leaveBalance->available       = 7;
            $leaveBalance->save();
        }
    }
}
