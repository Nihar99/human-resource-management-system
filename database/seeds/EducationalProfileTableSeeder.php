<?php

use App\EducationalProfile;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class EducationalProfileTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (!App::environment('production')) {
            $eductionalProfile                     = new EducationalProfile();
            $eductionalProfile->employee_id        = 1;
            $eductionalProfile->board_degree       = 'BSc IT';
            $eductionalProfile->year_of_passing    = 'Mar 2012';
            $eductionalProfile->institution_name   = 'Mumbai University';
            $eductionalProfile->principle_subjects = 'Science';
            $eductionalProfile->grade_percentage   = '65%';
            $eductionalProfile->save();
        }
    }
}
