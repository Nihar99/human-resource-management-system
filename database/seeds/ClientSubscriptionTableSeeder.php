<?php

use App\ClientSubscription;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class ClientSubscriptionTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (!App::environment('production')) {
            $subscription             = new ClientSubscription();
            $subscription->client_id  = 1;
            $subscription->expires_on = '2019-12-31';
            $subscription->save();
        }
    }
}
