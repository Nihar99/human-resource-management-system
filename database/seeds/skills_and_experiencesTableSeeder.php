<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use App\SkillsAndExperience;

class skills_and_experiencesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        if (!App::environment('production')) {
            $ssTable              = new SkillsAndExperience();
            $ssTable->details     = "abc xyz uvw";
            $ssTable->employee_id = 1;
            $ssTable->save();
        }

    }
}
