<?php

use App\WorkExperience;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class WorkExperienceSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (!App::environment('production')) {
            $workExperience                       = new WorkExperience();
            $workExperience->employee_id          = 1;
            $workExperience->organisation_name    = 'Approwess Tech Pvt Ltd.';
            $workExperience->organisation_address = 'Nalasopara';
            $workExperience->from_date            = '2016-01-01';
            $workExperience->to_date              = '2017-12-31';
            $workExperience->position_held        = 'Junior Web Developer';
            $workExperience->reporting_to         = 'CK Maurya';
            $workExperience->nature_of_work       = 'Developing web apps';
            $workExperience->reason_of_leaving    = 'Better oppurtunity';
            $workExperience->gross_annual         = '120000';
            $workExperience->save();
        }
    }
}
