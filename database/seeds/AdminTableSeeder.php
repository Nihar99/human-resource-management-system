<?php

use App\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class AdminTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (!App::environment('production')) {
            $admin            = new Admin;
            $admin->name      = 'Sharad Kamath';
            $admin->email     = 'sharad@3iology.com';
            $admin->contact   = '9999999999';
            $admin->client_id = 1;
            $admin->save();

            $admin2            = new Admin;
            $admin2->name      = 'Sharad Kamath';
            $admin2->email     = 'sharad@varadendra.com';
            $admin2->contact   = '9999999999';
            $admin2->client_id = 2;
            $admin2->save();
        }
    }
}
