<?php

use App\Designation;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class DesignationTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (!App::environment('production')) {
            $this->insert('Managers',1,1,null);
            $this->insert('Developers',1,1,null);
            $this->insert('Testers',1,null,1);
        }
    }

    public function insert($name, $client_id, $admin_id, $employee_id){
        $designation            = new Designation();
        $designation->name      = $name;
        $designation->client_id = $client_id;
        $designation->added_by_admin = $admin_id;
        $designation->added_by_employee = $employee_id;
        $designation->save();
    }
}
