<?php

use App\TimePolicy;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class TimePolicyTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (!App::environment('production')) {
            $data = [
                [
                    'client_id'                      => 1,
                    'added_by_admin'                 => 1,
                    'added_by_employee'              => null,
                    'day'                            => 1,
                    'time_in'                        => '09:00',
                    'time_out'                       => '17:30',
                    'break_in'                       => '13:00',
                    'break_out'                      => '13:30',
                    'late_entry_after'               => '09:10',
                    'early_exit_before'              => '17:20',
                    'halfday_employee_comes_after'   => '10:00',
                    'halfday_employee_leaves_before' => '16:00',
                    'weekoff'                        => false,
                ],
                [
                    'client_id'                      => 1,
                    'added_by_admin'                 => 1,
                    'added_by_employee'              => null,
                    'day'                            => 2,
                    'time_in'                        => '09:00',
                    'time_out'                       => '17:30',
                    'break_in'                       => '13:00',
                    'break_out'                      => '13:30',
                    'late_entry_after'               => '09:10',
                    'early_exit_before'              => '17:20',
                    'halfday_employee_comes_after'   => '10:00',
                    'halfday_employee_leaves_before' => '16:00',
                    'weekoff'                        => false,
                ],
                [
                    'client_id'                      => 1,
                    'added_by_admin'                 => 1,
                    'added_by_employee'              => null,
                    'day'                            => 3,
                    'time_in'                        => '09:00',
                    'time_out'                       => '17:30',
                    'break_in'                       => '13:00',
                    'break_out'                      => '13:30',
                    'late_entry_after'               => '09:10',
                    'early_exit_before'              => '17:20',
                    'halfday_employee_comes_after'   => '10:00',
                    'halfday_employee_leaves_before' => '16:00',
                    'weekoff'                        => false,
                ],
                [
                    'client_id'                      => 1,
                    'added_by_admin'                 => 1,
                    'added_by_employee'              => null,
                    'day'                            => 4,
                    'time_in'                        => '09:00',
                    'time_out'                       => '17:30',
                    'break_in'                       => '13:00',
                    'break_out'                      => '13:30',
                    'late_entry_after'               => '09:10',
                    'early_exit_before'              => '17:20',
                    'halfday_employee_comes_after'   => '10:00',
                    'halfday_employee_leaves_before' => '16:00',
                    'weekoff'                        => false,
                ],
                [
                    'client_id'                      => 1,
                    'added_by_admin'                 => 1,
                    'added_by_employee'              => null,
                    'day'                            => 5,
                    'time_in'                        => '09:00',
                    'time_out'                       => '17:30',
                    'break_in'                       => '13:00',
                    'break_out'                      => '13:30',
                    'late_entry_after'               => '09:10',
                    'early_exit_before'              => '17:20',
                    'halfday_employee_comes_after'   => '10:00',
                    'halfday_employee_leaves_before' => '16:00',
                    'weekoff'                        => false,
                ],
                [
                    'client_id'                      => 1,
                    'added_by_admin'                 => 1,
                    'added_by_employee'              => null,
                    'day'                            => 6,
                    'time_in'                        => '09:00',
                    'time_out'                       => '17:30',
                    'break_in'                       => '13:00',
                    'break_out'                      => '13:30',
                    'late_entry_after'               => '09:10',
                    'early_exit_before'              => '17:20',
                    'halfday_employee_comes_after'   => '10:00',
                    'halfday_employee_leaves_before' => '16:00',
                    'weekoff'                        => false,
                ],
                [
                    'client_id'                      => 1,
                    'added_by_admin'                 => 1,
                    'added_by_employee'              => null,
                    'day'                            => 7,
                    'time_in'                        => null,
                    'time_out'                       => null,
                    'break_in'                       => null,
                    'break_out'                      => null,
                    'late_entry_after'               => null,
                    'early_exit_before'              => null,
                    'halfday_employee_comes_after'   => null,
                    'halfday_employee_leaves_before' => null,
                    'weekoff'                        => 1,
                ],
            ];

            (new TimePolicy)->insertMultiple($data);
        }
    }
}
