<?php

use App\EmployeeAttendance;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class AttendanceTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (!App::environment('production')) {
            $empattendance                    = new EmployeeAttendance;
            $empattendance->employee_id       = 1;
            $empattendance->added_by_employee = 1;
            $empattendance->status            = 1;
            $empattendance->on_date           = '2019-05-31';
            $empattendance->time_in           = '09:00';
            $empattendance->time_out          = '17:30';
            $empattendance->break_in          = '13:00';
            $empattendance->break_out         = '13:30';
            $empattendance->save();
        }
    }
}
