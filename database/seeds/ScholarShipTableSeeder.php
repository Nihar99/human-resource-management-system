<?php

use App\ScholarShipPrizes;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class ScholarShipTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (!App::environment('production')) {
            $ssTable              = new ScholarShipPrizes();
            $ssTable->details     = "None";
            $ssTable->employee_id = 1;
            $ssTable->save();
        }
    }
}
