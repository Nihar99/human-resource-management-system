<?php

use App\ProfessionalDetails;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class ProfessionalTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (!App::environment('production')) {
            $professionalDetails                    = new ProfessionalDetails();
            $professionalDetails->email             = 'veeraj@3iology.com';
            $professionalDetails->department_id     = 1;
            $professionalDetails->designation_id    = 1;
            $professionalDetails->probation_from    = '2011-01-01';
            $professionalDetails->confirmation_date = '2011-04-01';
            $professionalDetails->employee_id       = 1;
            $professionalDetails->save();
        }
    }
}
