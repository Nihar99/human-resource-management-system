<?php

use App\Employee;
use App\EmployeeAuth;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;

class EmployeesTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (!App::environment('production')) {
            $employee                    = new Employee();
            $employee->client_id         = 1;
            $employee->added_by_admin    = 1;
            $employee->added_by_employee = null;
            $employee->ecode             = 'E0001';
            $employee->status            = 1;
            $employee->ess               = true;
            $employee->save();
            $empId = $employee->id;

            $empAuth                = new EmployeeAuth();
            $empAuth->employee_id   = $empId;
            $empAuth->password_hash = Hash::make('12345678');
            $empAuth->save();

            // $employee1                    = new Employee();
            // $employee1->client_id         = 1;
            // $employee1->added_by_admin    = null;
            // $employee1->added_by_employee = 1;
            // $employee1->ecode             = 'E0002';
            // $employee1->status            = 2;
            // $employee1->ess               = true;
            // $employee1->save();
            // $empId = $employee1->id;

            // $empAuth1                = new EmployeeAuth();
            // $empAuth1->employee_id   = $empId;
            // $empAuth1->password_hash = Hash::make('12345678');
            // $empAuth1->save();
        }
    }
}
