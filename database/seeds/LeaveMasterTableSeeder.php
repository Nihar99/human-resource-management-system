<?php

use App\LeaveMaster;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class LeaveMasterTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $leavemaster = new LeaveMaster();
        $leavemaster->insert([
            [
                'type'       => 'Casual',
                'total_leaves' => 5,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'type'       => 'Privilage',
                'total_leaves' => 18,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'type'       => 'Sick',
                'total_leaves' => 5,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'type'       => 'Maternal',
                'total_leaves' => 180,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
        ]);
    }
}
