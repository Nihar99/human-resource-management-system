<?php

use App\Client;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class ClientTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (!App::environment('production')) {
            $data = [
                ['name' => '3iology'],
                ['name' => 'Varadendra Infotech'],
            ];
            (new Client)->insertMany($data);
        }
    }
}
