<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReferredBiesTable extends Migration {
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('referred_bies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id');
            $table->string('name');
            $table->string('position');
            $table->string('address');
            $table->string('tel_no');
            $table->unsignedBigInteger('added_by_admin')->nullable();
            $table->unsignedBigInteger('added_by_employee')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('added_by_admin')->references('id')->on('admins');
            $table->foreign('added_by_employee')->references('id')->on('employees');
        });
        (new ReferredBySeeder())->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('referred_bies', function (Blueprint $table) {
            $table->dropForeign('referred_bies_employee_id_foreign');
            $table->dropForeign('referred_bies_added_by_admin_foreign');
            $table->dropForeign('referred_bies_added_by_employee_foreign');
        });
        Schema::dropIfExists('referred_bies');
    }
}
