<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('added_by_admin')->nullable();
            $table->unsignedBigInteger('added_by_employee')->nullable();
            $table->string('ecode');
            $table->tinyInteger('status')->comment('1-Working, 2-Resigned, 3-Absconding');
            $table->boolean('ess')->comment('True-Enabled, False-Disabled');
            $table->unsignedBigInteger('source_id')->nullable()->comment('Excel file source id');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('added_by_admin')->references('id')->on('admins');
            $table->foreign('added_by_employee')->references('id')->on('employees');
            $table->foreign('source_id')->references('id')->on('sources');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('employees', function (Blueprint $table) {
            $table->dropForeign('employees_client_id_foreign');
            $table->dropForeign('employees_source_id_foreign');
            $table->dropForeign('employees_added_by_admin_foreign');
            $table->dropForeign('employees_added_by_employee_foreign');
        });
        Schema::dropIfExists('employees');
    }
}
