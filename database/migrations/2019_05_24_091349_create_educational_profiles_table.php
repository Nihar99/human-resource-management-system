<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEducationalProfilesTable extends Migration {
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('educational_profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id');
            $table->string('board_degree');
            $table->string('year_of_passing');
            $table->string('institution_name');
            $table->string('principle_subjects');
            $table->string('grade_percentage');
            $table->unsignedBigInteger('added_by_admin')->nullable();
            $table->unsignedBigInteger('added_by_employee')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('added_by_admin')->references('id')->on('admins');
            $table->foreign('added_by_employee')->references('id')->on('employees');
        });
        (new EducationalProfileTableSeeder)->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('educational_profiles', function (Blueprint $table) {
            $table->dropForeign('educational_profiles_employee_id_foreign');
            $table->dropForeign('educational_profiles_added_by_admin_foreign');
            $table->dropForeign('educational_profiles_added_by_employee_foreign');
        });
        Schema::dropIfExists('educational_profiles');
    }
}
