<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLanguagesKnownsTable extends Migration {
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('languages_knowns', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id');
            $table->string('name');
            $table->boolean('read');
            $table->boolean('write');
            $table->boolean('speak');
            $table->boolean('understand');
            $table->unsignedBigInteger('added_by_admin')->nullable();
            $table->unsignedBigInteger('added_by_employee')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('added_by_admin')->references('id')->on('admins');
            $table->foreign('added_by_employee')->references('id')->on('employees');
        });
        (new LanguagesKnownTableSeeder)->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('languages_knowns', function (Blueprint $table) {
            $table->dropForeign('languages_knowns_employee_id_foreign');
            $table->dropForeign('languages_knowns_added_by_admin_foreign');
            $table->dropForeign('languages_knowns_added_by_employee_foreign');
        });
        Schema::dropIfExists('languages_knowns');
    }
}
