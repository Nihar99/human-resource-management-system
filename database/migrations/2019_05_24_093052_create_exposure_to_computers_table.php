<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExposureToComputersTable extends Migration {
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('exposure_to_computers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id');
            $table->string('language');
            $table->string('operating_system');
            $table->string('application');
            $table->string('hardware');
            $table->unsignedBigInteger('added_by_admin')->nullable();
            $table->unsignedBigInteger('added_by_employee')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('added_by_admin')->references('id')->on('admins');
            $table->foreign('added_by_employee')->references('id')->on('employees');
        });
        (new ComputerExposureSeeder)->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('exposure_to_computers', function (Blueprint $table) {
            $table->dropForeign('exposure_to_computers_employee_id_foreign');
            $table->dropForeign('exposure_to_computers_added_by_admin_foreign');
            $table->dropForeign('exposure_to_computers_added_by_employee_foreign');
        });
        Schema::dropIfExists('exposure_to_computers');
    }
}
