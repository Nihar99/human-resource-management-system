<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkExperiencesTable extends Migration {
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('work_experiences', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id');
            $table->string('organisation_name');
            $table->string('organisation_address');
            $table->date('from_date');
            $table->date('to_date');
            $table->string('position_held');
            $table->string('reporting_to');
            $table->string('nature_of_work');
            $table->string('reason_of_leaving');
            $table->unsignedBigInteger('added_by_admin')->nullable();
            $table->unsignedBigInteger('added_by_employee')->nullable();
            $table->string('gross_annual');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('added_by_admin')->references('id')->on('admins');
            $table->foreign('added_by_employee')->references('id')->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('work_experiences', function (Blueprint $table) {
            $table->dropForeign('work_experiences_employee_id_foreign');
            $table->dropForeign('work_experiences_added_by_admin_foreign');
            $table->dropForeign('work_experiences_added_by_employee_foreign');
        });
        Schema::dropIfExists('work_experiences');
    }
}
