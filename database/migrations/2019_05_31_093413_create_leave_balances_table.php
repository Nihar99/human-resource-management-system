<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeaveBalancesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('leave_balances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id');
            $table->unsignedBigInteger('leave_master_id');
            $table->float('available', 3, 1);
            $table->timestamps();
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('leave_master_id')->references('id')->on('leave_masters');
        });
        (new LeaveBalanceSeeder())->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('leave_balances', function (Blueprint $table) {
            $table->dropForeign('leave_balances_employee_id_foreign');
            $table->dropForeign('leave_balances_leave_master_id_foreign');
        });
        Schema::dropIfExists('leave_balances');
    }
}
