<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendanceLocationsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('attendance_locations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_attendance_id');
            $table->float('latitude');
            $table->float('longitude');
            $table->tinyInteger('status')->comment('1-Time In, 2-Timeout, 3-Break In, 4-Break Out');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('employee_attendance_id')->references('id')->on('employee_attendances');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('attendance_locations', function (Blueprint $table) {
            $table->dropForeign('attendance_locations_employee_attendance_id_foreign');
        });
        Schema::dropIfExists('attendance_locations');
    }
}
