<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeaveLedgersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('leave_ledgers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id');
            $table->unsignedBigInteger('leave_master_id');
            $table->boolean('added_removed')->comment('True - added, False - removed');
            $table->float('no_of_leaves', 3, 1);
            $table->tinyInteger('inserted_from')->comment('1-Policy 2-Excel File 3-Leaves Consumed 4-Manually 5-Previous Policy Year 6-Next Policy Year');
            $table->date('policy_from');
            $table->date('policy_to');
            $table->unsignedBigInteger('source_id')->nullable()->comment('Excel file source id');
            $table->softDeletes();
            $table->timestamps();
            $table->unsignedBigInteger('apply_leave_id')->nullable();

            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('source_id')->references('id')->on('sources');
            $table->foreign('leave_master_id')->references('id')->on('leave_masters');
            $table->foreign('apply_leave_id')->references('id')->on('apply_leaves');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('leave_ledgers', function (Blueprint $table) {
            $table->dropForeign('leave_ledgers_employee_id_foreign');
            $table->dropForeign('leave_ledgers_source_id_foreign');
            $table->dropForeign('leave_ledgers_leave_master_id_foreign');
            $table->dropForeign('leave_ledgers_apply_leave_id_foreign');
        });
        Schema::dropIfExists('leave_ledgers');
    }
}
