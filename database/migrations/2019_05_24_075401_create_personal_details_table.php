<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalDetailsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('personal_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id')->unique();
            $table->unsignedBigInteger('photo')->nullable();
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->boolean('gender')->comment('True - Male, False - Female');
            $table->date('date_of_birth');
            $table->unsignedBigInteger('nationality');
            $table->string('place_state_of_birth');
            $table->string('religion');
            $table->tinyInteger('marital_status')->comment('1-Married, 2-Un-married, 3-Divorsed');
            $table->string('mother_tongue');
            $table->string('email')->nullable();
            $table->string('hobbies_and_interests')->nullable();
            $table->timestamps();
            $table->foreign('photo')->references('id')->on('files');
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('nationality')->references('id')->on('countries');
        });

        (new PersonalDetailsTableSeeder)->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('personal_details', function (Blueprint $table) {
            $table->dropForeign('personal_details_photo_foreign');
            $table->dropForeign('personal_details_employee_id_foreign');
            $table->dropForeign('personal_details_nationality_foreign');
        });
        Schema::dropIfExists('personal_details');
    }
}
