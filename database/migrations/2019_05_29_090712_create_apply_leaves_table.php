<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplyLeavesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('apply_leaves', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id');
            $table->unsignedBigInteger('leave_master_id');
            $table->date('from_date');
            $table->date('to_date');
            $table->text('reason')->nullable(true);
            $table->tinyInteger('approved')->comment('1-Approve, 0-Rejected, 2-Pending')->default(2);
            $table->unsignedBigInteger('added_by_admin')->nullable();
            $table->unsignedBigInteger('added_by_employee')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('added_by_admin')->references('id')->on('admins');
            $table->foreign('added_by_employee')->references('id')->on('employees');
            $table->foreign('leave_master_id')->references('id')->on('leave_masters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('apply_leaves', function (Blueprint $table) {
            $table->dropForeign('apply_leaves_added_by_admin_foreign');
            $table->dropForeign('apply_leaves_added_by_employee_foreign');
            $table->dropForeign('apply_leaves_leave_master_id_foreign');
        });
        Schema::dropIfExists('apply_leaves');
    }
}
