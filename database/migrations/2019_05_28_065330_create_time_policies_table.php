<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimePoliciesTable extends Migration {
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('time_policies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('added_by_admin')->nullable();
            $table->unsignedBigInteger('added_by_employee')->nullable();
            $table->tinyInteger('day')->comment('1-Monday, 2-Tuesday, 3-Wednesday, 4-Thursday, 5-Friday, 6-Saturday, 7-Sunday');
            $table->time('time_in')->nullable();
            $table->time('time_out')->nullable();
            $table->time('break_in')->nullable();
            $table->time('break_out')->nullable();
            $table->time('late_entry_after')->nullable();
            $table->time('early_exit_before')->nullable();
            $table->time('halfday_employee_comes_after')->nullable();
            $table->time('halfday_employee_leaves_before')->nullable();
            $table->boolean('weekoff')->default(false);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('added_by_admin')->references('id')->on('admins');
            $table->foreign('added_by_employee')->references('id')->on('employees');
        });
        (new TimePolicyTableSeeder)->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('time_policies', function (Blueprint $table) {
            $table->dropForeign('time_policies_client_id_foreign');
            $table->dropForeign('time_policies_added_by_admin_foreign');
            $table->dropForeign('time_policies_added_by_employee_foreign');
        });
        Schema::dropIfExists('time_policies');
    }
}
