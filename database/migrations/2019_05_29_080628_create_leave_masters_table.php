<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeaveMastersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('leave_masters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type');
            $table->integer('total_leaves')->default(0);
            $table->timestamps();
        });
        (new LeaveMasterTableSeeder)->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('leave_masters');
    }
}
