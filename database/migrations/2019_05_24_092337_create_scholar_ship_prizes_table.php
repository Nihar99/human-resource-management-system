<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScholarShipPrizesTable extends Migration {
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('scholar_ship_prizes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id');
            $table->string('details');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('added_by_admin')->nullable();
            $table->unsignedBigInteger('added_by_employee')->nullable();
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('added_by_admin')->references('id')->on('admins');
            $table->foreign('added_by_employee')->references('id')->on('employees');
        });
        (new ScholarShipTableSeeder)->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('scholar_ship_prizes', function (Blueprint $table) {
            $table->dropForeign('scholar_ship_prizes_employee_id_foreign');
            $table->dropForeign('scholar_ship_prizes_added_by_admin_foreign');
            $table->dropForeign('scholar_ship_prizes_added_by_employee_foreign');
        });
        Schema::dropIfExists('scholar_ship_prizes');
    }
}
