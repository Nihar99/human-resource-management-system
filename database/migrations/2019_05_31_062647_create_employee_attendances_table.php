<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeAttendancesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('employee_attendances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id');
            $table->unsignedBigInteger('added_by_admin')->nullable();
            $table->unsignedBigInteger('added_by_employee')->nullable();
            $table->unsignedBigInteger('source_id')->nullable()->comment('Excel file source id');
            $table->date('on_date')->comment('Date of attendance');
            $table->time('time_in')->nullable();
            $table->time('time_out')->nullable();
            $table->time('break_in')->nullable();
            $table->time('break_out')->nullable();
            $table->tinyInteger('status')
                ->comment('1-Present, 2-Absent, 3-Late Entry, 4-Early Exit, 5-Half Day, 6-Outdoor Duty, 7-Paid Holiday, 8-Comp Off, 9-Leave');
            $table->unsignedBigInteger('leave_master_id')->nullable();
            $table->unsignedBigInteger('apply_leave_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('added_by_admin')->references('id')->on('admins');
            $table->foreign('added_by_employee')->references('id')->on('employees');
            $table->foreign('source_id')->references('id')->on('sources');
            $table->foreign('leave_master_id')->references('id')->on('leave_masters');
            $table->foreign('apply_leave_id')->references('id')->on('apply_leaves');
        });
        (new AttendanceTableSeeder)->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('employee_attendances', function (Blueprint $table) {
            $table->dropForeign('employee_attendances_employee_id_foreign');
            $table->dropForeign('employee_attendances_source_id_foreign');
            $table->dropForeign('employee_attendances_added_by_admin_foreign');
            $table->dropForeign('employee_attendances_added_by_employee_foreign');
            $table->dropForeign('employee_attendances_leave_master_id_foreign');
        });
        Schema::dropIfExists('employee_attendances');
    }
}
