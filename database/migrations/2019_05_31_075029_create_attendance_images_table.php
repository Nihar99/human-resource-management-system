<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendanceImagesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('attendance_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_attendance_id');
            $table->unsignedBigInteger('file_id');
            $table->tinyInteger('status')->comment('1-Time In, 2-Timeout, 3-Break In, 4-Break Out');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('employee_attendance_id')->references('id')->on('employee_attendances');
            $table->foreign('file_id')->references('id')->on('files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('attendance_images', function (Blueprint $table) {
            $table->dropForeign('attendance_images_employee_attendance_id_foreign');
            $table->dropForeign('attendance_images_file_id_foreign');
        });
        Schema::dropIfExists('attendance_images');
    }
}
