<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientSubscriptionsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('client_subscriptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('client_id')->unique();
            $table->date('expires_on');
            $table->timestamps();
            $table->foreign('client_id')->references('id')->on('clients');
        });
        (new ClientSubscriptionTableSeeder)->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('client_subscriptions', function (Blueprint $table) {
            $table->dropForeign('client_subscriptions_client_id_foreign');
        });
        Schema::dropIfExists('client_subscriptions');
    }
}
