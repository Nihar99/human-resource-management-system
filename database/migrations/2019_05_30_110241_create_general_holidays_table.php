<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneralHolidaysTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('general_holidays', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->date('holiday_date');
            $table->softDeletes();
            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('added_by_admin')->nullable();
            $table->unsignedBigInteger('added_by_employee')->nullable();
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('added_by_admin')->references('id')->on('admins');
            $table->foreign('added_by_employee')->references('id')->on('employees');
        });
        (new GeneralHolidayTableSeeder())->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('general_holidays', function (Blueprint $table) {
            $table->dropForeign('general_holidays_client_id_foreign');
            $table->dropForeign('general_holidays_added_by_admin_foreign');
            $table->dropForeign('general_holidays_added_by_employee_foreign');
        });
        Schema::dropIfExists('general_holidays');
    }
}
