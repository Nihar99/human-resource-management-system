<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFamilyDetailsTable extends Migration {
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('family_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('dependant_to');
            $table->string('full_name');
            $table->tinyInteger('age');
            $table->string('occupation');
            $table->string('relationship');
            $table->unsignedBigInteger('added_by_admin')->nullable();
            $table->unsignedBigInteger('added_by_employee')->nullable();
            $table->boolean('gender')->comment('True - Male, False - Female');
            $table->boolean('dependant')->comment('True - Yes, False - No');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('dependant_to')->references('id')->on('employees');
            $table->foreign('added_by_admin')->references('id')->on('admins');
            $table->foreign('added_by_employee')->references('id')->on('employees');
        });
        (new FamilyDetailsTableSeeder)->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('family_details', function (Blueprint $table) {
            $table->dropForeign('family_details_dependant_to_foreign');
            $table->dropForeign('family_details_added_by_admin_foreign');
            $table->dropForeign('family_details_added_by_employee_foreign');
        });
        Schema::dropIfExists('family_details');
    }
}
