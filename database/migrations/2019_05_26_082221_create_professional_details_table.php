<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfessionalDetailsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('professional_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id')->unique();
            $table->string('email')->nullable();
            $table->unsignedBigInteger('department_id');
            $table->unsignedBigInteger('designation_id');
            $table->date('probation_from')->nullable();
            $table->date('confirmation_date')->nullable();
            $table->timestamps();
            $table->foreign('department_id')->references('id')->on('departments');
            $table->foreign('designation_id')->references('id')->on('designations');
            $table->foreign('employee_id')->references('id')->on('employees');
        });
        (new ProfessionalTableSeeder)->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('professional_details', function (Blueprint $table) {
            $table->dropForeign('professional_details_department_id_foreign');
            $table->dropForeign('professional_details_designation_id_foreign');
            $table->dropForeign('professional_details_employee_id_foreign');
        });
        Schema::dropIfExists('professional_details');
    }
}
