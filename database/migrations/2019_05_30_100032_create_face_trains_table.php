<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaceTrainsTable extends Migration {
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('face_trains', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id')->unique();
            $table->unsignedBigInteger('photo')->nullable();
            $table->unsignedBigInteger('compressed_photo')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('added_by_admin')->nullable();
            $table->unsignedBigInteger('added_by_employee')->nullable();

            $table->foreign('photo')->references('id')->on('files');
            $table->foreign('compressed_photo')->references('id')->on('files');
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('added_by_admin')->references('id')->on('admins');
            $table->foreign('added_by_employee')->references('id')->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('face_trains', function (Blueprint $table) {
            $table->dropForeign('face_trains_photo_foreign');
            $table->dropForeign('face_trains_employee_id_foreign');
            $table->dropForeign('face_trains_added_by_admin_foreign');
            $table->dropForeign('face_trains_added_by_employee_foreign');
        });
        Schema::dropIfExists('face_trains');
    }
}
