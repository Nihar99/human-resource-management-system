<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeAddressesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('employee_addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id');
            $table->string('present_address');
            $table->string('present_address_tel_no')->nullable();
            $table->string('permanent_address');
            $table->string('permanent_address_tel_no')->nullable();
            $table->timestamps();
            $table->foreign('employee_id')->references('id')->on('employees');
        });
        (new EmployeeAddressTableSeeder)->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('employee_addresses', function (Blueprint $table) {
            $table->dropForeign('employee_addresses_employee_id_foreign');
        });
        Schema::dropIfExists('employee_addresses');
    }
}
