<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TimePolicy extends Model {
    use SoftDeletes;
    protected $softDelete = true;
    public function insertMultiple($data) {
        return $this->insert($data);
    }
    public function store(int $client_id,$added_by_admin,$added_by_employee,int $day,$time_in,$time_out,$break_in,
    $break_out,$late_entry_after,$early_exit_before,$halfday_employee_comes_after,$halfday_employee_leaves_before,
    int $weekoff) :int
    {
      $this->client_id = $client_id;
      $this->added_by_admin = $added_by_admin;
      $this->added_by_employee = $added_by_employee;
      $this->day = $day;
      $this->time_in = $time_in;
      $this->time_out = $time_out;
      $this->break_in = $break_in;
      $this->break_out = $break_out;
      $this->late_entry_after = $late_entry_after;
      $this->early_exit_before = $early_exit_before;
      $this->halfday_employee_comes_after = $halfday_employee_comes_after;
      $this->halfday_employee_leaves_before = $halfday_employee_leaves_before;
      $this->weekoff = $weekoff;
      $this->save();
      return $this->id;
    }
    public function fetchById($id) {
        $TimePolicybject = $this->find($id);
        return ($TimePolicybject != null) ? $TimePolicybject : false;
    }
    public function getAllPolicies()
    {
        return $this->all();
    }
    public function deleteById($timePolicyId)
    {
        $timePolicy = $this->find($timePolicyId);
        if ($timePolicy != null) {
            $timePolicy_delete = $timePolicy->delete();
            return $timePolicy_delete;
        } else {
            return false;
        }
    }
    public function updateById($timePolicyId,int $client_id,$added_by_admin,$added_by_employee,int $day,$time_in,$time_out,$break_in,
                               $break_out,$late_entry_after,$early_exit_before,$halfday_employee_comes_after,$halfday_employee_leaves_before,
                                int $weekoff) :int
    {
        $timePolicy = $this->find($timePolicyId);
        $timePolicy->day = $day;
        $timePolicy->client_id         = $client_id;
        $timePolicy->added_by_admin    = $added_by_admin;
        $timePolicy->added_by_employee = $added_by_employee;
        $timePolicy->time_in = $time_in;
        $timePolicy->time_out = $time_out;
        $timePolicy->break_in = $break_in;
        $timePolicy->break_out = $break_out;
        $timePolicy->late_entry_after = $late_entry_after;
        $timePolicy->early_exit_before = $early_exit_before;
        $timePolicy->halfday_employee_comes_after = $halfday_employee_comes_after;
        $timePolicy->halfday_employee_leaves_before = $halfday_employee_leaves_before;
        $timePolicy->weekoff = $weekoff;
        $timePolicy->save();
        return $timePolicy->id;
    }
    public function DayExistsInClient($client_id, $day_name) {
        $Day = $this->where('day', $day_name)->where('client_id', $client_id);
        return $Day->exists();
    }

    public function weekend($client_id)
    {
        return $this->where('weekoff',1)->where('client_id',$client_id)->get()->pluck('day');
    }

}
