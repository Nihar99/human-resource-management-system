<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Department;
use App\Designation;

class ProfessionalDetails extends Model {
    public function create(int $employee_id,$professional_email, int $department_id,int $designation_id,string $probation_from,$confirmation_date) :int
    {
        $this->employee_id = $employee_id;
        $this->email = $professional_email;
        $this->department_id = $department_id;
        $this->designation_id = $designation_id;
        $this->probation_from = $probation_from;
        $this->confirmation_date = $confirmation_date;
        $this->save();
        return $this->id;

    }
    public function fetchById($id) {
        $pro_detailsObject = $this->find($id);
        return ($pro_detailsObject != null) ? $pro_detailsObject : false;
    }
    public function getAllProfessionalDetails()
    {
        return $this->all();
    }
    public function fetchDepartmentById($id)
    {
        $pro_detailsObject = $this->where('employee_id',$id)->get()[0];
        return (new Department())->fetchNameById($pro_detailsObject->department_id);
    }
    public function fetchDesignationById($id)
    {
        $pro_detailsObject = $this->where('employee_id',$id)->get()[0];
        return (new Designation())->fetchNameById($pro_detailsObject->department_id);
    }

    public function insertBatch(
        int $employee_id,$professional_email, int $department_id,int $designation_id,string $probation_from,$confirmation_date
    ){
        $array = array(
            'employee_id' => $employee_id,
            'email' => $professional_email,
            'department_id' => $department_id,
            'designation_id' => $designation_id,
            'probation_from' => $probation_from,
            'confirmation_date' => $confirmation_date,
        );
        return $array;
    }
    public function UpdateById($id,int $employee_id,$professional_email, int $department_id,int $designation_id,string $probation_from,$confirmation_date)
    {
        $professional_details = $this->find($id);
        $professional_details->employee_id = $employee_id;
        $professional_details->email = $professional_email;
        $professional_details->department_id = $department_id;
        $professional_details->designation_id = $designation_id;
        $professional_details->probation_from = $probation_from;
        $professional_details->confirmation_date = $confirmation_date;
        $professional_details->save();
    }
}
