<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaceTrain extends Model
{
    public function store($employee_id, $photo, $compressed_photo,  $added_by_admin, $added_by_employee)
    {
       $this->employee_id = $employee_id;
       $this->photo = $photo;
       $this->compressed_photo = $compressed_photo;
       $this->added_by_admin = $added_by_admin;
       $this->added_by_employee = $added_by_employee;
       $this->save();
       return $this->id;
    }
}
