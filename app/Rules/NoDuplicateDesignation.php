<?php

namespace App\Rules;

use App\Designation;
use App\Http\Controllers\SessionController;
use Illuminate\Contracts\Validation\Rule;

class NoDuplicateDesignation implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return !(new Designation())->designationExistsInClient(SessionController::getClientIdFromSession(), $value);
    }


    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Duplicate Designation';
    }
}
