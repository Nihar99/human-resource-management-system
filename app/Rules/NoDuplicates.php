<?php

namespace App\Rules;

use App\Department;
use Illuminate\Contracts\Validation\Rule;

class NoDuplicates implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
       //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute,  $value){
        $department = new Department();
   if( $department->where('name', '=', $value)->first())
    {
        return false;
    }
    else{
            return true;
    }
}


    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Duplicate Names are not allowed.';
    }
}
