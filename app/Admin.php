<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model {
    public function insert(
        string $name,
        string $email,
        string $contact,
        int    $client_id
    ): int {
        $this->name      = $name;
        $this->email     = $email;
        $this->contact   = $contact;
        $this->client_id = $client_id;
        $this->save();
        return $this->id;
    }

    public function fetchById($id) {
        return $this->find($id);
    }

    public function fetchAdminByEmail(string $email) {
        $adminObject = $this->where('email', $email)->first();
        return ($adminObject != null) ? $adminObject : false;
    }
}
