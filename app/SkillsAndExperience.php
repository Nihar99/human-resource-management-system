<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SkillsAndExperience extends Model
{
    use SoftDeletes;
    protected $softDelete = true;
    public function store(int $employee_id,string $details,?int $adminId,?int $employeeId) :int
    {
        $this->employee_id = $employee_id;
        $this->details = $details;
        $this->added_by_admin = $adminId;
        $this->added_by_employee = $employeeId;
        $this->save();
        return $this->id;

    }
    public function fetchById($id)
    {
        $skillsObject = $this->find($id);
        return ($skillsObject != null) ? $skillsObject : false;
    }
    public function getAllSkills()
    {
        return $this->all();
    }
    public function updateById($Id,int $employee_id,string $details,?int $adminId,?int $employeeId) :int
    {
            $skill = $this->find($Id);
        $skill->employee_id = $employee_id;
        $skill->details = $details;
        $skill->added_by_admin = $adminId;
        $skill->added_by_employee = $employeeId;
        $skill->save();
        return $skill->id;
    }
    public function deleteById($ID) {
        $skill = $this->find($ID);
        if($skill != null) {
            $skill_delete = $skill->delete();
            return $skill_delete;
        }
        else
        {
            return false;
        }
    }
    public function getEmployeeSkillsExposures($id) {
        return $this->where('employee_id',$id)->get();
    }
}
