<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReferredBy extends Model
{
    use SoftDeletes;
    protected $softDelete = true;
    protected $table = 'referred_bies';

    public function store(int $employee_id,string $name,string $position, string $address,string $tel_no,?int $adminId,?int $employeeId) : int{
        $this->employee_id          = $employee_id;
        $this->name                 = $name;
        $this->position             = $position;
        $this->address              = $address;
        $this->tel_no               = $tel_no;
        $this->added_by_admin       = $adminId;
        $this->added_by_employee    = $employeeId;
        $this->save();
        return $this->id;
    }

    public function getAllReferredBy($employeeId) {
        return $this->all()->where('employee_id',$employeeId);
    }

    public function fetchById($id) {
        $referredByObject = $this->find($id);
        return ($referredByObject != null) ? $referredByObject : false;
    }

    public function deleteById($ID) {
        $referredBy = $this->find($ID);
        if($referredBy != null) {
            $referredBy_delete = $referredBy->delete();
            return $referredBy_delete;
        }
        else
        {
            return false;
        }
    }

}
