<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Designation extends Model
{
    use SoftDeletes;
    protected $softDelete = true;
    protected $fillable = [
        'name',
    ];
    protected $hidden = [
        'id',
    ];
    protected $casts = [

    ];
    public function store(string $designationname, int $client_id,int $admin_id=null, $employee_id=null): int {
        $this->name      = $designationname;
        $this->client_id = $client_id;
        $this->added_by_admin = $admin_id;
        $this->added_by_employee = $employee_id;
        $this->save();
        return $this->id;
    }
    public function getAllDesignation()
    {
        return $this->all();
    }
    public function fetchById($id) {
        $designationObject = $this->find($id);
        return ($designationObject != null) ? $designationObject : false;
    }
    public function designationExistsInClient($client_id, $designation_name) {
        $designation = $this->where('name', $designation_name)->where('client_id', $client_id);
        return $designation->exists();
    }
    public function fetchNameById($id)
    {
        $designationObject = $this->find($id);
        return $designationObject->name;
    }
    public function fetchDesignationIdByName($name)
    {
        $designationObject = $this->where('name', $name)->get()[0];
        return $designationObject->id;
    }
    public function deleteById($designationID)
    {
        $designation = $this->find($designationID);
        if ($designation != null) {
            $dept_delete = $designation->delete();
            return $dept_delete;
        } else {
            return false;
        }
    }
    public function updateById($designationId,string $designationname,int $client_id,$admin_id,$employee_id) :int
    {
        $designation = $this->find($designationId);
        $designation->name = $designationname;
        $designation->client_id         = $client_id;
        $designation->added_by_admin    = $admin_id;
        $designation->added_by_employee = $employee_id;
        $designation->save();
        return $designation->id;
    }

    public function validateDesignationByName($name)
    {
        $designationObject = $this->where('name', $name)->exists();
        return $designationObject;
    }
}
