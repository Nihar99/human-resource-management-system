<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class EmployeeAuth extends Authenticatable    {

    use HasApiTokens, Notifiable;

    public function getEmployeeAuth($empid) {
        if ($this->where('employee_id', $empid)->exists()) {
            return $this->where('employee_id', $empid)->first();
        } else {
            return false;
        }
    }



    public function store(int $employee_id , $password)
    {
        $this->employee_id = $employee_id;
        $this->password_hash = $password;
        $this->save();
    }


    public function insertBatch(
        $employee_id,$password
    ){
        $array = array(
            'employee_id' => $employee_id,
            'password_hash' => $password,
        );
        return $array;
    }
}
