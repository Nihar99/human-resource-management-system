<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkExperience extends Model
{
    use SoftDeletes;   
    public function store(int $employee_id, $organisation_name,$organisation_address, $from_date, $to_date, $position_held, $reporting_to, $nature_of_work, $reason_of_leaving, $gross_annual)
    {
        $this->employee_id = $employee_id;
        $this->organisation_name = $organisation_name;
        $this->organisation_address = $organisation_address;
        $this->from_date = $from_date;
        $this->to_date = $to_date;
        $this->position_held = $position_held;
        $this->reporting_to = $reporting_to;
        $this->nature_of_work = $nature_of_work;
        $this->reason_of_leaving = $reason_of_leaving;
        $this->gross_annual = $gross_annual;
        $this->save();
        return $this->id;
    }

    public function fetchById($id) {
        $experienceObject = $this->find($id);
        return ($experienceObject != null) ? $experienceObject : false;
    }

    public function getAllWorkExperience() {
        return $this->all();
    }

    public function deleteById($experienceId)
    {
        $experience = $this->find($experienceId);
        if ($experience != null) {
            $exp_delete = $experience->delete();
            return $exp_delete;
        } else {
            return false;
        }
    }

    public function updateById($experienceId,$organisation_name,$organisation_address, $from_date, $to_date, $position_held, $reporting_to, $nature_of_work, $reason_of_leaving, $gross_annual) :int
    {

        $experience = $this->find($experienceId);
        $experience->organisation_name = $organisation_name;
        $experience->organisation_address = $organisation_address;
        $experience->from_date = $from_date;
        $experience->to_date = $to_date;
        $experience->position_held = $position_held;
        $experience->reporting_to = $reporting_to;
        $experience->nature_of_work = $nature_of_work;
        $experience->reason_of_leaving = $reason_of_leaving;
        $experience->gross_annual = $gross_annual;

       
        $experience->save();
        return $experience->id;
    }

    public function getEmployeeWorkExperience($id) {
        return $this->where('employee_id',$id)->get();
    }
}
