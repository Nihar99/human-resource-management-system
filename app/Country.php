<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function getAllCountries()
    {
        return $this->all();
    }

    public function fetchCountryIdByName($name)
    {
        $countryObject = $this->where('name', $name)->get()[0];
        return $countryObject->id;
    }

    public function validateCountryByName($name)
    {
        $countryObject = $this->where('name', $name)->exists();
        return $countryObject;
    }

}
