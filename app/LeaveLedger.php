<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveLedger extends Model
{
    public function store($employee_id,$leave_master_id,$added_removed,$no_of_leaves,
    $inserted_from,$policy_from,$policy_to,$source_id,$apply_leave_id)
    {
        $this->employee_id = $employee_id;
        $this->leave_master_id = $leave_master_id;
        $this->added_removed = $added_removed;
        $this->no_of_leaves = $no_of_leaves;
        $this->inserted_from = $inserted_from;
        $this->policy_from = $policy_from;
        $this->policy_to = $policy_to;
        $this->source_id = $source_id;
        $this->apply_leave_id = $apply_leave_id;
        $this->save();
        return $this->id;
    }

    public function getLedgerById($id) {
        return $this->find($id);
    }
    public function getAllLeaveLedgers()
    {
        return $this->all();
    }
    public function getEmployeeLeaveLedgers($id)
    {
        $ledger =  $this->
        where('employee_id' , $id)->get();
        return $ledger;
    }

}
