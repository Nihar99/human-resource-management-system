<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FamilyDetails extends Model
{

    use SoftDeletes;
    protected $softDelete = true;

    public function store(int $dependent_to,string $full_name,int $age, string $occupation,string $relationship,?int $adminId,?int $employeeId,int $gender,int $dependant) : int{
        $this->dependant_to = $dependent_to;
        $this->full_name = $full_name;
        $this->age = $age;
        $this->occupation = $occupation;
        $this->relationship = $relationship;
        $this->added_by_admin = $adminId;
        $this->added_by_employee = $employeeId;
        $this->gender = $gender;
        $this->dependant = $dependant;
        $this->save();
        return $this->id;
    }
    public function getAllFamilyDetails() {
        return $this->all();
    }
    public function fetchById($id) {
        $familyObject = $this->find($id);
        return ($familyObject != null) ? $familyObject : false;
    }
    public function getEmployeeFamilyDetails($id) {
        return $this->where('dependant_to',$id)->get();
    }
    public function deleteById($ID) {
        $detail = $this->find($ID);
        if($detail != null) {
            $detail_delete = $detail->delete();
            return $detail_delete;
        }
        else
        {
            return false;
        }
    }
    public function updateById($Id,int $employee_id,string $full_name,int $age, string $occupation,string $relationship,?int $adminId,?int $employeeId,int $gender,int $dependant) :int
    {
        $detail = $this->find($Id);
        $detail->dependant_to = $employee_id;
        $detail->full_name = $full_name;
        $detail->age = $age;
        $detail->occupation = $occupation;
        $detail->relationship = $relationship;
        $detail->added_by_admin = $adminId;
        $detail->added_by_employee = $employeeId;
        $detail->gender = $gender;
        $detail->dependant = $dependant;
        $detail->save();
        return $detail->id;
    }

}
