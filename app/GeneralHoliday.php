<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GeneralHoliday extends Model
{
    use SoftDeletes;
    protected $softDelete = true;

    public function store(string $holidayname,$holiday_date, int $client_id, ?int $admin_id , ?int $employee_id ): int {
        $this->name              = $holidayname;
        $this->holiday_date      = $holiday_date;
        $this->client_id         = $client_id;
        $this->added_by_admin    = $admin_id;
        $this->added_by_employee = $employee_id;
        $this->save();
        return $this->id;
    }

    public function fetchById($id) {
        $holidayObject = $this->find($id);
        return ($holidayObject != null) ? $holidayObject : false;
    }
    public function getAllHolidays() {
        return $this->all();
    }
    public function HolidayExistsInClient($client_id, $holiday_name) {
        $holiday = $this->where('name', $holiday_name)->where('client_id', $client_id);
        return $holiday->exists();
    }
    public function deleteById($holidayID) {
        $holiday = $this->find($holidayID);
        if($holiday != null) {
            $holiday_delete = $holiday->delete();
            return $holiday_delete;
        }
        else
        {
            return false;
        }
    }
    public function updateById($holidayId,string $holidayname,$holiday_date,int $client_id,$admin_id,$employee_id) :int
    {
        $holiday = $this->find($holidayId);
        $holiday->name = $holidayname;
        $holiday->holiday_date = $holiday_date;
        $holiday->client_id         = $client_id;
        $holiday->added_by_admin    = $admin_id;
        $holiday->added_by_employee = $employee_id;
        $holiday->save();
        return $holiday->id;
    }
    public function fetchNameById($id) {
        $holidayObject = $this->find($id);
        return $holidayObject->name;
    }

    public function checkHasHolidayBetweenDate($fromDate,$todate){
        return $this->whereBetween('holiday_date',[$fromDate,$todate])->count();
    }
}
