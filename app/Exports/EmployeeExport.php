<?php

namespace App\Exports;

use App\Department;
use Illuminate\Support\Facades\DB;

class EmployeeExport
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {

    }

    /**
     * @return array
     */
    public function headings(): array
    {
        $array=[
            'First Name *',
            'Middle Name',
            'Last Name *',
            'Gender *',
            'Date of Birth *',
            'Nationality *',
            'Present Address *',
            'Permanent Address *',
            'Telephone No(Present Address)',
            'Telephone No(Permanent Address)',
            'Place & State of Birth *',
            'Religion *',
            'Marital Status *',
            'Mother Tongue *',
            'Personal Email Id',
            'Hobbies & Interests',
            'Employee Code *',
            'Company Email Id',
            'Department *',
            'Designation *',
            'On Probation From *',
            'Date of confirmation',
            'Enable ESS *',
            'Password'
        ];

        return $array;
    }
}
