<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EducationalProfile extends Model
{
    use SoftDeletes;
    protected $softDelete = true;
    public function store(int $employee_id,string $board_degree,string $year_of_passing,string $institution_name,string $principle_subjects,string $grade_percentage,?int $adminId,?int $employeeId) :int
    {
        $this->employee_id = $employee_id;
        $this->board_degree = $board_degree;
        $this->year_of_passing = $year_of_passing;
        $this->institution_name = $institution_name;
        $this->principle_subjects = $principle_subjects;
        $this->grade_percentage = $grade_percentage;
        $this->added_by_admin = $adminId;
        $this->added_by_employee = $employeeId;
        $this->save();
        return $this->id;

    }
    public function fetchById($id)
    {
        $profileObject = $this->find($id);
        return ($profileObject != null) ? $profileObject : false;
    }
    public function getAllProfiles()
    {
        return $this->all();
    }
    public function updateById($Id,int $employee_id,string $board_degree,string $year_of_passing,string $institution_name,string $principle_subjects,string $grade_percentage,?int $adminId,?int $employeeId) :int
    {
        $profile = $this->find($Id);
        $profile->employee_id = $employee_id;
        $profile->board_degree = $board_degree;
        $profile->year_of_passing = $year_of_passing;
        $profile->institution_name = $institution_name;
        $profile->principle_subjects = $principle_subjects;
        $profile->grade_percentage = $grade_percentage;
        $profile->added_by_admin = $adminId;
        $profile->added_by_employee = $employeeId;
        $profile->save();
        return $profile->id;
    }
    public function deleteById($ID) {
        $profile = $this->find($ID);
        if($profile != null) {
            $profile_delete = $profile->delete();
            return $profile_delete;
        }
        else
        {
            return false;
        }
    }
    public function getEmployeeEducationalProfile($id) {
        return $this->where('employee_id',$id)->get();
    }
}
