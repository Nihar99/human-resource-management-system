<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplyLeave extends Model {
    public function store(
        int    $empId,
        int    $leaveMasterId,
        string $fromDate,
        string $toDate,
        string $reason,
        int    $is_half_day,
        ?int   $addedByAdmin,
        ?int   $addedByEmployee
    ) {

        $this->employee_id       = $empId;
        $this->leave_master_id   = $leaveMasterId;
        $this->from_date         = $fromDate;
        $this->to_date           = $toDate;
        $this->is_half_day       = $is_half_day;
        $this->reason            = $reason;
        $this->added_by_admin    = $addedByAdmin;
        $this->added_by_employee = $addedByEmployee;
        $this->save();
        return $this->id;
    }

    public function updateById(
        int    $leave_id,
        int    $leaveMasterId,
        string $fromDate,
        string $toDate,
        int $status
        ) {

        $applied_leave = $this->find($leave_id);
        $applied_leave->leave_master_id   = $leaveMasterId;
        $applied_leave->from_date         = $fromDate;
        $applied_leave->to_date           = $toDate;
        $applied_leave->approved          = $status;
        $applied_leave->save();
        return $applied_leave->id;
    }

    public function getAppliedLeaveById($id) {
        return $this->find($id);
    }

    public function leaveAccepted($id) {
        $applied           = $this->getAppliedLeaveById($id);
        $applied->approved = true;
        $applied->save();
    }

    public function fetchById($id) {
        $designationObject = $this->find($id);
        return ($designationObject != null) ? $designationObject : false;
    }

    public  function getAllLeaves($client_id,$employee_id)
    {
        return $this->select('personal_details.*','departments.name as deptName','designations.name as designName','apply_leaves.*','employees.ecode','employees.client_id')
        ->join('employees','employees.id','=','apply_leaves.employee_id')
        ->join('personal_details','personal_details.employee_id','=','employees.id')
        ->join('professional_details','professional_details.employee_id','=','employees.id')
        ->join('departments','departments.id','=','professional_details.department_id')
        ->join('designations','designations.id','=','professional_details.designation_id')
        ->where(function ($query)use($employee_id,$client_id) {
            if($employee_id > 0) {
                $query->where('apply_leaves.employee_id',$employee_id);
            }
            $query->where('employees.client_id',$client_id);
        })
        ->get();
    }

    public function employee(){
        $this->belongsTo('App\Employee','employee_id');
    }
    public function getFromDate($apply_leave_id)
    {
        return $this->where('id' , $apply_leave_id)->value('from_date');
    }
    public function getToDate($apply_leave_id)
    {
        return $this->where('id',$apply_leave_id)->value('to_date');
    }

    public function checkAppliedLeaveByDate($empid,$from_date,$to_date) {
        $leaveObject = $this->whereBetween('from_date', [$from_date, $to_date])
            ->orWhereBetween('to_date', [$from_date, $to_date])
            ->where('employee_id',$empid)
            ->first();
        return $leaveObject;
    }

}
