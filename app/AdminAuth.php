<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminAuth extends Model {
    public function fetchById(int $id) {
        $adminauthobject = $this->find($id);
        return ($adminauthobject != null) ? $adminauthobject : false;
    }

    public function getPasswordByAdminId($adminId) {
        return $this->where('admin_id', $adminId)->first();
    }
}
