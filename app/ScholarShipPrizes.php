<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScholarShipPrizes extends Model
{
    use SoftDeletes;
    protected $softDelete = true;
    public function store(int $employee_id,string $details,?int $adminId,?int $employeeId) :int
    {
        $this->employee_id = $employee_id;
        $this->details = $details;
        $this->added_by_admin = $adminId;
        $this->added_by_employee = $employeeId;
        $this->save();
        return $this->id;

    }
    public function fetchById($id)
    {
        $prizeObject = $this->find($id);
        return ($prizeObject != null) ? $prizeObject : false;
    }
    public function getAllPrizes()
    {
        return $this->all();
    }
   public function updateById($Id,int $employee_id,string $details,?int $adminId,?int $employeeId) :int
    {
        $prize = $this->find($Id);
        $prize->employee_id = $employee_id;
        $prize->details = $details;
        $prize->added_by_admin = $adminId;
        $prize->added_by_employee = $employeeId;
        $prize->save();
        return $prize->id;
    }
    public function deleteById($ID) {
        $prize = $this->find($ID);
        if($prize != null) {
            $prize_delete = $prize->delete();
            return $prize_delete;
        }
        else
        {
            return false;
        }
    }
    public function getEmployeeScholarships($id) {
        return $this->where('employee_id',$id)->get();
    }
}
