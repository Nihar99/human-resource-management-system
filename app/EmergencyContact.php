<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmergencyContact extends Model {
    use SoftDeletes;
    protected $softDelete = true;

    public function getAllEmergencyContacts()
    {
        return $this->all();
    }

    public function store(int $employee_id,string $name,string $relationship,string $address,int $phone,?int $adminId,?int $employeeId) : int{
        $this->employee_id = $employee_id;
        $this->name = $name;
        $this->relationship = $relationship;
        $this->address = $address;
        $this->phone = $phone;
        $this->added_by_admin = $adminId;
        $this->added_by_employee = $employeeId;
        $this->save();
        return $this->id;
    }
    public function fetchById($id) {
        $contactObject = $this->find($id);
        return ($contactObject != null) ? $contactObject : false;
    }
    public function deleteById($contactID) {
        $contact = $this->find($contactID);
        if($contact != null) {
            $contact_delete = $contact->delete();
            return $contact_delete;
        }
        else
        {
            return false;
        }
    }
    public function updateById($Id,int $employee_id, string $name,string $relationship,string $address,int $phone,?int $adminId,?int $employeeId) :int
    {
        $contact = $this->find($Id);
        $contact->employee_id = $employee_id;
        $contact->name = $name;
        $contact->relationship         = $relationship;
        $contact->address    = $address;
        $contact->phone = $phone;
        $contact->added_by_admin = $adminId;
        $contact->added_by_employee = $employeeId;
        $contact->save();
        return $contact->id;
    }
    public function getEmployeeEmergencyContacts($id) {
        return $this->where('employee_id',$id)->get();
    }
}
