<?php
namespace App\Helpers;

use App\Files;
use App\Http\Controllers\SessionController;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

class FileManager {
    /**
     * ImageManager constructor.
     */
    public function __construct()
    {
    }

    function uploadFileToS3($file) {

        $filePath = md5(time() . $file->getClientOriginalName());

        Storage::disk('s3')->put($filePath, file_get_contents($file));//upload file to s3

        //if issue arises for env null then run this "php artisan config:clear" in terminal
        $url= Config::get('app.aws_s3_url');//s3 link url

//        $filePathUrl = $url . $filePath;

        $filePathUrl=$filePath;

        $files = new Files();
        $size =  $file->getClientSize();
        $filesId = $files->store(uniqid() , $filePathUrl,$size,SessionController::getClientIdFromSession(),SessionController::getAdminId(),SessionController::getEmployeeId());
        return $filesId;

    }

    function readFileFromS3($filename) {

        $adapter = Storage::disk('s3')->getDriver()->getAdapter();

        $result = $adapter->getClient()->getCommand('GetObject', [
            'Bucket' => $adapter->getBucket(),
            'Key'    => $adapter->getPathPrefix().$filename
        ]);
        $request = $adapter->getClient()->createPresignedRequest($result, '+100 minute');

        return (string) $request->getUri();
    }
}
?>
