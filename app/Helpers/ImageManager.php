<?php
namespace App\Helpers;

class ImageManager {
    /**
     * ImageManager constructor.
     */
    public function __construct()
    {
    }

    function decodeImage($image) {
        //get the base-64 from data
        $base64_str = substr($image, strpos($image, ",")+1);

        //decode base64 string
        $image = base64_decode($base64_str);
        return $image;
    }
}
?>
