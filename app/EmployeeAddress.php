<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeAddress extends Model {
    public function create(int $employee_id,string $present_address,$present_address_tel_no,string $permanent_address,$permanent_address_tel_no)
    : int
    {
        $this->employee_id = $employee_id;
        $this->present_address = $present_address;
        $this->present_address_tel_no = $present_address_tel_no;
        $this->permanent_address = $permanent_address;
        $this->permanent_address_tel_no = $permanent_address_tel_no;
        $this->save();
        return $this->id;
    }
    public function fetchById($id) {
        $employeeAddressObject = $this->find($id);
        return ($employeeAddressObject != null) ? $employeeAddressObject : false;
    }

    public function insertBatch(
        int $employee_id,string $present_address,$present_address_tel_no,string $permanent_address,$permanent_address_tel_no
    ){
        $array = array(
            'employee_id' => $employee_id,
            'present_address' => $present_address,
            'present_address_tel_no' => $present_address_tel_no,
            'permanent_address' => $permanent_address,
            'permanent_address_tel_no' => $permanent_address_tel_no,
        );
        return $array;
    }
    public function UpdateById($id,int $employee_id,string $present_address,$present_address_tel_no,string $permanent_address,$permanent_address_tel_no)
    {
        $address = $this->find($id);
        $address->employee_id = $employee_id;
        $address->present_address = $present_address;
        $address->present_address_tel_no = $present_address_tel_no;
        $address->permanent_address = $permanent_address;
        $address->permanent_address_tel_no = $permanent_address_tel_no;
        $address->save();

    }
}
