<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model {
    public function fetchById($id) {
        return $this->find($id);
    }

    public function insertOne(string $name): int {
        $this->name = $name;
        $this->save();
        return $this->id;
    }

    public function fetchAll() {
        return $this->all();
    }

    public function insertMany(array $data) {
        return $this->insert($data);
    }
}
