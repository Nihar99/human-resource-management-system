<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class exposure_to_computer extends Model
{
    use SoftDeletes;
    protected $softDelete = true;

    public function getAllExposures()
    {
        return $this->all();
    }

    public function store(int $employee_id,$language,$operating_system,$application,$hardware,?int $adminId,?int $employeeId) : int{
        $this->employee_id = $employee_id;
        $this->language = $language;
        $this->operating_system = $operating_system;
        $this->application = $application;
        $this->hardware = $hardware;
        $this->added_by_admin = $adminId;
        $this->added_by_employee = $employeeId;
        $this->save();
        return $this->id;
    }
    public function fetchById($id) {
        $exposureObject = $this->find($id);
        return ($exposureObject != null) ? $exposureObject : false;
    }
    public function deleteById($exposureID) {
        $exposure = $this->find($exposureID);
        if($exposure != null) {
            $exposure_delete = $exposure->delete();
            return $exposure_delete;
        }
        else
        {
            return false;
        }
    }
    public function updateById($Id,int $employee_id,$language,$operating_system,$application,$hardware,?int $adminId,?int $employeeId) :int
    {
        $exposure = $this->find($Id);
        $exposure->employee_id = $employee_id;
        $exposure->language = $language;
        $exposure->operating_system         = $operating_system;
        $exposure->application    = $application;
        $exposure->hardware = $hardware;
        $exposure->added_by_admin = $adminId;
        $exposure->added_by_employee = $employeeId;
        $exposure->save();
        return $exposure->id;
    }
    public function getEmployeeComputerExposures($id) {
        return $this->where('employee_id',$id)->get();
    }
}
