<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonalDetails extends Model
{
    public function create(int $employee_id,$photo,string $first_name,$middle_name,string $last_name,int $gender
        ,string $date_of_birth, int $nationality,string $place_state_of_birth,string $religion,int $marital_status,string $mother_tongue,
                           $personal_email,$hobbies_and_interests) :int
    {
        $this->employee_id = $employee_id;
        $this->photo = $photo;
        $this->first_name = $first_name;
        $this->middle_name = $middle_name;
        $this->last_name = $last_name;
        $this->gender = $gender;
        $this->date_of_birth = $date_of_birth;
        $this->nationality = $nationality;
        $this->place_state_of_birth = $place_state_of_birth;
        $this->religion = $religion;
        $this->marital_status = $marital_status;
        $this->mother_tongue = $mother_tongue;
        $this->email = $personal_email;
        $this->hobbies_and_interests = $hobbies_and_interests;
        $this->save();
        return $this->id;

    }
    public function fetchById($id) {
        $per_detailsObject = $this->find($id);
        return ($per_detailsObject != null) ? $per_detailsObject : false;
    }
    public function getAllPersonalDetails()
    {
        return $this->all();
    }
    public function fetchNameById($id)
    {
        $per_detailsObject = $this->where('employee_id',$id)->get()[0];
         return $per_detailsObject->first_name . " " . $per_detailsObject->middle_name . " " . $per_detailsObject->last_name;

    }

    public function insertBatch(
        int $employee_id, int $photo,string $first_name,$middle_name,string $last_name,int $gender
        ,string $date_of_birth, int $nationality,string $place_state_of_birth,string $religion,int $marital_status,string $mother_tongue,
        $personal_email,$hobbies_and_interests
    ){
        $array = array(
            'employee_id' => $employee_id,
            'photo'  => $photo,
            'first_name' => $first_name,
            'middle_name' => $middle_name,
            'last_name' => $last_name,
            'gender' => $gender,
            'date_of_birth' => $date_of_birth,
            'nationality' => $nationality,
            'place_state_of_birth' => $place_state_of_birth,
            'religion' => $religion,
            'marital_status' => $marital_status,
            'mother_tongue' => $mother_tongue,
            'email' => $personal_email,
            'hobbies_and_interests' => $hobbies_and_interests
        );
        return $array;
    }
    public function UpdateById($id,int $employee_id,$photo,string $first_name,$middle_name,string $last_name,int $gender
        ,string $date_of_birth, int $nationality,string $place_state_of_birth,string $religion,int $marital_status,string $mother_tongue,
                               $personal_email,$hobbies_and_interests)
    {
        $personal_details = $this->find($id);
        $personal_details->employee_id = $employee_id;
        $personal_details->photo = $photo;
        $personal_details->first_name = $first_name;
        $personal_details->middle_name = $middle_name;
        $personal_details->last_name = $last_name;
        $personal_details->gender = $gender;
        $personal_details->date_of_birth = $date_of_birth;
        $personal_details->nationality = $nationality;
        $personal_details->place_state_of_birth = $place_state_of_birth;
        $personal_details->religion = $religion;
        $personal_details->marital_status = $marital_status;
        $personal_details->mother_tongue = $mother_tongue;
        $personal_details->email = $personal_email;
        $personal_details->hobbies_and_interests = $hobbies_and_interests;
        $personal_details->save();
    }
    public function files()
    {
         return $this->hasOne('App\Files','id','photo');
    }
}
