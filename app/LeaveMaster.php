<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveMaster extends Model
{
    public function getAllLeaveType(){
        return $this->where('id',"!=",4)->get();
    }
}
