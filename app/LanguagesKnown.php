<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LanguagesKnown extends Model
{
    use SoftDeletes;
    protected $softDelete = true;

    public function getAllLanguagesKnown()
    {
        return $this->all();
    }

    public function store(int $employee_id,string $name,?int $read,?int $write, ?int $speak, ?int $understand,?int $adminId,?int $employeeId) : int{
        $this->employee_id = $employee_id;
        $this->name = $name;
        $this->read = $read;
        $this->write = $write;
        $this->speak = $speak;
        $this->understand = $understand;
        $this->added_by_admin = $adminId;
        $this->added_by_employee = $employeeId;
        $this->save();
        return $this->id;
    }
    public function fetchById($id) {
        $languageKnownObject = $this->find($id);
        return ($languageKnownObject != null) ? $languageKnownObject : false;
    }
    public function deleteById($ID) {
        $languageKnown = $this->find($ID);
        if($languageKnown != null) {
            $languageKnown_delete = $languageKnown->delete();
            return $languageKnown_delete;
        }
        else
        {
            return false;
        }
    }
    public function fetchNameById($id) {
        $languageObject = $this->find($id);
        return $languageObject->name;
    }
    public function updateById($Id,int $employee_id, string $name,?int $read,?int $write, ?int $speak, ?int $understand,?int $adminId,?int $employeeId) :int
    {
        $languageKnown = $this->find($Id);
        $languageKnown->employee_id = $employee_id;
        $languageKnown->name = $name;
        $languageKnown->read         = $read;
        $languageKnown->write    = $write;
        $languageKnown->speak = $speak;
        $languageKnown->understand = $understand;
        $languageKnown->added_by_admin = $adminId;
        $languageKnown->added_by_employee = $employeeId;
        $languageKnown->save();
        return $languageKnown->id;
    }
    public function getEmployeeLanguagesKnown($id) {
        return $this->where('employee_id',$id)->get();
    }
    public function languageExistsForEmployee($language,$employeeId)
    {
        $language = $this->where('name', $language)->where('employee_id', $employeeId);
        return $language->exists();
    }

}
