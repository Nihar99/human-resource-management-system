<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model {
    use SoftDeletes;
    protected $softDelete = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [

    ];
    public function store(string $departmentname, int $client_id, int $admin_id = null, int $employee_id = null): int {
        $this->name              = $departmentname;
        $this->client_id         = $client_id;
        $this->added_by_admin    = $admin_id;
        $this->added_by_employee = $employee_id;
        $this->save();
        return $this->id;
    }
    public function getAllDepartment() {
        return $this->all();
    }

    public function fetchById($id) {
        $departmentObject = $this->find($id);
        return ($departmentObject != null) ? $departmentObject : false;
    }
    public function fetchNameById($id) {
        $departmentObject = $this->find($id);
        return $departmentObject->name;
    }

    public function departmentExistsInClient($client_id, $department_name) {
        $department = $this->where('name', $department_name)->where('client_id', $client_id);
        return $department->exists();
    }
    public function fetchDepartmentIdByName($name) {
        $departmentObject = $this->where('name',$name)->get()[0];
        return $departmentObject->id;
    }

    public function deleteById($departmentID) {
        $department = $this->find($departmentID);
        if($department != null) {
            $dept_delete = $department->delete();
            return $dept_delete;
        }
        else
        {
            return false;
        }
    }
    public function updateById($departmentId,string $departmentname,int $client_id,$admin_id,$employee_id) :int
    {
        $department = $this->find($departmentId);
        $department->name = $departmentname;
        $department->client_id         = $client_id;
        $department->added_by_admin    = $admin_id;
        $department->added_by_employee = $employee_id;
        $department->save();
        return $department->id;
    }

    public function validateDepartmentByName($name)
    {
        $departmentObject = $this->where('name', $name)->exists();
        return $departmentObject;
    }

}
