<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveBalance extends Model {
    public function getAvailableLeaves(
        int $empId,
        int $leaveMasterId
    ): float {
        $noOfLeaves = 0;
        $leaves     = $this->where('employee_id', $empId)
            ->where('leave_master_id', $leaveMasterId)->get();
        if (count($leaves) !== 0) {
            $noOfLeaves = $leaves->first()->available;
        }
        return $noOfLeaves;
    }

    public function isLeaveExist(int $empId,
    int $leaveMasterId)
    {
        return $this->where('employee_id', $empId)
        ->where('leave_master_id', $leaveMasterId)->first();
    }

    public function getLeaveCount(int $empId,
    int $leaveMasterId)
    {
        return $this->where('employee_id', $empId)
        ->where('leave_master_id', $leaveMasterId)->count();
    }

    public function updateById($balance_id,$employee_id,$leave_master_id,$available) :int
    {
        $leaveBalance = $this->find($balance_id);
        $leaveBalance->employee_id = $employee_id;
        $leaveBalance->leave_master_id = $leave_master_id;
        $leaveBalance->available = $available;
        $leaveBalance->save();
        return $leaveBalance->id;
    }

    public function insertLeaveBalance($employee_id,$leave_master_id,$available) :int
    {
        $leaveBalance = $this;
        $leaveBalance->employee_id = $employee_id;
        $leaveBalance->leave_master_id = $leave_master_id;
        $leaveBalance->available = $available;
        $leaveBalance->save();
        return $leaveBalance->id;
    }
    public function getLeaveBalance($employee_id, $leave_master)
    {
        return $this
        ->where('employee_id' , $employee_id)
        ->where('leave_master_id', $leave_master)->value('available');
    }

}
