<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ApplyLeave;
use Yajra\DataTables\Facades\DataTables;
use App\LeaveMaster;
use App\Employee;
use Illuminate\Support\Facades\Input;
use App\LeaveBalance;
use Illuminate\Support\Carbon;
use App\LeaveLedger;
use App\TimePolicy;

class RequestLeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $appliedLeaves = ApplyLeave::orderBy('id', 'DESC')->get();
        $leaveMaster = new LeaveMaster();
        $employee = new Employee();
        $activeLeaveType = $leaveMaster->getAllLeaveType();
        $activeEmployee = $employee->getActiveEmployees();
        return response()->view('leave-management.request',compact('activeLeaveType','activeEmployee'), 200,['Content-Type']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function fetchLeave(Request $request)
    {
        $leaveData = ApplyLeave::find($request->leave_id);
        return response()->json(['success' => true, 'data' => $leaveData], 200, ['Content-Type' => 'text/json']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listLeaves($id,Request $request)
    {
        $applyLeaves = new ApplyLeave();

        $appliedLeaves = $applyLeaves->getAllLeaves(SessionController::getClientIdFromSession(),$id);

        return DataTables::of($appliedLeaves)
            ->editColumn('actions', function ($appliedLeaves) {

                return '<div class="col-12 p-0" data_id="' . $appliedLeaves->id . '">
                                <i class="fa fa-cog cursor-pointer mr-2 font-size-18 text-primary"
                                    id="actionLeaveRequest"
                                    title="'.__('leaveManagement/requestLeave.acceptLeaveRequest').'">
                                </i>
                        </div>';
            })->rawColumns(['actions'])->make(true);
    }

    public function acceptLeave(Request $request)
    {
        $leaveBalance = new LeaveBalance();

        $firstYear = new Carbon('first day of January ' . date('Y'));
        $firstYear = $firstYear->format('Y-m-d');

        $lastYear = new Carbon('last day of December ' . date('Y'));
        $lastYear = $lastYear->format('Y-m-d');
        $totalLeaves = 0;
        if($request->apply_leave_is_multiple==0){
            $request->apply_leave_management_from_date = $request->apply_leave_management_date;
            $request->apply_leave_management_to_date = $request->apply_leave_management_date;
        }




        $fromDate = Carbon::parse($this->DateFormat($request->apply_leave_management_from_date));
        $toDate = Carbon::parse($this->DateFormat($request->apply_leave_management_to_date));

        $totalLeaves = $request->apply_leave_management_total_leaves;

        $leaveApplied = ApplyLeave::find($request->leave_id);

        $balanceLeaves = $leaveBalance->getAvailableLeaves($leaveApplied->employee_id,$request->apply_leave_management_leave_type);
        $applyLeave = new ApplyLeave();
        if($balanceLeaves > ($totalLeaves)){

            $leave_id = $applyLeave->updateById($request->leave_id,$request->apply_leave_management_leave_type,
                $fromDate,$toDate,1
            );

            $leaveLedger = new LeaveLedger();

            $ledger_id = $leaveLedger->store($leaveApplied->employee_id,$request->apply_leave_management_leave_type, false,($totalLeaves),3,
                $firstYear,$lastYear,null,$leave_id
            );

            $leaveBalance = new LeaveBalance();
            $balance = $leaveBalance->isLeaveExist($leaveApplied->employee_id,$request->apply_leave_management_leave_type);
            $leaveBalance->updateById($balance->id,$leaveApplied->employee_id,$request->apply_leave_management_leave_type,($balanceLeaves - ($totalLeaves)));

            if($applyLeave->fetchById($leave_id) != false){
                return response()->json(['success' => true,'msg' => "Success"], 201, ['Content-Type' => 'text/json']);
            }else{
                return response()->json(['failed' => true,'msg' => "Somthing went wrong"] , 500, ['Content-Type' => 'text/json']);
            }

        }else{
            return response()->json(['failed' => true,'msg' => "Your leaves are exceeded"] , 200, ['Content-Type' => 'text/json']);
        }


    }

    public function DateFormat($getDate)
    {
        if($getDate != null) {
            $dateString = date('Y-m-d', strtotime($getDate));
            return $dateString;
        }
        else
        {
            return null;
        }
    }

    public function rejectLeave(Request $request)
    {

        $applyLeave = new ApplyLeave();
        $fromDate = Carbon::parse($this->DateFormat($request->apply_leave_management_from_date));
        $toDate = Carbon::parse($this->DateFormat($request->apply_leave_management_to_date));
        $leave_id = $applyLeave->updateById($request->leave_id,$request->apply_leave_management_leave_type,
                $fromDate,$toDate,0
            );


        if($applyLeave->fetchById($leave_id) != false){
            return response()->json(['success' => true,'msg' => "Success"], 201, ['Content-Type' => 'text/json']);
        }else{
            return response()->json(['failed' => true,'msg' => "Somthing went wrong"] , 500, ['Content-Type' => 'text/json']);
        }
    }
}

// <div data_id="' . $appliedLeaves->id . '" class="text-right">
//                                 <a href="javascript:void(0);" class="editWorkExperienceDetails" title="Edit" id="editWorkExperienceDetails" ><i class="fa fa-edit cursor-pointer mr-2 font-size-18 text-primary"></i></a>
//                                 <a href="javascript:void(0);" class="deletetWorkExperienceDetails" title="Delete" id="deleteWorkExperienceDetails"><i class="fa fa-trash cursor-pointer mr-2 font-size-18 text-danger"></i></a>
//                                 </div>
