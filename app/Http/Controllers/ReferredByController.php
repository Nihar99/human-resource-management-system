<?php

namespace App\Http\Controllers;

use App\ReferredBy;
use Illuminate\Contracts\Validation\Validator as IlluminateValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class ReferredByController extends Controller
{
    public function __construct() {
        $this->middleware('loggedIn');
    }

    public function fetchUTList($id) {
        $referredByDetails = (new ReferredBy())->getAllReferredBy($id);
        return DataTables::of($referredByDetails)->make(true);
    }

    public function store(Request $request)
    {
        /*dd($request);*/

        $referredBy = new ReferredBy();
        $validator = $this->validateInputs($request);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400, ['Content-Type' => 'text/json']);
        }   else {
            $referredById = $referredBy->store($request->employeeId, $request->add_referred_by_name, $request->add_referred_by_position, $request->add_referred_by_address, $request->add_referred_by_telephone, SessionController::getAdminId(), SessionController::getEmployeeId());
            return response()->json(['success' => true, 'referredById' => $referredById], 201, ['Content-Type' => 'text/json']);
        }
    }

    public function validateInputs(Request $request): IlluminateValidator {
        $validator = Validator::make($request->all(), [
            'add_referred_by_name' => 'required',
        ]);;
        return $validator;
    }

    public function update(Request $request,$referredById)
    {
        $referredBy = ReferredBy::find($referredById);
        $referredById = $referredBy->store($request->employeeId, $request->edit_referred_by_name, $request->edit_referred_by_position, $request->edit_referred_by_address, $request->edit_referred_by_telephone, SessionController::getAdminId(), SessionController::getEmployeeId());
        return response()->json(['success' => true, 'referredById' => $referredById], 201, ['Content-Type' => 'text/json']);
    }

    public function destroy($referredById)
    {
        $referredBy = new ReferredBy();
        $referredBy->deleteById($referredById);
        return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);
    }
}
