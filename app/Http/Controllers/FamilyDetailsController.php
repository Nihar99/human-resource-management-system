<?php

namespace App\Http\Controllers;

use App\FamilyDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Validation\Validator as IlluminateValidator;
use Yajra\DataTables\Facades\DataTables;

class FamilyDetailsController extends Controller
{
    public function __construct() {
        $this->middleware('loggedIn');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function fetchUTList(Request $request,$id) {
        $status     = $request->status;
        $family = new FamilyDetails();
        $familyDetails = $family->getEmployeeFamilyDetails($id);
        return DataTables::of($familyDetails)
            ->editColumn('gender' , function($familyDetails)
            {
                if($familyDetails->gender){return 'Male';}
                else{return 'Female';}
            })
            ->editColumn('dependant' , function($familyDetails)
            {
                if($familyDetails->dependant){return 'Yes';}
                else {return 'No';}
            })
            ->editColumn('actions', function ($familyDetails) {

                return '<div data_id="' . $familyDetails->id . '" class="text-right" data_name="' . $familyDetails->full_name . '" data_age="' . $familyDetails->age . '" data_occupation="' . $familyDetails->occupation . '" data_relationship="' . $familyDetails->relationship . '" data_gender="' . $familyDetails->gender . '" data_dependant="' . $familyDetails->dependant . '">
                                <a href="javascript:void(0);" class="editFamilyDetails" title="Edit" id="editFamilyDetails" ><i class="fa fa-edit cursor-pointer mr-2 font-size-18 text-primary"></i></a>
                                <a href="javascript:void(0);" class="deleteFamilyDetails" title="Delete" id="deleteFamilyDetails"><i class="fa fa-trash cursor-pointer mr-2 font-size-18 text-danger"></i></a>
                                </div>';
            })->rawColumns(['actions'])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
       ($request-> gender == 'Male') ? $gender = 1 : $gender = 0;
       ($request-> dependant == 'Yes') ?  $dependant = 1 : $dependant = 0;

        $familyDetails = new FamilyDetails();
        $validator = $this->validateInputs($request);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400, ['Content-Type' => 'text/json']);
        }   else {
            $familyDetailsId = $familyDetails->store($request->employeeId, $request->full_name, $request->age, $request->occupation, $request->relationship, SessionController::getAdminId(), SessionController::getEmployeeId(), $gender, $dependant);
            return response()->json(['success' => true, 'familyDetailsId' => $familyDetailsId], 201, ['Content-Type' => 'text/json']);
                 }
    }

    public function validateInputs(Request $request): IlluminateValidator {
        $validator = Validator::make($request->all(), [
            'full_name' => 'required|regex:/^[\pL\s\-]+$/u',
        ]);;
        return $validator;
    }
    public function validateInputsEdit(Request $request): IlluminateValidator {
        $validator = Validator::make($request->all(), [
            'edit_family_name' => 'required|regex:/^[\pL\s\-]+$/u',
        ]);;
        return $validator;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        ($request-> edit_family_gender == 'Male') ? $gender = 1 : $gender = 0;
        ($request-> edit_family_dependant == 'Yes') ?  $dependant = 1 : $dependant = 0;

         $familyDetails = new FamilyDetails();
         $validator = $this->validateInputsEdit($request);
         if ($validator->fails()) {
             return response()->json($validator->errors(), 400, ['Content-Type' => 'text/json']);
         }   else {
             $familyDetailsId = $familyDetails->updateById($id,$request->employeeId, $request->edit_family_name, $request->edit_family_age, $request->edit_family_occupation, $request->edit_family_relationship, SessionController::getAdminId(), SessionController::getEmployeeId(), $gender, $dependant);
             return response()->json(['success' => true, 'familyDetailsId' => $familyDetailsId], 201, ['Content-Type' => 'text/json']);
                  }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $details = new FamilyDetails();
        $details->deleteById($id);
        return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);
    }
}
