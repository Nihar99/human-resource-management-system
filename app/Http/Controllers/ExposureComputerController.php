<?php

namespace App\Http\Controllers;

use App\exposure_to_computer;
use Illuminate\Contracts\Validation\Validator as IlluminateValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class ExposureComputerController extends Controller
{
    public function __construct() {
        $this->middleware('loggedIn');
    }
    public function fetchUTList(Request $request,$id) {
        $exposureDetails = (new exposure_to_computer())->getEmployeeComputerExposures($id);
        return DataTables::of($exposureDetails)->make(true);
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $exposure = new exposure_to_computer();
        $validator = $this->validateInputs($request);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400, ['Content-Type' => 'text/json']);
        }   else {
            $exposureId = $exposure->store($request->employeeId, $request->add_exposure_language, $request->add_exposure_operating_system, $request->add_exposure_application, $request->add_exposure_hardware, SessionController::getAdminId(), SessionController::getEmployeeId());
            return response()->json(['success' => true, 'exposureId' => $exposureId], 201, ['Content-Type' => 'text/json']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $exposure = new exposure_to_computer();
        $validator = $this->validateInputsEdit($request);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400, ['Content-Type' => 'text/json']);
        } else {
            $exposure->updateById($id, $request->employeeId, $request->edit_exposure_language, $request->edit_exposure_operating_system, $request->edit_exposure_application,
                $request->edit_exposure_hardware, SessionController::getAdminId(), SessionController::getEmployeeId());
            return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $exposure = new exposure_to_computer();
        $exposure->deleteById($id);
        return response()->json(['success' => true, 'msg' => 'Heyy'], 201, ['Content-Type' => 'text/json']);
    }
    public function validateInputs(Request $request): IlluminateValidator {
        $validator = Validator::make($request->all(), [
            'add_exposure_language' => 'required',
            'add_exposure_operating_system' => 'required',
            'add_exposure_application' => 'required',
            'add_exposure_hardware' => 'required',

        ]);;
        return $validator;
    }
    public function validateInputsEdit(Request $request): IlluminateValidator {
        $validator = Validator::make($request->all(), [
            'edit_exposure_language' => 'required',
            'edit_exposure_operating_system' => 'required',
            'edit_exposure_application' => 'required',
            'edit_exposure_hardware' => 'required',
        ]);
        return $validator;
    }
}
