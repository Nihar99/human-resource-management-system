<?php

namespace App\Http\Controllers;

use App\GeneralHoliday;
use App\Rules\NoDuplicateDepartment;
use App\TimePolicy;
use App\Rules\NoDuplicateHoliday;
use Illuminate\Contracts\Validation\Validator as IlluminateValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class GeneralHolidaysController extends Controller
{
    public function __construct() {
        $this->middleware('loggedIn');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->view('general-holidays', ['Content-Type', 'text/html'], 200);
    }
    public function fetchUTList(Request $request) {
        $status     = $request->status;
        $generalHoliday = new GeneralHoliday();

        $holidays = $generalHoliday->getAllHolidays();
        return DataTables::of($holidays)
            ->editColumn('actions', function ($holidays) {

                return '<div data_id="' . $holidays->id . '" class="text-right" data_name="' . $holidays->name . '" data_holiday_date="' . $holidays->holiday_date . '">
                                <a href="javascript:void(0);" class="editHoliday" title="Edit" id="editHoliday" ><i class="fa fa-edit cursor-pointer mr-2 font-size-18 text-primary"></i></a>
                                <a href="javascript:void(0);" class="deleteHoliday" title="Delete" id="deleteHoliday"><i class="fa fa-trash cursor-pointer mr-2 font-size-18 text-danger"></i></a>
                                </div>';
            })->rawColumns(['actions'])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator1  = $this->validateInputs($request);
        if($validator1->fails())
        {
            return response()->json($validator1->errors(), 400, ['Content-Type' => 'text/json']);
        }
        $validator = $this->validateDuplicates($request);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400, ['Content-Type' => 'text/json']);
        } else {
            $getDate = $request->holiday_date;
            $newDate = date("Y-m-d", strtotime($getDate));
            $holiday = new GeneralHoliday();
            $holidayid = $holiday->store($request->name, $newDate, SessionController::getClientIdFromSession(), SessionController::getAdminId(), SessionController::getEmployeeId());
            return response()->json(['success' => true, 'holidayId' => $holidayid], 201, ['Content-Type' => 'text/json']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return GeneralHoliday::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $holiday = new GeneralHoliday();
        $validator1  = $this->validateInputs($request);
        if($validator1->fails())
        {
            return response()->json($validator1->errors(), 400, ['Content-Type' => 'text/json']);
        }
        $validator = $this->validateDuplicates($request);
        $name = $request->name;
        if($name == (new GeneralHoliday())->fetchNameById($id))
        {
            $getDate = $request->edit_holiday_date;
            $newDate = date("Y-m-d", strtotime($getDate));
            $holiday->updateById($id,$name,$newDate, SessionController::getClientIdFromSession(), SessionController::getAdminId(), SessionController::getEmployeeId());
            return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);

        }
        else {
            if ($validator->fails()) {
                return response()->json($validator->errors(), 400, ['Content-Type' => 'text/json']);
            } else {
                $getDate = $request->edit_holiday_date;
                $newDate = date("Y-m-d", strtotime($getDate));
                $holiday->updateById($id, $request->name, $newDate, SessionController::getClientIdFromSession(), SessionController::getAdminId(), SessionController::getEmployeeId());
                return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $holiday = new GeneralHoliday();
        $holiday->deleteById($id);
        return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);
    }
    public function validateDuplicates(Request $request): IlluminateValidator {
        $validator1 = Validator::make($request->all(), [
            'name' => new NoDuplicateHoliday,
        ]);
        return $validator1;
    }
    public function validateInputs(Request $request): IlluminateValidator {
        $validator = Validator::make($request->all(), [
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
        ]);
        return $validator;
    }
}
