<?php
/**
 * Complete Leave Management Business Logic
 * @author Veeraj Shenoy
 */
namespace App\Http\Controllers;

use App\LeaveBalance;
use Carbon\Carbon;

class LeaveManagementController extends Controller {
    private $empId;
    private $leaveType;

    public function __construct(
        int    $empId,
        int    $leaveType,
        Carbon $fromDate,
        Carbon $toDate
    ) {
        $this->empId     = $empId;
        $this->leaveType = $leaveType;
    }

    public function applyLeave() {

    }

    public function isEligibleForLeave(): bool {
        $leaves = (new LeaveBalance)->getAvailableLeaves($this->empId, $this->leaveType);
        return $leaves != 0;
    }

    public function leaveApplied() {
        if ($this->isEligibleForLeave()) {
        } else {
            throw \LogicException();
        }
    }

}
