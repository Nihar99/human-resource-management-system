<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LeaveBalance;
use App\Employee;
use App\LeaveMaster;
use App\LeaveLedger;
use Illuminate\Support\Carbon;

class CronController extends Controller
{
    public function runCron()
    {
        $activeEmployees = (new Employee())->getActiveEmployees();
        $leaveMaster = (new LeaveMaster())->getAllLeaveType();
        $activeEmployees->each(function($employee) use ($leaveMaster){

            $leaveMaster->each(function($leave) use ($employee){

                $firstYear = new Carbon('first day of January ' . date('Y'));
                $firstYear = $firstYear->format('Y-m-d');

                $lastYear = new Carbon('last day of December ' . date('Y'));
                $lastYear = $lastYear->format('Y-m-d');

                $leavesBalance = (new LeaveBalance())->isLeaveExist($employee->id, $leave->id);

                if($leavesBalance){ //if privilege

                    $totalLeaves = 0;

                    if($leave->id == 2){ // privilege leave = 2
                        $totalBalance = (new LeaveBalance())->getAvailableLeaves
                    ($employee->id, $leave->id);
                        $totalLeaves = $leave->total_leaves + (($totalBalance > 6) ? 6  : totalBalance);
                    }
                    else
                    {
                        $totalLeaves  = $leave->total_leaves;
                    }
                    (new LeaveBalance())->updateById($leavesBalance->id,$employee->id,$leave->id,$totalLeaves);

                }else{

                    (new LeaveBalance())->insertLeaveBalance($employee->id,$leave->id,$leave->total_leaves);
                    }
                //insert
                (new LeaveLedger())->store($employee->id ,$leave->id,1,$leave->total_leaves,1,$firstYear,$lastYear,null,null );
            });
        });

    }
}
