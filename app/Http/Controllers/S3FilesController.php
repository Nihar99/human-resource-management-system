<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Files;

class S3FilesController extends Controller
{
    public function index($file,$client_id,$admin_id,$employee_id) {
        $name = md5(time() . $file->getClientOriginalName());
        $filePath = 'images/' . $name;

        Storage::disk('s3')->put($filePath, file_get_contents($file));//upload file to s3
        //if issue arises for env null then run this "php artisan config:clear" in terminal
        $url = 'https://s3.' . config('app.rekognition_default_region') . '.amazonaws.com/' . config('app.rekognition_bucket') . '/';

        $filePath = $url . $filePath;

        DB::beginTransaction();
        $data = array();
        try {
    
            $files = new Files();
            $size =  $file->getClientSize();
            $filesId = $files->store(uniqid() , $filePath,$size,$client_id,$admin_id,$employee_id);
            $data[] = $filesId;
            $data[] = $filePath;

            DB::commit();
            return $data;

        } catch (\Exception $exception) {
            DB::rollBack();
            return $data;
        }
    }

    public function uploadFile($file,$addedby) {

        $decodedImage = base64_decode($file);
        $return =  file_put_contents("storage/images/" . md5(time()) . ".JPG", $decodedImage);
        $filePath='images/' .md5(time() .$return);
        Storage::disk('s3')->put($filePath, file_get_contents("storage/images/" . md5(time()) . ".JPG", $decodedImage));//upload file to s3
        $url = 'https://s3.' . config('app.rekognition_default_region') . '.amazonaws.com/' . config('app.rekognition_bucket') . '/';

        $filePath = $url . $filePath;

        DB::beginTransaction();
        $data = array();
        try {
            DB::table('s3files')->insert([
                "url" => $filePath,
                "added_by" => $addedby,
                "created_at" => Carbon::now()->toDateTimeString(),
                "updated_at" => Carbon::now()->toDateTimeString(),
                "filetype" => 'jpg'
            ]);

            $lastid = DB::getPdo()->lastInsertId();
            $data[] = $lastid;
            $data[] = $filePath;

            DB::commit();
            return $data;

        } catch (\Exception $exception) {
            DB::rollBack();
            return $data;
        }
    }
}
