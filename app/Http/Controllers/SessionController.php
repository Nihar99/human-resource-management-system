<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use App\Department;
use App\ProfessionalDetails;

class SessionController extends Controller {
    public static function getSession() {
        if (Session::exists('admin')) {
            return Session::get('admin');
        }

        if (Session::exists('employee')) {
            return Session::get('employee');
        }
    }

    public static function isAdmin(): bool {
        if (Session::exists('admin')) {
            return true;
        }
        return false;
    }

    public static function isEmployee(): bool {
        if (Session::exists('employee')) {
            return true;
        }
        return false;
    }

    public static function getEmployeeId(): ?int {
        if(self::isEmployee()){
            return self::getSession()->id;
        }else{
            return null;
        }
    }

    public static function getAdminId(): ?int {
        if(self::isAdmin()){
            return self::getSession()->id;
        }else{
            return null;
        }
    }

    public static function getClientIdFromSession(): int {
        if (Session::exists('admin')) {
            return Session::get('admin')->client_id;
        }

        if (Session::exists('employee')) {
            return Session::get('employee')->client_id;
        }
        return 0;
    }
}
