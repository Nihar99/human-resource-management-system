<?php

namespace App\Http\Controllers;

use App\ScholarShipPrizes;
use Illuminate\Contracts\Validation\Validator as IlluminateValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class ScholarshipPrizeController extends Controller
{
    public function __construct() {
        $this->middleware('loggedIn');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function fetchUTList(Request $request,$id) {
        $status     = $request->status;
        $prize = new ScholarShipPrizes();
        $scholarshipPrizes = $prize->getEmployeeScholarships($id);
        return DataTables::of($scholarshipPrizes)
            ->editColumn('actions', function ($scholarshipPrizes) {

                return '<div data_id="' . $scholarshipPrizes->id . '" class="text-right" data_details="' . $scholarshipPrizes->details . '">
                                <a href="javascript:void(0);" class="editScholarship" title="Edit" id="editScholarship" ><i class="fa fa-edit cursor-pointer mr-2 font-size-18 text-primary"></i></a>
                                <a href="javascript:void(0);" class="deleteScholarship" title="Delete" id="deleteScholarship"><i class="fa fa-trash cursor-pointer mr-2 font-size-18 text-danger"></i></a>
                                </div>';
            })->rawColumns(['actions'])->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $prize = new ScholarShipPrizes();
        $validator = $this->validateInputs($request);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400, ['Content-Type' => 'text/json']);
        }
        else
        {
            $scholarshipPrizesId = $prize->store($request->employeeId,
                $request->details,
                SessionController::getAdminId(),
                SessionController::getEmployeeId());
            return response()->json(['success' => true, 'scholarshipPrizesId' => $scholarshipPrizesId], 201, ['Content-Type' => 'text/json']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $prize = new ScholarShipPrizes();
        $validator = $this->validateInputsEdit($request);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400, ['Content-Type' => 'text/json']);
        } else {
            $prize->updateById($id, $request->employeeId, $request->edit_details, SessionController::getAdminId(), SessionController::getEmployeeId());
            return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prizes = new ScholarShipPrizes();
        $prizes->deleteById($id);
        return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);
    }
    public function validateInputs(Request $request): IlluminateValidator {
        $validator = Validator::make($request->all(), [
            'details' => 'required',
        ]);
        return $validator;
    }
    public function validateInputsEdit(Request $request): IlluminateValidator {
        $validator = Validator::make($request->all(), [
            'edit_details' => 'required',
        ]);
        return $validator;
    }
}
