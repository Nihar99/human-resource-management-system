<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\PersonalDetails;

class MonthlyAttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->view('monthly-attendance', ['Content-Type', 'text/html'], 200);
    }
    public function fetchUTList(Request $request) {

        $employee = new Employee();

        $employees= $employee->getAllEmployeeAttendance();
        return DataTables::of($employees)
        ->editColumn('ecode' , function($employees)
        {
            return $employees->ecode;
        })
            ->editColumn('full_name'  , function($employees)
            {
                return (new PersonalDetails())->fetchNameById($employees->id);
            })
            ->editColumn('days' , function($employees)
            {
                $array=$employees->employeeAttendance->toArray();
                $status=array();
                for($day = 1; $day <=  date('t'); $day++)
                {
                    $date = date('Y') . "-" . date('m') . "-" . str_pad($day, 2, '0', STR_PAD_LEFT);

                    if(in_array($date, array_column($array, 'on_date'))){
                        $key = array_search($date, array_column($array, 'on_date'));
                        $status[]= $array[$key]["status"];
                    }else{
                        $status[]= '0';
                    }
                }
                return $status;
            })
            ->rawColumns(['full_name' , 'days'])->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
