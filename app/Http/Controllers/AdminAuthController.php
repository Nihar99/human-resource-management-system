<?php

namespace App\Http\Controllers;

use App\Admin;
use App\AdminAuth;
use Illuminate\Contracts\Validation\Validator as IlluminateValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AdminAuthController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return response(
            view('login.admin'),
            200,
            ['Content-Type' => 'text/html']
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(
        Request   $request,
        Admin     $adminClass,
        AdminAuth $adminAuthClass
    ) {
        $validator = $this->validateInputs($request);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 200, ['Content-Type' => 'text/json']);

        } else {
            $admin = $adminClass->fetchAdminByEmail($request->email);
            if (!$admin || !Hash::check($request->password, $adminAuthClass->getPasswordByAdminId($admin->id)->password_hash)) {
                return response()->json(['success' => false, 'msg' => 'Invalid Email or Password'], 200, ['Content-Type' => 'text/json']);
            }
            $request->session()->put('admin', $admin);
            return response()->json(['success' => true], 200, ['Content-Type' => 'text/json']);
        }
    }

    public function validateInputs(Request $request): IlluminateValidator {
        $validator = Validator::make($request->all(), [
            'email'    => 'required|email',
            'password' => 'required',
        ]);
        return $validator;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
}
