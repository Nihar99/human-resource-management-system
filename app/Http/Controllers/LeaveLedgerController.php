<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use Yajra\DataTables\Facades\DataTables;
use App\PersonalDetails;
use App\ProfessionalDetails;
use App\LeaveBalance;
use App\LeaveLedger;
use App\ApplyLeave;
use Illuminate\Support\Carbon;

class LeaveLedgerController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(LeaveLedger $leaveLedger)
    {
        return response()->view('leave-management.history',['Content-Type', 'text/html'], 200 );
    }

    public function fetchUTList(Request $request) {
        $employees =(new Employee())->getAllEmployees();
        return DataTables::of($employees)
        ->editColumn('full_name'  , function($employees)
        {
            return (new PersonalDetails())->fetchNameById($employees->id);
        })
        ->editColumn('department', function ($employees) {
            return (new ProfessionalDetails())->fetchDepartmentById($employees->id);
        })
        ->editColumn('designation', function ($employees) {
            return (new ProfessionalDetails())->fetchDesignationById($employees->id);
        })
        ->editColumn('casual_leave' , function($employees)
        {
            $leave_balance = new LeaveBalance();
            if($leave_balance->getLeaveBalance($employees->id , 1)){return $leave_balance->getLeaveBalance($employees->id , 1);}
            else{return 0;}
        })
        ->editColumn('privilege_leave' , function($employees)
        {
            $leave_balance = new LeaveBalance();
            if($leave_balance->getLeaveBalance($employees->id , 2)){return $leave_balance->getLeaveBalance($employees->id , 2);}
            else{return 0;}
        })
        ->editColumn('sick_leave' , function($employees)
        {
            $leave_balance = new LeaveBalance();
            if($leave_balance->getLeaveBalance($employees->id , 3)){return $leave_balance->getLeaveBalance($employees->id , 3);}
            else{return 0;}
        })
        ->editColumn('maternity_leave' , function($employees)
        {
            $leave_balance = new LeaveBalance();
            if($leave_balance->getLeaveBalance($employees->id , 4)){return $leave_balance->getLeaveBalance($employees->id , 4);}
            else{return 0;}
        })
            ->editColumn('actions', function ($employees) {

                return '
                <div data_id="' . $employees->id . '" class="col-12 p-0 text-right">
                <button href="javascript:void(0);" class="btn btn-primary leaveLedgerDetails" title="Details" id="leaveLedgerDetails" >Details</button>
                </div>';
            })->rawColumns(['full_name','department','designation','actions'])->make(true);
    }

    public function fetchDetailsList(Request $request, $id) {

        $leaveLedger =  new LeaveLedger();
        $employeeLedgers = $leaveLedger->getEmployeeLeaveLedgers($id);

        return DataTables::of($employeeLedgers)
        ->editColumn('leave_type'  , function($employeeLedgers)
        {
            if($employeeLedgers->leave_master_id == 1){return 'Casual';}
            if($employeeLedgers->leave_master_id == 2){return 'Privilege';}
            if($employeeLedgers->leave_master_id == 3){return 'Sick';}
            if($employeeLedgers->leave_master_id == 4){return 'Maternity';}
        })
        ->editColumn('policy_from' , function($employeeLedgers)
        {
            if($employeeLedgers->apply_leave_id){
            return (new ApplyLeave())->getFromDate($employeeLedgers->apply_leave_id);
            }
            else
            {
                $firstYear = new Carbon('first day of January ' . date('Y'));
                $firstYear = $firstYear->format('Y-m-d');
                return $firstYear;
            }

        })
        ->editColumn('policy_to' , function($employeeLedgers)
        {
            if($employeeLedgers->apply_leave_id){
            return (new ApplyLeave())->getToDate($employeeLedgers->apply_leave_id);
            }
            else{

                $lastYear = new Carbon('last day of December ' . date('Y'));
                $lastYear = $lastYear->format('Y-m-d');
                return $lastYear;
            }

        })
        ->editColumn('added_removed', function ($employeeLedgers) {
            if($employeeLedgers->added_removed == 1){return 'Credited';}
            else{return 'Debited';}

        })
        ->rawColumns(['leave_type','Added/Removed'])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
