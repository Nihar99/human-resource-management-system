<?php

namespace App\Http\Controllers;

use App\LanguagesKnown;
use Illuminate\Contracts\Validation\Validator as IlluminateValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class LanguagesKnownController extends Controller
{
    public function __construct() {
        $this->middleware('loggedIn');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function fetchUTList(Request $request,$id) {
    $status     = $request->status;
    $language_known = new LanguagesKnown();
    $languagesKnown = $language_known->getEmployeeLanguagesKnown($id);
    return DataTables::of($languagesKnown)
        ->editColumn('read' , function($languagesKnown)
        {
            if($languagesKnown->read){return 'Yes';}
            else{return 'No';}
        })
        ->editColumn('write' , function($languagesKnown)
        {
            if($languagesKnown->write){return 'Yes';}
            else{return 'No';}
        })
        ->editColumn('speak' , function($languagesKnown)
        {
            if($languagesKnown->speak){return 'Yes';}
            else{return 'No';}
        })
        ->editColumn('understand' , function($languagesKnown)
        {
            if($languagesKnown->understand){return 'Yes';}
            else{return 'No';}
        })
        ->editColumn('actions', function ($languagesKnown) {

            return '<div data_id="' . $languagesKnown->id . '" class="text-right" data_name="' . $languagesKnown->name . '"
                        data_read="' . $languagesKnown->read . '" data_write="' . $languagesKnown->write . '"
                         data_speak="' . $languagesKnown->speak . '" data_understand="' . $languagesKnown->understand . '">
                                <a href="javascript:void(0);" class="editLanguageKnown" title="Edit" id="editLanguageKnown" ><i class="fa fa-edit cursor-pointer mr-2 font-size-18 text-primary"></i></a>
                                <a href="javascript:void(0);" class="deleteLanguageKnown" title="Delete" id="deleteLanguageKnown"><i class="fa fa-trash cursor-pointer mr-2 font-size-18 text-danger"></i></a>
                                </div>';
        })->rawColumns(['actions'])->make(true);
}
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ($request->read == 'Yes') ? $read= 1 : $read = 0;
        ($request->write == 'Yes') ? $write= 1 : $write = 0;
        ($request->speak == 'Yes') ? $speak= 1 : $speak = 0;
        ($request->understand == 'Yes') ? $understand= 1 : $understand = 0;

        $languages_known = new LanguagesKnown();
        if($languages_known->languageExistsForEmployee($request->name,$request->employeeId)) {
            return response()->json(['name' => 'Duplicate Language'], 400, ['Content-Type' => 'text/json']);
        }
        $validator = $this->validateInputs($request);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400, ['Content-Type' => 'text/json']);
        } else {
            $languagesKnownId = $languages_known->store($request->employeeId, $request->name, $read, $write,
                $speak,$understand, SessionController::getAdminId(), SessionController::getEmployeeId());
            return response()->json(['success' => true, 'languagesKnownId' => $languagesKnownId], 201, ['Content-Type' => 'text/json']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        ($request->edit_read == 'Yes') ? $read= 1 : $read = 0;
        ($request->edit_write == 'Yes') ? $write= 1 : $write = 0;
        ($request->edit_speak == 'Yes') ? $speak= 1 : $speak = 0;
        ($request->edit_understand == 'Yes') ? $understand= 1 : $understand = 0;
        $languages_known = new LanguagesKnown();
        $validator = $this->validateInputsEdit($request);
        if($request->edit_language == (new LanguagesKnown())->fetchNameById($id)){

            $languages_known->updateById($id,$request->employeeId, $request->edit_language, $read, $write,
            $speak,$understand, SessionController::getAdminId(), SessionController::getEmployeeId());
        return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);

    }
        else{
            if ($validator->fails()) {
            return response()->json($validator->errors(), 400, ['Content-Type' => 'text/json']);
        }
        else {
            if(!$languages_known->languageExistsForEmployee($request->edit_language,$request->employeeId)) {
            $languages_known->updateById($id,$request->employeeId, $request->edit_language, $read, $write,
                $speak,$understand, SessionController::getAdminId(), SessionController::getEmployeeId());
            return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);
            }
        }
    }
}


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $languagesKnown = new LanguagesKnown();
        $languagesKnown->deleteById($id);
        return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);
    }
    public function validateInputs(Request $request): IlluminateValidator {
    $validator = Validator::make($request->all(), [
        'name' => 'required|regex:/^[\pL\s\-]+$/u',
    ]);
    return $validator;
}
    public function validateInputsEdit(Request $request): IlluminateValidator {
        $validator = Validator::make($request->all(), [
            'edit_language' => 'required|regex:/^[\pL\s\-]+$/u',
        ]);
        return $validator;
    }
}
