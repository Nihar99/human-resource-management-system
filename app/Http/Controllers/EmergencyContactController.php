<?php

namespace App\Http\Controllers;

use App\EmergencyContact;
use Illuminate\Contracts\Validation\Validator as IlluminateValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use App\Employee;

class EmergencyContactController extends Controller
{

    public function __construct() {
        $this->middleware('loggedIn');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function fetchUTList(Request $request,$id) {
        $status     = $request->status;
        $contact = new EmergencyContact();
        $contactDetails = $contact->getEmployeeEmergencyContacts($id);
        return DataTables::of($contactDetails)
            ->editColumn('actions', function ($contactDetails) {

                return '<div data_id="' . $contactDetails->id . '" class="text-right" data_name="' . $contactDetails->name . '"
                        data_relationship="' . $contactDetails->relationship . '" data_address="' . $contactDetails->address . '"
                         data_phone="' . $contactDetails->phone . '">
                                <a href="javascript:void(0);" class="editEmergencyContact" title="Edit" id="editEmergencyContact" ><i class="fa fa-edit cursor-pointer mr-2 font-size-18 text-primary"></i></a>
                                <a href="javascript:void(0);" class="deleteEmergencyContact" title="Delete" id="deleteEmergencyContact"><i class="fa fa-trash cursor-pointer mr-2 font-size-18 text-danger"></i></a>
                                </div>';
            })->rawColumns(['actions'])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contact = new EmergencyContact();
        $validator = $this->validateInputs($request);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400, ['Content-Type' => 'text/json']);
        } else {
            $emergencyContactId = $contact->store($request->employeeId, $request->name, $request->relationship, $request->address,
                $request->phone, SessionController::getAdminId(), SessionController::getEmployeeId());
            return response()->json(['success' => true, 'emergencyContactId' => $emergencyContactId], 201, ['Content-Type' => 'text/json']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contact = new EmergencyContact();
        $validator = $this->validateInputsEdit($request);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400, ['Content-Type' => 'text/json']);
        } else {
            $contact->updateById($id, $request->employeeId, $request->edit_name, $request->edit_relationship, $request->edit_address,
                $request->edit_phone, SessionController::getAdminId(), SessionController::getEmployeeId());
            return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = new EmergencyContact();
        $contact->deleteById($id);
        return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);
    }
    public function validateInputs(Request $request): IlluminateValidator {
        $validator = Validator::make($request->all(), [
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'address' => 'required|regex:/^[\pL\s\-]+$/u',
            'relationship' => 'required|regex:/^[\pL\s\-]+$/u',
        ]);
        return $validator;
    }
    public function validateInputsEdit(Request $request): IlluminateValidator {
        $validator = Validator::make($request->all(), [
            'edit_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'edit_address' => 'required|regex:/^[\pL\s\-]+$/u',
            'edit_relationship' => 'required|regex:/^[\pL\s\-]+$/u',
        ]);
        return $validator;
    }
}
