<?php

namespace App\Http\Controllers;

use App\Designation;
use App\Rules\NoDuplicateDesignation;
use Illuminate\Contracts\Validation\Validator as IlluminateValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class DesignationController extends Controller {
    public function __construct() {
        $this->middleware('loggedIn');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return response()->view('designations', ['Content-Type', 'text/html'], 200);
    }
    public function fetchUTList(Request $request) {
        $status       = $request->status;
        $designation  = new Designation();
        $designations = $designation->getAllDesignation();

        return DataTables::of($designations)
            ->editColumn('actions', function ($designations) {

                return '<div data_id="' . $designations->id . '" class="text-right" data_name="' . $designations->name . '">
                                <a href="javascript:void(0);" class="editDesignation" title="Edit" id="editDesignation" ><i class="fa fa-edit cursor-pointer mr-2 font-size-18 text-primary"></i></a>
                                <a href="javascript:void(0);" class="deleteDesignation" title="Delete" id="deleteDesignation"><i class="fa fa-trash cursor-pointer mr-2 font-size-18 text-danger"></i></a>
                                </div>';
            })->rawColumns(['actions'])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Designation $designation) {
        $validator = $this->validateInputs($request);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400, ['Content-Type' => 'text/json']);
        }
        $validator1 = $this->validateDuplicates($request);
        if ($validator1->fails()) {
            return response()->json($validator1->errors(), 400, ['Content-Type' => 'text/json']);
        } else {
            $designation_id = $designation->store($request->name, SessionController::getClientIdFromSession(),SessionController::getAdminId(),SessionController::getEmployeeId());
            return response()->json(['success' => true, 'designation_id' => $designation_id], 201, ['Content-Type' => 'text/json']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        return Designation::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $designation = new Designation();

        $validator = $this->validateInputs($request);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400, ['Content-Type' => 'text/json']);
        }
        $validator1 = $this->validateDuplicates($request);
        if ($validator1->fails()) {
            return response()->json($validator1->errors(), 400, ['Content-Type' => 'text/json']);
        } else {
            $designation->updateById($id,$request->name, SessionController::getClientIdFromSession(),SessionController::getAdminId(),SessionController::getEmployeeId());
            return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $designation = new Designation();
        $designation->deleteById($id);
        return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);
    }

    public function validateInputs(Request $request): IlluminateValidator {
        $validator = Validator::make($request->all(), [
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
        ]);
        return $validator;
    }
    public function validateDuplicates(Request $request): IlluminateValidator {
        $validator1 = Validator::make($request->all(), [
            'name' => new NoDuplicateDesignation(),
        ]);
        return $validator1;
    }
}
