<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\LeaveMaster;
use App\ApplyLeave;
use App\LeaveBalance;
use Symfony\Component\HttpFoundation\Session\Session;
use App\GeneralHoliday;
use App\TimePolicy;
use Carbon\Carbon;

class ApplyLeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employee = new Employee();
        $leaveMaster = new LeaveMaster();
        $balance = new LeaveBalance();
        $activeLeaveType = $leaveMaster->getAllLeaveType();
        $activeEmployee = $employee->getActiveEmployees();
        $leaveBalanceCL = $balance->getAvailableLeaves(1,1);
        $leaveBalancePL = $balance->getAvailableLeaves(1,2);
        $leaveBalanceSL = $balance->getAvailableLeaves(1,3);
        return response()->view('leave-management.apply',compact('activeEmployee','activeLeaveType','leaveBalanceCL','leaveBalancePL','leaveBalanceSL'), 200,['Content-Type', 'text/html']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $leaveBalance = new LeaveBalance();

        if($request->apply_leave_is_multiple==0){
            $request->apply_leave_management_from_date = $request->apply_leave_management_date;
            $request->apply_leave_management_to_date = $request->apply_leave_management_date;
            if(!isset($request->apply_leave_is_half_day)){
                $request->apply_leave_is_half_day=0;
            }
        }else{
            $request->apply_leave_is_half_day=0;
        }
        $applyLeave = new ApplyLeave();
        if(isset($request->apply_leave_management_select_emp)){
            $employee_id = $request->apply_leave_management_select_emp;
        }else{
            $employee_id = SessionController::getEmployeeId();
        }
        
        $balanceLeaves = $leaveBalance->getAvailableLeaves($employee_id,$request->apply_leave_management_leave_type);
        $fromDate = Carbon::parse($this->DateFormat($request->apply_leave_management_from_date));
        $toDate = Carbon::parse($this->DateFormat($request->apply_leave_management_to_date));
        $totalLeaves = $fromDate->diffInDays($toDate);
        $fromDate = $fromDate->format('Y-m-d');
        $toDate = $toDate->format('Y-m-d');

        if($request->apply_leave_is_half_day==1){
            $totalLeaves=0.5;
        }

        $leaveExists=$applyLeave->checkAppliedLeaveByDate($employee_id,$fromDate,$toDate);
        if($leaveExists != null){
            $from_date = Carbon::parse($leaveExists->from_date)->format('d-m-Y');
            $to_date = Carbon::parse($leaveExists->to_date)->format('d-m-Y');
            return response()->json(['failed' => true,'msg' => "Leave Already Applied between ".$from_date." and ".$to_date] , 200, ['Content-Type' => 'text/json']);
        }
        else if($balanceLeaves > ($totalLeaves + 1)){
            $leave_id = $applyLeave->store(
                $employee_id,$request->apply_leave_management_leave_type, 
                $fromDate, $toDate,
                $request->reason,
                $request->apply_leave_is_half_day,
                SessionController::getAdminId(),
                SessionController::getEmployeeId()
            );
            
            if($applyLeave->fetchById($leave_id) != false){
                return response()->json(['success' => true,'msg' => "Success",'leave_id'=>$leave_id], 201, ['Content-Type' => 'text/json']);
            }else{
                return response()->json(['failed' => true,'msg' => "Somthing went wrong"] , 500, ['Content-Type' => 'text/json']);
            }

        }else{
            return response()->json(['failed' => true,'msg' => "Your leaves are exceeded"] , 200, ['Content-Type' => 'text/json']);
        }
    }

    public function checkTotalLeaves(Request $request)
    {
        // $fromDate = $request->formDate;
        // $toDate = $request->toDate;
   
        // return response()->json(['success' => true,'balance' => "balance"] , 200, ['Content-Type' => 'text/json']);

    }

    public function DateFormat($getDate)
    {
        if($getDate != null) {
            $dateString = date('Y-m-d', strtotime($getDate));
            return $dateString;
        }
        else
        {
            return null;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
