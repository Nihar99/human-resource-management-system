<?php

namespace App\Http\Controllers;

use App\Rules\NoDuplicateDay;
use App\TimePolicy;
use Illuminate\Contracts\Validation\Validator as IlluminateValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class TimePolicyController extends Controller
{
    public function __construct() {
        $this->middleware('loggedIn');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->view('time-policy', ['Content-Type', 'text/html'], '200');
    }
    public function fetchUTList(Request $request)
    {
        $timePolicies = (new TimePolicy())->getAllPolicies();
        return DataTables::of($timePolicies)
            ->editColumn('day',function($timePolicies)
            {
                if($timePolicies->day == 1){return 'Monday';}
                if($timePolicies->day == 2){return 'Tuesday';}
                if($timePolicies->day == 3){return 'Wednesday';}
                if($timePolicies->day == 4){return 'Thursday';}
                if($timePolicies->day == 5){return 'Friday';}
                if($timePolicies->day == 6){return 'Saturday';}
                if($timePolicies->day == 7){return 'Sunday';}
            })
            ->editColumn('weekoff',function($timePolicies)
            {
                if($timePolicies->weekoff == 0){return 'No';}
                if($timePolicies->weekoff == 1){return 'Yes';}

            })
            ->editColumn('time_in',function($timePolicies)
            {
                if($timePolicies->time_in) {
                    return $this->TimeFormat1($timePolicies->time_in);
                }
                else{return '--';}
            })
            ->editColumn('time_out',function($timePolicies)
            {
                if($timePolicies->time_out) {
                    return $this->TimeFormat1($timePolicies->time_out);
                }
                else{return '--';}
            })
            ->editColumn('break_in',function($timePolicies)
            {
                if($timePolicies->break_in) {
                    return $this->TimeFormat1($timePolicies->break_in);
                }
                else{return '--';}
            })
            ->editColumn('break_out',function($timePolicies)
            {
                if($timePolicies->break_out) {
                    return $this->TimeFormat1($timePolicies->break_out);
                }
                else{return '--';}
            })
            ->editColumn('actions', function ($timePolicies) {
                return '<div data_id="' . $timePolicies->id . '" data_weekoff="' . $timePolicies->weekoff . '" data_day="' . $timePolicies->day . '" data_time_in="' . $timePolicies->time_in . '"
                 data_time_out="' . $timePolicies->time_out . '" data_break_in="' . $timePolicies->break_in . '" data_break_out="' . $timePolicies->break_out . '"
                 data_late_entry_after="' . $timePolicies->late_entry_after . '" data_early_exit_before="' . $timePolicies->early_exit_before . '"
                 data_halfday_employee_comes_after="' . $timePolicies->halfday_employee_comes_after . '" data_halfday_employee_leaves_before="' . $timePolicies->halfday_employee_leaves_before . '" class="text-right">
                                <a href="'.url('/other-details').'" class="ViewTimePolicy" title="View Details" id="ViewTimePolicy"><i class="fa fa-eye cursor-pointer mr-2 font-size-18 text-info"></i></a>
                                <a href="javascript:void(0);" class="editTimePolicy" title="Edit" id="editTimePolicy" ><i class="fa fa-edit cursor-pointer mr-2 font-size-18 text-primary"></i></a>
                                <a href="javascript:void(0);" class="deleteTimePolicy" title="Delete" id="deleteTimePolicy"><i class="fa fa-trash cursor-pointer mr-2 font-size-18 text-danger"></i></a>

                        </div>';
            })
            ->rawColumns(['actions'])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $time_policy = new TimePolicy();
        // $validator = $this->validateDuplicates($request);
        // if($validator->fails())
        // {
        //     return response()->json($validator->errors(), 400, ['Content-Type' => 'text/json']);
        // }
            if ($request->day == "Monday") {
                $getday = 1;
            } else if ($request->day == "Tuesday") {
                $getday = 2;
            } else if ($request->day == 'Wednesday') {
                $getday = 3;
            } else if ($request->day == 'Thursday') {
                $getday = 4;
            } else if ($request->day == 'Friday') {
                $getday = 5;
            } else if ($request->day == 'Saturday') {
                $getday = 6;
            } else {
                $getday = 7;
            }
            if ($request->weekoff == 'Yes') {
                $get_week_off = 1;
            }
            if ($request->weekoff == 'No') {
                $get_week_off = 0;
            }
            $get_time_in = $request->time_in;
            $get_time_out = $request->time_out;
            $get_break_in = $request->break_in;
            $get_break_out = $request->break_out;
            $get_late_entry_after = $request->late_entry_after;
            $get_early_exit_before = $request->early_exit_before;
            $get_halfday_employee_comes_after = $request->halfday_employee_comes_after;
            $get_halfday_employee_leaves_before = $request->halfday_employee_leaves_before;
            $time_in = $this->TimeFormat($get_time_in);
            $time_out = $this->TimeFormat($get_time_out);
            $break_in = $this->TimeFormat($get_break_in);
            $break_out = $this->TimeFormat($get_break_out);
            $late_entry_after = $this->TimeFormat($get_late_entry_after);
            $early_exit_before = $this->TimeFormat($get_early_exit_before);
            $halfday_employee_comes_after = $this->TimeFormat($get_halfday_employee_comes_after);
            $halfday_employee_leaves_before = $this->TimeFormat($get_halfday_employee_leaves_before);

            $validator = Validator::make($request->all(), [
                'getday' => 'exists:time_policies,day,client_id,'.SessionController::getClientIdFromSession()
            ]);
            if (!$validator->fails()) {
                $msg=array();
                $msg['name']=array('Duplicate Day');
                return response()->json($msg, 400, ['Content-Type' => 'text/json']);
            }
            else{

            $time_policy->store(SessionController::getClientIdFromSession(),
                SessionController::getAdminId(),
                SessionController::getEmployeeId(),
                $getday,
                $time_in,
                $time_out,
                $break_in,
                $break_out,
                $late_entry_after,
                $early_exit_before,
                $halfday_employee_comes_after,
                $halfday_employee_leaves_before,
                $get_week_off);
            return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);
            }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return TimePolicy::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if($request->edit_day == 'Monday')
        {
            $getday = 1;
        }

        else if($request->edit_day == 'Tuesday')
        {
            $getday = 2;
        }
        else if($request->edit_day == 'Wednesday')
        {
            $getday = 3;
        }
        else if($request->edit_day == 'Thursday')
        {
            $getday = 4;
        }
        else if($request->edit_day == 'Friday')
        {
            $getday = 5;
        }
        else if($request->edit_day == 'Saturday')
        {
            $getday = 6;
        }
        else
        {
            $getday = 7;
        }
        if($request->edit_weekoff == 'Yes')
        {
            $get_week_off = 1;
        }
        if($request->edit_weekoff == 'No')
        {
            $get_week_off = 0;
        }
        $get_time_in = $request->edit_time_in;
        $get_time_out = $request->edit_time_out;
        $get_break_in = $request->edit_break_in;
        $get_break_out = $request->edit_break_out;
        $get_late_entry_after = $request->edit_late_entry_after;
        $get_early_exit_before = $request->edit_early_exit_before;
        $get_halfday_employee_comes_after = $request->edit_halfday_employee_comes_after;
        $get_halfday_employee_leaves_before = $request->edit_halfday_employee_leaves_before;
        $time_in = $this->TimeFormat($get_time_in);
        $time_out = $this->TimeFormat($get_time_out);
        $break_in = $this->TimeFormat($get_break_in);
        $break_out = $this->TimeFormat($get_break_out);
        $late_entry_after = $this->TimeFormat($get_late_entry_after);
        $early_exit_before = $this->TimeFormat($get_early_exit_before);
        $halfday_employee_comes_after = $this->TimeFormat($get_halfday_employee_comes_after);
        $halfday_employee_leaves_before = $this->TimeFormat($get_halfday_employee_leaves_before);
        $time_policy = new TimePolicy();

        //dd($getday);
        //dd($get_week_off);
        $time_policy->updateById($id,
            SessionController::getClientIdFromSession(),
            SessionController::getAdminId(),
            SessionController::getEmployeeId(),
            $getday,
            $time_in,
            $time_out,
            $break_in,
            $break_out,
            $late_entry_after,
            $early_exit_before,
            $halfday_employee_comes_after,
            $halfday_employee_leaves_before,
            $get_week_off);
            return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $timePolicy = new TimePolicy();
        $timePolicy->deleteById($id);
        return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);
    }
    public function TimeFormat($getTime)
    {
        if($getTime != null) {
            $time = date('H:i:s', strtotime($getTime));
            return $time;
        }
        else
        {
            return null;
        }
    }
    public function TimeFormat1($getTime)
    {
        $time = date('g:i A',strtotime($getTime));
        return $time;
    }
    public function validateDuplicates(Request $request): IlluminateValidator {
        $validator = Validator::make($request->all(),[
            'day' => new NoDuplicateDay(),
        ]);
        return $validator;
    }
}
