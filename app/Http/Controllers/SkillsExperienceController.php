<?php

namespace App\Http\Controllers;

use App\SkillsAndExperience;
use Illuminate\Contracts\Validation\Validator as IlluminateValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class SkillsExperienceController extends Controller
{
    public function __construct() {
        $this->middleware('loggedIn');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function fetchUTList(Request $request,$id) {
        $status     = $request->status;
        $skill = new SkillsAndExperience();
        $skillsExperiences = $skill->getEmployeeSkillsExposures($id);
        return DataTables::of($skillsExperiences)
            ->editColumn('actions', function ($skillsExperiences) {

                return '<div data_id="' . $skillsExperiences->id . '" class="text-right" data_details="' . $skillsExperiences->details . '">
                                <a href="javascript:void(0);" class="editSkills" title="Edit" id="editSkills" ><i class="fa fa-edit cursor-pointer mr-2 font-size-18 text-primary"></i></a>
                                <a href="javascript:void(0);" class="deleteSkills" title="Delete" id="deleteSkills"><i class="fa fa-trash cursor-pointer mr-2 font-size-18 text-danger"></i></a>
                                </div>';
            })->rawColumns(['actions'])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $skills = new SkillsAndExperience();
        $validator = $this->validateInputs($request);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400, ['Content-Type' => 'text/json']);
        }
        else
        {
            $skillsExperienceId = $skills->store($request->employeeId,
                $request->details,
                SessionController::getAdminId(),
                SessionController::getEmployeeId());
            return response()->json(['success' => true, 'skillsExperienceId' => $skillsExperienceId], 201, ['Content-Type' => 'text/json']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $skills = new SkillsAndExperience();
        $validator = $this->validateInputsEdit($request);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400, ['Content-Type' => 'text/json']);
        } else {
            $skills->updateById($id, $request->employeeId, $request->edit_skills_details, SessionController::getAdminId(), SessionController::getEmployeeId());
            return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $skill = new SkillsAndExperience();
        $skill->deleteById($id);
        return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);
    }
    public function validateInputs(Request $request): IlluminateValidator
    {
        $validator = Validator::make($request->all(), [
            'details' => 'required',
        ]);
        return $validator;
    }
    public function validateInputsEdit(Request $request): IlluminateValidator {
        $validator = Validator::make($request->all(), [
            'edit_skills_details' => 'required'
        ]);
        return $validator;
    }
    }

