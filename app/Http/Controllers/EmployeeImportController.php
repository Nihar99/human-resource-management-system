<?php

namespace App\Http\Controllers;

use App\Country;
use App\Department;
use App\Designation;
use App\Employee;
use App\EmployeeAddress;
use App\EmployeeAuth;
use App\Exports\EmployeeExport;
use App\PersonalDetails;
use App\ProfessionalDetails;
use App\ReferredBy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class EmployeeImportController extends Controller
{
    public function __construct() {
        $this->middleware('loggedIn');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->view('employee.import',[], 200, ['Content-Type', 'text/html; charset=UTF-8']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id )
    {

    }

    public function downloadSampleExcel(Request $request){
        $data=(new EmployeeExport())->headings();

        $departments=(new Department())->getAllDepartment();
        $designations=(new Designation())->getAllDesignation();
        $countries=(new Country())->getAllCountries();

        $type='csv';
        return Excel::create('Employee', function($excel) use ($data,$departments,$designations,$countries) {
            $excel->sheet('mySheet', function($sheet) use ($data,$departments,$designations,$countries)
            {
                $sheet->fromArray($data);

                $sheet->setCellValue('D4', 'Delete following before upload');
                $sheet->setCellValue('D5', 'Male');
                $sheet->setCellValue('D6', 'Female');

                $sheet->setCellValue('M4', 'Delete following before upload');
                $sheet->setCellValue('M5', 'Married');
                $sheet->setCellValue('M6', 'Unmarried');
                $sheet->setCellValue('M7', 'Divorse');

                $sheet->setCellValue('W4', 'Delete following before upload');
                $sheet->setCellValue('W5', 'Yes');
                $sheet->setCellValue('W6', 'No');

                $rowId=4;
                $sheet->setCellValue('F'.$rowId, 'Delete following before upload');
                foreach ($countries as $key => $county){
                    $rowId++;
                    $sheet->setCellValue('F'.$rowId,$county->name);
                }

                $rowId=4;
                $sheet->setCellValue('S'.$rowId, 'Delete following before upload');
                foreach ($departments as $key => $dept){
                    $rowId++;
                    $sheet->setCellValue('S'.$rowId,$dept->name);
                }

                $rowId=4;
                $sheet->setCellValue('T'.$rowId, 'Delete following before upload');
                foreach ($designations as $key => $desg){
                    $rowId++;
                    $sheet->setCellValue('T'.$rowId,$desg->name);
                }
            });
        })->download($type);
    }

    public function importExcel(Request $request,Country $country,Department $department,Designation $designation)
    {

        $validate = Validator::make($request->all(), [
            'import_file' =>'required'
        ]);

        if ($validate->fails()) {
            return back()->with('errors', 'The import file field is required.');
        }else {
            $path = $request->file('import_file')->getRealPath();
            $data = Excel::load($path)->get();

            $employeeaddress = new EmployeeAddress();
            $personal_details = new PersonalDetails();
            $professional_details = new ProfessionalDetails();
            $employeeAuth=array();

            DB::beginTransaction();
            if ($data->count()) {
                foreach ($data as $key => $value) {

                    $rowcount=$key+2;

                    if($value->first_name==''){
                        return back()->with('errors', 'Enter First Name in A '.$rowcount);
                    }

                    if($value->last_name==''){
                        return back()->with('errors', 'Enter Last Name in C '.$rowcount);
                    }
                    if($value->gender==''){
                        return back()->with('errors', 'Enter Gender in D '.$rowcount);
                    }else if($value->gender!='Male' && $value->gender!='Female'){
                        return back()->with('errors', 'Enter Gender as "Male" OR "Female" in D '.$rowcount);
                    }
                    if($value->date_of_birth==''){
                        return back()->with('errors', 'Enter Date of Birth in E '.$rowcount);
                    }else{
                        $inputs = array('date_of_birth' => $value->date_of_birth);
                        $rules = array('date_of_birth' => 'date');
                        $messages = array();
                        $validation = Validator::make($inputs, $rules, $messages);
                        if ($validation->fails()) {
                            return back()->with('errors', 'Invalid Date of Birth in E '.$rowcount);
                        }
                    }
                    if($value->nationality==''){
                        return back()->with('errors', 'Enter Nationality in F '.$rowcount);
                    }else if(!$country->validateCountryByName($value->nationality)){
                        return back()->with('errors', 'Invalid Nationality in F '.$rowcount);
                    }
                    if($value->present_address==''){
                        return back()->with('errors', 'Enter Present Address in G '.$rowcount);
                    }
                    if($value->permanent_address==''){
                        return back()->with('errors', 'Enter Permanent Address in H '.$rowcount);
                    }

                    if($value->place_state_of_birth==''){
                        return back()->with('errors', 'Enter Place & State of Birth in K '.$rowcount);
                    }
                    if($value->religion==''){
                        return back()->with('errors', 'Enter Religion in L '.$rowcount);
                    }
                    if($value->marital_status==''){
                        return back()->with('errors', 'Enter Marital Status in M '.$rowcount);
                    }else if($value->marital_status!='Married' && $value->marital_status!='Unmarried' && $value->marital_status!='Divorse'){
                        return back()->with('errors', 'Enter Marital Status as "Married" OR "Unmarried" OR "Divorse" in M '.$rowcount);
                    }
                    if($value->mother_tongue==''){
                        return back()->with('errors', 'Enter Mother Tongue in N '.$rowcount);
                    }

                    if($value->employee_code==''){
                        return back()->with('errors', 'Enter Employee Code in Q '.$rowcount);
                    }else{
                        $ecode=Employee::where([['ecode',$value->employee_code],['client_id',SessionController::getClientIdFromSession()]])->exists();
                        if($ecode){
                            return back()->with('errors', 'Employee Code already exists in Q '.$rowcount);
                        }
                    }

                    if($value->department==''){
                        return back()->with('errors', 'Enter Department in S '.$rowcount);
                    }else if(!$department->validateDepartmentByName($value->department)){
                        return back()->with('errors', 'Invalid Department in S '.$rowcount);
                    }

                    if($value->designation==''){
                        return back()->with('errors', 'Enter Designation in T '.$rowcount);
                    }else if(!$designation->validateDesignationByName($value->designation)){
                        return back()->with('errors', 'Invalid Designation in T '.$rowcount);
                    }

                    if($value->on_probation_from==''){
                        return back()->with('errors', 'Enter On Probation From in U '.$rowcount);
                    }else{
                        $inputs = array('on_probation_from' => $value->on_probation_from);
                        $rules = array('on_probation_from' => 'date');
                        $messages = array();
                        $validation = Validator::make($inputs, $rules, $messages);
                        if ($validation->fails()) {
                            return back()->with('errors', 'Invalid Date of Probation From in U '.$rowcount);
                        }
                    }

                    if($value->date_of_confirmation!=''){
                        $inputs = array('date_of_confirmation' => $value->date_of_confirmation);
                        $rules = array('date_of_confirmation' => 'date');
                        $messages = array();
                        $validation = Validator::make($inputs, $rules, $messages);
                        if ($validation->fails()) {
                            return back()->with('errors', 'Invalid Date of Confirmation in V '.$rowcount);
                        }
                    }

                    if($value->enable_ess==''){
                        return back()->with('errors', 'Enter On Enable ESS in W '.$rowcount);
                    }else if($value->enable_ess!='Yes' && $value->enable_ess!='No'){
                        return back()->with('errors', 'Enter ESS as "Yes" OR "No" in W '.$rowcount);
                    }

                    if($value->enable_ess=='Yes' && $value->password==''){
                        return back()->with('errors', 'Enter On Password in X '.$rowcount);
                    }

                    if ($value->gender == 'Male') {
                        $gender = 1;
                    } else {
                        $gender = 2;
                    }

                    if ($value->marital_status == 'Married') {
                        $marital_status = 1;
                    } else if ($value->marital_status == 'Unmarried') {
                        $marital_status = 2;
                    } else {
                        $marital_status = 3;
                    }

                    //change date format to mysql date..
                    $orig_dob = $value->date_of_birth;
                    $newDob = date("Y-m-d", strtotime($orig_dob));
                    $orig_prob_from = $value->on_probation_from;
                    $new_prob_from = date("Y-m-d", strtotime($orig_prob_from));
                    if($value->date_of_confirmation){
                        $orig_confirmation_on = $value->date_of_confirmation;
                        $new_conformatio_on = date("Y-m-d", strtotime($orig_confirmation_on));
                        }
                        else{$new_conformatio_on =null;}

                    ($value->enable_ess == 'Yes') ? $ess= 1 : $ess = 0;
                    $departmentId = (new Department())->fetchDepartmentIdByName($value->department);
                    $designationId = (new Designation())->fetchDesignationIdByName($value->designation);
                    $countryId = (new Country())->fetchCountryIdByName($value->nationality);

                    $employee = new Employee();
                    $employeeId = $employee->create(
                        SessionController::getClientIdFromSession(),
                        SessionController::getAdminId(),
                        SessionController::getEmployeeId(),
                        $value->employee_code,
                        1,
                        $ess);

                    if($ess)
                    {
                        $password = Hash::make($value->password);
                        $employeeAuth[] =  (new EmployeeAuth())->insertBatch($employeeId,$password);
                    }

                    $employeeaddressArray[] = $employeeaddress->insertBatch($employeeId,
                        $value->present_address,
                        $value->telephone_nopresent_address,
                        $value->permanent_address,
                        $value->telephone_nopermanent_address);

                    $employeePersonalDetailsArray[] = $personal_details->insertBatch($employeeId,
                        1,
                        $value->first_name,
                        $value->middle_name,
                        $value->last_name,
                        $gender,
                        $newDob,
                        $countryId,
                        $value->place_state_of_birth,
                        $value->religion,
                        $marital_status,
                        $value->mother_tongue,
                        $value->personal_email_id,
                        $value->hobbies_interests);

                    $employeeProfessionalDetailsArray[] = $professional_details->insertBatch($employeeId,
                        $value->company_email_id,
                        $departmentId,
                        $designationId,
                        $new_prob_from,
                        $new_conformatio_on);
                }

                if (!empty($employeeAuth)) {
                    EmployeeAuth::insert($employeeAuth);
                }

                if (!empty($employeeaddressArray)) {
                    EmployeeAddress::insert($employeeaddressArray);
                }

                if (!empty($employeePersonalDetailsArray)) {
                    PersonalDetails::insert($employeePersonalDetailsArray);
                }

                if (!empty($employeeProfessionalDetailsArray)) {
                    ProfessionalDetails::insert($employeeProfessionalDetailsArray);
                }
            }
            DB::commit();
            return back()->with('success', 'Insert Record successfully.');
        }
    }
}
