<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\EmployeeAuth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use App\FaceTrain;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    
    public function login(
        Request      $request
    ) {
        $employee = new Employee();
        $employeeauth = new EmployeeAuth();
        if ($employee->isEmployeePresentAndESSEnabled($request->ecode, $request->companyid)) {
            $emp  = $employee->getEmployee($request->ecode, $request->companyid);
            $auth = $employeeauth->getEmployeeAuth($emp->id);

            if ($auth) {
                if (Hash::check($request->password, $auth->password_hash)) {
                    return response()->json(
                        [
                            'success' => true,
                            'token'=>$auth->createToken('hrms')->accessToken,'employee_id'=>$emp->id, 
                            'client_id'=>$request->companyid
                        ],
                        201
                        ,['Content-Type' => 'text/json']);
                } else {
                    
                    return $this->responseFail();
                }
            } else {
                
                return $this->responseFail();
            }
        } else {
           
            return $this->responseFail();
        }
    }

    public function responseFail() {
        return response()->json(['name' => [0 => 'Invalid Credentials']], 400, ['Content-Type' => 'text/json']);
    }

    public function check(Request $request)
    {
        return response()->json(['status'=>'success','msg'=>"Successfull"], 200, ['Content-Type' => 'text/json']);
    }

    public function emp_training(Request $request)
    {
        $companyid = $request->client_id;
        $admin_id = null;
        $employee_id = $request->login_id;
        $awsface = new AWSController();

        $EID = $request->emp_id;
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            //create new collection
            if (!in_array($companyid, $awsface->listCollectionInRekognition()['CollectionIds'])) {
                $awsface->createCollection($companyid);
            }

            //check image
            $imagestatus = $awsface->checkimage($file);
            if ($imagestatus) {
                //Upload Image
                $s3 = new s3filesController();
                $resp = $s3->index($file,1,1,null);
                $urlid = $resp[0];
                $s3url = explode('images/', $resp[1]);
                $filePath = "images/" . $s3url[1];
                
                $ogimage = $file;
                $tempname0 = explode('.', $file->getClientOriginalName());
                $tempname = uniqid() . "." . $tempname0[1];
                $newimage = public_path($tempname);//define location for comp img
                $awsface->resize($ogimage, $newimage);//compress and save
                $tmp = explode('.', $tempname);
                $compressedimagelink = $awsface->uploadtos3facetraining($newimage, $tmp[1],$companyid,$admin_id,$employee_id);
                unlink($newimage);//remove img from temp storage

                $facecount = $awsface->facecounts3training($filePath);
                if ($facecount == 1) {

                    $faceexistsarray = $awsface->faceexistsfroms3training($filePath, $companyid);
                    $faceexistscount = count($faceexistsarray);

                    $indexfaces3 = $awsface->indexfacefroms3training($filePath, $companyid, $EID);
                    $faceid = $indexfaces3["FaceRecords"][0]["Face"]["FaceId"];
                    $imageid = $indexfaces3["FaceRecords"][0]["Face"]["ImageId"];
                    dd($compressedimagelink);
                    if ($faceid != '') {
                        //insert trained image
                        DB::beginTransaction();
                        $faceTrain = new FaceTrain();
                        $faceTrain->store($EID,$urlid,$compressedimagelink[0],null,$employee_id);
                        // DB::table('employee_training')->insert([
                        //     'emp_id' => $EID,
                        //     'img_url' => $urlid,
                        //     'comp_url' => $compressedimagelink[0],
                        //     'faceid' => $faceid,
                        //     'imageid' => $imageid,
                        //     'added_by' => 1,
                        //     'created_at' => Carbon::now()->toDateTimeString(),
                        //     'updated_at' => Carbon::now()->toDateTimeString(),
                        // ]);

                        //$faceTrain = new FaceTrain();
                        //$faceTrain->store($EID, $urlid,$admin_id, $employee_id);

                        DB::commit();

                        $status = 'success';
                        $msg = 'Employee trained successfully';
                    } else {
                        $status = 'failed';
                        $msg = 'Image not proper';
                    }
                } else if ($facecount == 0) {
                    $status = 'failed';
                    $msg = 'No Face found';
                } else {
                    $status = 'failed';
                    $msg = 'More than 1 face count';
                }
            } else {
                $status = 'failed';
                $msg = 'Invalid image file uploaded';
            }
        } else {
            $status = 'failed';
            $msg = 'File Not Found!!!';
        }

        return response()->json([
            'status' => $status,
            'msg' => $msg
        ]);
    }

}
