<?php

namespace App\Http\Controllers;
use App\Helpers\ImageManager;
use App\Helpers\FileManager;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use PhpParser\Node\Scalar\MagicConst\Dir;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //
    }

    public function store(Request $request)
    {
        if ($request->hasFile('highlights_attachment')) {
            $s3fileUpload=new FileManager();

            $attachment = $request->highlights_attachment;
            $fileName = str_random(10).'.'.$attachment->getClientOriginalExtension();
            $filePath = __('application.documentFolder').'/'. $fileName;
            $fileUrl=$s3fileUpload->uploadFileToS3($filePath,file_get_contents($attachment));
        }
    }
}
