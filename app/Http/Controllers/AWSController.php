<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Aws\Rekognition\RekognitionClient;
use App\Files;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class AWSController extends Controller
{
    private $options;
    private $rekognition;

    public function __construct() {
        $this->options = [
            'region' => config('app.rekognition_default_region'),
            'version' => 'latest',
            'credentials' => array(
                'key' => config('app.rekognition_access_key'),//Add key here
                'secret' => config('app.rekognition_secret_key'),//Add secret here
            )
        ];

        //Initiate an Amazon reKognition
        $this->rekognition = new RekognitionClient($this->options);
    }

    function listCollectionInRekognition() {
        return $result = $this->rekognition->listCollections();
    }

    function createCollection(string $collectionName) {
        $this->rekognition->createCollection([
            'CollectionId' => $collectionName
        ]);
        return true;
    }

    function checkimage(string $mediapath) {
        if (@is_array(getimagesize($mediapath))) {
            return true;
        } else {
            return false;
        }
    }

    function facecounts3training(string $imagename) {
        return count($this->detectFacess3($imagename, config('app.rekognition_bucket'))['FaceDetails']);
    }

    function detectFacess3(string $s3name, string $bucketname) {
        return $result = $this->rekognition->detectFaces([
            'Image' => [ // REQUIRED
                'S3Object' => [
                    'Bucket' => $bucketname,
                    'Name' => $s3name
                ]
            ],
        ]);
    }

    function faceexistsfroms3training(string $s3name, string $companyid) {
        return $this->getImageIdFromGivenFaceinS3($companyid, $s3name, config('app.rekognition_bucket'))['FaceMatches'];
    }

    function getImageIdFromGivenFaceinS3(string $collectionName, string $s3name, string $bucketname) {
        try{
            $result = $this->rekognition->searchFacesByImage([
                'CollectionId' => $collectionName, // REQUIRED
                'FaceMatchThreshold' => 90,
                'Image' => [ // REQUIRED
                    'S3Object' => [
                        'Bucket' => $bucketname,
                        'Name' => $s3name
                    ]
                ]
            ]);
        } catch (Exception $e) {
            return array('statusCode' => 500);
        }
        return $result;
    }

    function indexfacefroms3training(string $s3name, string $collectionName, string $EID) {
        return $this->indexFaces3($collectionName, $s3name, config('app.rekognition_bucket'), $EID);
    }

    function indexFaces3(string $collectionName, string $s3name, string $bucketname, string $EID) {
        return $result = $this->rekognition->indexFaces([
            'CollectionId' => $collectionName, // REQUIRED
            'DetectionAttributes' => ['ALL'],
            'ExternalImageId' => $EID,
            'Image' => [ // REQUIRED
                'S3Object' => [
                    'Bucket' => $bucketname,
                    'Name' => $s3name
                ]
            ],
            'QualityFilter' => 'AUTO',
        ]);
    }

    function resize($imagepath, $destinationpath) {
        $image = new \Gumlet\ImageResize($imagepath);
        $image->scale(10);
        $image->save($destinationpath);
        return $image;
    }

    function uploadtos3facetraining($imgpath, $ext,$client_id,$admin_id,$employee_id) {
        return $this->uploadimagetos3($imgpath, config('app.rekognition_bucket'), $ext,$client_id,$admin_id,$employee_id);
    }

    /**
     * @param string $fileimage
     * @param string $bucketname
     * @param string $ext
     * @return array
     */
    function uploadimagetos3($fileimage, string $bucketname, string $ext,$client_id,$admin_id,$employee_id) {
        $imagename = uniqid() . '.' . $ext;
        $name = $imagename;
        $filePath = 'images/' . $name;

        Storage::disk('s3')->put($filePath, file_get_contents($fileimage));//upload file to s3
        $url = 'https://s3.' . config('app.rekognition_default_region') . '.amazonaws.com/' . $bucketname . '/';

        $filePath = $url . $filePath;
       
        DB::beginTransaction();
        $data = array();
        try {
        
            $files = new Files();
            $size =  Storage::size($fileimage);
            $filesId = $files->store(uniqid() , $filePath,$size,$client_id,$admin_id,$employee_id);
            
            //$lastid = DB::getPdo()->lastInsertId();
            $data[] = $filesId;
            $data[] = $filePath;

            DB::commit();
            return $data;

        } catch (\Exception $exception) {
            DB::rollBack();
            return $data;
        }
    }

    function deletetrainedface(string $collectionName, string $faceid) {
        return $this->deleteFaces($collectionName, $faceid);
    }

    function deleteFaces(string $collectionName, string $faceId) {
        return $result = $this->rekognition->deleteFaces([
            'CollectionId' => $collectionName,
            'FaceIds' => [
                $faceId,
            ],
        ]);
    }

    function listAllFaces(string $collectionName) {
        return $result = $this->rekognition->listFaces([
            'CollectionId' => $collectionName
        ]);
    }
}
