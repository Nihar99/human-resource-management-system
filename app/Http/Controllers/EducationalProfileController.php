<?php

namespace App\Http\Controllers;

use App\EducationalProfile;
use Illuminate\Contracts\Validation\Validator as IlluminateValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class EducationalProfileController extends Controller
{
    public function __construct() {
        $this->middleware('loggedIn');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function fetchUTList(Request $request,$id) {
        $status     = $request->status;
        $profile = new EducationalProfile();
        $educationalProfiles = $profile->getEmployeeEducationalProfile($id);
        return DataTables::of($educationalProfiles)
            ->editColumn('actions', function ($educationalProfiles) {

                return '<div data_id="' . $educationalProfiles->id . '" class="text-right" data_board_degree="' . $educationalProfiles->board_degree . '"
                        data_year_of_passing="' . $educationalProfiles->year_of_passing . '" data_institution_name="' . $educationalProfiles->institution_name . '"
                         data_principle_subjects="' . $educationalProfiles->principle_subjects . '" data_grade_percentage="' . $educationalProfiles->grade_percentage . '">
                                <a href="javascript:void(0);" class="editEducationalProfile" title="Edit" id="editEducationalProfile" ><i class="fa fa-edit cursor-pointer mr-2 font-size-18 text-primary"></i></a>
                                <a href="javascript:void(0);" class="deleteEducationalProfile" title="Delete" id="deleteEducationalProfle"><i class="fa fa-trash cursor-pointer mr-2 font-size-18 text-danger"></i></a>
                                </div>';
            })->rawColumns(['actions'])->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $profile = new EducationalProfile();
        $validator = $this->validateInputs($request);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400, ['Content-Type' => 'text/json']);
        }
        else
        {
            $educationalProfileId = $profile->store($request->employeeId,
                $request->board_degree,
                $request->year_of_passing,
                $request->institution_name,
                $request->principle_subjects,
                $request->grade_percentage,
                SessionController::getAdminId(),
                SessionController::getEmployeeId());
            return response()->json(['success' => true, 'educationalProfileId' => $educationalProfileId], 201, ['Content-Type' => 'text/json']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profile = new EducationalProfile();
        $validator = $this->validateInputsEdit($request);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400, ['Content-Type' => 'text/json']);
        } else {
            $profile->updateById($id, $request->employeeId, $request->edit_board_degree, $request->edit_year_of_passing, $request->edit_institution_name,
                $request->edit_principle_subjects,$request->edit_grade_percentage, SessionController::getAdminId(), SessionController::getEmployeeId());
            return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profile = new EducationalProfile();
        $profile->deleteById($id);
        return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);
    }
    public function validateInputs(Request $request): IlluminateValidator {
        $validator = Validator::make($request->all(), [
            'board_degree' => 'required',
            'year_of_passing' => 'required',
            'institution_name' => 'required',
            'grade_percentage' => 'required',
            'principle_subjects' => 'required|regex:/^[\pL\s\-]+$/u',
        ]);
        return $validator;
    }
    public function validateInputsEdit(Request $request): IlluminateValidator {
        $validator = Validator::make($request->all(), [
            'edit_board_degree' => 'required',
            'edit_year_of_passing' => 'required',
            'edit_institution_name' => 'required',
            'edit_grade_percentage' => 'required',
            'edit_principle_subjects' => 'required|regex:/^[\pL\s\-]+$/u',
        ]);
        return $validator;
    }
}
