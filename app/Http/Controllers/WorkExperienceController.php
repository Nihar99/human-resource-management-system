<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WorkExperience;
use Yajra\DataTables\DataTables;
use Illuminate\Contracts\Validation\Validator as IlluminateValidator;
use Illuminate\Support\Facades\Validator;

class WorkExperienceController extends Controller
{
    public function store(Request $request){
        $validator = $this->validateInputs($request);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400, ['Content-Type' => 'text/json']);
        }else{
            $workExperience = new WorkExperience();
            $experience_id = $workExperience->store($request->employee_id,$request->organisation_name,$request->organisation_address,$this->DateFormat($request->from_date),$this->DateFormat($request->to_date),$request->position_held,$request->reporting_to,$request->nature_of_work,$request->reason_of_leaving,$request->gross_annual);
            if($workExperience->fetchById($experience_id) != false){
                return response()->json(['success' => true,'msg' => "Success"], 201, ['Content-Type' => 'text/json']);
            }else{
                return response()->json(['success' => true,'msg' => "Somthing went wrong"] , 500, ['Content-Type' => 'text/json']);
            }
        }
        

    }

    public function DateFormat($getDate)
    {
        if($getDate != null) {
            $dateString = date('Y-m-d', strtotime($getDate));
            return $dateString;
        }
        else
        {
            return null;
        }
    }

    public function fetchUTList(Request $request,$id) {

        $workExperience = new WorkExperience();
        $workExperienceDetails = $workExperience->getEmployeeWorkExperience($id);
        return DataTables::of($workExperienceDetails)
            ->editColumn('actions', function ($workExperienceDetails) {

                return '<div data_id="' . $workExperienceDetails->id . '" class="text-right">
                                <a href="javascript:void(0);" class="editWorkExperienceDetails" title="Edit" id="editWorkExperienceDetails" ><i class="fa fa-edit cursor-pointer mr-2 font-size-18 text-primary"></i></a>
                                <a href="javascript:void(0);" class="deletetWorkExperienceDetails" title="Delete" id="deleteWorkExperienceDetails"><i class="fa fa-trash cursor-pointer mr-2 font-size-18 text-danger"></i></a>
                                </div>';
            })->rawColumns(['actions'])->make(true);
    }

    public function show($id)
    {
        $workExperience = new WorkExperience();
        return response()->json(['success' => true,'data' => $workExperience->fetchById($id)], 201, ['Content-Type' => 'text/json']);
        
    }

    public function destroy($id) {
        $workExperience = new WorkExperience();
        $workExperience->deleteById($id);
        return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);
    }

    public function update(Request $request,$id)
    {
        $validator = $this->validateUpdateInputs($request);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400, ['Content-Type' => 'text/json']);
        }else{
            $workExperience = new WorkExperience();
            $experience_id = $workExperience->updateById($id,   $request->edit_work_experience_organisation_name,$request->edit_work_experience_organisation_address,
            $this->DateFormat($request->edit_work_experience_from_date),
            $this->DateFormat($request->edit_work_experience_to_date),$request->edit_work_experience_position_held,$request->edit_work_experience_reporting_to,$request->edit_work_experience_nature_work,$request->edit_work_experience_leaving_reason,$request->edit_work_experience_annual_gross);
            if($workExperience->fetchById($experience_id) != false){
                return response()->json(['success' => true,'msg' => "Success"], 201, ['Content-Type' => 'text/json']);
            }else{
                return response()->json(['success' => true,'msg' => "Somthing went wrong"] , 500, ['Content-Type' => 'text/json']);
            }
        }
    }

    public function validateInputs(Request $request): IlluminateValidator {
    
        $validator = Validator::make($request->all(), [
            'employee_id' => 'required',
            'organisation_name' => 'required',
            'organisation_address' => 'required',
            'from_date' => 'required',
            'to_date' => 'required',
            'position_held' => 'required',
            'reporting_to' => 'required',
            'nature_of_work' => 'required',
            'reason_of_leaving' => 'required',
            'gross_annual' => 'required',
        ]);;
        return $validator;
    }

    public function validateUpdateInputs(Request $request): IlluminateValidator {
    
        $validator = Validator::make($request->all(), [
            'edit_work_experience_organisation_name' => 'required',
            'edit_work_experience_organisation_address' => 'required',
            'edit_work_experience_from_date' => 'required',
            'edit_work_experience_to_date' => 'required',
            'edit_work_experience_position_held' => 'required',
            'edit_work_experience_reporting_to' => 'required',
            'edit_work_experience_nature_work' => 'required',
            'edit_work_experience_leaving_reason' => 'required',
            'edit_work_experience_annual_gross' => 'required',
        ]);;
        return $validator;
    }
}
