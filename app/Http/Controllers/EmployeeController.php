<?php

namespace App\Http\Controllers;

use App\Country;
use App\Department;
use App\Designation;
use App\Employee;
use App\EmployeeAddress;
use App\EmployeeAuth;
use App\Files;
use Illuminate\Support\Facades\DB;
use App\Helpers\FileManager;
use Illuminate\Support\Facades\Hash;
use App\PersonalDetails;
use App\ProfessionalDetails;
use Illuminate\Contracts\Validation\Validator as IlluminateValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use App\Rules\NoEmpCodeDuplicate;

class EmployeeController extends Controller
{
    public function __construct() {
        $this->middleware('loggedIn');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Country $country, Designation $designation, Department $department)
    {
        return response()->view('employee.create', ['countries' => $country->getAllCountries(), 'departments' => $department->getAllDepartment(), 'designations' => $designation->getAllDesignation()], 200, ['Content-Type', 'text/html; charset=UTF-8']);
    }

    public function fetchUTList(Request $request)
    {
        $employees = (new Employee())->getAllEmployees();
        return DataTables::of($employees)
            ->editColumn('full_name', function ($employees) {
                return (new PersonalDetails())->fetchNameById($employees->id);
            })
            ->editColumn('department', function ($employees) {
                return (new ProfessionalDetails())->fetchDepartmentById($employees->id);
            })
            ->editColumn('designation', function ($employees) {
                return (new ProfessionalDetails())->fetchDesignationById($employees->id);
            })
            ->editColumn('actions', function ($employees) {
                return '<div data_id="' . $employees->id . '" class="text-right">
                                <a class="editEmployee" title="Edit" id="editEmployee" ><i class="fa fa-edit cursor-pointer mr-2 font-size-18 text-primary"></i></a>
                                <a href="'.url('/other-details/' . $employees->id).'" class="otherDetails" title="Other Details" id="otherDetails"><i class="fa fa-eye cursor-pointer mr-2 font-size-18 text-info"></i></a>
                                <a href="javascript:void(0);" class="deleteEmployee" title="Delete" id="deleteEmployee"><i class="fa fa-trash cursor-pointer mr-2 font-size-18 text-danger"></i></a>

                        </div>';
            })
            ->rawColumns(['full_name', 'full_name', 'department', 'designation', 'actions'])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = $this->ValidateEmpCode($request);
        if($validator->fails())
        {
            return response()->json($validator->errors(), 400, ['Content-Type' => 'text/json']);
        }
        else {
            DB::beginTransaction();
            $employee = new Employee();
            $employeeaddress = new EmployeeAddress();
            $personal_details = new PersonalDetails();
            $professional_details = new ProfessionalDetails();
            //gender validation
            if ($request->gender == 'Male') {
                $gender = 1;
            } else {
                $gender = 2;
            }
            //marital status validation
            if ($request->marital_status == 'Married') {
                $marital_status = 1;
            } else if ($request->marital_status == 'Unmarried') {
                $marital_status = 2;
            } else {
                $marital_status = 3;
            }

            //change date format to mysql date..
            $orig_dob = $request->date_of_birth;
            $newDob = date("Y-m-d", strtotime($orig_dob));
            $orig_prob_from = $request->probation_from;
            $new_prob_from = date("Y-m-d", strtotime($orig_prob_from));
            if($request->confirmation_on){
            $orig_confirmation_on = $request->confirmation_on;
            $new_conformation_on = date("Y-m-d", strtotime($orig_confirmation_on));
            }
            else{$new_conformation_on =null;}

            ($request->enable_ess == 'Yes') ? $ess= 1 : $ess = 0;

            $departmentId = $request->department_id;
            $designationId = $request->designation_id;
            $employeeId = $employee->create(
                SessionController::getClientIdFromSession(),
                SessionController::getAdminId(),
                SessionController::getEmployeeId(),
                $request->ecode,
                1,
                $ess);

            if($ess)
            {
                $password = Hash::make($request->ess_password);
                (new EmployeeAuth())->store($employeeId,$password);
            }

            $employeeaddressId = $employeeaddress->create($employeeId,
                $request->present_address,
                $request->present_address_tel_no,
                $request->permanent_address,
                $request->permanent_address_tel_no);

            if ($request->hasFile('employee_image')) {
                $file = $request->employee_image;
                $Id = (new FileManager())->uploadFileToS3($file);
            }

            $employeePersonalDetailsId = $personal_details->create($employeeId,
                $Id,
                $request->first_name,
                $request->middle_name,
                $request->last_name,
                $gender,
                $newDob,
                $request->nationality,
                $request->place_state_of_birth,
                $request->religion,
                $marital_status,
                $request->mother_tongue,
                $request->personal_email,
                $request->hobbies_and_interests);


            $employeeProfessionalDetailsId = $professional_details->create($employeeId,
                $request->professional_email,
                $departmentId,
                $designationId,
                $new_prob_from,
                $new_conformation_on);
            DB::commit();
            return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);

        }

    }
     public function ValidateEmpCode(Request $request) : IlluminateValidator
     {
         $validator1 = Validator::make($request->all(), [
             'ecode' => new NoEmpCodeDuplicate(),
             'employee_image' => 'image',
         ]);
         return $validator1;
     }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $personal = PersonalDetails::with('files')->where('employee_id',$id)->get()[0];
        $personal->files->og_name=(new FileManager())->readFileFromS3($personal->files->og_name);
        $professional = ProfessionalDetails::where('employee_id',$id)->get()[0];
        $address  = EmployeeAddress::where('employee_id',$id)->get()[0];
        $employee = Employee::find($id);
        return response()->json(['success'=>true,'personal'=>$personal,'professional'=>$professional,'address'=>$address,'employee'=>$employee],201, ['Content-Type' => 'text/json']);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = new Country();
        $department = new Department();
        $designation = new Designation();
        return response()->view('employee.edit', ['id'=> $id ,'countries' => $country->getAllCountries(), 'departments' => $department->getAllDepartment(), 'designations' => $designation->getAllDesignation()], 200, ['Content-Type', 'text/html; charset=UTF-8']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'ecode' => 'exists:employees,ecode,id,!'.$id.',deleted_at,NULL,client_id,'.SessionController::getClientIdFromSession(),
            // 'employee_image' => 'image',
        ]);
        if (!$validator->fails()) {
            $msg=array();
            $msg['ecode']=array('The selected ecode is invalid.');
            return response()->json($msg, 400, ['Content-Type' => 'text/json']);
        }
        else {

//        if($request->ecode == (new Employee())->fetchEcodeById($id))
//        {
            DB::beginTransaction();
            $employee = new Employee();
            $employeeaddress = (new EmployeeAddress())->where('employee_id' , $id)->get()->first();
            $personal_details = (new PersonalDetails())->where('employee_id', $id)->get()->first();
            $professional_details = (new ProfessionalDetails())->where('employee_id', $id)->get()->first();
            //gender validation
            if ($request->gender == 'Male') {
                $gender = 1;
            } else {
                $gender = 2;
            }
            //marital status validation
            if ($request->marital_status == 'Married') {
                $marital_status = 1;
            } else if ($request->marital_status == 'Unmarried') {
                $marital_status = 2;
            } else {
                $marital_status = 3;
            }

            //change date format to mysql date..
            $orig_dob = $request->date_of_birth;
            $newDob = date("Y-m-d", strtotime($orig_dob));
            $orig_prob_from = $request->probation_from;
            $new_prob_from = date("Y-m-d", strtotime($orig_prob_from));
            $orig_confirmation_on = $request->confirmation_on;
            if($request->confirmation_on){
                $orig_confirmation_on = $request->confirmation_on;
                $new_conformatio_on = date("Y-m-d", strtotime($orig_confirmation_on));
                }
                else{$new_conformatio_on =null;}

            ($request->enable_ess == 'Yes') ? $ess = 1 : $ess = 0;

            $departmentId = $request->department_id;
            $designationId = $request->designation_id;
            $employeeId = $employee->UpdateById(
                $id,
                SessionController::getClientIdFromSession(),
                SessionController::getAdminId(),
                SessionController::getEmployeeId(),
                $request->ecode,
                1,
                $ess);

            if ($ess) {
                $password = Hash::make($request->ess_password);
                (new EmployeeAuth())->store($employeeId, $password);
            }

            $employeeaddress->UpdateById($employeeaddress->id,
                $id,
                $request->present_address,
                $request->present_address_tel_no,
                $request->permanent_address,
                $request->permanent_address_tel_no);

            $Id = $request->photo;
            if ($request->hasFile('employee_image')) {
                $file = $request->employee_image;
                $Id = (new FileManager())->uploadFileToS3($file);
            }

            (new PersonalDetails())->UpdateById($personal_details->id,
                $id,
                $Id,
                $request->edit_first_name,
                $request->middle_name,
                $request->last_name,
                $gender,
                $newDob,
                $request->nationality,
                $request->place_state_of_birth,
                $request->religion,
                $marital_status,
                $request->mother_tongue,
                $request->personal_email,
                $request->hobbies_and_interests);


            (new ProfessionalDetails())->UpdateById($professional_details->id,
                $id,
                $request->professional_email,
                $departmentId,
                $designationId,
                $new_prob_from,
                $new_conformatio_on);
            DB::commit();
            return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);
        }
        /*}
        else {
            $validator = $this->ValidateEmpCode($request);
            if ($validator->fails()) {
                return response()->json($validator->errors(), 400, ['Content-Type' => 'text/json']);
            } else {

                DB::beginTransaction();
                $employee = new Employee();
                $employeeaddress = new EmployeeAddress();
                $personal_details = new PersonalDetails();
                $professional_details = new ProfessionalDetails();
                //gender validation
                if ($request->gender == 'Male') {
                    $gender = 1;
                } else {
                    $gender = 2;
                }
                //marital status validation
                if ($request->marital_status == 'Married') {
                    $marital_status = 1;
                } else if ($request->marital_status == 'Unmarried') {
                    $marital_status = 2;
                } else {
                    $marital_status = 3;
                }

                //change date format to mysql date..
                $orig_dob = $request->date_of_birth;
                $newDob = date("Y-m-d", strtotime($orig_dob));
                $orig_prob_from = $request->probation_from;
                $new_prob_from = date("Y-m-d", strtotime($orig_prob_from));
                $orig_confirmation_on = $request->confirmation_on;
                $new_conformatio_on = date("Y-m-d", strtotime($orig_confirmation_on));

                ($request->enable_ess == 'Yes') ? $ess = 1 : $ess = 0;

                $departmentId = $request->department_id;
                $designationId = $request->designation_id;
                $employeeId = $employee->UpdateById(
                    $id,
                    SessionController::getClientIdFromSession(),
                    SessionController::getAdminId(),
                    SessionController::getEmployeeId(),
                    $request->ecode,
                    1,
                    $ess);

                if ($ess) {
                    $password = Hash::make($request->ess_password);
                    (new EmployeeAuth())->store($employeeId, $password);
                }

                $employeeaddress->UpdateById($id,
                    $employeeId,
                    $request->present_address,
                    $request->present_address_tel_no,
                    $request->permanent_address,
                    $request->permanent_address_tel_no);

                if ($request->hasFile('employee_image')) {
                    $file = $request->employee_image;
                    $Id = (new FileManager())->uploadFileToS3($file);
                }

                $personal_details->UpdateById($id,
                    $employeeId,
                    $Id,
                    $request->edit_first_name,
                    $request->middle_name,
                    $request->last_name,
                    $gender,
                    $newDob,
                    $request->nationality,
                    $request->place_state_of_birth,
                    $request->religion,
                    $marital_status,
                    $request->mother_tongue,
                    $request->personal_email,
                    $request->hobbies_and_interests);


                $professional_details->UpdateById($id,
                    $employeeId,
                    $request->professional_email,
                    $departmentId,
                    $designationId,
                    $new_prob_from,
                    $new_conformatio_on);
                DB::commit();
                return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);

            }
        }*/




}



    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id )
    {
        $employee = new Employee();
        $employee->deleteById($id);
        return response()->json(['success' => true], 201, ['Content-Type' => 'text/json']);
    }
}
