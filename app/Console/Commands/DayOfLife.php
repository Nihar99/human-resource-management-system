<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\CronController;

class DayOfLife extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leaves:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Assign Leaves to employee every year on 1st January';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cronController = new CronController();
        $cronController->runCron();
        $this->info('New Leaves applied to all users');
    }
}
