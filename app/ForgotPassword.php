<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ForgotPassword extends Model {
    public function insert(string $token, int $admin_id): int {
        $this->token    = $token;
        $this->admin_id = $admin_id;
        $this->save();
        return $this->id;
    }

    public function isTokenActive(string $token, int $admin_id): bool {
        $data = $this::where('token', $token)
            ->where('admin_id', $admin_id)
            ->where('created_at', '>=', Carbon::now()->subDay()->format('Y-m-d H:i:s'))
            ->get()
            ->first();
            /**
             * Changed Carbon -> subtract function as it is unable to location subtract function 
             * instead added subDay() to subtract 1 day from current date
             */
        return $data != null;
    }
}
