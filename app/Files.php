<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Files extends Model
{
    use SoftDeletes;
    protected $softDelete = true;
    public function store($ref_name , $orig_name , $size , $client_id , ?int $adminId , ?int $employeeId) :int
    {
    
        $this->ref_name = $ref_name;
        $this->og_name = $orig_name;
        $this->size = $size;
        $this->client_id = $client_id;
        $this->added_by_admin = $adminId;
        $this->added_by_employee = $employeeId;
        $this->save();
        
        return $this->id;
    }
}
