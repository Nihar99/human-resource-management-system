<?php

namespace App;

use App\Http\Controllers\SessionController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;

class Employee extends Model {
    use SoftDeletes;
    protected $softDelete = true;
    public function create(int $client_id,$added_by_admin, $added_by_employee ,string $ecode,int $status,
                           int $ess) : int
    {
        $this->client_id = $client_id;
        $this->added_by_admin = $added_by_admin;
        $this->added_by_employee = $added_by_employee;
        $this->ecode = $ecode;
        $this->status = $status;
        $this->ess = $ess;
        $this->save();

        return $this->id;
    }
    public function fetchById($id) {
        $employeeObject = $this->find($id);
        return ($employeeObject != null) ? $employeeObject : false;
    }
    public function isEmployeePresentAndESSEnabled($ecode, $companyId) {
        return $this->where('ecode', $ecode)
            ->where('client_id', $companyId)
            ->where('ess', true)->exists();
    }

    public function getActiveEmployees(){
        return $this->where('status',1)->get();
    }

    public function getEmployee($ecode, $companyId) {
        return $this->where('ecode', $ecode)
            ->where('client_id', $companyId)
            ->where('ess', true)->first();
    }
    public function getAllEmployees()
    {
        return $this->all();
    }
    public function EmpCodeExistsInClient($client_id, $ecode) {
        $Employee = $this->where('ecode', $ecode)->where('client_id', $client_id);
        return $Employee->exists();
    }
    public function deleteById($employeeID)
    {
        $employee = $this->find($employeeID);
        if ($employee != null) {
            $employee_delete = $employee->delete();
            return $employee_delete;
        } else {
            return false;
        }
    }

    public function insertBatch(
        $employee_code,$status,$ess
    ){
        $employee = array(
            'client_id' => SessionController::getClientIdFromSession(),
            'added_by_admin' => SessionController::getAdminId(),
            'added_by_employee' => SessionController::getEmployeeId(),
            'ecode' => $employee_code,
            'status' => $status,
            'ess' => $ess,
        );
       return $employee;
    }

    public function UpdateById($id,int $client_id,$added_by_admin, $added_by_employee ,string $ecode,int $status,
                           int $ess) :int
    {
        $employee = $this->find($id);
        $employee->client_id = $client_id;
        $employee->added_by_admin = $added_by_admin;
        $employee->added_by_employee = $added_by_employee;
        $employee->ecode = $ecode;
        $employee->status = $status;
        $employee->ess = $ess;
        $employee->save();

        return $employee->id;
    }
    public function fetchEcodeById($id) {
        $employeeObject = $this->find($id);
        return $employeeObject->ecode;
    }

    public function employeeDetails(){
        return $this->hasOne('App\PersonalDetails','employee_id');
    }

    public function employeeAttendance(){
        return $this->hasMany('App\EmployeeAttendance','employee_id');
    }

    public function getAllEmployeeAttendance(){
        return Employee::with('employeeAttendance');
    }
}
